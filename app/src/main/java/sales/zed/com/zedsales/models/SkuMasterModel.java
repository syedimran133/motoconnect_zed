package sales.zed.com.zedsales.models;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SkuMasterModel {

	@SerializedName("SkuLists")
	@Expose
	private List<SkuListsItem> skuLists;

	public void setSkuLists(List<SkuListsItem> skuLists){
		this.skuLists = skuLists;
	}

	public List<SkuListsItem> getSkuLists(){
		return skuLists;
	}

	@Override
 	public String toString(){
		return 
			"SkuMasterModel{" +
			"skuLists = '" + skuLists + '\'' + 
			"}";
		}
}