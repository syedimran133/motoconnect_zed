package sales.zed.com.zedsales.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.databinding.FragmentSchemeBinding;
import sales.zed.com.zedsales.databinding.FragmentSchemeInnerBinding;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FlowOrganizer;

/**
 * Created by Mukesh on 09/12/17.
 */

public class FragmentSchemeInner extends BaseFragment {

    private FragmentSchemeInnerBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_scheme_inner, container, false);
        AppSingle.getInstance().getActivity().setTitle("Scheme", true);
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("Scheme"));
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.txtViewCurrent.setOnClickListener(listener);
        binding.txtViewPrevious.setOnClickListener(listener);
//        binding.btnTextViewSubmit.setOnClickListener(listener);
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }
    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_text_view_submit:
                    callAsync();
                    break;
                case R.id.txt_view_current:
                    binding.txtViewPrevious.setBackgroundColor(getResources().getColor(R.color.bg_grey, null));
                    binding.txtViewCurrent.setBackgroundColor(getResources().getColor(R.color.bg_blue, null));
                    FlowOrganizer.getInstance().add(new FragmentSchemeMain(), true);
                    break;
                case R.id.txt_view_previous:
                    binding.txtViewPrevious.setBackgroundColor(getResources().getColor(R.color.bg_blue, null));
                    binding.txtViewCurrent.setBackgroundColor(getResources().getColor(R.color.bg_grey, null));
                    FlowOrganizer.getInstance().add(new FragmentSchemeInner(), true);

                    break;
            }
        }
    };

    private void callAsync() {
    }
}
