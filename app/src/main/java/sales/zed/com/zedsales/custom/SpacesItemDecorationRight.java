package sales.zed.com.zedsales.custom;


import android.graphics.Rect;
import android.support.v7.widget.RecyclerView;
import android.view.View;

/**
 * Created by Mukesh on 17-07-2017.
 */

public class SpacesItemDecorationRight extends RecyclerView.ItemDecoration {
    private int space;

    public SpacesItemDecorationRight(int space) {
        this.space = space;
    }

    @Override
    public void getItemOffsets(Rect outRect, View view,
                               RecyclerView parent, RecyclerView.State state) {
        outRect.left = 0;
        outRect.right = 0;
        outRect.bottom = 0;
        outRect.right = 0;
//        if (parent.getChildLayoutPosition(view) == 0)
//            outRect.left = space;
//        else
//            outRect.left = 0;
    }
}