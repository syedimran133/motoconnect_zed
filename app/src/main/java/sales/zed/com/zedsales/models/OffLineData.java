package sales.zed.com.zedsales.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class OffLineData {

    @PrimaryKey(autoGenerate = true)
    @SerializedName("ID")
    @Expose
    public int id;

    @SerializedName("Type")
    @Expose
    String type;

    @SerializedName("Name")
    @Expose
    String name;

    @SerializedName("RequestData")
    @Expose
    String requestData;

    @SerializedName("Status")
    @Expose
    String status;

    @SerializedName("Time")
    @Expose
    String time;

    @SerializedName("Remark")
    @Expose
    String remark;

    public String getType() {
        return type;
    }

    public void setType(String type) {
        this.type = type;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getRequestData() {
        return requestData;
    }

    public void setRequestData(String requestData) {
        this.requestData = requestData;
    }

    public int getId() {
        return id;
    }

    public void setId(int id) {
        this.id = id;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getTime() {
        return time;
    }

    public void setTime(String time) {
        this.time = time;
    }

    public String getRemark() {
        return remark;
    }

    public void setRemark(String remark) {
        this.remark = remark;
    }
}
