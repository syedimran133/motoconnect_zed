package sales.zed.com.zedsales;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageInfo;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.provider.Settings;
import android.support.v4.content.ContextCompat;
import android.support.v4.view.GravityCompat;
import android.support.v4.widget.DrawerLayout;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.support.v7.widget.LinearLayoutManager;
import android.util.Log;
import android.view.Gravity;
import android.view.View;
import android.view.inputmethod.InputMethodManager;
import android.widget.ListView;
import android.widget.Toast;
import com.google.firebase.iid.FirebaseInstanceId;
import java.text.DateFormat;
import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;
import retrofit2.*;
import sales.zed.com.zedsales.adapter.*;
import sales.zed.com.zedsales.apiz.*;
import sales.zed.com.zedsales.constants.AppConstants;
import sales.zed.com.zedsales.custom.SpacesItemDecorationRight;
import sales.zed.com.zedsales.databinding.ActivityMainBinding;
import sales.zed.com.zedsales.fragment.*;
import sales.zed.com.zedsales.models.*;
import sales.zed.com.zedsales.room.MyDatabase;
import sales.zed.com.zedsales.support.*;

public class MainActivity extends AppCompatActivity {

    public static ActivityMainBinding binding;
    public static DrawerListAdapter drawerListAdapter;
    private ListView lv_drawer;
    public static RVUpperMenu adapter;
    private List<MenuList> listData;
    private int currentNavPosition = -1;
    public static boolean isReport = false;
    private BaseFragment mBaseFragment;

    public MainActivity() {
        mBaseFragment = new BaseFragment();
    }

    @Override
    public void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        binding = DataBindingUtil.setContentView(this, R.layout.activity_main);
        mBaseFragment = new BaseFragment();
        AppSingle.getInstance().initActivity(this);
        AppPrefrence.getInstance().initPrefrence(this);
        FlowOrganizer.getInstance().initParentFrame(binding.frameParent);
        lv_drawer = binding.lvDrawer;
        initDeviceId();
        initList();
        listData = getMenuList();
        SetDrawer();
        setTitle("", true);
        try {
            String value = this.getIntent().getExtras().getString("menu");
            if (value != null) {
                Bundle bundle = new Bundle();
                bundle.putString("Menu", value);
                FlowOrganizer.getInstance().add(new FragmentLogin(), false, bundle);
                //Toast.makeText(this, value, Toast.LENGTH_SHORT).show();
            } else {
                Bundle bundle = new Bundle();
                bundle.putString("Menu", "-1");
                FlowOrganizer.getInstance().add(new FragmentLogin(), false, bundle);
                //Toast.makeText(this, value, Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
            Bundle bundle = new Bundle();
            bundle.putString("Menu", "-1");
            FlowOrganizer.getInstance().add(new FragmentLogin(), false, bundle);
            //Toast.makeText(this, e.toString(), Toast.LENGTH_SHORT).show();
        }
        enableDrawer(false);
        showToolBar(false);
        binding.btnImgMenu.setOnClickListener(listener);
        binding.btnImgSetting.setOnClickListener(listener);
        binding.relativeSetting.setOnClickListener(listener);
        binding.linearChangePassword.setOnClickListener(listener);
        binding.linearHome.setOnClickListener(listener);
        binding.linearLogout.setOnClickListener(listener);
        binding.linearSync.setOnClickListener(listener);
        binding.btnImgBack.setOnClickListener(listener);
        List<OffLineData> offLineData = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchOffLineDatawithstatus("1");
        if (offLineData.size() > 0) {
            binding.notificationsync.setVisibility(View.VISIBLE);
            binding.notificationsync.setText("" + offLineData.size());
        } else {
            binding.notificationsync.setVisibility(View.GONE);
        }

        try {
            PackageInfo pInfo = getPackageManager().getPackageInfo(getPackageName(), 0);
            int version = pInfo.versionCode;
            AppSingle.getInstance().setAppVersion(version);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }



    private void subscribeToPushService() {
        //FirebaseMessaging.getInstance().subscribeToTopic("news");
       // Log.d("AndroidBash", "Subscribed");
        //Toast.makeText(MainActivity.this, "Subscribed", Toast.LENGTH_SHORT).show();
        String token = FirebaseInstanceId.getInstance().getToken();
        // Log and toast
        Log.d("AndroidBash", token);
        AppPrefrence.getInstance().setPushToken(token);
        //Toast.makeText(MainActivity.this, token, Toast.LENGTH_SHORT).show();
    }

    public static void setnotification() {
        List<OffLineData> offLineData = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchOffLineDatawithstatus("1");
        if (offLineData.size() > 0) {
            binding.notificationsync.setVisibility(View.VISIBLE);
            binding.notificationsync.setText("" + offLineData.size());
        } else {
            binding.notificationsync.setVisibility(View.GONE);
        }
    }

    public void papulatelistmenu(List<MenuList> menu) {
        binding.rvMainList.addItemDecoration(new SpacesItemDecorationRight(AppSingle.getInstance().getActivity().getResources().getDimensionPixelOffset(R.dimen._2dp)));
        adapter = new RVUpperMenu(AppSingle.getInstance().getActivity(),/*AppSingle.getInstance().getActivity().getMenuList()*/ menu);
        adapter.registerItemClickLisener(new RVUpperMenu.IonItemClickListener() {
            @Override
            public void onItemClick(int position, int pa) {
                openPageUpper(position);
                adapter.setselected(pa);
                currentNavPosition = pa;
            }
        });
        binding.rvMainList.setAdapter(adapter);
        binding.rvMainList.setLayoutManager(new LinearLayoutManager(AppSingle.getInstance().getActivity(), LinearLayoutManager.HORIZONTAL, false));
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.relative_setting:
                    binding.relativeSetting.setVisibility(View.GONE);
                    break;
                case R.id.linear_change_password:
                    FlowOrganizer.getInstance().add(new FragmentChangePassword(), true);
                    binding.relativeSetting.setVisibility(View.GONE);
                    break;
                case R.id.linear_home:
                    FlowOrganizer.getInstance().popUpBackToMain();
                    binding.relativeSetting.setVisibility(View.GONE);
                    AppSingle.getInstance().getActivity().setTitle("Menu", false);
                    AppSingle.getInstance().getActivity().setTopNavBar(false);
                    AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
                    break;
                case R.id.linear_sync:
                    FlowOrganizer.getInstance().add(new FragmentSync(), true);
                    binding.relativeSetting.setVisibility(View.GONE);
                    break;
                case R.id.linear_logout:
                    if (!NetworkUtil.isConnectivityAvailable(AppSingle.getInstance().getActivity())) {
                        AppConstants.isoffline = false;
                        FlowOrganizer.getInstance().add(new FragmentLogin());
                        Toast.makeText(AppSingle.getInstance().getActivity(), "Logout successfully", Toast.LENGTH_LONG).show();
                        showToolBar(false);
                        binding.relativeSetting.setVisibility(View.GONE);
                    } else {
                        AppConstants.isoffline = false;
                        callAsynclogout();
                    }
                    break;
                case R.id.btn_img_back:
                    if (isReport) {
                        FlowOrganizer.getInstance().add(new FragmentReport(), true);
                        AppSingle.getInstance().getActivity().setTopNavBar(false);
                        isReport = false;
                    } else {
                        FlowOrganizer.getInstance().popUpBackToMain();
                        binding.relativeSetting.setVisibility(View.GONE);
                        AppSingle.getInstance().getActivity().setTitle("Menu", false);
                        AppSingle.getInstance().getActivity().setTopNavBar(false);
                        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
                    }
                    break;
                case R.id.btn_img_menu:
                    toggleDrawer();
                    break;
                case R.id.btn_img_setting:
                    binding.relativeSetting.setVisibility(View.VISIBLE);
                    if (binding.drawerLayout.isDrawerOpen(Gravity.LEFT))
                        binding.drawerLayout.closeDrawer(Gravity.LEFT);
                    break;
//
            }
        }
    };

    public void setTopNavSelectedPos(int pos) {
        try {
            currentNavPosition = pos;
            adapter.setselected(currentNavPosition);
        } catch (Exception e) {
        }
    }

    public void setTitle(String titel, boolean backbutton) {
        binding.toptitel.setText(titel);
        if (backbutton) {
            binding.btnImgBack.setVisibility(View.VISIBLE);
            //binding.llimgBack.setVisibility(View.VISIBLE);
        } else {
            binding.btnImgBack.setVisibility(View.GONE);
            // binding.llimgBack.setVisibility(View.GONE);
        }
    }

    public void setTopNavBar(boolean topmenu) {
        if (topmenu) {
            binding.linearTopNavBar.setVisibility(View.VISIBLE);
        } else {
            binding.linearTopNavBar.setVisibility(View.GONE);
        }
    }

    public void showToolBar(boolean show) {
        if (show) {
            binding.relativeToolBar.setVisibility(View.VISIBLE);
        } else {
            binding.relativeToolBar.setVisibility(View.GONE);
        }
    }

    /**
     * getCurrentTime() it will return system time
     *
     * @return
     */
    public static String getCurrentTime() {
        //date output format
        DateFormat dateFormat = new SimpleDateFormat("dd-MMM-yyyy HH:mm:ss");
        Calendar cal = Calendar.getInstance();
        return dateFormat.format(cal.getTime());
    }// end of getCurrentTime()

    public void enableDrawer(boolean enable) {
        if (enable) {
            binding.btnImgMenu.setVisibility(View.VISIBLE);
            binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_UNLOCKED);
        } else {
            binding.btnImgMenu.setVisibility(View.GONE);
            binding.drawerLayout.setDrawerLockMode(DrawerLayout.LOCK_MODE_LOCKED_CLOSED);
        }
    }

    private void initDeviceId() {
        String deviceId = AppPrefrence.getInstance().getDeviceId();
        if (AppValidate.isValidString(deviceId))
            return;
        try {
            deviceId = Settings.Secure.getString(getApplicationContext().getContentResolver(),
                    Settings.Secure.ANDROID_ID);
            AppPrefrence.getInstance().setDeviceId(deviceId);
        } catch (Exception e) {
        }
    }

    private void callAsynclogout() {
        try {

            ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
            Call<ForgetModel> call = apiService.logout(AppSingle.getInstance().getLoginUser().getUserId(), RequestFormater.logout(AppPrefrence.getInstance().getDeviceId()));
            setProgressDialog();
            call.enqueue(new Callback<ForgetModel>() {
                @Override
                public void onResponse(Call<ForgetModel> call, Response<ForgetModel> response) {
                    dismissProgressDialog();
                    if (response.code() == 200) {
                        try {
                            if (response.body().getStatusCode() == 0) {
                                FlowOrganizer.getInstance().add(new FragmentLogin());
                                Toast.makeText(AppSingle.getInstance().getActivity(), "Logout successfully", Toast.LENGTH_LONG).show();
                                showToolBar(false);
                                binding.relativeSetting.setVisibility(View.GONE);
                            }
                        } catch (Exception e) {
                            Toast.makeText(AppSingle.getInstance().getActivity(), e.toString(), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        try {
                            Toast.makeText(AppSingle.getInstance().getActivity(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                        } catch (Exception e) {
                            Toast.makeText(AppSingle.getInstance().getActivity(), e.toString(), Toast.LENGTH_LONG).show();
                        }
                    }
                }

                @Override
                public void onFailure(Call<ForgetModel> call, Throwable t) {
                    dismissProgressDialog();
                    Toast.makeText(AppSingle.getInstance().getActivity(), "Error.....", Toast.LENGTH_LONG).show();
                }
            });
        } catch (Exception e) {
        }
    }

    private void SetDrawer() {
        drawerListAdapter = new DrawerListAdapter(this, AppSingle.getInstance().getActivity().getMenuList());
        lv_drawer.setAdapter(drawerListAdapter);
        drawerListAdapter.registerItemClickLisener(new RVAdapterMenu.IonItemClickListener() {
            @Override
            public void onItemClick(int position) {
                openPage(position);
            }
        });
    }

    private void openPage(int position) {
        FlowOrganizer.getInstance().popUpBackToMain();
        switch (position) {
            case 0:
                FlowOrganizer.getInstance().add(new FragmentGRNSearch(), true);
                break;
            case 1:
                FlowOrganizer.getInstance().add(new FragmentMyOrderNewOrder(), true);
                break;
            case 2:
                try {
                    if (ContextCompat.checkSelfPermission(AppSingle.getInstance().getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        FlowOrganizer.getInstance().add(new FragmentMysaleCurrent(), true);
                    } else {
                        mBaseFragment.showToast("Permission not found.");
                    }
                } catch (Exception e) {
                    mBaseFragment.showToast(e.toString());
                }
                break;
            case 3:
                FlowOrganizer.getInstance().add(new FragmentMyReturns(), true);
                break;
            case 4:
                FlowOrganizer.getInstance().add(new FragmentScheme(), true);
                mBaseFragment.showToast("Coming Soon");
                break;
            case 5:
                FlowOrganizer.getInstance().add(new FragmentTargetList(), true);
                break;
            case 6:
                FlowOrganizer.getInstance().add(new FragmentHierarchy(), true);
                break;
            case 7:
                FlowOrganizer.getInstance().add(new FragmentQueryManagement(), true);
                mBaseFragment.showToast("Coming Soon");
                break;
            case 8:
                FlowOrganizer.getInstance().add(new FragmentClaimPriceDrop(), true);
                mBaseFragment.showToast("Coming Soon");
                break;
            case 9:
                FlowOrganizer.getInstance().add(new FragmentNotification(), true);
                //mBaseFragment.showToast("Coming Soon");
                break;
            case 10:
                FlowOrganizer.getInstance().add(new FragmentShowProfileDetails(), true);
                break;
            case 11:
                FlowOrganizer.getInstance().add(new FragmentBulletin(), true);
                break;
            case 12:
                //FlowOrganizer.getInstance().add(new FragmentQueryManagement(), true);
                mBaseFragment.showToast("Coming Soon");
                break;
            case 13:
                FlowOrganizer.getInstance().add(new FragmentReport(), true);
                break;
            default:
                mBaseFragment.showToast("Menu not in the list");
                break;
        }
        toggleDrawer();
    }

    private void initList() {
        listData = new ArrayList<>();
        MenuList menu = new MenuList();
        menu.setTitle("Manage GRN");
        menu.setImage(R.drawable.grn);
        menu.setMenuid(1);
        listData.add(menu);
        menu = new MenuList();
        menu.setTitle("My Order");
        menu.setImage(R.drawable.order);
        menu.setMenuid(2);
        listData.add(menu);
        menu = new MenuList();
        menu.setTitle("My Sale");
        menu.setImage(R.drawable.sale);
        menu.setMenuid(3);
        listData.add(menu);
        menu = new MenuList();
        menu.setTitle("My Returns");
        menu.setImage(R.drawable.return_ic);
        menu.setMenuid(4);
        listData.add(menu);
        menu = new MenuList();
        menu.setTitle("Scheme");
        menu.setImage(R.drawable.scheme);
        menu.setMenuid(5);
        listData.add(menu);
        menu = new MenuList();
        menu.setTitle("Target");
        menu.setImage(R.drawable.target);
        menu.setMenuid(6);
        listData.add(menu);
        menu = new MenuList();
        menu.setTitle("Hierarchy");
        menu.setImage(R.drawable.hierarchy);
        menu.setMenuid(7);
        listData.add(menu);
        menu = new MenuList();
        menu.setTitle("Query Management");
        menu.setImage(R.drawable.query);
        menu.setMenuid(8);
        listData.add(menu);
        menu = new MenuList();
        menu.setTitle("Claim Price Drop");
        menu.setImage(R.drawable.claim_price_drop);
        menu.setMenuid(9);
        listData.add(menu);
        menu = new MenuList();
        menu.setTitle("Notification");
        menu.setImage(R.drawable.notification);
        menu.setMenuid(10);
        listData.add(menu);
        menu = new MenuList();
        menu.setTitle("My Profile");
        menu.setImage(R.drawable.profile);
        menu.setMenuid(11);
        listData.add(menu);
        menu = new MenuList();
        menu.setTitle("Bulletin");
        menu.setImage(R.drawable.bulletin);
        menu.setMenuid(12);
        listData.add(menu);
        menu = new MenuList();
        menu.setTitle("Track Yy Location");
        menu.setImage(R.drawable.location);
        menu.setMenuid(13);
        listData.add(menu);
        menu = new MenuList();
        menu.setTitle("Reports");
        menu.setImage(R.drawable.report);
        menu.setMenuid(14);
        listData.add(menu);
    }

    private void openPageUpper(int position) {
        FlowOrganizer.getInstance().popUpBackToMain();
        switch (position) {
            case 0:
                FlowOrganizer.getInstance().add(new FragmentGRNSearch(), true);
                break;
            case 1:
                FlowOrganizer.getInstance().add(new FragmentMyOrderNewOrder(), true);
                break;
            case 2:
                try {
                    if (ContextCompat.checkSelfPermission(AppSingle.getInstance().getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        FlowOrganizer.getInstance().add(new FragmentMysaleCurrent(), true);
                    } else {
                        mBaseFragment.showToast("Permission not found.");
                    }
                } catch (Exception e) {
                    mBaseFragment.showToast(e.toString());
                }
                break;
            case 3:
                FlowOrganizer.getInstance().add(new FragmentMyReturnsInner(), true);
                break;
            case 4:
                FlowOrganizer.getInstance().add(new FragmentSchemeMain(), true);
                mBaseFragment.showToast("Coming Soon");
                break;
            case 5:
                FlowOrganizer.getInstance().add(new FragmentTargetList(), true);
                break;
            case 6:
                FlowOrganizer.getInstance().add(new FragmentHierarchy(), true);
                break;
            case 7:
                FlowOrganizer.getInstance().add(new FragmentQueryManagement(), true);
                mBaseFragment.showToast("Coming Soon");
                break;
            case 8:
                FlowOrganizer.getInstance().add(new FragmentClaimPriceDrop(), true);
                mBaseFragment.showToast("Coming Soon");
                break;
            case 9:
                FlowOrganizer.getInstance().add(new FragmentNotification(), true);
                break;
            case 10:
                FlowOrganizer.getInstance().add(new FragmentShowProfileDetails(), true);
                break;
            case 11:
                FlowOrganizer.getInstance().add(new FragmentBulletin(), true);
                break;
            case 12:
                //FlowOrganizer.getInstance().add(new FragmentQueryManagement(), true);
                mBaseFragment.showToast("Coming Soon");
                break;
            case 13:
                FlowOrganizer.getInstance().add(new FragmentReport(), true);
                break;
            default:
                mBaseFragment.showToast("Menu not in the list");
                break;
        }
    }

    public void toggleDrawer() {
        hideKeyboard();
        if (binding.drawerLayout.isDrawerOpen(Gravity.LEFT)) {
            binding.drawerLayout.closeDrawer(Gravity.LEFT);
        } else {
            binding.drawerLayout.openDrawer(Gravity.LEFT);
        }
    }

    public void setProgressDialog() {
        AppSingle.getInstance().setAllowBack(false);
        AppSingle.getInstance().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                hideSoftKeyboard();
                binding.linearProgressBar.setVisibility(View.VISIBLE);
                binding.avi.show();
            }
        });
    }

    public void hideKeyboard() {
        try {
            InputMethodManager inputManager = (InputMethodManager) getSystemService(Context.INPUT_METHOD_SERVICE);
            inputManager.hideSoftInputFromWindow(getCurrentFocus()
                    .getWindowToken(), InputMethodManager.HIDE_NOT_ALWAYS);
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    @Override
    public void onBackPressed() {
        if (binding.linearProgressBar.getVisibility() == View.VISIBLE)
            return;
        DrawerLayout drawer = (DrawerLayout) findViewById(R.id.drawer_layout);
        if (drawer.isDrawerOpen(GravityCompat.START)) {
            drawer.closeDrawer(GravityCompat.START);
        } else if (binding.relativeSetting.isShown()) {
            binding.relativeSetting.setVisibility(View.GONE);
        } else {
            if (isReport) {
                FlowOrganizer.getInstance().add(new FragmentReport(), true);
                AppSingle.getInstance().getActivity().setTopNavBar(false);
                isReport = false;
            } else if (binding.toptitel.getText().toString().equalsIgnoreCase("Menu")) {
                super.onBackPressed();
            }/*else if (binding.toptitel.getText().toString().equalsIgnoreCase("Menu")) {
                super.onBackPressed();
            } */ else {
                FlowOrganizer.getInstance().popUpBackToMain();
                binding.relativeSetting.setVisibility(View.GONE);
                AppSingle.getInstance().getActivity().setTitle("Menu", false);
                AppSingle.getInstance().getActivity().setTopNavBar(false);
                AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
            }
        }
    }

    public void hideSoftKeyboard() {
        try {
            if (AppSingle.getInstance().getActivity().getCurrentFocus() != null) {
                InputMethodManager inputMethodManager = (InputMethodManager) AppSingle.getInstance().getActivity().getSystemService(AppSingle.getInstance().getActivity().INPUT_METHOD_SERVICE);
                inputMethodManager.hideSoftInputFromWindow(AppSingle.getInstance().getActivity().getCurrentFocus().getWindowToken(), 0);
            }
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    public List<MenuList> getMenuList() {
        return listData;
    }


    public void dismissProgressDialog() {
        AppSingle.getInstance().setAllowBack(true);
        AppSingle.getInstance().getActivity().runOnUiThread(new Runnable() {
            @Override
            public void run() {
                binding.linearProgressBar.setVisibility(View.GONE);
            }
        });
    }

    public DrawerListAdapter getDrawerListAdapter() {
        return drawerListAdapter;
    }

    @Override
    protected void onNewIntent(Intent intent) {
        super.onNewIntent(intent);
        if (getIntent().getExtras() != null) {
            for (String key : getIntent().getExtras().keySet()) {
                String value = getIntent().getExtras().getString(key);
                if (key.equals("menu")) {
                    //Toast.makeText(this, value, Toast.LENGTH_SHORT).show();
                }
            }
        }
    }
}
