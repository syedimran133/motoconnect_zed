package sales.zed.com.zedsales.apiz;

import retrofit2.Call;
import retrofit2.http.Body;
import retrofit2.http.GET;
import retrofit2.http.Headers;
import retrofit2.http.POST;
import retrofit2.http.Path;
import retrofit2.http.Query;
import sales.zed.com.zedsales.models.AddOrderResponse;
import sales.zed.com.zedsales.models.BrandListModel;
import sales.zed.com.zedsales.models.CurrentStockSkuList;
import sales.zed.com.zedsales.models.ForgetModel;
import sales.zed.com.zedsales.models.GRNDetail;
import sales.zed.com.zedsales.models.GRNReportList;
import sales.zed.com.zedsales.models.GRNStatusList;
import sales.zed.com.zedsales.models.ListBulletin;
import sales.zed.com.zedsales.models.NotificationList;
import sales.zed.com.zedsales.models.OrderReportList;
import sales.zed.com.zedsales.models.PendingGrnList;
import sales.zed.com.zedsales.models.PreviousTargetList;
import sales.zed.com.zedsales.models.ProfileData;
import sales.zed.com.zedsales.models.ReportTypeList;
import sales.zed.com.zedsales.models.SaleReportList;
import sales.zed.com.zedsales.models.SerialMasterList;
import sales.zed.com.zedsales.models.SkuOpeningStockList;
import sales.zed.com.zedsales.models.StockItemsList;
import sales.zed.com.zedsales.models.StockTypeMaster;
import sales.zed.com.zedsales.models.SuggestedModel;
import sales.zed.com.zedsales.models.TargetModelList;
import sales.zed.com.zedsales.models.UserModel;
import sales.zed.com.zedsales.models.MasterModel;
import sales.zed.com.zedsales.models.MenuModel;
import sales.zed.com.zedsales.models.ProductCategoryModel;
import sales.zed.com.zedsales.models.ProductModel;
import sales.zed.com.zedsales.models.SkuMasterModel;

/**
 * Created by wildcoder on 02/09/17.
 */

public interface ApiInterface {

    //User Profile
    //<<=========================================================================================>>

    //Signing using email

    @Headers("Content-Type: application/json")
    @POST("Login")
    Call<UserModel> login(@Body String body);

    @Headers("Content-Type: application/json")
    @POST("ForgotPassword")
    Call<ForgetModel> forget(@Body String body);

    @Headers("Content-Type: application/json")
    @POST("ChangePassword")
    Call<ForgetModel> changepass(@Body String body);

    @Headers("Content-Type: application/json")
    @GET("MenuItem/{userId}")
    Call<MenuModel> menu(@Path("userId") int userId);

    @Headers("Content-Type: application/json")
    @GET("GetHierarchy/{userId}")
    Call<Object> GetHierarchy(@Path("userId") int userId);

    @Headers("Content-Type: application/json")
    @GET("GetBulletinDetail/{userId}")
    Call<ListBulletin> GetBulletinList(@Path("userId") int userId);

    @Headers("Content-Type: application/json")
    @GET("BrandMaster?")
    Call<BrandListModel> getBrand(@Query("fromdate") String fromdate);

    @Headers("Content-Type: application/json")
    @GET("ProductCategoryMaster?")
    Call<ProductCategoryModel> getCategory(@Query("fromdate") String fromdate);

    @Headers("Content-Type: application/json")
    @GET("ProductMaster?")
    Call<ProductModel> getProduct(@Query("fromdate") String fromdate);

    @Headers("Content-Type: application/json")
    @GET("ModelMaster?")
    Call<MasterModel> getMaster(@Query("fromdate") String fromdate);

    @Headers("Content-Type: application/json")
    @GET("ReportTypeMaster/{userId}")
    Call<ReportTypeList> getReportType(@Path("userId") int userId,@Query("fromdate") String fromdate);

    @Headers("Content-Type: application/json")
    @GET("GRNStatusMaster/{userId}")
    Call<GRNStatusList> getGRNStatusMaster(@Path("userId") int userId, @Query("fromdate") String fromdate);

    @Headers("Content-Type: application/json")
    @POST("GetMyCurrentStock/{userId}/{pageIndex}")
    Call<CurrentStockSkuList> getcurrentstock(@Path("userId") int userId, @Path("pageIndex") int pageIndex, @Body String body);

    @Headers("Content-Type: application/json")
    @POST("GetMyCurrentStock/{userId}/{pageIndex}")
    Call<ForgetModel> getcurrentstockmailto(@Path("userId") int userId, @Path("pageIndex") int pageIndex, @Body String body);

    @Headers("Content-Type: application/json")
    @GET("SerialMaster/{userId}")
    Call<SerialMasterList> getSerialMaster(@Path("userId") int userId,@Query("fromdate") String fromdate);

    @Headers("Content-Type: application/json")
    @GET("RetailerStock/{userId}")
    Call<StockItemsList> getStockList(@Path("userId") int userId, @Query("fromdate") String fromdate);

    @Headers("Content-Type: application/json")
    @GET("StockTypeMaster?")
    Call<StockTypeMaster> getStockType(@Query("fromdate") String fromdate);

    @Headers("Content-Type: application/json")
    @GET("SkuMaster?")
    Call<SkuMasterModel> getSkuMaster(@Query("fromdate") String fromdate);

    @Headers("Content-Type: application/json")
    @GET("RetailerProfile/{userId}")
    Call<ProfileData> getprofiledata(@Path("userId") int userId);

    @Headers("Content-Type: application/json")
    @POST("GetPendingGRN/{userId}/{pageIndex}")
    Call<PendingGrnList> getpendinggrn(@Path("userId") int userId, @Path("pageIndex") int pageIndex, @Body String body);

    @Headers("Content-Type: application/json")
    @POST("SaveGrn/{userId}")
    Call<ForgetModel> savependinggrn(@Path("userId") int userId, @Body String body);


    @Headers("Content-Type: application/json")
    @POST("UserLogout/{userId}")
    Call<ForgetModel> logout(@Path("userId") int userId, @Body String body);

    @Headers("Content-Type: application/json")
    @POST("GetPendingGRNDetail/{userId}")
    Call<GRNDetail> pendingGNRDetails(@Path("userId") int userId, @Body String body);

    @Headers("Content-Type: application/json")
    @GET("GetTargetDetail/{userId}/{pageIndex}")
    Call<TargetModelList> getTargetList(@Path("userId") Integer userId,@Path("pageIndex") int pageIndex);

    @Headers("Content-Type: application/json")
    @POST("GetPreviousTarget/{userId}/{pageIndex}")
    Call<PreviousTargetList> getTargetPrvList(@Path("userId") Integer userId, @Path("pageIndex") int pageIndex, @Body String body);


    @Headers("Content-Type: application/json")
    @POST("GetPreviousSale/{userId}")
    Call<SaleReportList> getPreviousSale(@Path("userId") Integer userId, @Body String body);

    @Headers("Content-Type: application/json")
    @POST("GetPreviousSale/{userId}")
    Call<ForgetModel> getPreviousSaleMailTo(@Path("userId") Integer userId, @Body String body);


    @Headers("Content-Type: application/json")
    @POST("GetPreviousOrder/{userId}")
    Call<OrderReportList> getPreviousOrder(@Path("userId") Integer userId, @Body String body);

    @Headers("Content-Type: application/json")
    @POST("GetPreviousOrder/{userId}")
    Call<ForgetModel> getPreviousOrderMailTo(@Path("userId") Integer userId, @Body String body);

    @Headers("Content-Type: application/json")
    @POST("GetPreviousGrn/{userId}")
    Call<GRNReportList> getPreviousGRN(@Path("userId") Integer userId, @Body String body);

    @Headers("Content-Type: application/json")
    @POST("GetPreviousGrn/{userId}")
    Call<ForgetModel> getPreviousGRNMailTo(@Path("userId") Integer userId, @Body String body);

    @Headers("Content-Type: application/json")
    @POST("SaveSale/{userId}")
    Call<ForgetModel> saveSale(@Path("userId") int userId, @Body String body);

    @Headers("Content-Type: application/json")
    @POST("AddOpeningStock/{userId}")
    Call<ForgetModel> addOpeningStock(@Path("userId") int userId, @Body String body);

    @Headers("Content-Type: application/json")
    @POST("AddOrder/{userId}")
    Call<AddOrderResponse> addorder(@Path("userId") int userId, @Body String body);

    @Headers("Content-Type: application/json")
    @GET("GetSuggestedOrder/{userId}")
    Call<SuggestedModel> getsuggestedorder(@Path("userId") int userId);

    @Headers("Content-Type: application/json")
    @POST("UpdateRetailerProfile/{userId}")
    Call<ForgetModel> update_profile(@Path("userId") int userId, @Query("fromdate") String fromdate,@Body String body);

    @Headers("Content-Type: application/json")
    @GET("GetIMEIDetails?")
    Call<SkuOpeningStockList> getIMEIDetails(@Query("Imeinumber") String imeinumber);

    @Headers("Content-Type: application/json")
    @POST("GetLatestAPKVersion")
    Call<String> checkVersion(@Body String body);

    @Headers("Content-Type: application/json")
    @GET("GetNotification/{userId}")
    Call<NotificationList> GetNotification(@Path("userId") int userId);//Title,Description,DateTime

    @Headers("Content-Type: application/json")
    @POST("AddOpeningStockWithZeroQuantity/{userId}")
    Call<ForgetModel> OpeningStockWithZeroQuantity(@Path("userId") int userId,@Body String body);


}
