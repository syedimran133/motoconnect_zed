package sales.zed.com.zedsales.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.databinding.SynclayoutBinding;
import sales.zed.com.zedsales.interfaces.MyAdapterListener;
import sales.zed.com.zedsales.models.OffLineData;

public class RVOfflineData extends RecyclerView.Adapter<RVOfflineData.ViewHolder> {

    private List<OffLineData> listData;
    private LayoutInflater mInflater;
    private MyAdapterListener click_listener;

    public RVOfflineData(Context context, List<OffLineData> listData, MyAdapterListener click_listener) {
        this.listData = listData;
        this.click_listener = click_listener;
        mInflater = LayoutInflater.from(context);
    }


    @Override
    public int getItemCount() {
        if (listData == null)
            return 0;
        return listData.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        //if (listData.get(position).getStatus().equalsIgnoreCase("1")) {
            OffLineData offlineitem = listData.get(position);
            holder.binding.txtName.setText(offlineitem.getName());
            holder.binding.txtTime.setText(offlineitem.getTime());
            holder.binding.txtStatus.setText(offlineitem.getRemark());
            if (offlineitem.getStatus().equalsIgnoreCase("0")) {
                holder.binding.btnSync.setVisibility(View.GONE);
            } else {
                holder.binding.btnSync.setVisibility(View.VISIBLE);
            }
        //}
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.synclayout, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        SynclayoutBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);

            binding.btnSync.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (click_listener != null)
                        click_listener.iconButtonViewOnClick(v, getAdapterPosition());
                }
            });
        }
    }

    public void updateData(List<OffLineData> viewModels) {
        if (listData == null || listData.size() == 0)
            return;
        if (listData != null && listData.size() > 0)
            listData.clear();
        listData.addAll(viewModels);
        notifyDataSetChanged();
    }
}
