
package sales.zed.com.zedsales.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BulletinCategoryList {

    @SerializedName("BulletinSubCategory")
    @Expose
    private List<BulletinSubCategory> bulletinSubCategory = null;
    @SerializedName("CategoryName")
    @Expose
    private String categoryName;

    public List<BulletinSubCategory> getBulletinSubCategory() {
        return bulletinSubCategory;
    }

    public void setBulletinSubCategory(List<BulletinSubCategory> bulletinSubCategory) {
        this.bulletinSubCategory = bulletinSubCategory;
    }

    public String getCategoryName() {
        return categoryName;
    }

    public void setCategoryName(String categoryName) {
        this.categoryName = categoryName;
    }

}
