package sales.zed.com.zedsales.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;

import java.util.List;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.databinding.NotificationlayoutBinding;
import sales.zed.com.zedsales.models.NotificationData;

public class RNNotificatiopn extends RecyclerView.Adapter<RNNotificatiopn.ViewHolder> {

    private List<NotificationData> listData;
    private LayoutInflater mInflater;
    public RelativeLayout viewBackground;
    public RelativeLayout viewForeground;

    public RNNotificatiopn(Context context, List<NotificationData> listData) {
        this.listData = listData;
        mInflater = LayoutInflater.from(context);
    }
    @Override
    public int getItemCount() {
        if (listData == null)
            return 0;
        return listData.size();
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        NotificationData offlineitem = listData.get(position);
        holder.binding.txtTitel.setText(offlineitem.getTitle());
        holder.binding.txtTime.setText(offlineitem.getDateTime());
        holder.binding.txtDescription.setText(offlineitem.getDescription());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.notificationlayout, parent, false);
        ViewHolder holder = new ViewHolder(view);
        return holder;
    }
    public void removeItem(int position) {
        listData.remove(position);
        // notify the item removed by position
        // to perform recycler view delete animations
        // NOTE: don't call notifyDataSetChanged()
        notifyItemRemoved(position);
    }

    public void restoreItem(NotificationData item, int position) {
        listData.add(position, item);
        // notify item added by position
        notifyItemInserted(position);
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        NotificationlayoutBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            viewBackground=binding.viewBackground;
            viewForeground=binding.viewForeground;
        }
    }
}
