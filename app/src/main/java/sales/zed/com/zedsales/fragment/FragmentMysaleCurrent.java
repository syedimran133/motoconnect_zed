package sales.zed.com.zedsales.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.adapter.AdapterDialogListItem;
import sales.zed.com.zedsales.adapter.SaleAdapter;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.apiz.RequestFormater;
import sales.zed.com.zedsales.constants.AppConstants;
import sales.zed.com.zedsales.databinding.DialogListBinding;
import sales.zed.com.zedsales.databinding.FragmentMysaleCurrentBinding;
import sales.zed.com.zedsales.interfaces.IDialogListClickListener;
import sales.zed.com.zedsales.models.BrandListsItem;
import sales.zed.com.zedsales.models.ForgetModel;
import sales.zed.com.zedsales.models.ModelListsItem;
import sales.zed.com.zedsales.models.OffLineData;
import sales.zed.com.zedsales.models.ProductCategoryListsItem;
import sales.zed.com.zedsales.models.SaleSerialListItems;
import sales.zed.com.zedsales.models.SaleSkuListItem;
import sales.zed.com.zedsales.models.SerialMaster;
import sales.zed.com.zedsales.models.SkuListsItem;
import sales.zed.com.zedsales.room.MyDatabase;
import sales.zed.com.zedsales.support.AppPrefrence;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FlowOrganizer;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by root on 09/12/17.
 */

public class FragmentMysaleCurrent extends Fragment {

    private List<SaleSkuListItem> itemList = new ArrayList<>();
    private List<SaleSkuListItem> saleSkuListItems = new ArrayList<>();
    final ArrayList<String> listproductcatid = new ArrayList<>();
    final ArrayList<String> listbrandid = new ArrayList<>();
    final ArrayList<String> listskuid = new ArrayList<>();
    final ArrayList<String> listmodelid = new ArrayList<>();
    private FragmentMysaleCurrentBinding binding;
    private String brandid = "", productcatid = "", modelid = "", skuid = "";
    private SerialMaster serialMaster = null;
    private int selectedBrandPos = -1, selectedProductCategoryPos = -1, selectedModelPos = -1, selectedSkuPos = -1, selectedSerialPos = -1;
    private List<String> listBrand, listPorductCategory, listModel, listSku, listSerial;
    private List<BrandListsItem> fetchBrandList;
    private List<ProductCategoryListsItem> fetchProductCategoryList;
    private List<ModelListsItem> fetchMasterList;
    private List<SkuListsItem> fetchSkuMasterList;
    private List<SerialMaster> fetchSerialMasterList;
    private BaseFragment mConBas;
    private Fragment fragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_mysale_current, container, false);
        fragment = this;
        AppSingle.getInstance().getActivity().setTitle("My Sale", true);
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("My Sale"));
        mConBas = new BaseFragment();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        featchDB();
        binding.btnSaveSaleSubmit.setOnClickListener(listener);
        binding.relativeBrand.setOnClickListener(listener);
        binding.relativeModel.setOnClickListener(listener);
        binding.relativeSku.setOnClickListener(listener);
        binding.relativeProduct.setOnClickListener(listener);
        binding.relativeSerialNo.setOnClickListener(listener);
        binding.ivBarcode.setOnClickListener(listener);
        binding.belowAdd.setOnClickListener(listener);
        binding.barAdd.setOnClickListener(listener);
        binding.btnCurPrevSale.setOnClickListener(listener);
        binding.btnCurTxtSale.setOnClickListener(listener);
    }

    View.OnClickListener listener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_cur_txt_sale:

                    binding.btnCurPrevSale.setBackgroundColor(Color.parseColor("#A39F9E"));
                    binding.btnCurTxtSale.setBackgroundColor(Color.parseColor("#00bfff"));
                    FlowOrganizer.getInstance().add(new FragmentMysaleCurrent(), true);
                    break;
                case R.id.btn_cur_prev_sale:
                    binding.btnCurPrevSale.setBackgroundColor(Color.parseColor("#00bfff"));
                    binding.btnCurTxtSale.setBackgroundColor(Color.parseColor("#A39F9E"));
                    FlowOrganizer.getInstance().add(new FragmentReportMySaleViewpr(), true);
                    break;
                case R.id.btn_save_sale_submit:
                    //if (isempty()) {
                    if (itemList.size() != 0) {
                        callAsync();
                    } else {
                        mConBas.showToast("please add product before save sale.");
                    }
                   /* } else {
                        mConBas.showToast("please fill customer details before save sale.");
                    }*/
                    break;
                case R.id.relative_brand:
                    featchDB();
                    serialMaster = null;
                    setBrandList();
                    break;
                case R.id.relative_product:
                    setProductList();
                    break;
                case R.id.relative_model:
                    if (!binding.txtViewBrand.getText().toString().equalsIgnoreCase("") && !binding.txtViewProduct.getText().toString().equalsIgnoreCase("")) {
                        setModelList();
                    } else {
                        mConBas.showToast("Please select Brand and Product frist.");
                    }
                    break;
                case R.id.relative_sku:
                    if (!binding.txtViewModel.getText().toString().equalsIgnoreCase("")) {
                        setSkuList();
                    } else {
                        mConBas.showToast("Please select model frist.");
                    }
                    break;
                case R.id.relative_serial_no:
                    if (!binding.txtViewSku.getText().toString().equalsIgnoreCase("")) {
                        setSerialMaster();
                    } else {
                        mConBas.showToast("Please select sku frist.");
                    }
                    break;
                case R.id.below_add:

                    if (!binding.txtViewSku.getText().toString().equalsIgnoreCase("")) {
                        if (binding.relativeSerialNo.isShown()) {
                            if (!binding.txtViewSerialNo.getText().toString().equalsIgnoreCase("")) {
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().deleteSerialMasterBySr(binding.txtViewSerialNo.getText().toString().trim());
                                itemList = getSaleItem(binding.txtViewSku.getText().toString());
                                SaleAdapter adapter = new SaleAdapter(AppSingle.getInstance().getActivity(), itemList, mConBas);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
                                binding.rvListSales.setLayoutManager(mLayoutManager);
                                binding.rvListSales.setItemAnimator(new DefaultItemAnimator());
                                binding.rvListSales.setAdapter(adapter);
                                setempty();
                                Toast.makeText(AppSingle.getInstance().getActivity(), "Added Successfully.", Toast.LENGTH_SHORT).show();
                            } else {
                                mConBas.showToast("Please select values");
                            }
                        } else {

                            if (!binding.tvAddsaleQty.getText().toString().equalsIgnoreCase("")) {
                                if (Integer.parseInt(binding.tvAddsaleQty.getText().toString()) > 0) {
                                    itemList = getSaleItem(binding.txtViewSku.getText().toString());
                                    SaleAdapter adapter = new SaleAdapter(AppSingle.getInstance().getActivity(), itemList, mConBas);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
                                    binding.rvListSales.setLayoutManager(mLayoutManager);
                                    binding.rvListSales.setItemAnimator(new DefaultItemAnimator());
                                    binding.rvListSales.setAdapter(adapter);
                                    setempty();
                                    Toast.makeText(AppSingle.getInstance().getActivity(), "Added Successfully.", Toast.LENGTH_SHORT).show();
                                } else {
                                    Toast.makeText(AppSingle.getInstance().getActivity(), "Qty must be greater than zero", Toast.LENGTH_SHORT).show();
                                }
                            } else {
                                mConBas.showToast("Please select values");
                            }
                        }
                    } else {
                        mConBas.showToast("Please select values");
                    }
                    break;
                case R.id.bar_add:
                    if (!binding.txtSalebar.getText().toString().equalsIgnoreCase("")) {
                        SerialMaster srdata = getserila(binding.txtSalebar.getText().toString());
                        try {
                            if (srdata != null) {
                                itemList = getSaleItemSerial(getskuname(srdata.getSkuId()), binding.txtSalebar.getText().toString());
                                SaleAdapter adapter = new SaleAdapter(AppSingle.getInstance().getActivity(), itemList, mConBas);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
                                binding.rvListSales.setLayoutManager(mLayoutManager);
                                binding.rvListSales.setItemAnimator(new DefaultItemAnimator());
                                binding.rvListSales.setAdapter(adapter);
                                binding.txtSalebar.setText("");
                            } else {
                                Toast.makeText(AppSingle.getInstance().getActivity(), "Serial not found in master data.please check with your admin", Toast.LENGTH_LONG).show();
                            }
                        } catch (Exception e) {
                            Toast.makeText(AppSingle.getInstance().getActivity(), e.toString(), Toast.LENGTH_LONG).show();
                        }
                    } else {
                        Toast.makeText(AppSingle.getInstance().getActivity(), "Please scan first to get the data", Toast.LENGTH_LONG).show();
                    }
                    break;
                case R.id.iv_barcode:
                    try {
                        if (ContextCompat.checkSelfPermission(AppSingle.getInstance().getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                            IntentIntegrator.forSupportFragment(fragment).initiateScan();
                        } else {
                            mConBas.showToast("Permission not found.");
                        }
                    } catch (Exception e) {
                        mConBas.showToast(e.toString());
                    }
                    break;
            }
        }
    };

    @Override
    public void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }

    private void setBrandList() {

        setdata(1);
        openListDialog("Select Brand", selectedBrandPos, listBrand, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedBrandPos = selectedPos;
                    binding.txtViewBrand.setText(listBrand.get(selectedPos));
                    brandid = listbrandid.get(selectedPos);
                } catch (Exception e) {
                }
            }
        });

    }

    private void setProductList() {
        setdata(2);
        openListDialog("Select Product Category", selectedProductCategoryPos, listPorductCategory, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedProductCategoryPos = selectedPos;
                    binding.txtViewProduct.setText(listPorductCategory.get(selectedPos));
                    productcatid = listproductcatid.get(selectedPos);

                } catch (Exception e) {
                }
            }
        });

    }

    private void setModelList() {
        setdata(3);
        openListDialog("Select Model", selectedModelPos, listModel, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedModelPos = selectedPos;
                    binding.txtViewModel.setText(listModel.get(selectedPos));
                    isSerial(listModel.get(selectedPos));
                    modelid = listmodelid.get(selectedPos);

                } catch (Exception e) {
                }
            }
        });

    }

    private void setSkuList() {

        setdata(4);
        openListDialog("Select Product Category", selectedSkuPos, listSku, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedSkuPos = selectedPos;
                    binding.txtViewSku.setText(listSku.get(selectedPos));
                    skuid = listskuid.get(selectedPos);

                } catch (Exception e) {
                }
            }
        });

    }

    private void setSerialMaster() {

        setdata(5);
        openListDialog("Select Serial", selectedSerialPos, listSerial, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                selectedSerialPos = selectedPos;
                binding.txtViewSerialNo.setText(listSerial.get(selectedPos));
                serialMaster = new SerialMaster();
                for (SerialMaster srdata : fetchSerialMasterList) {
                    if (srdata.getSerialNumber1().equalsIgnoreCase(listSerial.get(selectedPos))) {
                        serialMaster.setSerialNumber1(srdata.getSerialNumber1());
                        serialMaster.setSerialNumber2(srdata.getSerialNumber2());
                        serialMaster.setSerialNumber3(srdata.getSerialNumber3());
                        serialMaster.setSerialNumber4(srdata.getSerialNumber4());
                    }
                }
            }
        });

    }

    private boolean isSerial(String m_name) {
        boolean result = false;
        for (int i = 0; i < fetchMasterList.size(); i++) {
            if (fetchMasterList.get(i).getModelName().equalsIgnoreCase(m_name)) {
                if (("" + fetchMasterList.get(i).getSerialisedMode()).equalsIgnoreCase("3")) {
                    binding.relativeSerialNo.setVisibility(View.VISIBLE);
                    binding.tvAddsaleQty.setVisibility(View.GONE);
                } else {
                    binding.relativeSerialNo.setVisibility(View.GONE);
                    binding.tvAddsaleQty.setVisibility(View.VISIBLE);
                }
            }
        }
        return result;
    }

    public void setdata(int code) {
        switch (code) {
            case 1:
                if (listBrand == null) {
                    listBrand = new ArrayList<>();
                    for (BrandListsItem data : fetchBrandList) {
                        if (data.isStatus()) {
                            listBrand.add(data.getBrandName());
                            listbrandid.add("" + data.getBrandId());
                        }

                    }
                }
                try {
                    listPorductCategory = null;
                    listModel = null;
                    listSku = null;
                    listSerial = null;
                    productcatid = "";
                    modelid = "";
                    skuid = "";
                    listproductcatid.clear();
                    listmodelid.clear();
                    listSku.clear();
                    listSerial.clear();
                } catch (Exception e) {
                }
                binding.txtViewProduct.setText("");
                binding.txtViewModel.setText("");
                binding.txtViewSku.setText("");
                binding.txtViewSerialNo.setText("");
                break;
            case 2:
                if (listPorductCategory == null) {
                    listPorductCategory = new ArrayList<>();
                    for (ProductCategoryListsItem data : fetchProductCategoryList) {
                        if (data.isStatus()) {
                            listPorductCategory.add(data.getProductCategoryName());
                            listproductcatid.add("" + data.getProductCategoryId());
                        }
                    }
                }
                try {
                    listModel = null;
                    listSku = null;
                    listSerial = null;
                    modelid = "";
                    skuid = "";
                    listmodelid.clear();
                    listSku.clear();
                    listSerial.clear();
                } catch (Exception e) {
                }
                binding.txtViewModel.setText("");
                binding.txtViewSku.setText("");
                binding.txtViewSerialNo.setText("");
                break;
            case 3:

                if (listModel == null) {
                    listModel = new ArrayList<>();
                    for (ModelListsItem data : fetchMasterList) {
                        if (data.isStatus()) {
                            if ((data.getBrandId() + "").equalsIgnoreCase(brandid) && ("" + data.getProductCategoryId()).equalsIgnoreCase(productcatid)) {
                                listModel.add(data.getModelName());
                                listmodelid.add("" + data.getModelId());
                            }
                        }
                    }
                }
                try {
                    listSku = null;
                    listSerial = null;
                    skuid = "";
                    listSku.clear();
                    listSerial.clear();
                } catch (Exception e) {
                }
                binding.txtViewSku.setText("");
                binding.txtViewSerialNo.setText("");
                break;
            case 4:
                if (listSku == null) {
                    listSku = new ArrayList<>();
                    for (SkuListsItem data : fetchSkuMasterList) {
                        if (data.isStatus() && modelid.equalsIgnoreCase("" + data.getModelId())) {
                            listSku.add(data.getSkuName());
                            listskuid.add("" + data.getSkuId());
                        }
                    }
                }
                try {
                    listSerial = null;
                    listSerial.clear();
                } catch (Exception e) {
                }
                binding.txtViewSerialNo.setText("");
                break;
            case 5:
                fetchSerialMasterList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchSerialMasterSale(AppPrefrence.getInstance().getEntityId()/*AppSingle.getInstance().getLoginUser().getEntityId()*/, getskuid(binding.txtViewSku.getText().toString()));
                listSerial = null;
                if (listSerial == null) {
                    listSerial = new ArrayList<>();
                    for (SerialMaster data : fetchSerialMasterList) {
                        if (data.getStockBinTypeMasterId().equalsIgnoreCase("2")) {
                            listSerial.add(data.getSerialNumber1());
                        }
                    }
                }
                break;
        }
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case IntentIntegrator.REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    // Parsing bar code reader result
                    IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
                    //if (Debug) Log.d(TAG, "Parsing bar code reader result: " + result.toString());
                    binding.txtSalebar.setText(result.getContents());
                    //Toast.makeText(AppSingle.getInstance().getActivity(), result.getContents(), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    private SerialMaster getserila(String serial) {
        SerialMaster serialMaster = null;
        for (int i = 0; i < fetchSerialMasterList.size(); i++) {
            if (fetchSerialMasterList.get(i).getSerialNumber1().equalsIgnoreCase(serial)) {
                serialMaster = fetchSerialMasterList.get(i);
                break;
            }
        }
        return serialMaster;
    }

    private String getskuid(String skuname) {
        String id = "";
        for (SkuListsItem data : fetchSkuMasterList) {
            if (data.isStatus() && data.getSkuName().equalsIgnoreCase(skuname)) {
                id = "" + data.getSkuId();
            }
        }
        return id;
    }

    private String getskuname(String skuid) {
        String id = "";
        for (SkuListsItem data : fetchSkuMasterList) {
            if (data.isStatus() && ("" + data.getSkuId()).equalsIgnoreCase(skuid)) {
                id = "" + data.getSkuName();
            }
        }
        return id;
    }

    private List<SaleSkuListItem> getSaleItem(String skuname) {
        SaleSkuListItem skuListItem = new SaleSkuListItem();
        SkuListsItem skuListsItem = new SkuListsItem();
        for (int i = 0; i < fetchSkuMasterList.size(); i++) {
            if (fetchSkuMasterList.get(i).getSkuName().equalsIgnoreCase(skuname)) {
                skuListsItem = fetchSkuMasterList.get(i);
            }
        }
        skuListItem.setSkuId("" + skuListsItem.getSkuId());
        skuListItem.setSkuName(skuListsItem.getSkuName());
        skuListItem.setSerialisedMode(skuListsItem.getSerialisedMode());
        if (!binding.tvAddsaleQty.getText().toString().equalsIgnoreCase("")) {
            skuListItem.setSkuQuantity(binding.tvAddsaleQty.getText().toString());
        } else {
            skuListItem.setSkuQuantity("1");
        }
        skuListItem.setSkuCode(skuListsItem.getSkuCode());
        if (serialMaster != null) {
            List<SaleSerialListItems> saleSerialList = new ArrayList<>();
            SaleSerialListItems saleSerialListItems = new SaleSerialListItems();
            saleSerialListItems.setSerial1(serialMaster.getSerialNumber1());
            saleSerialListItems.setSerial2(serialMaster.getSerialNumber2());
            saleSerialListItems.setSerial3(serialMaster.getSerialNumber3());
            saleSerialListItems.setSerial4(serialMaster.getSerialNumber4());
            saleSerialList.add(saleSerialListItems);
            skuListItem.setSerialList(saleSerialList);
        } else {
            List<SaleSerialListItems> saleSerialList = new ArrayList<>();
            SaleSerialListItems saleSerialListItems = new SaleSerialListItems();
            saleSerialListItems.setSerial1("");
            saleSerialListItems.setSerial2("");
            saleSerialListItems.setSerial3("");
            saleSerialListItems.setSerial4("");
            skuListItem.setSerialList(saleSerialList);
        }
        saleSkuListItems.add(skuListItem);
        return saleSkuListItems;
    }

    private List<SaleSkuListItem> getSaleItemSerial(String skuname, String serial) {
        SaleSkuListItem skuListItem = new SaleSkuListItem();
        SkuListsItem skuListsItem = new SkuListsItem();
        for (int i = 0; i < fetchSkuMasterList.size(); i++) {
            if (fetchSkuMasterList.get(i).getSkuName().equalsIgnoreCase(skuname)) {
                skuListsItem = fetchSkuMasterList.get(i);
            }
        }
        skuListItem.setSkuId("" + skuListsItem.getSkuId());
        skuListItem.setSkuName(skuListsItem.getSkuName());
        skuListItem.setSerialisedMode(3);
        skuListItem.setSkuQuantity("1");
        skuListItem.setSkuCode(skuListsItem.getSkuCode());

        List<SaleSerialListItems> saleSerialList = new ArrayList<>();
        SaleSerialListItems saleSerialListItems = new SaleSerialListItems();
        saleSerialListItems.setSerial1(serial);
        saleSerialListItems.setSerial2(serial);
        saleSerialListItems.setSerial3(serial);
        saleSerialListItems.setSerial4(serial);
        saleSerialList.add(saleSerialListItems);
        skuListItem.setSerialList(saleSerialList);

        saleSkuListItems.add(skuListItem);
        return saleSkuListItems;
    }

    protected void openListDialog(String title, int selectedPos, final List<String> listData, final IDialogListClickListener listener) {
        DialogListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(AppSingle.getInstance().getActivity().getApplicationContext()), R.layout.dialog_list, null, false);
        final Dialog dialog = new Dialog(AppSingle.getInstance().getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(binding.getRoot());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        binding.txtViewTitle.setText(title);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        LinearLayoutManager lLayout = new LinearLayoutManager(AppSingle.getInstance().getActivity());
        RecyclerView rView = binding.rvList;
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);
        AdapterDialogListItem adapter = new AdapterDialogListItem(listData, selectedPos);
        rView.setAdapter(adapter);
        adapter.registerOnItemClickListener(new AdapterDialogListItem.IonItemSelect() {
            @Override
            public void onItemSelect(int position) {
                if (listener != null)
                    listener.onClick(listData.get(position), position);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void setempty() {
        binding.txtViewBrand.setText("");
        binding.txtViewProduct.setText("");
        binding.txtViewModel.setText("");
        binding.txtViewSku.setText("");
        binding.txtViewSerialNo.setText("");
        binding.tvAddsaleQty.setText("");
        //listset.clear();
    }

    private void callAsync() {

        String customerName = binding.etCustomerName.getText().toString();
        String mobileNo = binding.etMobileNo.getText().toString();
        String invcNo = binding.edInvcNo.getText().toString();

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            mConBas.showToast("Unable to process your request you request save offline when ever get connectivity we send it to server");
            OffLineData data = new OffLineData();
            data.setName("SaveSale");
            data.setTime(MainActivity.getCurrentTime());
            data.setType("3");
            data.setRequestData(RequestFormater.savesale(invcNo, customerName, mobileNo, itemList));
            data.setStatus("1");
            data.setRemark("Save in local database");
            MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertOffLineData(data);
            MainActivity.setnotification();
            itemList.clear();
            SaleAdapter adapter = new SaleAdapter(AppSingle.getInstance().getActivity(), itemList, mConBas);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
            binding.rvListSales.setLayoutManager(mLayoutManager);
            binding.rvListSales.setItemAnimator(new DefaultItemAnimator());
            binding.rvListSales.setAdapter(adapter);
            return;
        } else {
            if (!isdublicate(RequestFormater.savesale(invcNo, customerName, mobileNo, itemList))) {
                ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
                Call<ForgetModel> call = apiService.saveSale(AppSingle.getInstance().getLoginUser().getUserId(), RequestFormater.savesale(invcNo, customerName, mobileNo, itemList));
                mConBas.showProgessBar();
                call.enqueue(new Callback<ForgetModel>() {
                    @Override
                    public void onResponse(Call<ForgetModel> call, Response<ForgetModel> response) {
                        mConBas.hideProgessBar();
                        if (response.code() == 200) {
                            if (response.body() != null) {
                                if (response.body().getStatusCode() == 0) {
                                    itemList.clear();
                                    SaleAdapter adapter = new SaleAdapter(AppSingle.getInstance().getActivity(), itemList, mConBas);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
                                    binding.rvListSales.setLayoutManager(mLayoutManager);
                                    binding.rvListSales.setItemAnimator(new DefaultItemAnimator());
                                    binding.rvListSales.setAdapter(adapter);
                                    if (response.body().getMessage() != null) {
                                        mConBas.showToast(response.body().getMessage());
                                        //AppSingle.getInstance().getfeatchobj(mConBas);
                                        //FlowOrganizer.getInstance().add(new FragmentMenu(), false);
                                    } else {
                                        mConBas.showToast("Message not received from server.");
                                    }
                                } else {
                                    mConBas.showToast(response.body().getMessage());
                                }
                            } else {
                                mConBas.showToast("Response not received from server.");
                            }
                        } else {
                            mConBas.showResponseMessage(response.code());
                            if (response.code() == 401)
                                callAsync();
                        }
                    }

                    @Override
                    public void onFailure(Call<ForgetModel> call, Throwable t) {
                        mConBas.hideProgessBar();
                        mConBas.showToast(AppConstants.IMessages.FAIL);
                    }
                });
            } else {
                mConBas.showToast("This request already in the offline data plz accept their only");
            }
        }
    }

    private boolean isdublicate(String str) {

        boolean dublicate = false;
        try {
            List<OffLineData> data = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchOffLineData();
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).getRequestData().equalsIgnoreCase(str))
                    dublicate = true;
            }
        } catch (Exception e) {
        }
        return dublicate;
    }

    public boolean isempty() {
        if (binding.etCustomerName.getText().toString().equalsIgnoreCase("") && binding.etMobileNo.getText().toString().equalsIgnoreCase("") && binding.edInvcNo.getText().toString().equalsIgnoreCase("")) {
            return false;
        } else {
            return true;
        }
    }

    public void featchDB() {
        try {
            fetchBrandList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchBrandList();
            fetchProductCategoryList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchProductCategoryList();
            fetchMasterList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchMasterList();
            fetchSkuMasterList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchSkuMasterList();
            fetchSerialMasterList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchSerialMaster(AppPrefrence.getInstance().getEntityId());
        } catch (Exception e) {
        }
    }

}
