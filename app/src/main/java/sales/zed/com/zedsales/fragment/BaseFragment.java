package sales.zed.com.zedsales.fragment;

import android.support.v4.app.Fragment;
import android.widget.Toast;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.apiz.RequestFormater;
import sales.zed.com.zedsales.constants.AppConstants;
import sales.zed.com.zedsales.models.ProfileItems;
import sales.zed.com.zedsales.models.UserModel;
import sales.zed.com.zedsales.support.AppPrefrence;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.AppValidate;
import sales.zed.com.zedsales.support.FlowOrganizer;
import sales.zed.com.zedsales.support.Log;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by root on 09/12/17.
 */

public class BaseFragment extends Fragment implements AppConstants.IMessages {

    public void showToast(String message) {
        Toast.makeText(AppSingle.getInstance().getActivity(), message, Toast.LENGTH_SHORT).show();
    }

    public void showResponseMessage(int code) {
        switch (code) {
            case 200:
                //showToast("Success");
                break;
            case 400:
                showToast("Missing parameters");
                break;
            case 401:
                callAsync();
                //showToast("Your authKey is expired "/*please try again same transaction*/);
                break;
            case 404:
                showToast("Not Found");
                break;
            case 500:
                showToast("Internal server error");
                break;

            default:
                showToast("Undefine Error Code");
                break;
        }

    }

    public String responseMessage(int code) {

        String result = "";

        switch (code) {
            case 200:
                //showToast("Success");
                break;
            case 400:
                result = "Missing parameters";
                break;
            case 401:
                //callAsync();
                result = "Your authKey is expired ";/*please try again same transaction);*/
                break;
            case 404:
                result = "Not Found";
                break;
            case 500:
                result = "Internal server error";
                break;

            default:
                result = "Undefine Error Code";
                break;
        }
        return result;
    }

    private void callAsync() {

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }

        ApiInterface apiService = ApiClient.getDefaultClient().create(ApiInterface.class);
        Call<UserModel> call = apiService.login(RequestFormater.login(AppPrefrence.getInstance().getUserNamee(), AppPrefrence.getInstance().getPasword(), AppPrefrence.getInstance().getAccessToken(), AppPrefrence.getInstance().getDeviceId(), AppPrefrence.getInstance().getPushToken()));
        showProgessBar();
        call.enqueue(new Callback<UserModel>() {
            @Override
            public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    AppPrefrence.getInstance().setToken(response.body().getAuthKey());
                    AppPrefrence.getInstance().setIsActivityExecuted(true);
                    AppPrefrence.getInstance().setStock("");
                    AppPrefrence.getInstance().setUserid(response.body().getUserId());
                    AppSingle.getInstance().setLoginUser(response.body());
                    AppPrefrence.getInstance().setStock(response.body().getIsOpeningStockEntered());
                } else {
                    if (response.code() == 401) {
                        showToast("Entered Username or Password Incorrect");
                    } else {
                        showResponseMessage(response.code());
                    }
                }
            }

            @Override
            public void onFailure(Call<UserModel> call, Throwable t) {
                hideProgessBar();
                showToast("Login Failed");
            }
        });

    }
    public String getString(ProfileItems data) {
        return
                "{" +
                        "\"CompanyName\" :\"" + data.getCompanyName() + '\"' +','+
                        "\"UserCode\" :\"" + data.getUserCode() + '\"' +','+
                        "\"UserName\" :\"" + data.getUserName() + '\"' +','+
                        "\"RegisteredMobileNo\" :\"" + data.getRegisteredMobileNo() + '\"' +','+
                        "\"AlternateMobileNo\" :\"" + data.getAlternateMobileNo() + '\"' +','+
                        "\"EmailId\" :\"" + data.getEmailId() + '\"' +','+
                        "\"DateOfBirth\" :\"" + data.getDateOfBirth() + '\"' +','+
                        "\"Married\" :\"" + data.getMarried() + '\"' +','+
                        "\"DateOfAnniversary\" :\"" + data.getDateOfAnniversary() + '\"' +','+
                        "\"Address\" :\"" + data.getAddress() + '\"' +','+
                        "\"City\" :\"" + data.getCity() + '\"' +','+
                        "\"State\" :\"" + data.getState() + '\"' +','+
                        "\"PinCode\" :\"" + data.getPinCode() + '\"' +','+
                        "\"CounterPotentialValue\" :\"" + data.getCounterPotentialValue() + '\"' +','+
                        "\"CounterPotentialVolume\" :\"" + data.getCounterPotentialVolume() + '\"' +','+
                        "\"AccountHolderName\" :\"" + data.getAccountHolderName() + '\"' +','+
                        "\"AccountNumber\" :\"" + data.getAccountNumber() + '\"' +','+
                        "\"BankName\" :\"" + data.getBankName() + '\"' +','+
                        "\"BranchName\" :\"" + data.getBranchName() + '\"' +','+
                        "\"BranchLocation\" :\"" + data.getBranchLocation() + '\"' +','+
                        "\"IfscCode\" :\"" + data.getIfscCode() + '\"' +','+
                        "\"AadharNumber\" :\"" + data.getAadharNumber() + '\"' +','+
                        "\"PanNumber\" :\"" + data.getPanNumber() + '\"' +','+
                        "\"GstinNo\" :\"" + data.getGstinNo() + '\"'+
                        "}";
    }
    public ProfileItems getprofile() {
        ProfileItems items = new ProfileItems();
        JSONObject jObject;
        try {
            jObject = new JSONObject(AppPrefrence.getInstance().getProfile());
            items.setCompanyName(jObject.getString("CompanyName"));
            items.setUserCode(jObject.getString("UserCode"));
            items.setUserName(jObject.getString("UserName"));
            items.setRegisteredMobileNo(jObject.getString("RegisteredMobileNo"));
            items.setAlternateMobileNo(jObject.getString("AlternateMobileNo"));
            items.setEmailId(jObject.getString("EmailId"));
            items.setDateOfBirth(jObject.getString("DateOfBirth"));
            items.setMarried(jObject.getString("Married"));
            items.setDateOfAnniversary(jObject.getString("DateOfAnniversary"));
            items.setAddress(jObject.getString("Address"));
            items.setCity(jObject.getString("City"));
            items.setState(jObject.getString("State"));
            items.setPinCode(jObject.getString("PinCode"));
            items.setCounterPotentialValue(jObject.getString("CounterPotentialValue"));
            items.setCounterPotentialVolume(jObject.getString("CounterPotentialVolume"));
            items.setAccountHolderName(jObject.getString("AccountHolderName"));
            items.setAccountNumber(jObject.getString("AccountNumber"));
            items.setBankName(jObject.getString("BankName"));
            items.setBranchName(jObject.getString("BranchName"));
            items.setBranchLocation(jObject.getString("BranchLocation"));
            items.setIfscCode(jObject.getString("IfscCode"));
            items.setAadharNumber(jObject.getString("AadharNumber"));
            items.setPanNumber(jObject.getString("PanNumber"));
            items.setGstinNo(jObject.getString("GstinNo"));

        } catch (Exception e) {
            Log.e("ERROR :: ",e.toString());
        }
        return items;
    }

    public void showProgessBar() {
        AppSingle.getInstance().getActivity().setProgressDialog();
    }

    public void hideProgessBar() {
        AppSingle.getInstance().getActivity().dismissProgressDialog();
    }
}
