package sales.zed.com.zedsales.models;


import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class ProductListsItem {

    @SerializedName("Status")
    @Expose
    private boolean status;

    @SerializedName("ProductName")
    @Expose
    private String productName;

    @SerializedName("ModifiedOn")
    @Expose
    private String modifiedOn;

    @SerializedName("ProductCode")
    @Expose
    private String productCode;

    @SerializedName("RecordCreationDate")
    @Expose
    private String recordCreationDate;

    @PrimaryKey
    @SerializedName("ProductId")
    @Expose
    private int productId;

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }

    public void setProductName(String productName) {
        this.productName = productName;
    }

    public String getProductName() {
        return productName;
    }

    public void setModifiedOn(String modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String getModifiedOn() {
        return modifiedOn;
    }

    public void setProductCode(String productCode) {
        this.productCode = productCode;
    }

    public String getProductCode() {
        return productCode;
    }

    public void setRecordCreationDate(String recordCreationDate) {
        this.recordCreationDate = recordCreationDate;
    }

    public String getRecordCreationDate() {
        return recordCreationDate;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getProductId() {
        return productId;
    }

    @Override
    public String toString() {
        return
                "ProductListsItem{" +
                        "status = '" + status + '\'' +
                        ",productName = '" + productName + '\'' +
                        ",modifiedOn = '" + modifiedOn + '\'' +
                        ",productCode = '" + productCode + '\'' +
                        ",recordCreationDate = '" + recordCreationDate + '\'' +
                        ",productId = '" + productId + '\'' +
                        "}";
    }
}