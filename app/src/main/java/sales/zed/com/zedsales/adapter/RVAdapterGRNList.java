package sales.zed.com.zedsales.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Button;
import android.widget.TextView;

import java.util.List;

import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.custom.CustomTextView;
import sales.zed.com.zedsales.interfaces.MyAdapterListener;
import sales.zed.com.zedsales.models.PendingGRN;

/**
 * Created by Mukesh on 09/12/17.
 */

public class RVAdapterGRNList extends RecyclerView.Adapter<RVAdapterGRNList.ViewHolder> {
    private List<PendingGRN> listData;
    private LayoutInflater mInflater;
    private MyAdapterListener click_listener;

    public RVAdapterGRNList(Context context, List<PendingGRN> listData, MyAdapterListener listener) {
        this.listData = listData;
        click_listener = listener;
        mInflater = LayoutInflater.from(context);
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.adapter_grn_list, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final PendingGRN data = listData.get(position);
        holder.tvinvnogrn.setText(data.getInvoiceNumber());
        holder.tvinvdategrn.setText(data.getInvoiceDate());
        holder.tvfromname.setText(data.getFrom());
        holder.tvtotalqty.setText(data.getTotalQuantity());
    }

    @Override
    public int getItemCount() {
        if (listData == null)
            return 0;
        return listData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        public CustomTextView tvinvnogrn, tvinvdategrn, tvfromname, tvtotalqty;
        Button viewdetail;

        public ViewHolder(View itemView) {
            super(itemView);

            tvinvnogrn = (CustomTextView) itemView.findViewById(R.id.tv_invno_grn);
            tvinvdategrn = (CustomTextView) itemView.findViewById(R.id.tv_inv_date_grn);
            tvfromname = (CustomTextView) itemView.findViewById(R.id.tv_from_name);
            tvtotalqty = (CustomTextView) itemView.findViewById(R.id.tv_total_qty);
            viewdetail = (Button) itemView.findViewById(R.id.btnViewDetail);

            viewdetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    click_listener.iconButtonViewOnClick(v, getAdapterPosition());
                }
            });
        }
    }
}