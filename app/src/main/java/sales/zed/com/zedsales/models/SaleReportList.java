package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 2/14/18.
 */

public class SaleReportList {

    @SerializedName("PreviousSaleList")
    @Expose
    private List<SaleReportModel> previousSaleList;

    public List<SaleReportModel> getPreviousSaleList() {
        return previousSaleList;
    }

    public void setPreviousSaleList(List<SaleReportModel> previousSaleList) {
        this.previousSaleList = previousSaleList;
    }
}
