package sales.zed.com.zedsales.models;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductModel{

	@SerializedName("ProductLists")
	@Expose
	private List<ProductListsItem> productLists;

	public void setProductLists(List<ProductListsItem> productLists){
		this.productLists = productLists;
	}

	public List<ProductListsItem> getProductLists(){
		return productLists;
	}

	@Override
 	public String toString(){
		return 
			"ProductModel{" + 
			"productLists = '" + productLists + '\'' + 
			"}";
		}
}