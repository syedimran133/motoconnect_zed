package sales.zed.com.zedsales.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v4.app.Fragment;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.fragment.BaseFragment;
import sales.zed.com.zedsales.fragment.FragmentMysaleCurrent;
import sales.zed.com.zedsales.interfaces.MyAdapterListener;
import sales.zed.com.zedsales.models.GRNParent;
import sales.zed.com.zedsales.models.SaleList;
import sales.zed.com.zedsales.models.SaleSkuListItem;
import sales.zed.com.zedsales.models.SerialMaster;
import sales.zed.com.zedsales.models.SerialMasterList;
import sales.zed.com.zedsales.room.MyDatabase;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FeatchMaster;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by root on 1/30/18.
 */

public class SaleAdapter extends RecyclerView.Adapter<SaleViewHolder> {

    List<SaleSkuListItem> listSaleData;
    private LayoutInflater mSaleInflater;
    Context context;
    private MyAdapterListener click_listener_sale;
    int count = 0;
    BaseFragment baseFragment;

    public SaleAdapter(Context context, List<SaleSkuListItem> listSaleData, BaseFragment baseFragment) {
        this.listSaleData = listSaleData;
        this.context = context;
        this.baseFragment = baseFragment;
        mSaleInflater = LayoutInflater.from(context);
    }

    @Override
    public void onBindViewHolder(final SaleViewHolder holder, final int position) {

        SaleSkuListItem saleSkuListItem = listSaleData.get(position);
        holder.mSaleSkuName.setText(saleSkuListItem.getSkuName());
        try {
            holder.mSaleSerial.setText(saleSkuListItem.getSerialList().get(0).getSerial1());
        } catch (Exception e) {

        }
        holder.mSaleQty.setText(saleSkuListItem.getSkuQuantity());
        try {
            count = Integer.parseInt(saleSkuListItem.getSkuQuantity());
        } catch (Exception e) {

        }

        if (saleSkuListItem.getSerialisedMode() == 3) {
            holder.llPlusminus.setVisibility(View.GONE);
            holder.serlayout.setVisibility(View.VISIBLE);
            TextView tv = new TextView(context);
            tv.setText(saleSkuListItem.getSkuQuantity());
            tv.setTextColor(Color.parseColor("#000000"));
            holder.serlayout.addView(tv);
            //holder.serlayout.setVisibility(View.VISIBLE);
        } else {
            holder.llPlusminus.setVisibility(View.VISIBLE);
            holder.serlayout.setVisibility(View.GONE);
            // holder.serlayout.setVisibility(View.INVISIBLE);
        }

        holder.mSaleDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeAt(position);
                callAsyncSerialMaster();
            }
        });

        holder.minus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count > 1) {
                    count--;
                    holder.mSaleQty.setText(String.valueOf(count));
                    listSaleData.get(position).setSkuQuantity("" + count);
                }
            }
        });

        holder.plus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                holder.mSaleQty.setText(String.valueOf(count));
                listSaleData.get(position).setSkuQuantity("" + count);
            }
        });
    }

    public void registerClickListener(MyAdapterListener listener) {
        this.click_listener_sale = listener;
    }

    @Override
    public int getItemCount() {
        if (listSaleData == null)
            return 0;
        return listSaleData.size();
    }

    @Override
    public SaleViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mSaleInflater.inflate(R.layout.sale_item_list, parent, false);
        final SaleViewHolder holder = new SaleViewHolder(view);
        return holder;
    }

    public void removeAt(int position) {
        listSaleData.remove(position);
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, listSaleData.size());
    }

    private void callAsyncSerialMaster() {
        if (!NetworkUtil.isConnectivityAvailable(baseFragment.getActivity())) {
            //baseFragment.showToast("NO_INTERNET");
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<SerialMasterList> call = apiService.getSerialMaster(AppSingle.getInstance().getLoginUser().getUserId(), FeatchMaster.currentDate);
        baseFragment.showProgessBar();
        call.enqueue(new Callback<SerialMasterList>() {
            @Override
            public void onResponse(Call<SerialMasterList> call, Response<SerialMasterList> response) {
                baseFragment.hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null)
                        if (response.body().getSerialLists() != null && response.body().getSerialLists().size() > 0)
                            for (SerialMaster data : response.body().getSerialLists()) {
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertSerialMaster(data);
                            }
                } else {
                    //baseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<SerialMasterList> call, Throwable t) {
                baseFragment.hideProgessBar();
            }
        });
    }
}
