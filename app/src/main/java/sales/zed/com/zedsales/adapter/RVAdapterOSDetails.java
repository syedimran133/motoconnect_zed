package sales.zed.com.zedsales.adapter;

import android.app.Dialog;
import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.view.LayoutInflater;
import android.view.MotionEvent;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.List;

import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.custom.CustomTextView;
import sales.zed.com.zedsales.databinding.AdapterGrnDetailListPBinding;
import sales.zed.com.zedsales.databinding.DilogSerialPopupBinding;
import sales.zed.com.zedsales.databinding.OpeningstockListBinding;
import sales.zed.com.zedsales.databinding.SeriallistEditableBinding;
import sales.zed.com.zedsales.interfaces.MyAdapterListener;
import sales.zed.com.zedsales.models.GRNParent;
import sales.zed.com.zedsales.models.OffLineData;
import sales.zed.com.zedsales.models.SaleSerialListItems;
import sales.zed.com.zedsales.models.SkuOpeningStockList;
import sales.zed.com.zedsales.support.AppSingle;

/**
 * Created by root on 2/9/18.
 */

public class RVAdapterOSDetails extends RecyclerView.Adapter<RVAdapterOSDetails.ViewHolder> {

    private List<SkuOpeningStockList> listData;
    private LayoutInflater mInflater;
    RVAdapterOSDetails rvAdapterOSDetails;
    private MyAdapterListener click_listener;
    int count = 0;

    public RVAdapterOSDetails(Context context, List<SkuOpeningStockList> listData) {
        this.listData = listData;
        mInflater = LayoutInflater.from(context);
        rvAdapterOSDetails = this;
    }

    public void registerClickListener(MyAdapterListener listener) {
        this.click_listener = listener;
    }

    public List<SkuOpeningStockList> getListData() {
        return listData;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        try {
            final SkuOpeningStockList stockList = listData.get(position);
            try {
                count = Integer.parseInt(stockList.getSkuQuantity());
            } catch (Exception e) {

            }
            holder.binding.txtListOsSkuname.setText(stockList.getSkuName());
            holder.binding.txtListOsQty.setText(stockList.getSkuQuantity());
            holder.binding.linearOsHead.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (stockList.isSerail()) {
                        //Toast.makeText(AppSingle.getInstance().getActivity(), "" + position, Toast.LENGTH_LONG).show();
                        openSerialDialog(listData.get(position).getSerialOpeningStockList(), position);
                    } else {
                        Toast.makeText(AppSingle.getInstance().getActivity(), "Record is non serial", Toast.LENGTH_LONG).show();
                    }

                }
            });
            if (stockList.isSerail()) {
                holder.binding.osMinus.setVisibility(View.GONE);
                holder.binding.osPlus.setVisibility(View.GONE);
            } else {
                holder.binding.osMinus.setVisibility(View.VISIBLE);
                holder.binding.osPlus.setVisibility(View.VISIBLE);
            }
            holder.binding.osMinus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!stockList.isSerail()) {
                        if (count > 1) {
                            count--;
                            holder.binding.txtListOsQty.setText(String.valueOf(count));
                            listData.get(position).setSkuQuantity("" + count);
                        }
                    } else {
                        Toast.makeText(AppSingle.getInstance().getActivity(), "Record is serial", Toast.LENGTH_LONG).show();
                    }
                }
            });

            holder.binding.osPlus.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (!stockList.isSerail()) {
                        count++;
                        holder.binding.txtListOsQty.setText(String.valueOf(count));
                        listData.get(position).setSkuQuantity("" + count);
                    } else {
                        Toast.makeText(AppSingle.getInstance().getActivity(), "Record is serial", Toast.LENGTH_LONG).show();
                    }
                }
            });

        } catch (Exception e) {
            Toast.makeText(AppSingle.getInstance().getActivity(), "Error in list.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public int getItemCount() {
        if (listData == null)
            return 0;
        return listData.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = mInflater.inflate(R.layout.openingstock_list, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        OpeningstockListBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (click_listener != null)
                        click_listener.iconButtonViewOnClick(v, getAdapterPosition());
                }
            });
        }
    }

    protected void openSerialDialog(List<SaleSerialListItems> list, final int p) {
        SeriallistEditableBinding binding = DataBindingUtil.inflate(LayoutInflater.from(AppSingle.getInstance().getActivity().getApplicationContext()), R.layout.seriallist_editable, null, false);
        final Dialog dialog = new Dialog(AppSingle.getInstance().getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(binding.getRoot());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        dialog.setCancelable(false);
        dialog.setCanceledOnTouchOutside(false);
        LinearLayoutManager lLayout = new LinearLayoutManager(AppSingle.getInstance().getActivity());
        RecyclerView rView = binding.rvOsDilogList;
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);
        final RVAdapterOSDetailsSubList adapter = new RVAdapterOSDetailsSubList(AppSingle.getInstance().getActivity(), list, rvAdapterOSDetails, p);
        rView.setAdapter(adapter);
        binding.btnSerialClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                listData.get(p).setSerialOpeningStockList(adapter.getListData());
                listData.get(p).setSkuQuantity("" + adapter.getListData().size());
                if (listData.get(p).getSkuQuantity().equalsIgnoreCase("0")) {
                    listData.remove(p);
                }
                notifyItemRemoved(p);
                notifyItemRangeChanged(p, listData.size());
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void updateData(List<SkuOpeningStockList> viewModels) {
        listData.clear();
        listData.addAll(viewModels);
        notifyDataSetChanged();
    }

    public void updateList(List<SkuOpeningStockList> newlist) {

        try {
            listData.clear();
        } catch (Exception e) {
        }
        listData.addAll(newlist);
        notifyDataSetChanged();
    }
}
