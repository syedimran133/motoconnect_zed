package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 2/20/18.
 */

public class PreviousTargetList {

    @SerializedName("TargetList")
    @Expose
    private List<PreviousTargetItems> targetList;

    @SerializedName("PageCount")
    @Expose
    private int pageCount;

    public List<PreviousTargetItems> getTargetList() {
        return targetList;
    }

    public void setTargetList(List<PreviousTargetItems> targetList) {
        this.targetList = targetList;
    }

    public int getPageCount() {
        return pageCount;
    }

    public void setPageCount(int pageCount) {
        this.pageCount = pageCount;
    }
}
