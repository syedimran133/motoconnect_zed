package sales.zed.com.zedsales.constants;

/**
 * Created by Mukesh on 23-12-2016.
 */

public class AppConstants {

    public static boolean isoffline=false;
    public interface IPrefConstant {

        String PREF_TODAY_DATA = "todayData";
        String PREF_INITIATOR = "initiator";
        String PREF_PIN = "pin";
        String PREF_NEW_UPDATE = "newUpdate";
        String PREF_IS_TO_REMEMBER = "isToRemember";
        String PREF_IS_DATA_SYNC = "isDataSync";
        String PREF_ACTIVITY_EXECUTED = "activityExecuted";
        String PREF_ACCESS_TOKEN = "AccessToken";
        String PREF_DEVICE_IMEI = "deviceIMEI";
        String PREF_CURRENT_DATE = "currentDate";
        String PREF_LOGS_DELETE_TIME = "logs_time";
        String PREF_BILLER_LIST_TIME = "biller_list_time";
        String PREF_TOKEN = "token";
        String PREF_PUSH_TOKEN = "push_token";
        String PREF_PUSH_STR= "push_str";
        String PREF_RET_NAME = "ret_name";
        String PREF_LAST_LOGIN = "last_login";
        String STOCK_FLAG = "stock";
        String USER_ID= "user_id";
        String ENTITY_ID= "Entity_id";
        String HIERARCHY= "hierarchy";
        String PROFILE= "profile";
        String PREF_DEVICE_ID = "device_id";
        String PREF_PHONE_HEIGHT = "phone_height";
        String PREF_PHONE_WIDTH = "phone_width";
        String PREF_NAV_HEIGHT = "nav_height";
        String PREF_TOP_HEIGHT = "top_height";
        String PREF_SWIPE_HEIGHT = "swipe_height";
        String PREF_HEIGHT = "phone_height";
        String PREF_WIDTH = "phone_width";
        String PREF_IMAGE_HEIGHT = "image_height";
        String PREF_WIDTH_WIDTH = "image_width";
        String PREF_VIEW_HEIGHT = "view_height";
        String reporttype[]={"Graph","Data","Export Detail On Mail"};
    }

    public interface IUtils {
        String FOLDER_NAME = "ScoverApp";
    }

    public interface IAppPref {
        int REQUEST_TIME_OUT = 120;
        int REQUEST_TIME_OUT2 = 30;
    }


    public interface IAppUrls {
        //        String URL_PHOTO_PATH = "https://s3.us-east-2.amazonaws.com/scover-app/uploads/";
        String URL_PHOTO_PATH = "http://scover-app.us-east-2.elasticbeanstalk.com/";
//        https://dynamicsinplay.com/scover.today/places-listing.html
//        https://dynamicsinplay.com/scover.today/places-detail.html
//        https://dynamicsinplay.com/scover.today/login-register.html
//        https://dynamicsinplay.com/scover.today/holidays-page.html
//        https://dynamicsinplay.com/scover.today/forgot-password.html
//        https://dynamicsinplay.com/scover.today/contact-us.html

        String URL_HELP = "https://dynamicsinplay.com/scover.today/contact-us.html";
        String URL_REPORT = "https://dynamicsinplay.com/scover.today/scover-support.html";
        String URL_PRIVACY = "https://dynamicsinplay.com/scover.today/scover-today.html";
        String URL_TERMS = "https://dynamicsinplay.com/scover.today/holidays-page.html";
        String URL_ABOUT = "https://dynamicsinplay.com/scover.today/scover-us.html";
        String URL_INSPIRATION = "https://dynamicsinplay.com/scover.today/places-listing.html";
//        String URL_HELP = "http://scover.today/help";
//        String URL_REPORT = "http://scover.today/report-problem";
//        String URL_PRIVACY = "http://scover.today/privacy-policy";
//        String URL_TERMS = "http://scover.today/terms-of-use";
//        String URL_ABOUT = "http://scover.today/about";
//        String URL_INSPIRATION = "http://scover.today/inspiration";
    }


    public interface IRequestResponseUtils {
        String PREPAID = "Prepaid";
        String DTH = "DTH";
        String MOBILE = "Mobile";
        String LANDLINE = "Landline";
        int user_channel = 8;
        String STATIC_TARGET = "aggregator_trio";
        String STATUS = "status";
        String RESULT = "result";
        String RESULT_CODE = "resultCode";
        String RESULT_CODE_NS2 = "ns2:resultCode";
        String RESULT_CODE_NS3 = "ns3:resultCode";
        String RESULT_CODE_NS4 = "ns4:resultCode";
        String RESULT_NAMESPACE = "result_namespace";
        String RESULT_NAMESPACE2 = "resultNameSpace";
        String RESULT_NAMESPACE_NS2 = "ns2:resultNameSpace";
        String RESULT_NAMESPACE_NS3 = "ns3:resultNameSpace";
        String RESULT_NAMESPACE_NS4 = "ns4:resultNameSpace";
        String TRANS_ID = "transid";
        String TRANS_ID2 = "transId";
        String TRANS_ID_NS2 = "ns2:transId";
        String TRANS_ID_NS3 = "ns3:transId";
        String TRANS_ID_NS4 = "ns4:transId";
        String FAULT_STRING = "faultstring";
        String SESSION_ID = "sessionid";
        String PROD_VERSION = "prodversion";
        String PRODUCT_LIST = "productlist";
        String BILL_DETAILS = "billdetails";
        String AGG_RESULT_CODE = "agg_resultcode";
        String SBI_FRANCHISE_PORTAL = "SbiFranchisePortal";
        String DESCENDANTS = "Descendants";
        String TRANSACTION_SUMMARY = "TransactionSummary";
        String T_USER_DETAILS = "userDetails";
        String REQ_LOGIN = "login";
        String R_CASH_IN = "cashin";
        String R_CASH_OUT = "cashout";
        String R_RECHARGE = "recharge";
        String R_REC_OTHER = "recother";
        String R_BILL_VERIFY = "billverify";
        String R_VERIFY_OTHER = "verifyother";
        String R_BILL_PAY = "billpay";
        String R_PAY_OTHER = "payother";
        String R_STOCK_RETURN = "stockreturn";
        String R_W_TO_W = "wtow";
        String R_W_TO_IFSC = "wtoifsc";
        String R_W_TO_MMID = "wtommid";
        String R_W_TO_SBI = "wtosbi";
        String R_CB = "cb";
        String R_ACTIVE_PRODUCT = "activeproduct";
        String R_PROD_V = "prodv";
        String R_CHANGE_PIN = "changepin";
        String R_TRANS_DETAIL = "transdetail";
    }

    public interface IRequestType {
        int SIGN_IN = 1;
        int SIGN_UP = 2;
        int GET_NEW_SESSION = 3;
        int RECHARGE = 4;
        int BILL_PAY = 5;
        int VEREFY_BILL_PAY = 6;
        int LOGIN = 7;
        int REPORT_LIST = 8;
        int USER_DETAILS = 9;
        int CHECK_BAL = 10;
        int INIT_EKYC = 11;
        int CONFIRM_EKYC = 12;
        int CASH_IN = 13;
        int CASH_OUT = 14;
        int RETURN_STOCK = 15;
        int W_TO_W = 16;
        int W_TO_IMPS = 17;
        int W_TO_IFSC = 18;
        int W_TO_SBI = 19;
        int CHANGE_PIN = 20;
        int FULL_KYC = 21;
    }

    public interface IDialogMessages {
        String M_SIGN_IN = "Signing IN...";
        String M_SIGN_UP = "Signing UP...";
        String M_GET_NEW_SESSION = "Updating Session...";
        String M_RECHARGE = "Recharge Progress...";
        String M_BILL_PAY = "BillPay Progress...";
        String M_VEREFY_BILL_PAY = "Verifying Bill...";
        String M_LOGIN = "Credential authentication in progress...";
        String M_REPORT_LIST = "MiniStatement in progress...";
        String UPDATE_UTILS = "Updating Data...";
        String M_CHECK_BAL = "Checking Balance...";
        String LOADING = "Loading...";
        String M_INIT_EKYC = "Initializing E-KYC...";
        String M_CONFIRM_EKYC = "Confirming E-KYC...";
        String M_CASH_IN = "CashIn in progress...";
        String M_CASH_OUT = "CashOut in progress...";
        String M_RETURN_STOCK = "Return Stock in progress...";
        String M_W_TO_W = "Wallet to Wallet in progress...";
        String M_W_TO_IMPS = "Wallet to IMPS in progress...";
        String M_W_TO_IFSC = "Wallet to IFSC in progress...";
        String M_W_TO_SBI = "Wallet to SBI in progress...";
        String M_CHANGE_PIN = "Change Pin in progress...";
        String M_FULL_KYC = "FullKYC in progress...";
    }

    public interface IMessages {
        String SUCCESS = "Success";
        String FAIL = "Fail";
        String NO_INTERNET = "No Internet Connection";
        String CANNOT_CHECKIN = "You can't check-in in this place.";
        String TO_FAR_FROM_LOC = "You are too far from this place.";
        String CHECK_IN_FOR_PIC = "You have to check-in to upload photo";
        String CHECK_IN_AFTER_12 = "You had already check-in here";
        String ALREADY_LIKE = "Location Already Liked";
        String ALREADY_DIS_LIKE = "Location Already Disliked";
        String ISSUE_WITH_BILL_AMOUNT = "Issue with Bill Amount";
        String MIN_BILL_AMT = "Minimum payable bill amount should be Rs ";
        String SERVER_GOT_ERROR = "Server error please try again";
        String SERVER_TIMEOUT = "Server Time Out Error";
        String VALID_4_DIGIT = "Please enter valid 4 digit Retailer pin";
        String VALID_10_DIGIT = "Please enter valid 10 digit Retailer Mobile Number";
        String VALID_NUMBER = "Please enter valid Number";
        String ENTER_VALID = "Please enter valid ";
        String VALID_6_DIGIT = "Please enter valid 6 digit OTP";
        String PIN_NOT_MATCHED = "New and Confirm Pin not matched";
        String VALID_AMOUNT = "Please enter valid Amount";
        String VALID_MMID = "Please enter valid MMID";
        String VALID_ACCESS = "Please enter valid Access Token";
        String VALID_EMAIL = "Please enter valid email";
        String VALID_PWD = "Please enter valid password";
        String VALID_NEW_PWD = "Please enter new password";
        String VALID_CONFIRM_PWD = "Please enter confirm password";
        String VALID_NAME = "Please enter name";
        String PASSWORD_NOT_MATCHED = "New and Confirm password didn't matched";
        String VALID_11_DIGIT = "Please enter valid 11 digit IFSC code";
        String VALID_SBI_AC = "Please enter valid 11 or 17 digits SBI Account number";
    }
}
