package sales.zed.com.zedsales.adapter;


import android.app.Activity;
import android.content.Context;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseAdapter;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.List;

import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.models.MenuList;


public class DrawerListAdapter extends BaseAdapter {
    Activity activity;
    private static LayoutInflater inflater = null;
    List<MenuList> titles;
    private RVAdapterMenu.IonItemClickListener listener;

    public DrawerListAdapter(Activity activity, List<MenuList> titles) {
        this.titles = titles;
        this.activity = activity;
        inflater = (LayoutInflater) activity.getSystemService(Context.LAYOUT_INFLATER_SERVICE);
    }

    public void updatelist(List<MenuList> titles) {
        this.titles = titles;
        notifyDataSetChanged();

    }

    public void registerItemClickLisener(RVAdapterMenu.IonItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public int getCount() {
        return titles.size();
    }

    @Override
    public Object getItem(int position) {
        return position;
    }

    @Override
    public long getItemId(int position) {
        return position;
    }

    public class Holder {
        TextView tv_title;
        ImageView im_icon;
    }

    @Override
    public View getView(final int position, View convertView, ViewGroup parent) {
        Holder holder = new Holder();
        View view;
        view = inflater.inflate(R.layout.layout_drawer_item, null);
        holder.tv_title = (TextView) view.findViewById(R.id.tv_title);
        holder.im_icon = (ImageView) view.findViewById(R.id.im_icon);
        holder.tv_title.setText(titles.get(position).getTitle());
        holder.im_icon.setImageResource(titles.get(position).getImage());
        view.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View view) {
                if (listener != null)
                    listener.onItemClick(titles.get(position).getMenuid() - 1);
            }
        });
        return view;
    }

}