package sales.zed.com.zedsales.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.databinding.FragmentMyreturnsBinding;
import sales.zed.com.zedsales.support.AppSingle;

/**
 * Created by Mukesh on 09/12/17.
 */

public class FragmentMyReturns extends BaseFragment {

    private FragmentMyreturnsBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_myreturns, container, false);
        AppSingle.getInstance().getActivity().setTitle("My Returns", true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("My Profile"));
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_text_view_submit:
                    callAsync();
                    break;
            }
        }
    };

    private void callAsync() {
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }
}
