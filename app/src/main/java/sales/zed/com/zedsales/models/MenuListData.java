package sales.zed.com.zedsales.models;

/**
 * Created by root on 09/12/17.
 */
import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class MenuListData {

    @PrimaryKey
    @SerializedName("MenuId")
    @Expose
    private int menuId;

    @SerializedName("MenuName")
    @Expose
    private String menuName;

    @SerializedName("DisplayOrder")
    @Expose
    private String displayOrder;

    @SerializedName("ShowInMenuBar")
    @Expose
    private String showInMenuBar;

    public int getMenuId() {
        return menuId;
    }

    public void setMenuId(int menuId) {
        this.menuId = menuId;
    }

    public String getMenuName() {
        return menuName;
    }

    public void setMenuName(String menuName) {
        this.menuName = menuName;
    }

    public String getDisplayOrder() {
        return displayOrder;
    }

    public void setDisplayOrder(String displayOrder) {
        this.displayOrder = displayOrder;
    }

    public String getShowInMenuBar() {
        return showInMenuBar;
    }

    public void setShowInMenuBar(String showInMenuBar) {
        this.showInMenuBar = showInMenuBar;
    }

    @Override
    public String toString() {
        return
                "MenuLists{" +
                        "MenuId = '" + menuId + '\'' +
                        ",MenuName = '" + menuName + '\'' +
                        ",DisplayOrder = '" + displayOrder + '\'' +
                        ",ShowInMenuBar = '" + showInMenuBar + '\'' +
                        "}";
    }


}