package sales.zed.com.zedsales.models;

import android.arch.persistence.room.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 1/24/18.
 */
@Entity
public class EditableProfileData {

    @SerializedName("CompanyName")
    @Expose
    private String CompanyName;

    @SerializedName("UserCode")
    @Expose
    private String UserCode;

    @SerializedName("UserName")
    @Expose
    private String UserName;

    @SerializedName("RegisteredMobileNo")
    @Expose
    private String RegisteredMobileNo;

    @SerializedName("AlternateMobileNo")
    @Expose
    private String AlternateMobileNo;

    @SerializedName("EmailId")
    @Expose
    private String EmailId;

    @SerializedName("DateOfBirth")
    @Expose
    private String DateOfBirth;

    @SerializedName("Married")
    @Expose
    private String Married;

    @SerializedName("DateOfAnniversary")
    @Expose
    private String DateOfAnniversary;

    @SerializedName("Address")
    @Expose
    private String Address;

    @SerializedName("City")
    @Expose
    private String City;

    @SerializedName("State")
    @Expose
    private String State;

    @SerializedName("PinCode")
    @Expose
    private String PinCode;

    @SerializedName("CounterPotentialValue")
    @Expose
    private String CounterPotentialValue;

    @SerializedName("CounterPotentialVolume")
    @Expose
    private String CounterPotentialVolume;

    @SerializedName("AccountHolderName")
    @Expose
    private String AccountHolderName;

    @SerializedName("AccountNumber")
    @Expose
    private String AccountNumber;

    @SerializedName("BankName")
    @Expose
    private String BankName;

    @SerializedName("BranchName")
    @Expose
    private String BranchName;

    @SerializedName("BranchLocation")
    @Expose
    private String BranchLocation;

    @SerializedName("IfscCode")
    @Expose
    private String IfscCode;

    @SerializedName("AadharNumber")
    @Expose
    private String AadharNumber;

    @SerializedName("PanNumber")
    @Expose
    private String PanNumber;

    @SerializedName("GstinNo")
    @Expose
    private String GstinNo;

    public String getCompanyName() {
        return CompanyName;
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getUserCode() {
        return UserCode;
    }

    public void setUserCode(String userCode) {
        UserCode = userCode;
    }

    public String getUserName() {
        return UserName;
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getRegisteredMobileNo() {
        return RegisteredMobileNo;
    }

    public void setRegisteredMobileNo(String registeredMobileNo) {
        RegisteredMobileNo = registeredMobileNo;
    }

    public String getAlternateMobileNo() {
        return AlternateMobileNo;
    }

    public void setAlternateMobileNo(String alternateMobileNo) {
        AlternateMobileNo = alternateMobileNo;
    }

    public String getEmailId() {
        return EmailId;
    }

    public void setEmailId(String emailId) {
        EmailId = emailId;
    }

    public String getDateOfBirth() {
        return DateOfBirth;
    }

    public void setDateOfBirth(String dateOfBirth) {
        DateOfBirth = dateOfBirth;
    }

    public String getMarried() {
        return Married;
    }

    public void setMarried(String married) {
        Married = married;
    }

    public String getDateOfAnniversary() {
        return DateOfAnniversary;
    }

    public void setDateOfAnniversary(String dateOfAnniversary) {
        DateOfAnniversary = dateOfAnniversary;
    }

    public String getAddress() {
        return Address;
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCity() {
        return City;
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {
        return State;
    }

    public void setState(String state) {
        State = state;
    }

    public String getPinCode() {
        return PinCode;
    }

    public void setPinCode(String pinCode) {
        PinCode = pinCode;
    }

    public String getCounterPotentialValue() {
        return CounterPotentialValue;
    }

    public void setCounterPotentialValue(String counterPotentialValue) {
        CounterPotentialValue = counterPotentialValue;
    }

    public String getCounterPotentialVolume() {
        return CounterPotentialVolume;
    }

    public void setCounterPotentialVolume(String counterPotentialVolume) {
        CounterPotentialVolume = counterPotentialVolume;
    }

    public String getAccountHolderName() {
        return AccountHolderName;
    }

    public void setAccountHolderName(String accountHolderName) {
        AccountHolderName = accountHolderName;
    }

    public String getAccountNumber() {
        return AccountNumber;
    }

    public void setAccountNumber(String accountNumber) {
        AccountNumber = accountNumber;
    }

    public String getBankName() {
        return BankName;
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getBranchName() {
        return BranchName;
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }

    public String getBranchLocation() {
        return BranchLocation;
    }

    public void setBranchLocation(String branchLocation) {
        BranchLocation = branchLocation;
    }

    public String getIfscCode() {
        return IfscCode;
    }

    public void setIfscCode(String ifscCode) {
        IfscCode = ifscCode;
    }

    public String getAadharNumber() {
        return AadharNumber;
    }

    public void setAadharNumber(String aadharNumber) {
        AadharNumber = aadharNumber;
    }

    public String getPanNumber() {
        return PanNumber;
    }

    public void setPanNumber(String panNumber) {
        PanNumber = panNumber;
    }

    public String getGstinNo() {
        return GstinNo;
    }

    public void setGstinNo(String gstinNo) {
        GstinNo = gstinNo;
    }
}
