package sales.zed.com.zedsales.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.databinding.ReportsMySaleListBinding;
import sales.zed.com.zedsales.models.SaleReportModel;

/**
 * Created by root on 2/14/18.
 */

public class SaleReportAdapter extends RecyclerView.Adapter<SaleReportAdapter.ViewHolder> {

    private List<SaleReportModel> listData;
    private LayoutInflater mInflater;

    public SaleReportAdapter(Context context, List<SaleReportModel> listData) {
        this.listData = listData;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        SaleReportModel reportData=listData.get(position);

        holder.binding.listTxtModel.setText(reportData.getModel());
        holder.binding.listTxtQyt.setText(reportData.getQuantity());
    }

    @Override
    public int getItemCount() {
        if (listData == null)
            return 0;
        return listData.size();
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.reports_my_sale_list, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        ReportsMySaleListBinding binding;
        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
        }
    }
}
