package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by root on 2/16/18.
 */

public class CurrentStockSkuList {

    @SerializedName("CurrentStockSkuList")
    @Expose
    private List<CurrentStockSkuItem> currentStockSkuList;

    @SerializedName("PageCount")
    @Expose
    private String PageCount;

    public List<CurrentStockSkuItem> getCurrentStockSkuList() {
        return currentStockSkuList;
    }

    public void setCurrentStockSkuList(List<CurrentStockSkuItem> currentStockSkuList) {
        this.currentStockSkuList = currentStockSkuList;
    }

    public String getPageCount() {
        return PageCount;
    }

    public void setPageCount(String pageCount) {
        PageCount = pageCount;
    }
}
