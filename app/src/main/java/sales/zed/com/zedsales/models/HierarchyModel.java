package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;
import java.util.List;

/**
 * Created by root on 2/19/18.
 */

public class HierarchyModel {

    @SerializedName("DSE")
    @Expose
    private List<HierarchyItems> DSE;

    @SerializedName("Distributor")
    @Expose
    private List<HierarchyItems> Distributor;

    @SerializedName("TertiarySaleManager")
    @Expose
    private List<HierarchyItems> TertiarySaleManager;

    @SerializedName("AreaSalesManager")
    @Expose
    private List<HierarchyItems> AreaSalesManager;

    @SerializedName("RegionalSalesManager")
    @Expose
    private List<HierarchyItems> RegionalSalesManager;

    @SerializedName("HeadOffice")
    @Expose
    private List<HierarchyItems> HeadOffice;

    public List<HierarchyItems> getDSE() {
        return DSE;
    }

    public void setDSE(List<HierarchyItems> DSE) {
        this.DSE = DSE;
    }

    public List<HierarchyItems> getDistributor() {
        return Distributor;
    }

    public void setDistributor(List<HierarchyItems> distributor) {
        Distributor = distributor;
    }

    public List<HierarchyItems> getTertiarySaleManager() {
        return TertiarySaleManager;
    }

    public void setTertiarySaleManager(List<HierarchyItems> tertiarySaleManager) {
        TertiarySaleManager = tertiarySaleManager;
    }

    public List<HierarchyItems> getAreaSalesManager() {
        return AreaSalesManager;
    }

    public void setAreaSalesManager(List<HierarchyItems> areaSalesManager) {
        AreaSalesManager = areaSalesManager;
    }

    public List<HierarchyItems> getRegionalSalesManager() {
        return RegionalSalesManager;
    }

    public void setRegionalSalesManager(List<HierarchyItems> regionalSalesManager) {
        RegionalSalesManager = regionalSalesManager;
    }

    public List<HierarchyItems> getHeadOffice() {
        return HeadOffice;
    }

    public void setHeadOffice(List<HierarchyItems> headOffice) {
        HeadOffice = headOffice;
    }
}
