package sales.zed.com.zedsales.adapter;

import android.content.Context;
import android.graphics.Color;
import android.support.v7.widget.CardView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;

import java.util.List;

import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.custom.CustomTextView;
import sales.zed.com.zedsales.models.GRNParent;

/**
 * Created by root on 3/8/18.
 */

public class GRNEepdlistview extends BaseExpandableListAdapter{

    private Context context;
    private List<GRNParent> listData;


    public GRNEepdlistview(Context context, List<GRNParent> listData) {
        this.context = context;
        this.listData = listData;
    }

    @Override
    public List<String> getChild(int groupPosition, int childPosition) {
        return listData.get(groupPosition).getChildObjectList();
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        final String expandedListText = listData.get(groupPosition).getChildObjectList().get(childPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.adapter_grn_detail_list_c, null);
        }
        TextView expandedListTextView = (TextView) convertView
                .findViewById(R.id.txt_view_data);
        expandedListTextView.setText(expandedListText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return listData.get(groupPosition).getChildObjectList().size();
    }

    @Override
    public GRNParent getGroup(int groupPosition) {
        return listData.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        GRNParent grnParent = listData.get(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.layout_grn_expd, null);
        }
        CustomTextView tvSkuName=(CustomTextView)convertView.findViewById(R.id.tv_sku_name);
        CustomTextView tvPrice=(CustomTextView)convertView.findViewById(R.id.tv_price);
        CustomTextView tvQty=(CustomTextView)convertView.findViewById(R.id.tv_qty2);
        RelativeLayout linearLayout=(RelativeLayout) convertView.findViewById(R.id.linear_head);
        tvSkuName.setText(grnParent.getName());
        tvPrice.setText(grnParent.getPrice());
        tvQty.setText(grnParent.getQty());
        if(isExpanded){
            linearLayout.setBackgroundColor(Color.parseColor("#2D9CC9"));
            tvSkuName.setTextColor(Color.parseColor("#FFFFFF"));
            tvPrice.setTextColor(Color.parseColor("#FFFFFF"));
            tvQty.setTextColor(Color.parseColor("#FFFFFF"));
        }else{
            linearLayout.setBackgroundColor(Color.parseColor("#ffffff"));
            tvSkuName.setTextColor(Color.parseColor("#000000"));
            tvPrice.setTextColor(Color.parseColor("#000000"));
            tvQty.setTextColor(Color.parseColor("#000000"));
        }
        return convertView;
    }

    @Override
    public int getGroupCount() {
        return listData.size();
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
