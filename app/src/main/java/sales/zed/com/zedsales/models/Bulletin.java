
package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class Bulletin {

    @SerializedName("Subject")
    @Expose
    private String subject;
    @SerializedName("PublishDate")
    @Expose
    private String publishDate;
    @SerializedName("ExpireDate")
    @Expose
    private String expireDate;
    @SerializedName("Description")
    @Expose
    private String description;
    @SerializedName("Link1")
    @Expose
    private String link1;
    @SerializedName("Link2")
    @Expose
    private String link2;

    public String getSubject() {
        return subject;
    }

    public void setSubject(String subject) {
        this.subject = subject;
    }

    public String getPublishDate() {
        return publishDate;
    }

    public void setPublishDate(String publishDate) {
        this.publishDate = publishDate;
    }

    public String getExpireDate() {
        return expireDate;
    }

    public void setExpireDate(String expireDate) {
        this.expireDate = expireDate;
    }

    public String getDescription() {
        return description;
    }

    public void setDescription(String description) {
        this.description = description;
    }

    public String getLink1() {
        return link1;
    }

    public void setLink1(String link1) {
        this.link1 = link1;
    }

    public String getLink2() {
        return link2;
    }

    public void setLink2(String link2) {
        this.link2 = link2;
    }

}
