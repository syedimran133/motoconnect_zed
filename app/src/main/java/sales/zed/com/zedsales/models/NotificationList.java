package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

public class NotificationList {

    @SerializedName("NotificationData")
    @Expose
    private List<NotificationData> notificationData = null;

    public List<NotificationData> getNotificationData() {
        return notificationData;
    }

    public void setNotificationData(List<NotificationData> notificationData) {
        this.notificationData = notificationData;
    }
}
