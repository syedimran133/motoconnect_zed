package sales.zed.com.zedsales.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.OrientationHelper;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.TextView;

import java.util.List;

import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.databinding.AdapterGrnDetailListPBinding;
import sales.zed.com.zedsales.interfaces.MyAdapterListener;
import sales.zed.com.zedsales.models.GRNParent;
import sales.zed.com.zedsales.support.AppSingle;

/**
 * Created by root on 09/12/17.
 */

public class RVAdapterGRNViewDetails extends RecyclerView.Adapter<RVAdapterGRNViewDetails.ViewHolder> {
    private List<GRNParent> listData;
    private LayoutInflater mInflater;
    private Context context;
    private MyAdapterListener click_listener;
    int count = 0;

    public RVAdapterGRNViewDetails(Context context, List<GRNParent> listData) {
        this.context = context;
        this.listData = listData;
        mInflater = LayoutInflater.from(context);
    }

    public void registerClickListener(MyAdapterListener listener) {
        this.click_listener = listener;
    }


    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.adapter_grn_detail_list_p, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, final int position) {
        final GRNParent data = listData.get(position);
        holder.binding.tvSkuName.setText(data.getName());
        holder.binding.tvPrice.setText(data.getPrice());
        holder.binding.tvQty.setText(data.getQty());
        try {
            count = Integer.parseInt(data.getQty());
        } catch (Exception e) {

        }
        /*holder.binding.grnDecrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (count > 1) {
                    count--;
                    holder.binding.tvQty.setText(String.valueOf(count));
                    listData.get(position).setQty("" + count);
                }
            }
        });

        holder.binding.grnIncrease.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                count++;
                holder.binding.tvQty.setText(String.valueOf(count));
                listData.get(position).setQty("" + count);
            }
        });*/
        if (false /*saleSkuListItem.getSerialisedMode() == 3*/) {
            holder.binding.llGrnPlusminus.setVisibility(View.GONE);
            holder.binding.llSerlayout.setVisibility(View.VISIBLE);
            holder.binding.tvQty2.setText(data.getQty());
            holder.binding.tvQty2.setTextColor(Color.parseColor("#000000"));
        } else {
            holder.binding.llGrnPlusminus.setVisibility(View.VISIBLE);
            holder.binding.llSerlayout.setVisibility(View.GONE);
            // holder.serlayout.setVisibility(View.INVISIBLE);
        }
        if (data.getChildObjectList() != null && data.getChildObjectList().size() > 0) {
            RVAdapterGRNViewDetailsSubList adapter2 = new RVAdapterGRNViewDetailsSubList(AppSingle.getInstance().getActivity(), data.getChildObjectList());
            LinearLayoutManager linearLayoutManager2 = new LinearLayoutManager(AppSingle.getInstance().getActivity(), OrientationHelper.HORIZONTAL, true);
            holder.binding.rvSubList.setLayoutManager(linearLayoutManager2);
            holder.binding.rvSubList.setAdapter(adapter2);
        }
        if (data.getChildObjectList().size() != 0) {
            holder.binding.txtSerialRv.setVisibility(View.VISIBLE);
            if (data.isIsselected()) {
                holder.binding.linearList.setVisibility(View.VISIBLE);
                holder.binding.ivDrop.setRotation(90);
                holder.binding.tvSkuName.setTextColor(Color.parseColor("#ffffff"));
                holder.binding.tvPrice.setTextColor(Color.parseColor("#ffffff"));
                holder.binding.tvQty.setTextColor(Color.parseColor("#ffffff"));
                holder.binding.linearHead.setBackgroundColor(AppSingle.getInstance().getActivity().getResources().getColor(R.color.bg_blue, null));
            } else {
                holder.binding.linearList.setVisibility(View.GONE);
                holder.binding.ivDrop.setRotation(-90);
                holder.binding.tvSkuName.setTextColor(Color.parseColor("#000000"));
                holder.binding.tvPrice.setTextColor(Color.parseColor("#000000"));
                holder.binding.tvQty.setTextColor(Color.parseColor("#000000"));
                holder.binding.linearHead.setBackgroundColor(AppSingle.getInstance().getActivity().getResources().getColor(R.color.bg_white, null));
            }
        } else {
            holder.binding.txtSerialRv.setVisibility(View.GONE);
            holder.binding.linearList.setVisibility(View.GONE);
        }
    }

    @Override
    public int getItemCount() {
        if (listData == null)
            return 0;
        return listData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        AdapterGrnDetailListPBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (click_listener != null)
                        click_listener.iconButtonViewOnClick(v, getAdapterPosition());
                }
            });
        }
    }
}