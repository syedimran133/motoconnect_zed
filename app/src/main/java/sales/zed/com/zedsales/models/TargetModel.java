package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 2/14/18.
 */

public class TargetModel {
    @SerializedName("TargetName")
    @Expose
    private String targetName;
    @SerializedName("DaysLeft")
    @Expose
    private String daysLeft;
    @SerializedName("Target")
    @Expose
    private String target;
    @SerializedName("TargetAchieved")
    @Expose
    private String targetAchieved;

    @SerializedName("AchievementPercent")
    @Expose
    private String achievementPercent;

    public String getTargetName() {
        return targetName;
    }

    public void setTargetName(String targetName) {
        this.targetName = targetName;
    }

    public String getDaysLeft() {
        return daysLeft;
    }

    public void setDaysLeft(String daysLeft) {
        this.daysLeft = daysLeft;
    }

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getTargetAchieved() {
        return targetAchieved;
    }

    public void setTargetAchieved(String targetAchieved) {
        this.targetAchieved = targetAchieved;
    }

    public String getAchievementPercent() {
        return achievementPercent;
    }

    public void setAchievementPercent(String achievementPercent) {
        this.achievementPercent = achievementPercent;
    }
}
