package sales.zed.com.zedsales.models;

/**
 * Created by Mukesh on 13/12/17.
 */


import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ItemList {

    @SerializedName("skuId")
    @Expose
    private String skuId;
    @SerializedName("skuName")
    @Expose
    private String skuName;
    @SerializedName("totalQuantity")
    @Expose
    private String totalQuantity;
    @SerializedName("serialList")
    @Expose
    private List<SerialList> serialList = null;

    public String getSkuId() {
        return skuId;
    }

    public void setSkuId(String skuId) {
        this.skuId = skuId;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(String totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public List<SerialList> getSerialList() {
        return serialList;
    }

    public void setSerialList(List<SerialList> serialList) {
        this.serialList = serialList;
    }

}