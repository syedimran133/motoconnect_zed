package sales.zed.com.zedsales.fragment;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.databinding.FragmentMyreturnsInnerBinding;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FlowOrganizer;

/**
 * Created by Mukesh on 09/12/17.
 */

public class FragmentMyReturnsInner extends BaseFragment {

    private FragmentMyreturnsInnerBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_myreturns_inner, container, false);
        AppSingle.getInstance().getActivity().setTitle("My Returns", true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("My Returns"));
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.txtMainMyReturns.setOnClickListener(listener);
        binding.txtMainPreviousReturns.setOnClickListener(listener);
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_text_view_submit:
                    callAsync();
                    break;

                case R.id.txt_main_my_returns:
                    binding.txtMainPreviousReturns.setBackgroundColor(Color.parseColor("#A39F9E"));
                    binding.txtMainMyReturns.setBackgroundColor(Color.parseColor("#00bfff"));
                    FlowOrganizer.getInstance().add(new FragmentMyReturnsInner(), true);

                    break;
                case R.id.txt_main_previous_returns:
                    binding.txtMainPreviousReturns.setBackgroundColor(Color.parseColor("#00bfff"));
                    binding.txtMainMyReturns.setBackgroundColor(Color.parseColor("#A39F9E"));
                    FlowOrganizer.getInstance().add(new FragmentMyReturnsViewPr(), true);

                    break;
            }
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }

    private void callAsync() {
    }
}
