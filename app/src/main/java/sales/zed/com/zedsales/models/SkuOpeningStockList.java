package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 2/8/18.
 */

public class SkuOpeningStockList {

    @SerializedName("SkuCode")
    @Expose
    private String SkuCode;

    @SerializedName("SkuName")
    @Expose
    private String SkuName;

    @SerializedName("SkuQuantity")
    @Expose
    private String SkuQuantity;

    @SerializedName("StockBinType")
    @Expose
    private String StockBinType;

    private boolean serail;

    @SerializedName("SerialOpeningStockList")
    @Expose
    private List<SaleSerialListItems> SerialOpeningStockList;

    public String getSkuCode() {
        return SkuCode;
    }

    public void setSkuCode(String skuCode) {
        SkuCode = skuCode;
    }

    public String getSkuName() {
        return SkuName;
    }

    public void setSkuName(String skuName) {
        SkuName = skuName;
    }

    public String getSkuQuantity() {
        return SkuQuantity;
    }

    public void setSkuQuantity(String skuQuantity) {
        SkuQuantity = skuQuantity;
    }

    public String getStockBinType() {
        return StockBinType;
    }

    public void setStockBinType(String stockBinType) {
        StockBinType = stockBinType;
    }

    public List<SaleSerialListItems> getSerialOpeningStockList() {
        return SerialOpeningStockList;
    }

    public void setSerialOpeningStockList(List<SaleSerialListItems> serialOpeningStockList) {
        SerialOpeningStockList = serialOpeningStockList;
    }

    public boolean isSerail() {
        return serail;
    }

    public void setSerail(boolean serail) {
        this.serail = serail;
    }

}
