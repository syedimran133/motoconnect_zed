package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 2/19/18.
 */

public class HierarchyItems {

    @SerializedName("Name")
    @Expose
    private String Name;

    @SerializedName("Email")
    @Expose
    private String Email;

    @SerializedName("MobileNo")
    @Expose
    private String MobileNo;

    public String getName() {
        return Name;
    }

    public void setName(String name) {
        Name = name;
    }

    public String getEmail() {
        return Email;
    }

    public void setEmail(String email) {
        Email = email;
    }

    public String getMobileNo() {
        return MobileNo;
    }

    public void setMobileNo(String mobileNo) {
        MobileNo = mobileNo;
    }
}
