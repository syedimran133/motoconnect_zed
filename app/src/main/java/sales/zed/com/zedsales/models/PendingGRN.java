package sales.zed.com.zedsales.models;

import android.arch.persistence.room.Entity;

/**
 * Created by root on 1/23/18.
 */
@Entity
public class PendingGRN {

    private String InvoiceNumber;
    private String InvoiceDate;
    private String From;
    private String TotalQuantity;
    private String InvoiceId;

    public String getInvoiceId() {
        return InvoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        InvoiceId = invoiceId;
    }

    public String getInvoiceNumber() {
        return InvoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        InvoiceNumber = invoiceNumber;
    }

    public String getInvoiceDate() {
        return InvoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        InvoiceDate = invoiceDate;
    }

    public String getFrom() {
        return From;
    }

    public void setFrom(String from) {
        From = from;
    }

    public String getTotalQuantity() {
        return TotalQuantity;
    }

    public void setTotalQuantity(String totalQuantity) {
        TotalQuantity = totalQuantity;
    }

    @Override
    public String toString(){
        return
                "PendingGrnList{" +
                        "InvoiceId = '" + InvoiceId + '\'' +
                        ",InvoiceNumber = '" + InvoiceNumber + '\'' +
                        ",InvoiceDate = '" + InvoiceDate + '\'' +
                        ",From = '" + From + '\'' +
                        ",TotalQuantity = '" + TotalQuantity + '\'' +
                        "}";
    }
}
