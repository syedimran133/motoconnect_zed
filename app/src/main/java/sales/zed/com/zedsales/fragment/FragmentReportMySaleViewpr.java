package sales.zed.com.zedsales.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.adapter.AdapterDialogListItem;
import sales.zed.com.zedsales.adapter.SaleReportAdapter;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.apiz.RequestFormater;
import sales.zed.com.zedsales.constants.AppConstants;
import sales.zed.com.zedsales.databinding.DialogListBinding;
import sales.zed.com.zedsales.databinding.FragmentMysalePreviousBinding;
import sales.zed.com.zedsales.interfaces.IDialogListClickListener;
import sales.zed.com.zedsales.models.BrandListsItem;
import sales.zed.com.zedsales.models.ForgetModel;
import sales.zed.com.zedsales.models.ModelListsItem;
import sales.zed.com.zedsales.models.ReportTypeItem;
import sales.zed.com.zedsales.models.SaleReportList;
import sales.zed.com.zedsales.room.MyDatabase;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FlowOrganizer;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by Mukesh on 09/12/17.
 */

public class FragmentReportMySaleViewpr extends BaseFragment {

    private FragmentMysalePreviousBinding binding;
    private final int INT_FROM = 0, INT_TO = 1;
    private int Date_day, Date_month, Date_year, selectedDateOption;
    private List<BrandListsItem> fetchBrandList;
    private List<ModelListsItem> fetchMasterList;
    private List<ReportTypeItem> fetchReportTypeList;
    private List<String> listBrand, listBrandid, listModel, listModelid, listReportType = null;
    private int selectedBrandPos = -1, selectedModelPos = -1, selectedReportPos = -1;
    private String brandid, productcatid;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_mysale_previous, container, false);
        AppSingle.getInstance().getActivity().setTitle("My Sale", true);
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("My Sale"));
        featchDB();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.llSaleUpper.setVisibility(View.VISIBLE);
        binding.btnProcess.setOnClickListener(listener);
        binding.ivSalereportFrom.setOnClickListener(listener);
        binding.ivSalereportTo.setOnClickListener(listener);
        binding.relativeSalereportBrand.setOnClickListener(listener);
        binding.relativeSalereportModel.setOnClickListener(listener);
        binding.relativeSalereportData.setOnClickListener(listener);
        //binding.btnReportReset.setOnClickListener(listener);
        binding.txtSalereportFromdate.setText(AppSingle.getfirstDate());
        binding.txtSalereportTodate.setText(AppSingle.getCurrentDate());
        binding.btnPrePrevSale.setOnClickListener(listener);
        binding.btnPerTxtSale.setOnClickListener(listener);
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_per_txt_sale:

                    binding.btnPrePrevSale.setBackgroundColor(Color.parseColor("#A39F9E"));
                    binding.btnPerTxtSale.setBackgroundColor(Color.parseColor("#00bfff"));
                    FlowOrganizer.getInstance().add(new FragmentMysaleCurrent(), true);
                    break;
                case R.id.btn_pre_prev_sale:
                    binding.btnPrePrevSale.setBackgroundColor(Color.parseColor("#00bfff"));
                    binding.btnPerTxtSale.setBackgroundColor(Color.parseColor("#A39F9E"));
                    FlowOrganizer.getInstance().add(new FragmentReportMySaleViewpr(), true);
                    break;
                case R.id.btn_process:
                    sendToServer(getReportTypeIndex(binding.txtSalereportData.getText().toString()));
                    break;
                case R.id.iv_salereport_from:
                    showDatePicker(INT_FROM);
                    break;
                case R.id.iv_salereport_to:
                    showDatePicker(INT_TO);
                    break;
                case R.id.relative_salereport_brand:
                    listModel = null;
                    binding.txtSalereportBrand.setText("");
                    binding.txtSalereportBrand.setHint("Brand");
                    binding.txtSalereportBrand.setHintTextColor(Color.parseColor("#C9C9C9"));
                    binding.txtSalereportModel.setText("");
                    binding.txtSalereportModel.setHint("Model");
                    binding.txtSalereportModel.setHintTextColor(Color.parseColor("#C9C9C9"));
                    setBrandList();
                    break;
                case R.id.relative_salereport_model:
                    binding.txtSalereportModel.setText("");
                    binding.txtSalereportModel.setHint("Model");
                    binding.txtSalereportModel.setHintTextColor(Color.parseColor("#C9C9C9"));
                    setModelList();
                    break;
                case R.id.relative_salereport_data:
                    setReportTypeList();
                    break;
            }
        }
    };

    private void setBrandList() {
        if (listBrand == null) {
            listBrand = new ArrayList<>();
            listBrandid = new ArrayList<>();
            for (BrandListsItem data : fetchBrandList) {
                if (data.isStatus()) {
                    listBrand.add(data.getBrandName());
                    listBrandid.add("" + data.getBrandId());
                }

            }
        }
        openListDialog("Select Brand", selectedBrandPos, listBrand, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedBrandPos = selectedPos;
                    binding.txtSalereportBrand.setText(listBrand.get(selectedPos));
                    binding.txtSalereportBrand.setHint(listBrandid.get(selectedPos));
                    binding.txtSalereportBrand.setHintTextColor(Color.parseColor("#FFFFFF"));
                    brandid = "" + listBrandid.get(selectedPos);
                } catch (Exception e) {
                }
            }
        });
    }

    private void setReportTypeList() {

        if (listReportType == null) {
            listReportType = new ArrayList<>();
            for (ReportTypeItem data : fetchReportTypeList) {
                if (data.getStatus().equalsIgnoreCase("1")) {
                    listReportType.add(data.getReportName());
                }

            }
        }
        openListDialog("Select Data", selectedReportPos, listReportType, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedReportPos = selectedPos;
                    binding.txtSalereportData.setText(listReportType.get(selectedPos));
                } catch (Exception e) {
                }
            }
        });

    }

    private void setModelList() {
        if (listModel == null) {
            listModel = new ArrayList<>();
            listModelid = new ArrayList<>();
            for (ModelListsItem data : fetchMasterList) {
                if (data.isStatus()) {
                    if (("" + data.getBrandId()).equalsIgnoreCase(brandid)) {
                        listModel.add(data.getModelName());
                        listModelid.add("" + data.getModelId());
                    }
                }
            }
        }
        openListDialog("Select Model", selectedModelPos, listModel, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedModelPos = selectedPos;
                    binding.txtSalereportModel.setText(listModel.get(selectedPos));
                    binding.txtSalereportModel.setHint(listModelid.get(selectedPos));
                    binding.txtSalereportModel.setHintTextColor(Color.parseColor("#FFFFFF"));

                } catch (Exception e) {
                }
            }
        });

    }

    protected void openListDialog(String title, int selectedPos, final List<String> listData, final IDialogListClickListener listener) {
        DialogListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(AppSingle.getInstance().getActivity().getApplicationContext()), R.layout.dialog_list, null, false);
        final Dialog dialog = new Dialog(AppSingle.getInstance().getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(binding.getRoot());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        binding.txtViewTitle.setText(title);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        LinearLayoutManager lLayout = new LinearLayoutManager(AppSingle.getInstance().getActivity());
        RecyclerView rView = binding.rvList;
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);
        AdapterDialogListItem adapter = new AdapterDialogListItem(listData, selectedPos);
        rView.setAdapter(adapter);
        adapter.registerOnItemClickListener(new AdapterDialogListItem.IonItemSelect() {
            @Override
            public void onItemSelect(int position) {
                if (listener != null)
                    listener.onClick(listData.get(position), position);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void callAsync() {
        String brand = "";
        String model = "";
        String fromdate = binding.txtSalereportFromdate.getText().toString();
        String todate = binding.txtSalereportTodate.getText().toString();
        if (!binding.txtSalereportBrand.getHint().toString().equalsIgnoreCase("Brand")) {
            brand = binding.txtSalereportBrand.getHint().toString();
        }
        if (!binding.txtSalereportModel.getHint().toString().equalsIgnoreCase("Model")) {
            model = binding.txtSalereportModel.getHint().toString();
        }
        String data = binding.txtSalereportData.getText().toString();
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<SaleReportList> call = apiService.getPreviousSale(AppSingle.getInstance().getLoginUser().getUserId(), RequestFormater.getsalereport(brand, model, fromdate, todate, data));
        showProgessBar();
        call.enqueue(new Callback<SaleReportList>() {
            @Override
            public void onResponse(Call<SaleReportList> call, Response<SaleReportList> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null)
                        if (response.body().getPreviousSaleList() != null && response.body().getPreviousSaleList().size() > 0) {
                            binding.llSaleUpper.setVisibility(View.GONE);
                            SaleReportAdapter adapter = new SaleReportAdapter(AppSingle.getInstance().getActivity(), response.body().getPreviousSaleList());
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
                            binding.rvListSalesreport.setLayoutManager(mLayoutManager);
                            binding.rvListSalesreport.setItemAnimator(new DefaultItemAnimator());
                            binding.rvListSalesreport.setAdapter(adapter);
                            //Toast.makeText(AppSingle.getInstance().getActivity(), response.body().getPreviousSaleList().get(0).getModel(), Toast.LENGTH_SHORT).show();
                        } else {
                            showToast("List is empty.");
                        }
                } else {
                    showResponseMessage(response.code());
                    if (response.code() == 401)
                        callAsync();
                }
            }

            @Override
            public void onFailure(Call<SaleReportList> call, Throwable t) {
                hideProgessBar();
                showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    public void sendToServer(int type) {

        switch (type) {
            case 0:
                showToast("Coming soon");
                break;
            case 1:
                callAsync();
                break;
            case 2:
                callAsyncMainTo();
                break;
        }
    }

    public int getReportTypeIndex(String name) {
        int index = 0;
        for (int i = 0; i < AppConstants.IPrefConstant.reporttype.length; i++) {
            if (AppConstants.IPrefConstant.reporttype[i].equalsIgnoreCase(name.trim())) {
                index = i;
            }
        }
        return index;
    }

    private void callAsyncMainTo() {
        String brand = "";
        String model = "";
        String fromdate = binding.txtSalereportFromdate.getText().toString();
        String todate = binding.txtSalereportTodate.getText().toString();
        if (!binding.txtSalereportBrand.getHint().toString().equalsIgnoreCase("Brand")) {
            brand = binding.txtSalereportBrand.getHint().toString();
        }
        if (!binding.txtSalereportModel.getHint().toString().equalsIgnoreCase("Model")) {
            model = binding.txtSalereportModel.getHint().toString();
        }
        String data = binding.txtSalereportData.getText().toString();
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<ForgetModel> call = apiService.getPreviousSaleMailTo(AppSingle.getInstance().getLoginUser().getUserId(), RequestFormater.getsalereport(brand, model, fromdate, todate, data));
        showProgessBar();
        call.enqueue(new Callback<ForgetModel>() {
            @Override
            public void onResponse(Call<ForgetModel> call, Response<ForgetModel> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().getStatusCode() == 0) {
                            Toast.makeText(AppSingle.getInstance().getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            showToast(response.body().getMessage());
                        }
                    } else {
                        showToast("Message body id null");
                    }
                } else {
                    showResponseMessage(response.code());
                    if (response.code() == 401)
                        callAsyncMainTo();
                }
            }

            @Override
            public void onFailure(Call<ForgetModel> call, Throwable t) {
                hideProgessBar();
                showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    private void showDatePicker(int type) {
        try {
            selectedDateOption = type;
            resetDateToToday();
            getDateDialog(type).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetDateToToday() {
        final Calendar c1 = Calendar.getInstance();
        Date_year = c1.get(Calendar.YEAR);
        Date_month = c1.get(Calendar.MONTH);
        Date_day = c1.get(Calendar.DAY_OF_MONTH);
    }

    private DatePickerDialog getDateDialog(int type) {
        switch (type) {
            case INT_FROM:
                DatePickerDialog dialog = new DatePickerDialog(AppSingle.getInstance().getActivity(),
                        pickerListener, Date_year, Date_month, Date_day);
                try {
                    dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return dialog;
            case INT_TO:
                DatePickerDialog dialoga = new DatePickerDialog(AppSingle.getInstance().getActivity(),
                        pickerListener, Date_year, Date_month, Date_day);
                try {
                    dialoga.getDatePicker().setMaxDate(System.currentTimeMillis());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return dialoga;
            default:
                return null;
        }
    }


    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {

            Date_year = selectedYear;
            Date_month = selectedMonth;
            Date_day = selectedDay;
            Calendar calendar = Calendar.getInstance();
            calendar.set(selectedYear, selectedMonth, selectedDay);
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
            String strDate = format.format(calendar.getTime());
            try {
                switch (selectedDateOption) {
                    case INT_FROM:
                        binding.txtSalereportFromdate.setText(strDate.trim());
                        break;
                    case INT_TO:
                        binding.txtSalereportTodate.setText(strDate.trim());
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void featchDB() {
        fetchBrandList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchBrandList();
        fetchMasterList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchMasterList();
        fetchReportTypeList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchReportTypeList();
    }
}
