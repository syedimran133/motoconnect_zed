package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 1/29/18.
 */

public class GRNSkuList {

    @SerializedName("SerialList")
    @Expose
    private List<GRNSerialList> SerialList;

    @SerializedName("SkuId")
    @Expose
    private String SkuId;

    @SerializedName("SkuCode")
    @Expose
    private String SkuCode;

    @SerializedName("SkuName")
    @Expose
    private String SkuName;

    @SerializedName("SkuQuantity")
    @Expose
    private String SkuQuantity;

    @SerializedName("SkuPrice")
    @Expose
    private String SkuPrice;

    @SerializedName("StockBinTypeCode")
    @Expose
    private String StockBinTypeCode;

    @SerializedName("serialisedMode")
    @Expose
    private String serialisedMode;

    public List<GRNSerialList> getSerialList() {
        return SerialList;
    }

    public void setSerialList(List<GRNSerialList> serialList) {
        SerialList = serialList;
    }

    public String getSkuId() {
        return SkuId;
    }

    public void setSkuId(String skuId) {
        SkuId = skuId;
    }

    public String getSkuCode() {
        return SkuCode;
    }

    public void setSkuCode(String skuCode) {
        SkuCode = skuCode;
    }

    public String getSkuName() {
        return SkuName;
    }

    public void setSkuName(String skuName) {
        SkuName = skuName;
    }

    public String getSkuQuantity() {
        return SkuQuantity;
    }

    public void setSkuQuantity(String skuQuantity) {
        SkuQuantity = skuQuantity;
    }

    public String getSkuPrice() {
        return SkuPrice;
    }

    public void setSkuPrice(String skuPrice) {
        SkuPrice = skuPrice;
    }

    public String getStockBinTypeCode() {
        return StockBinTypeCode;
    }

    public void setStockBinTypeCode(String stockBinTypeCode) {
        StockBinTypeCode = stockBinTypeCode;
    }

    public String getSerialisedMode() {
        return serialisedMode;
    }

    public void setSerialisedMode(String serialisedMode) {
        this.serialisedMode = serialisedMode;
    }
}
