package sales.zed.com.zedsales.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 1/30/18.
 */
@Entity
public class SerialMaster {


    @PrimaryKey
    @SerializedName("SerialMasterId")
    @Expose
    private int SerialMasterId;

    @SerializedName("SkuId")
    @Expose
    private String SkuId;

    @SerializedName("SalesChannelId")
    @Expose
    private String SalesChannelId;

    @SerializedName("RetailerId")
    @Expose
    private String RetailerId;

    @SerializedName("SerialNumber1")
    @Expose
    private String SerialNumber1;

    @SerializedName("SerialNumber2")
    @Expose
    private String SerialNumber2;

    @SerializedName("SerialNumber3")
    @Expose
    private String SerialNumber3;

    @SerializedName("SerialNumber4")
    @Expose
    private String SerialNumber4;

    @SerializedName("StockBinTypeMasterId")
    @Expose
    private String StockBinTypeMasterId;

    @SerializedName("GrnDate")
    @Expose
    private String GrnDate;

    @SerializedName("RecordCreationDate")
    @Expose
    private String RecordCreationDate;

    @SerializedName("ModifiedOn")
    @Expose
    private String ModifiedOn;

    @SerializedName("OwnerMode")
    @Expose
    private String OwnerMode;

    public int getSerialMasterId() {
        return SerialMasterId;
    }

    public String getSkuId() {
        return SkuId;
    }

    public String getSalesChannelId() {
        return SalesChannelId;
    }

    public String getRetailerId() {
        return RetailerId;
    }

    public String getSerialNumber1() {
        return SerialNumber1;
    }

    public String getSerialNumber2() {
        return SerialNumber2;
    }

    public String getSerialNumber3() {
        return SerialNumber3;
    }

    public String getSerialNumber4() {
        return SerialNumber4;
    }

    public String getStockBinTypeMasterId() {
        return StockBinTypeMasterId;
    }

    public String getGrnDate() {
        return GrnDate;
    }

    public String getRecordCreationDate() {
        return RecordCreationDate;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public String getOwnerMode() {
        return OwnerMode;
    }

    public void setSerialMasterId(int serialMasterId) {
        SerialMasterId = serialMasterId;
    }

    public void setSkuId(String skuId) {
        SkuId = skuId;
    }

    public void setSalesChannelId(String salesChannelId) {
        SalesChannelId = salesChannelId;
    }

    public void setRetailerId(String retailerId) {
        RetailerId = retailerId;
    }

    public void setSerialNumber1(String serialNumber1) {
        SerialNumber1 = serialNumber1;
    }

    public void setSerialNumber2(String serialNumber2) {
        SerialNumber2 = serialNumber2;
    }

    public void setSerialNumber3(String serialNumber3) {
        SerialNumber3 = serialNumber3;
    }

    public void setSerialNumber4(String serialNumber4) {
        SerialNumber4 = serialNumber4;
    }

    public void setStockBinTypeMasterId(String stockBinTypeMasterId) {
        StockBinTypeMasterId = stockBinTypeMasterId;
    }

    public void setGrnDate(String grnDate) {
        GrnDate = grnDate;
    }

    public void setRecordCreationDate(String recordCreationDate) {
        RecordCreationDate = recordCreationDate;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    public void setOwnerMode(String ownerMode) {
        OwnerMode = ownerMode;
    }

    @Override
    public String toString() {
        return
                "SerialMasterItem{" +
                        "SerialMasterId = '" + SerialMasterId + '\'' +
                        ",SkuId = '" + SkuId + '\'' +
                        ",SalesChannelId = '" + SalesChannelId + '\'' +
                        ",RetailerId = '" + RetailerId + '\'' +
                        ",SerialNumber1 = '" + SerialNumber1 + '\'' +
                        ",SerialNumber2 = '" + SerialNumber2 + '\'' +
                        ",SerialNumber3 = '" + SerialNumber3 + '\'' +
                        ",SerialNumber4 = '" + SerialNumber4 + '\'' +
                        ",StockBinTypeMasterId = '" + StockBinTypeMasterId + '\'' +
                        ",GrnDate = '" + GrnDate + '\'' +
                        ",RecordCreationDate = '" + RecordCreationDate + '\'' +
                        ",ModifiedOn = '" + ModifiedOn + '\'' +
                        ",OwnerMode = '" + OwnerMode + '\'' +
                        "}";
    }
}
