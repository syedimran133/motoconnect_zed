package sales.zed.com.zedsales.adapter;

import android.content.Context;
import android.graphics.Color;
import android.util.DisplayMetrics;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.RelativeLayout;
import android.widget.TextView;
import java.util.List;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.custom.CustomTextView;
import sales.zed.com.zedsales.models.CurrentStockSerial;
import sales.zed.com.zedsales.models.CurrentStockSkuItem;
import sales.zed.com.zedsales.models.SaleSerialListItems;

/**
 * Created by root on 2/16/18.
 */

public class CurrentStockAdapter extends BaseExpandableListAdapter {

    private Context context;
    private List<CurrentStockSkuItem> currentstock;

    public CurrentStockAdapter(Context context,List<CurrentStockSkuItem> currentstock) {
        this.context = context;
        this.currentstock=currentstock;
    }

    @Override
    public CurrentStockSerial getChild(int groupPosition, int childPosition) {
        return this.currentstock.get(groupPosition).getCurrentStockSerialList().get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {
        final String expandedListText = currentstock.get(groupPosition).getCurrentStockSerialList().get(childPosition).getSerial1();
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.report_myccurrentstock_innerlist, null);
        }
        TextView expandedListTextView = (TextView) convertView
                .findViewById(R.id.child_txt_serial);
        expandedListTextView.setTextColor(Color.parseColor("#000000"));
        expandedListTextView.setText(expandedListText);
        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.currentstock.get(groupPosition).getCurrentStockSerialList().size();
    }

    @Override
    public CurrentStockSkuItem getGroup(int groupPosition) {
        return this.currentstock.get(groupPosition);
    }

    @Override
    public int getGroupCount() {
        return this.currentstock.size();
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        CurrentStockSkuItem currentStockSkuItem = currentstock.get(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.reports_my_current_stock_list, null);
        }
        DisplayMetrics dm = new DisplayMetrics();
        int width = dm.widthPixels;
        RelativeLayout llhead=(RelativeLayout) convertView.findViewById(R.id.llhead);
        //llhead.setLayoutParams(new LinearLayout.LayoutParams(width, LinearLayout.LayoutParams.WRAP_CONTENT));
        CustomTextView listmodelTextView = (CustomTextView) convertView
                .findViewById(R.id.list_currentstock_txt_model);
        CustomTextView listskuTextView = (CustomTextView) convertView
                .findViewById(R.id.list_currentstock_txt_sku);
        CustomTextView listqtyTextView = (CustomTextView) convertView
                .findViewById(R.id.list_currentstock_txt_qty);
        listmodelTextView.setText(currentStockSkuItem.getModel());
        listskuTextView.setText(currentStockSkuItem.getSkuName());
        listqtyTextView.setText(currentStockSkuItem.getQuantity());
        if(isExpanded){
            llhead.setBackgroundColor(Color.parseColor("#2D9CC9"));
            listmodelTextView.setTextColor(Color.parseColor("#FFFFFF"));
            listskuTextView.setTextColor(Color.parseColor("#FFFFFF"));
            listqtyTextView.setTextColor(Color.parseColor("#FFFFFF"));
        }else{
            llhead.setBackgroundColor(Color.parseColor("#ffffff"));
            listmodelTextView.setTextColor(Color.parseColor("#000000"));
            listskuTextView.setTextColor(Color.parseColor("#000000"));
            listqtyTextView.setTextColor(Color.parseColor("#000000"));
        }
        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
