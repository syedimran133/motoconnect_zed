package sales.zed.com.zedsales.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 2/15/18.
 */

@Entity
public class ReportTypeItem {

    @PrimaryKey
    @SerializedName("ReportTypeMasterId")
    @Expose
    private int reportTypeMasterId;

    @SerializedName("ReportName")
    @Expose
    private String reportName;

    @SerializedName("Status")
    @Expose
    private String status;

    @SerializedName("CreatedOn")
    @Expose
    private String createdOn;

    @SerializedName("ModifiedOn")
    @Expose
    private String modifiedOn;

    public int getReportTypeMasterId() {
        return reportTypeMasterId;
    }

    public void setReportTypeMasterId(int reportTypeMasterId) {
        this.reportTypeMasterId = reportTypeMasterId;
    }

    public String getReportName() {
        return reportName;
    }

    public void setReportName(String reportName) {
        this.reportName = reportName;
    }

    public String getStatus() {
        return status;
    }

    public void setStatus(String status) {
        this.status = status;
    }

    public String getCreatedOn() {
        return createdOn;
    }

    public void setCreatedOn(String createdOn) {
        this.createdOn = createdOn;
    }

    public String getModifiedOn() {
        return modifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    @Override
    public String toString() {
        return
                "ReportTypeMasterList{" +
                        "ReportTypeMasterId = '" + reportTypeMasterId + '\'' +
                        ",ReportName = '" + reportName + '\'' +
                        ",Status = '" + status + '\'' +
                        ",CreatedOn = '" + createdOn + '\'' +
                        ",ModifiedOn = '" + modifiedOn + '\'' +
                        "}";
    }
}
