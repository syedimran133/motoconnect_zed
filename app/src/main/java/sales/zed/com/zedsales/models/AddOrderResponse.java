package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 2/2/18.
 */

public class AddOrderResponse {

    @SerializedName("status")
    @Expose
    private String StatusCode;

    @SerializedName("message")
    @Expose
    private String Message;

    @SerializedName("OrderNumber")
    @Expose
    private String OrderNumber;


    public String getStatusCode() {
        return StatusCode;
    }

    public void setStatusCode(String statusCode) {
        StatusCode = statusCode;
    }

    public String getMessage() {
        return Message;
    }

    public void setMessage(String message) {
        Message = message;
    }

    public String getOrderNumber() {
        return OrderNumber;
    }

    public void setOrderNumber(String orderNumber) {
        OrderNumber = orderNumber;
    }

}
