package sales.zed.com.zedsales.models;

import android.arch.persistence.room.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 1/30/18.
 */
@Entity
public class SerialMasterList {

    @SerializedName("SerialMasterLists")
    @Expose
    private List<SerialMaster> serialLists;

    public List<SerialMaster> getSerialLists() {
        return serialLists;
    }

    public void setSerialLists(List<SerialMaster> serialLists) {
        this.serialLists = serialLists;
    }

    @Override
    public String toString(){
        return
                "SerialMasterLists{" +
                        "serialLists = '" + serialLists + '\'' +
                        "}";
    }
}
