package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 3/16/18.
 */

public class CurrentStockSerial {
    @SerializedName("Serial1")
    @Expose
    private String Serial1;

    @SerializedName("Serial2")
    @Expose
    private String Serial2;

    @SerializedName("Serial3")
    @Expose
    private String Serial3;

    @SerializedName("Serial4")
    @Expose
    private String Serial4;


    public String getSerial1() {
        return Serial1;
    }

    public void setSerial1(String serial1) {
        Serial1 = serial1;
    }

    public String getSerial2() {
        return Serial2;
    }

    public void setSerial2(String serial2) {
        Serial2 = serial2;
    }

    public String getSerial3() {
        return Serial3;
    }

    public void setSerial3(String serial3) {
        Serial3 = serial3;
    }

    public String getSerial4() {
        return Serial4;
    }

    public void setSerial4(String serial4) {
        Serial4 = serial4;
    }
}
