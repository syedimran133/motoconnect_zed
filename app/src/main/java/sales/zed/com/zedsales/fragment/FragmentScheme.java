package sales.zed.com.zedsales.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.databinding.FragmentSchemeBinding;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FlowOrganizer;

/**
 * Created by root on 09/12/17.
 */

public class FragmentScheme extends BaseFragment {

    private FragmentSchemeBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_scheme, container, false);
        AppSingle.getInstance().getActivity().setTitle("Scheme",false);
        return binding.getRoot();
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        //binding.btnTextViewSubmit.setOnClickListener(listener);
        FlowOrganizer.getInstance().setInnerFrame(binding.frameScheme);
        FlowOrganizer.getInstance().addToInnerFrame(new FragmentSchemeMain());
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_text_view_submit:
                    callAsync();
                    break;
            }
        }
    };

    private void callAsync() {
    }
}
