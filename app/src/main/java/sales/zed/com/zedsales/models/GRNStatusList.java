package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 2/15/18.
 */

public class GRNStatusList {

    @SerializedName("GRNStatusList")
    @Expose
    private List<GRNStatusItem> GRNStatusList;

    public List<GRNStatusItem> getGRNStatusList() {
        return GRNStatusList;
    }

    public void setGRNStatusList(List<GRNStatusItem> GRNStatusList) {
        this.GRNStatusList = GRNStatusList;
    }
}
