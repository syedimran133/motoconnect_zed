package sales.zed.com.zedsales.models;

/**
 * Created by apple on 09/12/17.
 */

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class UserModel {

    @SerializedName("AuthKey")
    @Expose
    private String authKey;
    @PrimaryKey
    @SerializedName("UserId")
    @Expose
    private Integer userId;
    @SerializedName("EntityId")
    @Expose
    private String entityId;
    @SerializedName("UserName")
    @Expose
    private String userName;

    @SerializedName("UserType")
    @Expose
    private String userType;

    @SerializedName("IsOpeningStockEntered")
    @Expose
    private String isOpeningStockEntered;

    @SerializedName("RetailerName")
    @Expose
    private String retName;

    @SerializedName("LastLoginTime")
    @Expose
    private String lastLogin;

    public String getAuthKey() {
        return authKey;
    }

    public void setAuthKey(String authKey) {
        this.authKey = authKey;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }

    public String getEntityId() {
        return entityId;
    }

    public void setEntityId(String entityId) {
        this.entityId = entityId;
    }

    public String getUserName() {
        return userName;
    }

    public void setUserName(String userName) {
        this.userName = userName;
    }

    public String getUserType() {
        return userType;
    }

    public void setUserType(String userType) {
        this.userType = userType;
    }

    public String getIsOpeningStockEntered() {
        return isOpeningStockEntered;
    }

    public void setIsOpeningStockEntered(String isOpeningStockEntered) {
        this.isOpeningStockEntered = isOpeningStockEntered;
    }

    public String getRetName() {
        return retName;
    }

    public void setRetName(String retName) {
        this.retName = retName;
    }

    public String getLastLogin() {
        return lastLogin;
    }

    public void setLastLogin(String lastLogin) {
        this.lastLogin = lastLogin;
    }
}
