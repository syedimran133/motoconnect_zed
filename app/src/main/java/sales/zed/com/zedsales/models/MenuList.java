package sales.zed.com.zedsales.models;

/**
 * Created by Mukesh on 09/12/17.
 */

public class MenuList {

    private String title;
    private int image;
    private int menuid;

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public int getImage() {
        return image;
    }

    public void setImage(int image) {
        this.image = image;
    }

    public int getMenuid() {
        return menuid;
    }

    public void setMenuid(int menuid) {
        this.menuid = menuid;
    }
}
