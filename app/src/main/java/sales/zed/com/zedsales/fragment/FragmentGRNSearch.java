package sales.zed.com.zedsales.fragment;

import android.app.DatePickerDialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.adapter.RVAdapterGRNList;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.apiz.RequestFormater;
import sales.zed.com.zedsales.custom.CustomEditText;
import sales.zed.com.zedsales.databinding.FragmentGrnSearchBinding;
import sales.zed.com.zedsales.interfaces.MyAdapterListener;
import sales.zed.com.zedsales.models.PendingGRN;
import sales.zed.com.zedsales.models.PendingGrnList;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FlowOrganizer;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by root on 09/12/17.
 */

public class FragmentGRNSearch extends BaseFragment {
    private List<PendingGRN> grnList;
    private FragmentGrnSearchBinding binding;
    private final int INT_FROM = 0, INT_TO = 1;
    private int Date_day, Date_month, Date_year, selectedDateOption;
    private RVAdapterGRNList adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_grn_search, container, false);
        AppSingle.getInstance().getActivity().setTitle("GRN", true);
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("Manage GRN"));
        binding.fromdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker(INT_FROM);
            }
        });
        binding.dateto.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker(INT_TO);
            }
        });
        binding.txtGrn.setOnClickListener(listener);
        binding.txtPreviousgrn.setOnClickListener(listener);
        return binding.getRoot();
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.txt_grn:
                    binding.txtPreviousgrn.setBackgroundColor(Color.parseColor("#A39F9E"));
                    binding.txtGrn.setBackgroundColor(Color.parseColor("#00bfff"));
                    //FlowOrganizer.getInstance().addToInnerFrame(binding.frameGrn, new FragmentGRNSearch());
                    FlowOrganizer.getInstance().add(new FragmentGRNSearch(), true);
                    break;
                case R.id.txt_previousgrn:
                    binding.txtPreviousgrn.setBackgroundColor(Color.parseColor("#00bfff"));
                    binding.txtGrn.setBackgroundColor(Color.parseColor("#A39F9E"));
                    //FlowOrganizer.getInstance().addToInnerFrame(binding.frameGrn, new FragmentReportMyPurchase());
                    FlowOrganizer.getInstance().add(new FragmentReportMyPurchase(), true);
                    break;
            }
        }
    };
    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.btnSearch.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAsync(MainActivity.adapter.getIsselected());
            }
        });
        binding.edFromdate.setText(AppSingle.getfirstDate());
        binding.edTodate.setText(AppSingle.getCurrentDate());
    }

    private void callAsync(int index) {

        String invno = binding.edInvno.getText().toString();
        String fromdate = binding.edFromdate.getText().toString();
        String todate = binding.edTodate.getText().toString();

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<PendingGrnList> call = apiService.getpendinggrn(AppSingle.getInstance().getLoginUser().getUserId(), index, RequestFormater.getpendinggrn(invno, fromdate, todate));
        showProgessBar();
        call.enqueue(new Callback<PendingGrnList>() {
            @Override
            public void onResponse(Call<PendingGrnList> call, Response<PendingGrnList> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null)
                        if (response.body().getPendingGrnList() != null && response.body().getPendingGrnList().size() > 0) {
                            binding.rvListgrn.setVisibility(View.VISIBLE);
                            grnList = response.body().getPendingGrnList();
                            adapter = new RVAdapterGRNList(AppSingle.getInstance().getActivity(), response.body().getPendingGrnList(), new MyAdapterListener() {
                                @Override
                                public void iconButtonViewOnClick(View v, int position) {
                                    Bundle bundle = new Bundle();
                                    bundle.putString("InvoiceID", grnList.get(position).getInvoiceId());
                                    FlowOrganizer.getInstance().add(new FragmentGRNDetails(), true, bundle);
                                }
                            });
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
                            binding.rvListgrn.setLayoutManager(mLayoutManager);
                            binding.rvListgrn.setItemAnimator(new DefaultItemAnimator());
                            binding.rvListgrn.setAdapter(adapter);
                        } else {
                            binding.rvListgrn.setVisibility(View.GONE);
                            showToast("GRN List is empty.");
                        }
                } else {
                    showResponseMessage(response.code());
                    if (response.code() == 401)
                        callAsync(0);
                }
            }

            @Override
            public void onFailure(Call<PendingGrnList> call, Throwable t) {
                hideProgessBar();
                showToast("Fail");
            }
        });

    }


    private void showDatePicker(int type) {
        try {
            selectedDateOption = type;
            resetDateToToday();
            getDateDialog(type).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetDateToToday() {
        final Calendar c1 = Calendar.getInstance();
        Date_year = c1.get(Calendar.YEAR);
        Date_month = c1.get(Calendar.MONTH);
        Date_day = c1.get(Calendar.DAY_OF_MONTH);
    }

    private DatePickerDialog getDateDialog(int type) {
        switch (type) {
            case INT_FROM:
                DatePickerDialog dialog = new DatePickerDialog(AppSingle.getInstance().getActivity(),
                        pickerListener, Date_year, Date_month, Date_day);
                try {
                    dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return dialog;
            case INT_TO:
                DatePickerDialog dialoga = new DatePickerDialog(AppSingle.getInstance().getActivity(),
                        pickerListener, Date_year, Date_month, Date_day);
                try {
                    dialoga.getDatePicker().setMaxDate(System.currentTimeMillis());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return dialoga;
            default:
                return null;
        }
    }


    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            try {
                switch (selectedDateOption) {
                    case INT_FROM:
                        Calendar calendar = Calendar.getInstance();
                        calendar.set(selectedYear, selectedMonth, selectedDay);
                        SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
                        String strDate = format.format(calendar.getTime());
                        binding.edFromdate.setText(strDate.trim());
                        break;
                    case INT_TO:
                        Calendar calendarto = Calendar.getInstance();
                        calendarto.set(selectedYear, selectedMonth, selectedDay);
                        SimpleDateFormat formatto = new SimpleDateFormat("dd MMM yyyy");
                        String strDateto = formatto.format(calendarto.getTime());
                        binding.edTodate.setText(strDateto.trim());
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }
}
