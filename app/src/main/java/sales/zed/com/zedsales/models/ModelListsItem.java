package sales.zed.com.zedsales.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class ModelListsItem{

	@SerializedName("Status")
	@Expose
	private boolean status;

	@SerializedName("ModelName")
	@Expose
	private String modelName;

	@SerializedName("ModelCode")
	@Expose
	private String modelCode;

	@SerializedName("ModifiedOn")
	@Expose
	private String modifiedOn;

	@SerializedName("ProductCategoryId")
	@Expose
	private int productCategoryId;

	@SerializedName("RecordCreationDate")
	@Expose
	private String recordCreationDate;

	@SerializedName("ModelType")
	@Expose
	private int modelType;

	@SerializedName("ProductId")
	@Expose
	private int productId;

	@SerializedName("BrandId")
	@Expose
	private int brandId;

	@SerializedName("SerialisedMode")
	@Expose
	private int serialisedMode;

	@PrimaryKey
	@SerializedName("ModelId")
	@Expose
	private int modelId;

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	public void setModelName(String modelName){
		this.modelName = modelName;
	}

	public String getModelName(){
		return modelName;
	}

	public void setModelCode(String modelCode){
		this.modelCode = modelCode;
	}

	public String getModelCode(){
		return modelCode;
	}

	public void setModifiedOn(String modifiedOn){
		this.modifiedOn = modifiedOn;
	}

	public String getModifiedOn(){
		return modifiedOn;
	}

	public void setProductCategoryId(int productCategoryId){
		this.productCategoryId = productCategoryId;
	}

	public int getProductCategoryId(){
		return productCategoryId;
	}

	public void setRecordCreationDate(String recordCreationDate){
		this.recordCreationDate = recordCreationDate;
	}

	public String getRecordCreationDate(){
		return recordCreationDate;
	}

	public void setModelType(int modelType){
		this.modelType = modelType;
	}

	public int getModelType(){
		return modelType;
	}

	public void setProductId(int productId){
		this.productId = productId;
	}

	public int getProductId(){
		return productId;
	}

	public void setBrandId(int brandId){
		this.brandId = brandId;
	}

	public int getBrandId(){
		return brandId;
	}

	public void setSerialisedMode(int serialisedMode){
		this.serialisedMode = serialisedMode;
	}

	public int getSerialisedMode(){
		return serialisedMode;
	}

	public void setModelId(int modelId){
		this.modelId = modelId;
	}

	public int getModelId(){
		return modelId;
	}

	@Override
 	public String toString(){
		return 
			"ModelListsItem{" + 
			"status = '" + status + '\'' + 
			",modelName = '" + modelName + '\'' + 
			",modelCode = '" + modelCode + '\'' + 
			",modifiedOn = '" + modifiedOn + '\'' + 
			",productCategoryId = '" + productCategoryId + '\'' + 
			",recordCreationDate = '" + recordCreationDate + '\'' + 
			",modelType = '" + modelType + '\'' + 
			",productId = '" + productId + '\'' + 
			",brandId = '" + brandId + '\'' + 
			",serialisedMode = '" + serialisedMode + '\'' + 
			",modelId = '" + modelId + '\'' + 
			"}";
		}
}