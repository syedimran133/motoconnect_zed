package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 2/14/18.
 */

public class SaleReportModel {

    @SerializedName("Model")
    @Expose
    private String Model;

    @SerializedName("Quantity")
    @Expose
    private String Quantity;

    public String getModel() {
        return Model;
    }

    public void setModel(String model) {
        Model = model;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }
}
