package sales.zed.com.zedsales.fragment;

import android.Manifest;
import android.app.Activity;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.os.Debug;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;
import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.databinding.FragmentReportsBinding;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FlowOrganizer;

/**
 * Created by root on 09/12/17.
 */

public class FragmentReport extends Fragment {

    private FragmentReportsBinding binding;
    private Fragment fragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_reports, container, false);
        AppSingle.getInstance().getActivity().setTitle("Report",true);
        fragment=this;
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.llGrn.setOnClickListener(listener);
        binding.llMysale.setOnClickListener(listener);
        binding.llClaimpricedrop.setOnClickListener(listener);
        binding.llMyorder.setOnClickListener(listener);
        binding.llMyreturn.setOnClickListener(listener);
        binding.llCurrentstock.setOnClickListener(listener);
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.ll_grn:
                    setSelected(0);
                    MainActivity.isReport=true;
                    FlowOrganizer.getInstance().add(new FragmentReportMyPurchase(), true);
                    break;
                case R.id.ll_mysale:
                    setSelected(1);
                    MainActivity.isReport=true;
                    FlowOrganizer.getInstance().add(new FragmentReportMySaleViewpr(), true);
                    break;
                case R.id.ll_claimpricedrop:
                    setSelected(2);
                    MainActivity.isReport=true;
                  /*  try {
                        if (ContextCompat.checkSelfPermission(AppSingle.getInstance().getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                            IntentIntegrator.forSupportFragment(fragment).initiateScan();
                        }
                    } catch (Exception e) {
                        //showToast(e.toString());
                    }*/
                    break;
                case R.id.ll_myorder:
                    setSelected(3);
                    MainActivity.isReport=true;
                    FlowOrganizer.getInstance().add(new FragmentMyOrderViewpr(), true);
                    break;
                case R.id.ll_myreturn:
                    setSelected(4);
                    MainActivity.isReport=true;
                    FlowOrganizer.getInstance().add(new FragmentReportMyReturns(), true);
                    break;
                case R.id.ll_currentstock:
                    setSelected(5);
                    MainActivity.isReport=true;
                    FlowOrganizer.getInstance().add(new FragmentReportMyCurrentStock(), true);
                    break;

            }
        }
    };

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);

        switch (requestCode) {
            case IntentIntegrator.REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    // Parsing bar code reader result
                    IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
                    //if (Debug) Log.d(TAG, "Parsing bar code reader result: " + result.toString());
                    Toast.makeText(AppSingle.getInstance().getActivity(),result.toString(),Toast.LENGTH_LONG).show();
                }
                break;
        }
    }
    private void setSelected(int position) {
        switch (position) {
            case 0:
               binding.llGrn.setSelected(true);
                binding.llMysale.setSelected(false);
                binding.llClaimpricedrop.setSelected(false);
                binding.llMyorder.setSelected(false);
                binding.llMyreturn.setSelected(false);
                binding.llCurrentstock.setSelected(false);
                break;
            case 1:
                binding.llGrn.setSelected(false);
                binding.llMysale.setSelected(true);
                binding.llClaimpricedrop.setSelected(false);
                binding.llMyorder.setSelected(false);
                binding.llMyreturn.setSelected(false);
                binding.llCurrentstock.setSelected(false);
                break;
            case 2:
                binding.llGrn.setSelected(false);
                binding.llMysale.setSelected(false);
                binding.llClaimpricedrop.setSelected(true);
                binding.llMyorder.setSelected(false);
                binding.llMyreturn.setSelected(false);
                binding.llCurrentstock.setSelected(false);
                break;
            case 3:
                binding.llGrn.setSelected(false);
                binding.llMysale.setSelected(false);
                binding.llClaimpricedrop.setSelected(false);
                binding.llMyorder.setSelected(true);
                binding.llMyreturn.setSelected(false);
                binding.llCurrentstock.setSelected(false);
                break;
            case 4:
                binding.llGrn.setSelected(false);
                binding.llMysale.setSelected(false);
                binding.llClaimpricedrop.setSelected(false);
                binding.llMyorder.setSelected(false);
                binding.llMyreturn.setSelected(true);
                binding.llCurrentstock.setSelected(false);
                break;
            case 5:
                binding.llGrn.setSelected(false);
                binding.llMysale.setSelected(false);
                binding.llClaimpricedrop.setSelected(false);
                binding.llMyorder.setSelected(false);
                binding.llMyreturn.setSelected(false);
                binding.llCurrentstock.setSelected(true);
                break;
            default:
                binding.llGrn.setSelected(false);
                binding.llMysale.setSelected(false);
                binding.llClaimpricedrop.setSelected(false);
                binding.llMyorder.setSelected(false);
                binding.llMyreturn.setSelected(false);
                binding.llCurrentstock.setSelected(false);
                break;
        }
    }
    @Override
    public void onDestroy() {
        super.onDestroy();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }
}
