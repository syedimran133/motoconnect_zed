package sales.zed.com.zedsales.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.databinding.FragmentusermanualBinding;
import sales.zed.com.zedsales.support.AppSingle;


public class FragmentUserManual extends BaseFragment  {

    FragmentusermanualBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragmentusermanual, container, false);

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.pdfView.fromAsset("usermanual.pdf").load();
    }
}
