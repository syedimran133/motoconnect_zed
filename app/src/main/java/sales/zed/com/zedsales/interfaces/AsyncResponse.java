package sales.zed.com.zedsales.interfaces;

import sales.zed.com.zedsales.models.SkuOpeningStockList;

/**
 * @author Imran
 *
 */
public interface AsyncResponse {
	void processFinish(SkuOpeningStockList output,int resultcode);
}
