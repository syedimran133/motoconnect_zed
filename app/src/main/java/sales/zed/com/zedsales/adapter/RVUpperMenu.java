package sales.zed.com.zedsales.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.Toast;

import java.util.Collections;
import java.util.List;

import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.databinding.UppermenuBinding;
import sales.zed.com.zedsales.models.MenuList;

/**
 * Created by root on 3/20/18.
 */

public class RVUpperMenu extends RecyclerView.Adapter<RVUpperMenu.ViewHolder> {

    private List<MenuList> listData;
    private LayoutInflater mInflater;
    Context mContext;
    int isselected = 0;
    LinearLayout mainbg;
    private RVUpperMenu.IonItemClickListener listener;

    public RVUpperMenu(Context context, List<MenuList> listData) {
        try {
            this.listData = listData;
            mContext = context;
            mInflater = LayoutInflater.from(context);
            isselected = getItemCount() - 1;
        } catch (Exception e) {
        }
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        try {
            final MenuList data = listData.get(position);
            mainbg = holder.binding.menu;
            holder.binding.menu.setVisibility(View.VISIBLE);
            if (position == isselected)
                holder.binding.menu.setBackgroundColor(Color.parseColor("#3577B4")/*mContext.getResources().getColor(R.color.colorPrimary, null)*/);
            else
                holder.binding.menu.setBackgroundColor(Color.parseColor("#2C6D9D")/*mContext.getResources().getColor(R.color.colorPrimary, null)*/);
            holder.binding.txtViewTitle.setText(data.getTitle().replace("Manage ", ""));
            holder.binding.imgViewLogo.setImageResource(data.getImage());
        } catch (Exception e) {
        }
    }

    public void registerItemClickLisener(RVUpperMenu.IonItemClickListener listener) {
        try {
            this.listener = listener;
        } catch (Exception e) {
        }
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {

        View view = mInflater.inflate(R.layout.uppermenu, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public int getItemCount() {
        if (listData == null)
            return 0;
        return listData.size();
    }

    public void updatelist(List<MenuList> listData) {
        try {
            this.listData = listData;
            notifyDataSetChanged();
        } catch (Exception e) {
        }
    }

    public void setselected(int p) {
        try {
            isselected = p;
            notifyDataSetChanged();
            notifyItemRangeChanged(p, listData.size());
        } catch (Exception e) {
        }
    }

    public int getIsselected() {
        return isselected;
    }

    public int getcurrent(String current) {
        int index = 0;
        try {
            for (int i = 0; i < listData.size(); i++) {
                if (listData.get(i).getTitle().equalsIgnoreCase(current)) {
                    index = i;
                    break;
                }
            }
        } catch (Exception e) {
        }

        return index;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public UppermenuBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            try {
                binding = DataBindingUtil.bind(itemView);
                binding.getRoot().setOnClickListener(new View.OnClickListener() {
                    @Override
                    public void onClick(View view) {
                        if (listener != null) {
                            try {
                                listener.onItemClick(listData.get(getAdapterPosition()).getMenuid() - 1, getAdapterPosition());
                            } catch (Exception e) {
                            }
                        }
                    }
                });
            } catch (Exception e) {
            }
        }
    }

    public interface IonItemClickListener {
        void onItemClick(int position, int pa);
    }
}
