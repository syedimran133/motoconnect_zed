package sales.zed.com.zedsales.fragment;

import android.app.DatePickerDialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.util.Log;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;
import android.widget.ImageView;
import android.widget.RadioButton;
import android.widget.RadioGroup;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.apiz.RequestFormater;
import sales.zed.com.zedsales.constants.AppConstants;
import sales.zed.com.zedsales.custom.CustomEditText;
import sales.zed.com.zedsales.custom.CustomTextView;
import sales.zed.com.zedsales.databinding.FragmentGrnSearchBinding;
import sales.zed.com.zedsales.databinding.FragmentMyprofilePrsnlDtlBinding;
import sales.zed.com.zedsales.models.ForgetModel;
import sales.zed.com.zedsales.models.ProfileData;
import sales.zed.com.zedsales.support.AppPrefrence;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.AppValidate;
import sales.zed.com.zedsales.support.FeatchMaster;
import sales.zed.com.zedsales.support.FlowOrganizer;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by Mukesh on 09/12/17.
 */

public class FragmentMyProfilePrsnlDtl extends BaseFragment {

    private FragmentMyprofilePrsnlDtlBinding binding;
    private ImageView ivdob, ivdoa;
    private RadioButton rbm_no;
    private RadioButton rbm_yes;
    private String mrd = "No";
    private final int INT_DOB = 0, INT_DOA = 1;
    private int Date_day, Date_month, Date_year, selectedDateOption;
    private ProfileData profileData;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_myprofile_prsnl_dtl, container, false);
        rbm_no = binding.rbNo;
        rbm_yes = binding.rbYes;
        ivdob = binding.ivdob;
        ivdoa = binding.ivdoa;
        AppSingle.getInstance().getActivity().setTitle("Profile", true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("My Profile"));
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        callAsyncProfile();
        ivdob.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker(INT_DOB);
            }
        });

        ivdoa.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                showDatePicker(INT_DOA);
            }
        });
        binding.rgDoa.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                RadioButton rb = binding.rgDoa.findViewById(checkedId);
                if (rb.getText().toString().equalsIgnoreCase("No")) {
                    binding.llDoa.setVisibility(View.GONE);
                } else {
                    binding.llDoa.setVisibility(View.VISIBLE);
                }
            }
        });
       /* binding.r.setOnCheckedChangeListener(new RadioGroup.OnCheckedChangeListener()
        {
            public void onCheckedChanged(RadioGroup group, int checkedId) {
                // checkedId is the RadioButton selected
                RadioButton rb=(RadioButton)findViewById(checkedId);
                textViewChoice.setText("You Selected " + rb.getText());
                //Toast.makeText(getApplicationContext(), rb.getText(), Toast.LENGTH_SHORT).show();
            }
        });*/
        binding.btnupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (AppValidate.isValidMobileNo(binding.etEnterRegmobno.getText().toString())) {
                    if (AppValidate.isValidMobileNo(binding.etEnterComp.getText().toString())) {
                        if (AppValidate.isValidEmail(binding.etEnterEmail.getText().toString())) {
                            callAsyncProfileUpdateData();
                        } else {
                            binding.etEnterEmail.setError("Please enter valid email");
                        }
                    } else {
                        binding.etEnterComp.setError("Please enter valid Mobile Number");
                    }
                } else {
                    binding.etEnterRegmobno.setError("Please enter valid Mobile Number");
                }
            }
        });

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {

            binding.textviewUpdateprsnlBankDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    binding.textviewUpdateprsnlBankDetail.setBackgroundColor(Color.parseColor("#00bfff"));
                    binding.textviewUpdateprsnlPrsnlDetail.setBackgroundColor(Color.parseColor("#A39F9E"));
                    FlowOrganizer.getInstance().add(new FragmentShowProfileBank(), true);
                    //FlowOrganizer.getInstance().addToInnerFrame(new FragmentShowProfileBank());
                }
            });

            binding.textviewUpdateprsnlPrsnlDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    binding.textviewUpdateprsnlPrsnlDetail.setBackgroundColor(Color.parseColor("#00bfff"));
                    binding.textviewUpdateprsnlBankDetail.setBackgroundColor(Color.parseColor("#A39F9E"));
                    FlowOrganizer.getInstance().add(new FragmentShowProfileDetails(), true);
                    //FlowOrganizer.getInstance().addToInnerFrame(new FragmentShowProfileDetails());
                }
            });
        } catch (Exception e) {
        }
//        binding.btnTextViewSubmit.setOnClickListener(listener);
    }

    private void callAsyncProfile() {

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        AppPrefrence.getInstance().getToken();
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<ProfileData> call = apiService.getprofiledata(AppSingle.getInstance().getLoginUser().getUserId());
        showProgessBar();
        call.enqueue(new Callback<ProfileData>() {
            @Override
            public void onResponse(Call<ProfileData> call, Response<ProfileData> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    profileData = response.body();
                    setProfileText(profileData);
                    setenable(profileData);
                } else {
                    showResponseMessage(response.code());
                    if (response.code() == 401)
                        callAsyncProfile();
                }
            }

            @Override
            public void onFailure(Call<ProfileData> call, Throwable t) {
                hideProgessBar();
                showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    private void callAsyncProfileUpdateData() {

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        if (binding.rbYes.isChecked()) {
            mrd = "Yes";
        } else {
            mrd = "No";
        }
        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<ForgetModel> call = apiService.update_profile(AppSingle.getInstance().getLoginUser().getUserId(), FeatchMaster.currentDate, RequestFormater.getupdateprofile(binding.etEnterCompanyname.getText().toString(), binding.etEnterRetailercode.getText().toString(), binding.etEnterRetailername.getText().toString(), binding.etEnterRegmobno.getText().toString(), binding.etEnterComp.getText().toString(), binding.etEnterEmail.getText().toString(), binding.etEnterDob.getText().toString(), mrd, binding.etEnterDoa.getText().toString(), binding.etEnterAddress.getText().toString(), binding.etEnterCity.getText().toString(), binding.etEnterState.getText().toString(), binding.etEnterPincode.getText().toString(), binding.etEnterCpValue.getText().toString(), profileData.getRetailerProfile().get(0).getAccountHolderName(), profileData.getRetailerProfile().get(0).getAccountNumber(), profileData.getRetailerProfile().get(0).getBankName(), profileData.getRetailerProfile().get(0).getBranchName(), profileData.getRetailerProfile().get(0).getBranchLocation(), profileData.getRetailerProfile().get(0).getIfscCode(), profileData.getRetailerProfile().get(0).getAadharNumber(), profileData.getRetailerProfile().get(0).getPanNumber(), profileData.getRetailerProfile().get(0).getGstinNo()));
        showProgessBar();
        call.enqueue(new Callback<ForgetModel>() {
            @Override
            public void onResponse(Call<ForgetModel> call, Response<ForgetModel> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    FlowOrganizer.getInstance().add(new FragmentShowProfileDetails(), true);
                } else {
                    showResponseMessage(response.code());
                    if (response.code() == 401)
                        callAsyncProfileUpdateData();
                }
            }

            @Override
            public void onFailure(Call<ForgetModel> call, Throwable t) {
                hideProgessBar();
                showToast(AppConstants.IMessages.FAIL);
            }
        });
    }


    private void showDatePicker(int type) {
        try {
            selectedDateOption = type;
            resetDateToToday();
            getDateDialog(type).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetDateToToday() {
        final Calendar c1 = Calendar.getInstance();
        Date_year = c1.get(Calendar.YEAR);
        Date_month = c1.get(Calendar.MONTH);
        Date_day = c1.get(Calendar.DAY_OF_MONTH);
    }

    private DatePickerDialog getDateDialog(int type) {
        switch (type) {
            case INT_DOB:
                DatePickerDialog dialog = new DatePickerDialog(AppSingle.getInstance().getActivity(),
                        pickerListener, Date_year, Date_month, Date_day);
                try {
                    dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return dialog;
            case INT_DOA:
                DatePickerDialog dialoga = new DatePickerDialog(AppSingle.getInstance().getActivity(),
                        pickerListener, Date_year, Date_month, Date_day);
                try {
                    dialoga.getDatePicker().setMaxDate(System.currentTimeMillis());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return dialoga;
            default:
                return null;
        }
    }


    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            Date_year = selectedYear;
            Date_month = selectedMonth;
            Date_day = selectedDay;
            String day1, month1;
            if (-1 < Date_month && Date_month < 9) {
                month1 = "0" + (Date_month + 1);
            } else {
                month1 = String.valueOf(Date_month + 1);
            }
            if (0 < Date_day && Date_day <= 9) {
                day1 = "0" + Date_day;
            } else {
                day1 = String.valueOf(Date_day);
            }
            String year1 = String.valueOf(Date_year);
            final Calendar c = Calendar.getInstance();
            String c_date = (new StringBuilder().append(c.get(Calendar.YEAR)).append("-").append(c.get(Calendar.MONTH) + 1).append("-").append(c.get(Calendar.DAY_OF_MONTH)).append(" ")).toString();
            try {
                StringBuffer Date = new StringBuffer(day1).append("-").append(month1).append("-").append(year1);//(day1+"-"+month1+"-"+year1).toString();

                switch (selectedDateOption) {
                    case INT_DOB:
                        String st = (new StringBuilder().append(Date_year).append("-").append(Date_month + 1).append("-").append(Date_day).append(" ")).toString();
                        //java.util.Date d2 = new SimpleDateFormat("yyyy-MM-dd").parse(st);
                        //if (dateComp(d2)) {
                        binding.etEnterDob.setText(st.trim());
                        //} else {
                        //    Toast.makeText(AppSingle.getInstance().getActivity(), "Minimum 18 years old", Toast.LENGTH_LONG).show();
                        //}
                        break;
                    case INT_DOA:
                        String sta = (new StringBuilder().append(Date_year).append("-").append(Date_month + 1).append("-").append(Date_day).append(" ")).toString();
                        // java.util.Date d2a = new SimpleDateFormat("yyyy-MM-dd").parse(sta);
                        //if (dateComp(d2a)) {
                        binding.etEnterDoa.setText(sta.trim());
                        //} else {
                        //   Toast.makeText(AppSingle.getInstance().getActivity(), "Minimum 18 years old", Toast.LENGTH_LONG).show();
                        //}
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public static boolean dateComp(Date dt1) {
        {
            try {
                Date dt2 = new Date();
                int diffInDays = (int) ((dt2.getTime() - dt1.getTime()) / (1000 * 60 * 60 * 24));
                int day = 18 * 365;
                Log.e("@@@@@@@@@@@@", "Difference in number of days (2) : " + diffInDays + " day : " + day);
                System.err.println("Difference in number of days (2) : " + diffInDays + " day : " + day);
                if (diffInDays >= day) {
                    return true;
                } else {
                    return false;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
        return false;
    }

    private void setenable(ProfileData data) {
        try {
            if (data.getEdtabledata().get(0).getCompanyName().equalsIgnoreCase("0")) {
                binding.etEnterCompanyname.setEnabled(false);
                binding.etEnterCompanyname.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            if (data.getEdtabledata().get(0).getUserCode().equalsIgnoreCase("0")) {
                binding.etEnterRetailercode.setEnabled(false);
                binding.etEnterRetailercode.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            if (data.getEdtabledata().get(0).getUserName().equalsIgnoreCase("0")) {
                binding.etEnterRetailername.setEnabled(false);
                binding.etEnterRetailername.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            if (data.getEdtabledata().get(0).getRegisteredMobileNo().equalsIgnoreCase("0")) {
                binding.etEnterRegmobno.setEnabled(false);
                binding.etEnterRegmobno.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            if (data.getEdtabledata().get(0).getAlternateMobileNo().equalsIgnoreCase("0")) {
                binding.etEnterComp.setEnabled(false);
                binding.etEnterComp.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            if (data.getEdtabledata().get(0).getEmailId().equalsIgnoreCase("0")) {
                binding.etEnterEmail.setEnabled(false);
                binding.etEnterEmail.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            if (data.getEdtabledata().get(0).getDateOfBirth().equalsIgnoreCase("0")) {
                binding.etEnterDob.setEnabled(false);
                binding.etEnterDob.setBackgroundColor(Color.parseColor("#D3D3D3"));
                binding.ivdob.setEnabled(false);
            }
            if (data.getEdtabledata().get(0).getDateOfAnniversary().equalsIgnoreCase("0")) {
                binding.etEnterDoa.setEnabled(false);
                binding.etEnterDoa.setBackgroundColor(Color.parseColor("#D3D3D3"));
                binding.ivdoa.setEnabled(false);
            }
            if (data.getEdtabledata().get(0).getAddress().equalsIgnoreCase("0")) {
                binding.etEnterAddress.setEnabled(false);
                binding.etEnterAddress.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            if (data.getEdtabledata().get(0).getCity().equalsIgnoreCase("0")) {
                binding.etEnterCity.setEnabled(false);
                binding.etEnterCity.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            if (data.getEdtabledata().get(0).getState().equalsIgnoreCase("0")) {
                binding.etEnterState.setEnabled(false);
                binding.etEnterState.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            if (data.getEdtabledata().get(0).getPinCode().equalsIgnoreCase("0")) {
                binding.etEnterPincode.setEnabled(false);
                binding.etEnterPincode.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            if (data.getEdtabledata().get(0).getCounterPotentialValue().equalsIgnoreCase("0")) {
                binding.etEnterCpValue.setEnabled(false);
                binding.etEnterCpValue.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            if (data.getEdtabledata().get(0).getCounterPotentialVolume().equalsIgnoreCase("0")) {
                binding.etEnterCpVolume.setEnabled(false);
                binding.etEnterCpVolume.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
        } catch (Exception e) {
        }
    }

    private void setProfileText(ProfileData profileData) {
        try {
            binding.etEnterCompanyname.setText(profileData.getRetailerProfile().get(0).getCompanyName());
            binding.etEnterRetailercode.setText(profileData.getRetailerProfile().get(0).getUserCode());
            binding.etEnterRetailername.setText(profileData.getRetailerProfile().get(0).getUserName());
            binding.etEnterRegmobno.setText(profileData.getRetailerProfile().get(0).getRegisteredMobileNo());
            binding.etEnterComp.setText(profileData.getRetailerProfile().get(0).getAlternateMobileNo());
            binding.etEnterEmail.setText(profileData.getRetailerProfile().get(0).getEmailId());
            binding.etEnterDob.setText(profileData.getRetailerProfile().get(0).getDateOfBirth());
            binding.etEnterDoa.setText(profileData.getRetailerProfile().get(0).getDateOfAnniversary());
            binding.etEnterAddress.setText(profileData.getRetailerProfile().get(0).getAddress());
            binding.etEnterCity.setText(profileData.getRetailerProfile().get(0).getCity());
            binding.etEnterState.setText(profileData.getRetailerProfile().get(0).getState());
            binding.etEnterPincode.setText(profileData.getRetailerProfile().get(0).getPinCode());
            binding.etEnterCpValue.setText(profileData.getRetailerProfile().get(0).getCounterPotentialValue());
            binding.etEnterCpVolume.setText(profileData.getRetailerProfile().get(0).getCounterPotentialVolume());
            if (profileData.getRetailerProfile().get(0).getMarried().equalsIgnoreCase("no")) {
                binding.rbNo.setChecked(true);
            } else {
                binding.rbYes.setChecked(true);
            }
        } catch (Exception e) {
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }
}
