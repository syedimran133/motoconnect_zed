package sales.zed.com.zedsales.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.databinding.AdapterMenuBinding;
import sales.zed.com.zedsales.fragment.FragmentMenu;
import sales.zed.com.zedsales.models.MenuList;
import sales.zed.com.zedsales.models.MenuListData;

/**
 * Created by root on 09/12/17.
 */

public class RVAdapterMenu extends RecyclerView.Adapter<RVAdapterMenu.ViewHolder> {

    private List<MenuList> listData;
    private LayoutInflater mInflater;
    private boolean isShowing = false;
    private int menu;

    public RVAdapterMenu(Context context, List<MenuList> listData, int menu) {
        this.listData = listData;
        this.menu = menu;
        mInflater = LayoutInflater.from(context);
    }

    private IonItemClickListener listener;

    public void registerItemClickLisener(IonItemClickListener listener) {
        this.listener = listener;
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.adapter_menu, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {

        final MenuList data = listData.get(position);
        holder.binding.menu.setVisibility(View.VISIBLE);
        holder.binding.txtViewTitle.setText(data.getTitle());
        holder.binding.imgViewLogo.setImageResource(data.getImage());
        if (position == listData.size() - 1) {
            if (menu != -1) {
                FragmentMenu.openPage(menu);
            }
        }
    }

    @Override
    public int getItemCount() {
        if (listData == null)
            return 0;
        return listData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        public AdapterMenuBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);
            binding.getRoot().setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View view) {
                    if (listener != null)
                        listener.onItemClick(listData.get(getAdapterPosition()).getMenuid() - 1);
                }
            });
        }
    }

    public interface IonItemClickListener {
        void onItemClick(int position);
    }

    public boolean isShowing() {
        return isShowing;
    }
}