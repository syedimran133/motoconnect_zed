package sales.zed.com.zedsales.models;

/**
 * Created by Mukesh on 13/12/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class SerialList {

    @SerializedName("serialNumber")
    @Expose
    private String serialNumber;

    public String getSerialNumber() {
        return serialNumber;
    }

    public void setSerialNumber(String serialNumber) {
        this.serialNumber = serialNumber;
    }

}