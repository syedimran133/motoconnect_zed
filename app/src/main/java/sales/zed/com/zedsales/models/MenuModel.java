package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Mukesh on 09/12/17.
 */

public class MenuModel {

    @SerializedName("MenuLists")
    @Expose
    private List<MenuListData> menuList = null;

    public List<MenuListData> getMenuList() {
        return menuList;
    }

    public void setMenuList(List<MenuListData> menuList) {
        this.menuList = menuList;
    }

}
