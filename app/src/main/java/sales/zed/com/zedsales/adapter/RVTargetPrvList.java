package sales.zed.com.zedsales.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.databinding.Target1InnerElementsBinding;
import sales.zed.com.zedsales.interfaces.MyAdapterListener;
import sales.zed.com.zedsales.models.PreviousTargetItems;

/**
 * Created by root on 2/20/18.
 */

public class RVTargetPrvList extends RecyclerView.Adapter<RVTargetPrvList.ViewHolder> {


    private List<PreviousTargetItems> listData;
    private LayoutInflater mInflater;
    private MyAdapterListener click_listener;

    public RVTargetPrvList(Context context, List<PreviousTargetItems> listData) {
        this.listData = listData;
        mInflater = LayoutInflater.from(context);
    }


    @Override
    public int getItemCount() {
        if (listData == null)
            return 0;
        return listData.size();
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        PreviousTargetItems previousTargetItems=listData.get(position);
        holder.binding.txtTargetMain.setText(previousTargetItems.getTarget());
        holder.binding.txtTargetType.setText(previousTargetItems.getTargetType());
        holder.binding.txtTargetSubType.setText(previousTargetItems.getTargetSubType());
        holder.binding.txtTargetPeriod.setText(previousTargetItems.getPeriod());
        holder.binding.txtTargetBaseon.setText(previousTargetItems.getTargetBasedOn());
        holder.binding.txtTargetSku.setText(previousTargetItems.getSkuName());
        holder.binding.txtMoredetails.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (holder.binding.txtMoredetails.getText().toString().equalsIgnoreCase("more Details")) {
                    holder.binding.txtMoredetails.setText("less Details");
                    holder.binding.linerAchievement.setVisibility(View.VISIBLE);
                } else {
                    holder.binding.txtMoredetails.setText("more Details");
                    holder.binding.linerAchievement.setVisibility(View.GONE);
                }
            }
        });
        holder.binding.txtTargetModel.setText(previousTargetItems.getModelName());
        holder.binding.txtTargetBelow.setText(previousTargetItems.getTotalTarget());
        holder.binding.txtTargetAchieved.setText(previousTargetItems.getTargetAchieved());
        holder.binding.txtBelowAchievements.setText(previousTargetItems.getTargetAchievedPercent());
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.target_1_inner_elements, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        Target1InnerElementsBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (click_listener != null)
                        click_listener.iconButtonViewOnClick(v, getAdapterPosition());
                }
            });
        }
    }
}
