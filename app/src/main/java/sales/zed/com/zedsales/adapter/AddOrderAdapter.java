package sales.zed.com.zedsales.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;

import java.util.ArrayList;
import java.util.List;

import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.custom.CustomTextViewBold;
import sales.zed.com.zedsales.models.AddOrderModel;
import sales.zed.com.zedsales.models.OffLineData;
import sales.zed.com.zedsales.models.SkuListsItem;
import sales.zed.com.zedsales.room.MyDatabase;
import sales.zed.com.zedsales.support.AppSingle;

/**
 * Created by root on 2/2/18.
 */

public class AddOrderAdapter extends RecyclerView.Adapter<AddOrderHolder> {

    List<AddOrderModel> listAddOrderData;
    private LayoutInflater mAddOrderInflater;
    Context context;
    CustomTextViewBold total_amount, total_qty;
    LinearLayout totallayout;
    private List<SkuListsItem> fetchSkuMasterList;
    private double totalamount = 0, totalqty = 0;

    public AddOrderAdapter(Context context, List<AddOrderModel> listAddOrderData, CustomTextViewBold total_amount, CustomTextViewBold total_qty, LinearLayout totallayout) {
        this.listAddOrderData = listAddOrderData;
        mAddOrderInflater = LayoutInflater.from(context);
        this.total_amount = total_amount;
        this.total_qty = total_qty;
        fetchSkuMasterList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchSkuMasterList();
        this.context = context;
        this.totallayout = totallayout;
    }

    public float getprice(String skuid) {
        float result = 0;
        for (int i = 0; i < fetchSkuMasterList.size(); i++) {
            if (("" + fetchSkuMasterList.get(i).getSkuId()).equalsIgnoreCase(skuid))
                result = Float.parseFloat("" + fetchSkuMasterList.get(i).getPrice());
        }
        return result;
    }

    @Override
    public int getItemCount() {
        return listAddOrderData.size();
    }

    @Override
    public void onBindViewHolder(final AddOrderHolder holder, final int position) {
        final AddOrderModel addorderdata = listAddOrderData.get(position);

        holder.mAddOrderSkuName.setText(addorderdata.getOrderskuName());
        holder.mAddOrderPrice.setText("" + getprice(listAddOrderData.get(position).getOrderskuId()) * Double.parseDouble(listAddOrderData.get(position).getOrderQuantity()));
        listAddOrderData.get(position).setOrdertotalPrice("" + getprice(listAddOrderData.get(position).getOrderskuId()) * Double.parseDouble(listAddOrderData.get(position).getOrderQuantity()));
        holder.mAddOrderQty.setText(addorderdata.getOrderQuantity());
        holder.mOrderDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeAt(position);
            }
        });
        //gettotalqty();
        holder.orderminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int count = Integer.parseInt(listAddOrderData.get(position).getOrderQuantity());
                    if (count > 1) {
                        count--;

                        holder.mAddOrderQty.setText(String.valueOf(count));
                        addorderdata.setOrdertotalPrice("" + (Float.parseFloat(addorderdata.getOrdertotalPrice()) - getprice(addorderdata.getOrderskuId())));
                        holder.mAddOrderPrice.setText("" + (Float.parseFloat(holder.mAddOrderPrice.getText().toString()) - getprice(addorderdata.getOrderskuId())));
                        listAddOrderData.get(position).setOrderQuantity("" + count);
                        gettotalqty();
                        gettotalamount();
                        notifyDataSetChanged();
                    }
                } catch (Exception e) {
                }
            }
        });
        holder.orderplus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int count = Integer.parseInt(listAddOrderData.get(position).getOrderQuantity());
                    count++;
                    addorderdata.setOrdertotalPrice("" + (Float.parseFloat(addorderdata.getOrdertotalPrice()) + Float.parseFloat(addorderdata.getOrderskuId())));
                    holder.mAddOrderQty.setText(String.valueOf(count));
                    holder.mAddOrderPrice.setText("" + (Float.parseFloat(holder.mAddOrderPrice.getText().toString()) + Float.parseFloat(addorderdata.getOrderskuId())));
                    listAddOrderData.get(position).setOrderQuantity("" + count);
                    gettotalqty();
                    gettotalamount();
                    notifyDataSetChanged();
                } catch (Exception e) {
                }
            }
        });
        gettotalqty();
        gettotalamount();
    }

    @Override
    public AddOrderHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mAddOrderInflater.inflate(R.layout.myorder_orderlist_element_list, parent, false);
        final AddOrderHolder holder = new AddOrderHolder(view);
        return holder;
    }

    public void removeAt(int position) {
        listAddOrderData.remove(position);
        gettotalqty();
        gettotalamount();
        notifyItemRemoved(position);
        notifyItemRangeChanged(position, listAddOrderData.size());
    }

    public List<AddOrderModel> getorderlist() {

        List<AddOrderModel> addOrderlist = new ArrayList<>();
        for (int i = 0; i < listAddOrderData.size(); i++) {
            AddOrderModel orderModel = new AddOrderModel();
            orderModel.setOrderskuId(listAddOrderData.get(i).getOrderskuId());
            orderModel.setOrderskuName(listAddOrderData.get(i).getOrderskuName());
            orderModel.setOrderQuantity(listAddOrderData.get(i).getOrderQuantity());
            orderModel.setOrdertotalPrice(listAddOrderData.get(i).getOrdertotalPrice());
            addOrderlist.add(orderModel);
        }
        return addOrderlist;
    }

    public void gettotalqty() {
        int total = 0;
        try {
            for (int i = 0; i < listAddOrderData.size(); i++) {
                total = total + (int) Double.parseDouble(listAddOrderData.get(i).getOrderQuantity());
            }
        } catch (Exception e) {
        }
        total_qty.setText("" + total);
        if (total > 0) {
            totallayout.setVisibility(View.VISIBLE);
        } else {
            totallayout.setVisibility(View.GONE);
        }
    }

    public void gettotalamount() {
        double total = 0;
        for (int i = 0; i < listAddOrderData.size(); i++) {
            total = total + Double.parseDouble(listAddOrderData.get(i).getOrdertotalPrice());
        }
        total_amount.setText("" + total);
    }

    public void updateData(List<AddOrderModel> viewModels) {
        if(listAddOrderData == null || listAddOrderData.size()==0)
            return;
        if (listAddOrderData != null && listAddOrderData.size()>0)
            listAddOrderData.clear();
        totallayout.setVisibility(View.GONE);
        listAddOrderData.addAll(viewModels);
        notifyDataSetChanged();
    }
}
