package sales.zed.com.zedsales.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.Toast;

import com.github.mikephil.charting.charts.PieChart;
import com.github.mikephil.charting.data.Entry;
import com.github.mikephil.charting.data.PieData;
import com.github.mikephil.charting.data.PieDataSet;
import com.github.mikephil.charting.formatter.PercentFormatter;
import com.github.mikephil.charting.utils.ColorTemplate;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.adapter.AdapterDialogListItem;
import sales.zed.com.zedsales.adapter.SaleReportAdapter;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.apiz.RequestFormater;
import sales.zed.com.zedsales.constants.AppConstants;
import sales.zed.com.zedsales.databinding.DialogListBinding;
import sales.zed.com.zedsales.databinding.FragmentGrnSearchBinding;
import sales.zed.com.zedsales.databinding.FragmentReportsMyPurchaseBinding;
import sales.zed.com.zedsales.interfaces.IDialogListClickListener;
import sales.zed.com.zedsales.models.BrandListsItem;
import sales.zed.com.zedsales.models.ForgetModel;
import sales.zed.com.zedsales.models.GRNReportList;
import sales.zed.com.zedsales.models.GRNStatusItem;
import sales.zed.com.zedsales.models.ModelListsItem;
import sales.zed.com.zedsales.models.ReportTypeItem;
import sales.zed.com.zedsales.models.SaleReportList;
import sales.zed.com.zedsales.models.SaleReportModel;
import sales.zed.com.zedsales.room.MyDatabase;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FlowOrganizer;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by root on 09/12/17.
 */

public class FragmentReportMyPurchase extends BaseFragment {

    private FragmentReportsMyPurchaseBinding binding;
    private final int INT_FROM = 0, INT_TO = 1;
    private int Date_day, Date_month, Date_year, selectedDateOption;
    private List<BrandListsItem> fetchBrandList;
    private List<ModelListsItem> fetchMasterList;
    private List<ReportTypeItem> fetchReportTypeList;
    private List<GRNStatusItem> fetchGRNStatusList;
    private List<String> listBrand, listBrandid, listModel, listModelid, listReportType = null, listGRNStatus = null;
    private int selectedBrandPos = -1, selectedModelPos = -1, selectedReportPos = -1, selectedGRNStatusPos = -1;
    private String brandid;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_reports_my_purchase, container, false);
        AppSingle.getInstance().getActivity().setTitle("GRN", true);
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("Manage GRN"));
        featchDB();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.llUpper.setVisibility(View.VISIBLE);
        binding.btnGrnProcess.setOnClickListener(listener);
        binding.ivGrnreportFrom.setOnClickListener(listener);
        binding.ivGrnreportTo.setOnClickListener(listener);
        binding.relativeGrnreportBrand.setOnClickListener(listener);
        binding.relativeGrnreportModel.setOnClickListener(listener);
        binding.relativeGrnreportData.setOnClickListener(listener);
        binding.relativeGrnreportStutes.setOnClickListener(listener);
        binding.txtReportGrn.setOnClickListener(listener);
        binding.txtReportPreviousgrn.setOnClickListener(listener);
        binding.txtGrnreportFromdate.setText(AppSingle.getfirstDate());
        binding.txtGrnreportTodate.setText(AppSingle.getCurrentDate());

    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_grn_process:
                    sendToServer(getReportTypeIndex(binding.txtGrnreportData.getText().toString()));
                    break;
                case R.id.iv_grnreport_from:
                    showDatePicker(INT_FROM);
                    break;
                case R.id.iv_grnreport_to:
                    showDatePicker(INT_TO);
                    break;
                case R.id.relative_grnreport_brand:
                    listModel = null;
                    binding.txtGrnreportBrand.setText("");
                    binding.txtGrnreportBrand.setHint("Brand");
                    binding.txtGrnreportBrand.setHintTextColor(Color.parseColor("#C9C9C9"));
                    binding.txtGrnreportModel.setText("");
                    binding.txtGrnreportModel.setHint("Model");
                    binding.txtGrnreportModel.setHintTextColor(Color.parseColor("#C9C9C9"));
                    setBrandList();
                    break;
                case R.id.relative_grnreport_model:
                    binding.txtGrnreportModel.setText("");
                    binding.txtGrnreportModel.setHint("Model");
                    binding.txtGrnreportModel.setHintTextColor(Color.parseColor("#C9C9C9"));
                    setModelList();
                    break;
                case R.id.relative_grnreport_stutes:
                    setGRNStatusList();
                    break;
                case R.id.relative_grnreport_data:
                    setReportTypeList();
                    break;
                case R.id.txt_report_grn:
                    binding.txtReportPreviousgrn.setBackgroundColor(Color.parseColor("#A39F9E"));
                    binding.txtReportGrn.setBackgroundColor(Color.parseColor("#00bfff"));
                    //FlowOrganizer.getInstance().addToInnerFrame(binding.frameGrn, new FragmentGRNSearch());
                    FlowOrganizer.getInstance().add(new FragmentGRNSearch(), true);
                    break;
                case R.id.txt_report_previousgrn:
                    binding.txtReportPreviousgrn.setBackgroundColor(Color.parseColor("#00bfff"));
                    binding.txtReportGrn.setBackgroundColor(Color.parseColor("#A39F9E"));
                    //FlowOrganizer.getInstance().addToInnerFrame(binding.frameGrn, new FragmentReportMyPurchase());
                    FlowOrganizer.getInstance().add(new FragmentReportMyPurchase(), true);
                    break;
            }
        }
    };

    public void sendToServer(int type) {

        switch (type) {
            case 0:
                callAsyncGraph();
                break;
            case 1:
                callAsync();
                break;
            case 2:
                callAsyncmailto();
                break;
        }
    }

    public int getReportTypeIndex(String name) {
        int index = 0;
        for (int i = 0; i < AppConstants.IPrefConstant.reporttype.length; i++) {
            if (AppConstants.IPrefConstant.reporttype[i].equalsIgnoreCase(name.trim())) {
                index = i;
            }
        }
        return index;
    }

    private void setBrandList() {

        if (listBrand == null) {
            listBrand = new ArrayList<>();
            listBrandid = new ArrayList<>();
            for (BrandListsItem data : fetchBrandList) {
                if (data.isStatus()) {
                    listBrand.add(data.getBrandName());
                    listBrandid.add("" + data.getBrandId());
                }

            }
        }
        openListDialog("Select Brand", selectedBrandPos, listBrand, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedBrandPos = selectedPos;
                    binding.txtGrnreportBrand.setText(listBrand.get(selectedPos));
                    binding.txtGrnreportBrand.setHint(listBrandid.get(selectedPos));
                    binding.txtGrnreportBrand.setHintTextColor(Color.parseColor("#FFFFFF"));
                } catch (Exception e) {
                }
            }
        });

    }

    private void setReportTypeList() {

        if (listReportType == null) {
            listReportType = new ArrayList<>();
            for (ReportTypeItem data : fetchReportTypeList) {
                if (data.getStatus().equalsIgnoreCase("1")) {
                    listReportType.add(data.getReportName());
                }

            }
        }
        openListDialog("Select Data", selectedReportPos, listReportType, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedReportPos = selectedPos;
                    binding.txtGrnreportData.setText(listReportType.get(selectedPos));
                } catch (Exception e) {
                }
            }
        });

    }

    private void setGRNStatusList() {

        if (listGRNStatus == null) {
            listGRNStatus = new ArrayList<>();
            for (GRNStatusItem data : fetchGRNStatusList) {
                if (data.getStatus().equalsIgnoreCase("1")) {
                    listGRNStatus.add(data.getStatusName());
                }

            }
        }
        openListDialog("Select Status", selectedGRNStatusPos, listGRNStatus, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedGRNStatusPos = selectedPos;
                    binding.txtGrnreportStutes.setText(listGRNStatus.get(selectedPos));
                } catch (Exception e) {
                }
            }
        });

    }

    private void setModelList() {
        if (listModel == null) {
            listModel = new ArrayList<>();
            listModelid = new ArrayList<>();
            for (ModelListsItem data : fetchMasterList) {
                if (data.isStatus()) {
                    if ((""+data.getBrandId()).equalsIgnoreCase(binding.txtGrnreportBrand.getHint().toString())) {
                        listModel.add(data.getModelName());
                        listModelid.add("" + data.getModelId());
                    }
                }
            }
        }
        openListDialog("Select Model", selectedModelPos, listModel, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedModelPos = selectedPos;
                    binding.txtGrnreportModel.setText(listModel.get(selectedPos));
                    binding.txtGrnreportModel.setHint(listModelid.get(selectedPos));
                    binding.txtGrnreportModel.setHintTextColor(Color.parseColor("#FFFFFF"));
                } catch (Exception e) {
                }
            }
        });

    }

    protected void openListDialog(String title, int selectedPos, final List<String> listData, final IDialogListClickListener listener) {
        DialogListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(AppSingle.getInstance().getActivity().getApplicationContext()), R.layout.dialog_list, null, false);
        final Dialog dialog = new Dialog(AppSingle.getInstance().getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(binding.getRoot());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        binding.txtViewTitle.setText(title);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        LinearLayoutManager lLayout = new LinearLayoutManager(AppSingle.getInstance().getActivity());
        RecyclerView rView = binding.rvList;
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);
        AdapterDialogListItem adapter = new AdapterDialogListItem(listData, selectedPos);
        rView.setAdapter(adapter);
        adapter.registerOnItemClickListener(new AdapterDialogListItem.IonItemSelect() {
            @Override
            public void onItemSelect(int position) {
                if (listener != null)
                    listener.onClick(listData.get(position), position);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void callAsync() {
        String brand = "";
        String model = "";
        String fromdate = binding.txtGrnreportFromdate.getText().toString().trim();
        String todate = binding.txtGrnreportTodate.getText().toString().trim();
        if (!binding.txtGrnreportBrand.getHint().toString().equalsIgnoreCase("Brand")) {
            brand = binding.txtGrnreportBrand.getHint().toString();
        }
        if (!binding.txtGrnreportModel.getHint().toString().equalsIgnoreCase("Model")) {
            model = binding.txtGrnreportModel.getHint().toString();
        }
        String status = binding.txtGrnreportStutes.getText().toString().trim();
        String data = binding.txtGrnreportData.getText().toString().trim();

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        AppSingle.getInstance().getLoginUser().getAuthKey();
        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<GRNReportList> call = apiService.getPreviousGRN(AppSingle.getInstance().getLoginUser().getUserId(), RequestFormater.getgrnreport(brand, model, fromdate, todate, status, data));
        showProgessBar();
        call.enqueue(new Callback<GRNReportList>() {
            @Override
            public void onResponse(Call<GRNReportList> call, Response<GRNReportList> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null)
                        if (response.body().getPreviousGrnList() != null && response.body().getPreviousGrnList().size() > 0) {
                            binding.purchaseList.setVisibility(View.VISIBLE);
                            binding.purchaseGraph.setVisibility(View.GONE);
                            binding.llUpper.setVisibility(View.GONE);
                            SaleReportAdapter adapter = new SaleReportAdapter(AppSingle.getInstance().getActivity(), response.body().getPreviousGrnList());
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
                            binding.rvListGrnreport.setLayoutManager(mLayoutManager);
                            binding.rvListGrnreport.setItemAnimator(new DefaultItemAnimator());
                            binding.rvListGrnreport.setAdapter(adapter);
                        } else {
                            showToast("List is empty.");
                        }
                } else {
                    showResponseMessage(response.code());
                    if (response.code() == 401)
                        callAsync();
                }
            }

            @Override
            public void onFailure(Call<GRNReportList> call, Throwable t) {
                hideProgessBar();
                showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    private void callAsyncGraph() {
        String brand = "";
        String model = "";
        String fromdate = binding.txtGrnreportFromdate.getText().toString().trim();
        String todate = binding.txtGrnreportTodate.getText().toString().trim();
        if (!binding.txtGrnreportBrand.getHint().toString().equalsIgnoreCase("Brand")) {
            brand = binding.txtGrnreportBrand.getHint().toString();
        }
        if (!binding.txtGrnreportModel.getHint().toString().equalsIgnoreCase("Model")) {
            model = binding.txtGrnreportModel.getHint().toString();
        }
        String status = binding.txtGrnreportStutes.getText().toString().trim();
        String data = binding.txtGrnreportData.getText().toString().trim();

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        AppSingle.getInstance().getLoginUser().getAuthKey();
        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<GRNReportList> call = apiService.getPreviousGRN(AppSingle.getInstance().getLoginUser().getUserId(), RequestFormater.getgrnreport(brand, model, fromdate, todate, status, data));
        showProgessBar();
        call.enqueue(new Callback<GRNReportList>() {
            @Override
            public void onResponse(Call<GRNReportList> call, Response<GRNReportList> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null)
                        if (response.body().getPreviousGrnList() != null && response.body().getPreviousGrnList().size() > 0) {
                            try {
                                binding.purchaseList.setVisibility(View.GONE);
                                binding.purchaseGraph.setVisibility(View.VISIBLE);
                                binding.llUpper.setVisibility(View.GONE);
                                binding.piechart.setUsePercentValues(true);
                                PieDataSet dataSet = new PieDataSet(graphdata(response.body().getPreviousGrnList()), "Report Purchase");
                                PieData data = new PieData(graphdataname(response.body().getPreviousGrnList()), dataSet);
                                data.setValueFormatter(new PercentFormatter());
                                binding.piechart.setData(data);
                                binding.piechart.setDrawHoleEnabled(true);
                                binding.piechart.setTransparentCircleRadius(0f);
                                binding.piechart.setHoleRadius(0f);
                                dataSet.setColors(ColorTemplate.VORDIPLOM_COLORS);
                                data.setValueTextSize(13f);
                                data.setValueTextColor(Color.DKGRAY);
                                binding.piechart.animateXY(1400, 1400);
                            } catch (Exception e) {
                            }
                        } else {
                            showToast("List is empty.");
                        }
                } else {
                    showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<GRNReportList> call, Throwable t) {
                hideProgessBar();
                showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    public ArrayList<Entry> graphdata(List<SaleReportModel> saleReportModels) {
        ArrayList<Entry> data = new ArrayList<Entry>();
        try {
            int total = 0;
            for (int j = 0; j < saleReportModels.size(); j++) {
                total = total + Integer.parseInt(saleReportModels.get(j).getQuantity());
            }
            for (int i = 0; i < saleReportModels.size(); i++) {
                int qty = Integer.parseInt(saleReportModels.get(i).getQuantity());
                float percentage = (float) ((qty * 100) / total);
                data.add(new Entry(percentage, i));
            }
        } catch (Exception e) {
        }
        return data;
    }

    public ArrayList<String> graphdataname(List<SaleReportModel> saleReportModels) {
        ArrayList<String> name = new ArrayList<String>();
        try {
            for (int i = 0; i < saleReportModels.size(); i++) {
                name.add(saleReportModels.get(i).getModel());
            }
        } catch (Exception e) {
        }
        return name;
    }

    private void callAsyncmailto() {
        String brand = "";
        String model = "";
        binding.txtGrnreportFromdate.getHint().toString();
        String fromdate = binding.txtGrnreportFromdate.getText().toString();
        String todate = binding.txtGrnreportTodate.getText().toString();
        if (!binding.txtGrnreportBrand.getHint().toString().equalsIgnoreCase("Brand")) {
            brand = binding.txtGrnreportBrand.getHint().toString();
        }
        if (!binding.txtGrnreportModel.getHint().toString().equalsIgnoreCase("Model")) {
            model = binding.txtGrnreportModel.getHint().toString();
        }
        String status = binding.txtGrnreportStutes.getText().toString();
        String data = binding.txtGrnreportData.getText().toString();
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        AppSingle.getInstance().getLoginUser().getAuthKey();
        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<ForgetModel> call = apiService.getPreviousGRNMailTo(AppSingle.getInstance().getLoginUser().getUserId(), RequestFormater.getgrnreport(brand, model, fromdate, todate, status, data));
        showProgessBar();
        call.enqueue(new Callback<ForgetModel>() {
            @Override
            public void onResponse(Call<ForgetModel> call, Response<ForgetModel> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().getStatusCode() == 0) {
                            Toast.makeText(AppSingle.getInstance().getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            showToast(response.body().getMessage());
                        }
                    } else {
                        showToast("Message body id null");
                    }
                } else {
                    showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<ForgetModel> call, Throwable t) {
                hideProgessBar();
                showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    private void showDatePicker(int type) {
        try {
            selectedDateOption = type;
            resetDateToToday();
            getDateDialog(type).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetDateToToday() {
        final Calendar c1 = Calendar.getInstance();
        Date_year = c1.get(Calendar.YEAR);
        Date_month = c1.get(Calendar.MONTH);
        Date_day = c1.get(Calendar.DAY_OF_MONTH);
    }

    private DatePickerDialog getDateDialog(int type) {
        switch (type) {
            case INT_FROM:
                DatePickerDialog dialog = new DatePickerDialog(AppSingle.getInstance().getActivity(),
                        pickerListener, Date_year, Date_month, Date_day);
                try {
                    dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return dialog;
            case INT_TO:
                DatePickerDialog dialoga = new DatePickerDialog(AppSingle.getInstance().getActivity(),
                        pickerListener, Date_year, Date_month, Date_day);
                try {
                    dialoga.getDatePicker().setMaxDate(System.currentTimeMillis());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return dialoga;
            default:
                return null;
        }
    }


    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {

            Date_year = selectedYear;
            Date_month = selectedMonth;
            Date_day = selectedDay;
            Calendar calendar = Calendar.getInstance();
            calendar.set(selectedYear, selectedMonth, selectedDay);
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
            String strDate = format.format(calendar.getTime());
            try {
                switch (selectedDateOption) {
                    case INT_FROM:
                        binding.txtGrnreportFromdate.setText(strDate.trim());
                        break;
                    case INT_TO:
                        binding.txtGrnreportTodate.setText(strDate.trim());
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void featchDB() {
        fetchBrandList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchBrandList();
        fetchMasterList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchMasterList();
        fetchReportTypeList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchReportTypeList();
        fetchGRNStatusList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchGRNStatus();
    }
}
