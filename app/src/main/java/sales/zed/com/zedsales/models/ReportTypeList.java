package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 2/15/18.
 */

public class ReportTypeList {

    @SerializedName("ReportTypeMasterList")
    @Expose
    private List<ReportTypeItem> ReportTypeMasterList;

    public List<ReportTypeItem> getReportTypeMasterList() {
        return ReportTypeMasterList;
    }

    public void setReportTypeMasterList(List<ReportTypeItem> reportTypeMasterList) {
        ReportTypeMasterList = reportTypeMasterList;
    }
}
