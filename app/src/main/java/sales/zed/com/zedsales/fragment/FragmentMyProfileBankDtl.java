package sales.zed.com.zedsales.fragment;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.apiz.RequestFormater;
import sales.zed.com.zedsales.constants.AppConstants;
import sales.zed.com.zedsales.custom.CustomEditText;
import sales.zed.com.zedsales.custom.CustomTextView;
import sales.zed.com.zedsales.databinding.FragmentGrnSearchBinding;
import sales.zed.com.zedsales.databinding.FragmentMyprofileBankDtlBinding;
import sales.zed.com.zedsales.models.ForgetModel;
import sales.zed.com.zedsales.models.ProfileData;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.AppValidate;
import sales.zed.com.zedsales.support.FeatchMaster;
import sales.zed.com.zedsales.support.FlowOrganizer;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by Mukesh on 09/12/17.
 */

public class FragmentMyProfileBankDtl extends BaseFragment {

    private FragmentMyprofileBankDtlBinding binding;
    private ProfileData profileData;
    private CustomEditText et_enter_ac_name;
    private CustomEditText et_enter_ac_no;
    private CustomEditText et_enter_bankname;
    private CustomEditText et_enter_branchname;
    private CustomEditText et_enter_branchloc;
    private CustomEditText et_enter_ifsc_code;
    private CustomEditText et_enter_aadhar_no;
    private CustomEditText et_enter_pan_no;
    private CustomEditText et_enter_gstinno;
    private CustomTextView btnbankupdate;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_myprofile_bank_dtl, container, false);
        AppSingle.getInstance().getActivity().setTitle("Profile", true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("My Profile"));
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        callAsyncProfile();

        et_enter_ac_name = binding.etEnterAcName;
        et_enter_ac_no = binding.etEnterAcNo;
        et_enter_bankname = binding.etEnterBankname;
        et_enter_branchname = binding.etEnterBranchname;
        et_enter_branchloc = binding.etEnterBranchloc;
        et_enter_ifsc_code = binding.etEnterIfscCode;
        et_enter_aadhar_no = binding.etEnterAadharNo;
        et_enter_pan_no = binding.etEnterPanNo;
        et_enter_gstinno = binding.etEnterGstinno;
        //AppValidate.isValidAccount("321456")
        binding.btnbankupdate.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                String ac = et_enter_ac_no.getText().toString();
                if (ac.trim().length() > 9 && ac.trim().length() < 18) {
                    if (AppValidate.isValidIFSC(et_enter_ifsc_code.getText().toString())) {
                        callAsyncProfileUpdate();
                    } else {
                        et_enter_ifsc_code.setError("Please enter valid ifsc code.");
                    }
                } else {
                    et_enter_ac_no.setError("Please enter valid account number.");
                }
            }
        });
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {

            binding.textviewUpdatebankBankDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    binding.textviewUpdatebankBankDetail.setBackgroundColor(Color.parseColor("#00bfff"));
                    binding.textviewUpdatebankPrsnlDetail.setBackgroundColor(Color.parseColor("#A39F9E"));
                    FlowOrganizer.getInstance().add(new FragmentShowProfileBank(), true);
                    //FlowOrganizer.getInstance().addToInnerFrame(new FragmentShowProfileBank());
                }
            });

            binding.textviewUpdatebankPrsnlDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    binding.textviewUpdatebankPrsnlDetail.setBackgroundColor(Color.parseColor("#00bfff"));
                    binding.textviewUpdatebankBankDetail.setBackgroundColor(Color.parseColor("#A39F9E"));
                    FlowOrganizer.getInstance().add(new FragmentShowProfileDetails(), true);
                    //FlowOrganizer.getInstance().addToInnerFrame(new FragmentShowProfileDetails());
                }
            });
        } catch (Exception e) {
        }
//        binding.btnTextViewSubmit.setOnClickListener(listener);
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_text_view_submit:
                    // callAsync();
                    break;
            }
        }
    };

    private void callAsyncProfile() {

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<ProfileData> call = apiService.getprofiledata(AppSingle.getInstance().getLoginUser().getUserId());
        showProgessBar();
        call.enqueue(new Callback<ProfileData>() {
            @Override
            public void onResponse(Call<ProfileData> call, Response<ProfileData> response) {
                hideProgessBar();
                profileData = response.body();
                if (response.code() == 200) {
                    setProfileText(profileData);
                    setProfileenable(profileData);
                } else {
                    showResponseMessage(response.code());
                    if (response.code() == 401)
                        callAsyncProfile();
                }
            }

            @Override
            public void onFailure(Call<ProfileData> call, Throwable t) {
                hideProgessBar();
            }
        });
    }

    private void setProfileText(ProfileData dataset) {
        try {
            et_enter_ac_name.setText(dataset.getRetailerProfile().get(0).getAccountHolderName());
            et_enter_ac_no.setText(dataset.getRetailerProfile().get(0).getAccountNumber());
            et_enter_bankname.setText(dataset.getRetailerProfile().get(0).getBankName());
            et_enter_branchname.setText(dataset.getRetailerProfile().get(0).getBranchName());
            et_enter_branchloc.setText(dataset.getRetailerProfile().get(0).getBranchLocation());
            et_enter_ifsc_code.setText(dataset.getRetailerProfile().get(0).getIfscCode());
            et_enter_aadhar_no.setText(dataset.getRetailerProfile().get(0).getAadharNumber());
            et_enter_pan_no.setText(dataset.getRetailerProfile().get(0).getPanNumber());
            et_enter_gstinno.setText(dataset.getRetailerProfile().get(0).getGstinNo());
        } catch (Exception e) {
        }
    }

    private void setProfileenable(ProfileData dataseteb) {
        try {
            if (dataseteb.getEdtabledata().get(0).getAccountHolderName().equalsIgnoreCase("0")) {
                et_enter_ac_name.setEnabled(false);
                et_enter_ac_name.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            if (dataseteb.getEdtabledata().get(0).getAccountNumber().equalsIgnoreCase("0")) {
                et_enter_ac_no.setEnabled(false);
                et_enter_ac_no.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            if (dataseteb.getEdtabledata().get(0).getBankName().equalsIgnoreCase("0")) {
                et_enter_bankname.setEnabled(false);
                et_enter_bankname.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            if (dataseteb.getEdtabledata().get(0).getBranchName().equalsIgnoreCase("0")) {
                et_enter_branchname.setEnabled(false);
                et_enter_branchname.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            if (dataseteb.getEdtabledata().get(0).getBranchLocation().equalsIgnoreCase("0")) {
                et_enter_branchloc.setEnabled(false);
                et_enter_branchloc.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            if (dataseteb.getEdtabledata().get(0).getIfscCode().equalsIgnoreCase("0")) {
                et_enter_ifsc_code.setEnabled(false);
                et_enter_ifsc_code.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            if (dataseteb.getEdtabledata().get(0).getAadharNumber().equalsIgnoreCase("0")) {
                et_enter_aadhar_no.setEnabled(false);
                et_enter_aadhar_no.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            if (dataseteb.getEdtabledata().get(0).getPanNumber().equalsIgnoreCase("0")) {
                et_enter_pan_no.setEnabled(false);
                et_enter_pan_no.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
            if (dataseteb.getEdtabledata().get(0).getGstinNo().equalsIgnoreCase("0")) {
                et_enter_gstinno.setEnabled(false);
                et_enter_gstinno.setBackgroundColor(Color.parseColor("#D3D3D3"));
            }
        } catch (Exception e) {
        }
    }

    private void callAsyncProfileUpdate() {

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<ForgetModel> call = apiService.update_profile(AppSingle.getInstance().getLoginUser().getUserId(), FeatchMaster.currentDate, RequestFormater.getupdateprofile(profileData.getRetailerProfile().get(0).getCompanyName(), profileData.getRetailerProfile().get(0).getUserCode(), profileData.getRetailerProfile().get(0).getUserName(), profileData.getRetailerProfile().get(0).getRegisteredMobileNo(), profileData.getRetailerProfile().get(0).getAlternateMobileNo(), profileData.getRetailerProfile().get(0).getEmailId(), profileData.getRetailerProfile().get(0).getDateOfBirth(), profileData.getRetailerProfile().get(0).getMarried(), profileData.getRetailerProfile().get(0).getDateOfAnniversary(), profileData.getRetailerProfile().get(0).getAddress(), profileData.getRetailerProfile().get(0).getCity(), profileData.getRetailerProfile().get(0).getState(), profileData.getRetailerProfile().get(0).getPinCode(), profileData.getRetailerProfile().get(0).getCounterPotentialValue(), et_enter_ac_name.getText().toString(), et_enter_ac_no.getText().toString(), et_enter_bankname.getText().toString(), et_enter_branchname.getText().toString(), et_enter_branchloc.getText().toString(), et_enter_ifsc_code.getText().toString(), et_enter_aadhar_no.getText().toString(), et_enter_pan_no.getText().toString(), et_enter_gstinno.getText().toString()));
        showProgessBar();
        call.enqueue(new Callback<ForgetModel>() {
            @Override
            public void onResponse(Call<ForgetModel> call, Response<ForgetModel> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    FlowOrganizer.getInstance().add(new FragmentShowProfileBank(), true);
                } else {
                    showResponseMessage(response.code());
                    if (response.code() == 401)
                        callAsyncProfileUpdate();
                }
            }

            @Override
            public void onFailure(Call<ForgetModel> call, Throwable t) {
                hideProgessBar();
                showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }
}
