package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 2/14/18.
 */

public class OrderReportList {

    @SerializedName("PreviousOrderList")
    @Expose
    private List<SaleReportModel> PreviousOrderList;

    public List<SaleReportModel> getPreviousOrderList() {
        return PreviousOrderList;
    }

    public void setPreviousOrderList(List<SaleReportModel> previousOrderList) {
        PreviousOrderList = previousOrderList;
    }
}
