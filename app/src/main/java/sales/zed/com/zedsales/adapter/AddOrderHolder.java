package sales.zed.com.zedsales.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;

import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.custom.CustomEditText;
import sales.zed.com.zedsales.custom.CustomTextView;
import sales.zed.com.zedsales.custom.CustomTextViewBold;

/**
 * Created by root on 2/2/18.
 */

public class AddOrderHolder extends RecyclerView.ViewHolder {

    public CustomTextViewBold mAddOrderSkuName;
    public CustomTextViewBold mAddOrderPrice;
    public CustomTextView mAddOrderQty;
    public ImageView mOrderDelete;
    public ImageView orderminus, orderplus;
    public LinearLayout llincrement, llqty;

    public AddOrderHolder(View itemView) {
        super(itemView);

        mAddOrderSkuName = (CustomTextViewBold) itemView.findViewById(R.id.tv_addorder_skuname);
        mAddOrderPrice = (CustomTextViewBold) itemView.findViewById(R.id.tv_addorder_price);
        mAddOrderQty = (CustomTextView) itemView.findViewById(R.id.tv_addorder_qty);
        mOrderDelete = (ImageView) itemView.findViewById(R.id.iv_order_delete);
        orderminus = (ImageView) itemView.findViewById(R.id.iv_addminus);
        orderplus = (ImageView) itemView.findViewById(R.id.iv_addpluse);
        llincrement = (LinearLayout) itemView.findViewById(R.id.ll_add_plusminus);
        llqty = (LinearLayout) itemView.findViewById(R.id.ll_add_serlayout);
    }
}
