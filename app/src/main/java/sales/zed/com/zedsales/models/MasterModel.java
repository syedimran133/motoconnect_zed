package sales.zed.com.zedsales.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class MasterModel{

	@SerializedName("ModelLists")
	@Expose
	private List<ModelListsItem> modelLists;

	public void setModelLists(List<ModelListsItem> modelLists){
		this.modelLists = modelLists;
	}

	public List<ModelListsItem> getModelLists(){
		return modelLists;
	}

	@Override
 	public String toString(){
		return 
			"MasterModel{" + 
			"modelLists = '" + modelLists + '\'' + 
			"}";
		}
}