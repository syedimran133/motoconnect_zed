package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 2/2/18.
 */

public class SuggestedItems {

    @SerializedName("SkuId")
    @Expose
    private String SkuId;

    @SerializedName("SkuName")
    @Expose
    private String SkuName;

    @SerializedName("SuggestedQuantity")
    @Expose
    private String SuggestedQuantity;

    @SerializedName("Stock")
    @Expose
    private String Stock;

    @SerializedName("Price")
    @Expose
    private String price;

    @SerializedName("Selected")
    @Expose
    private boolean selected=false;

    public String getSkuId() {
        return SkuId;
    }

    public void setSkuId(String skuId) {
        SkuId = skuId;
    }

    public String getSkuName() {
        return SkuName;
    }

    public void setSkuName(String skuName) {
        SkuName = skuName;
    }

    public String getSuggestedQuantity() {
        return SuggestedQuantity;
    }

    public void setSuggestedQuantity(String suggestedQuantity) {
        SuggestedQuantity = suggestedQuantity;
    }

    public String getStock() {
        return Stock;
    }

    public void setStock(String stock) {
        Stock = stock;
    }
    public boolean isSelected() {
        return selected;
    }

    public void setSelected(boolean selected) {
        this.selected = selected;
    }

    public String getPrice() {
        return price;
    }

    public void setPrice(String price) {
        this.price = price;
    }
}
