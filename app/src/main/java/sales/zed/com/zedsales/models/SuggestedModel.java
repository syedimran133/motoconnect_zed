package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 2/2/18.
 */

public class SuggestedModel {

    @SerializedName("ItemLists")
    @Expose
    private List<SuggestedItems> ItemLists;

    public List<SuggestedItems> getItemLists() {
        return ItemLists;
    }

    public void setItemLists(List<SuggestedItems> itemLists) {
        ItemLists = itemLists;
    }
}
