package sales.zed.com.zedsales.interfaces;


/**
 * Created by Mukesh on 26-07-2017.
 */

public interface IDialogListClickListener {

    void onClick(String data, int selectedPos);
}
