package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 1/30/18.
 */

public class SaleSkuListItem {

    @SerializedName("SkuId")
    @Expose
    private String SkuId;

    @SerializedName("SkuCode")
    @Expose
    private String SkuCode;

    @SerializedName("SkuName")
    @Expose
    private String SkuName;

    @SerializedName("SkuQuantity")
    @Expose
    private String SkuQuantity;

    @SerializedName("SerialList")
    @Expose
    private List<SaleSerialListItems> SerialList;

    @SerializedName("SerialisedMode")
    @Expose
    private int serialisedMode;

    public String getSkuId() {
        return SkuId;
    }

    public void setSkuId(String skuId) {
        SkuId = skuId;
    }

    public String getSkuCode() {
        return SkuCode;
    }

    public void setSkuCode(String skuCode) {
        SkuCode = skuCode;
    }

    public String getSkuName() {
        return SkuName;
    }

    public void setSkuName(String skuName) {
        SkuName = skuName;
    }

    public String getSkuQuantity() {
        return SkuQuantity;
    }

    public void setSkuQuantity(String skuQuantity) {
        SkuQuantity = skuQuantity;
    }

    public List<SaleSerialListItems> getSerialList() {
        return SerialList;
    }

    public void setSerialList(List<SaleSerialListItems> serialList) {
        SerialList = serialList;
    }

    public int getSerialisedMode() {
        return serialisedMode;
    }

    public void setSerialisedMode(int serialisedMode) {
        this.serialisedMode = serialisedMode;
    }

}
