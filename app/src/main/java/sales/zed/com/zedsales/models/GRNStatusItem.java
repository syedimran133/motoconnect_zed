package sales.zed.com.zedsales.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 2/15/18.
 */
@Entity
public class GRNStatusItem {

    @PrimaryKey
    @SerializedName("StatusId")
    @Expose
    private int StatusId;

    @SerializedName("StatusName")
    @Expose
    private String StatusName;

    @SerializedName("Status")
    @Expose
    private String Status;

    public int getStatusId() {
        return StatusId;
    }

    public void setStatusId(int statusId) {
        StatusId = statusId;
    }

    public String getStatusName() {
        return StatusName;
    }

    public void setStatusName(String statusName) {
        StatusName = statusName;
    }

    public String getStatus() {
        return Status;
    }

    public void setStatus(String status) {
        Status = status;
    }

    @Override
    public String toString() {
        return
                "GRNStatusList{" +
                        "StatusId = '" + StatusId + '\'' +
                        ",StatusName = '" + StatusName + '\'' +
                        ",Status = '" + Status + '\'' +
                        "}";
    }
}
