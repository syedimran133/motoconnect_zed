package sales.zed.com.zedsales.support;

import android.app.IntentService;
import android.content.Context;
import android.content.Intent;
import android.os.Bundle;
import android.widget.Toast;

public class InternetService extends IntentService {

    Context context;

    public InternetService(Context context, String name) {
        super(name);
        this.context = context;
    }

    @Override
    protected void onHandleIntent(Intent intent) {
        Bundle extras = intent.getExtras();
        boolean isNetworkConnected = extras.getBoolean("isNetworkConnected");
        Toast.makeText(context, "" + isNetworkConnected, Toast.LENGTH_LONG).show();

    }

}
