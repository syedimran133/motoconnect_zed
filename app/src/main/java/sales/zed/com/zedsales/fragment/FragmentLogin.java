package sales.zed.com.zedsales.fragment;

import static android.Manifest.permission.CALL_PHONE;
import static android.Manifest.permission.CAMERA;

import android.app.AlertDialog;
import android.app.Dialog;
import android.content.DialogInterface;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.net.Uri;
import android.os.Build;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.ActivityCompat;
import android.support.v4.content.ContextCompat;
import android.text.format.DateFormat;
import android.text.method.PasswordTransformationMethod;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import org.json.JSONObject;

import java.util.ArrayList;
import java.util.Date;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.apiz.RequestFormater;
import sales.zed.com.zedsales.constants.AppConstants;
import sales.zed.com.zedsales.custom.CustomTextView;
import sales.zed.com.zedsales.custom.CustomTextViewBold;
import sales.zed.com.zedsales.databinding.FragmentLoginBinding;
import sales.zed.com.zedsales.models.MenuList;
import sales.zed.com.zedsales.models.MenuListData;
import sales.zed.com.zedsales.models.UserModel;
import sales.zed.com.zedsales.support.AppPrefrence;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.AppValidate;
import sales.zed.com.zedsales.support.FlowOrganizer;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by apple on 09/12/17.
 */

public class FragmentLogin extends BaseFragment {

    private FragmentLoginBinding binding;
    private boolean isToRember, isToShowPwd;
    private String[] perms = {"android.permission.CAMERA"};
    private int permsRequestCode = 200;
    private static final int PERMISSION_REQUEST_CODE = 200;
    private String menu = "-1";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_login, container, false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        callAsyncVersionCheck();

        /*Bundle bundle = getArguments();
        String mDocNum = bundle.getString("DocNum");
        Toast.makeText(AppSingle.getInstance().getActivity(), mDocNum, Toast.LENGTH_SHORT).show();
*/
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        ActivityCompat.requestPermissions(AppSingle.getInstance().getActivity(), perms, permsRequestCode);
        binding.btnTextViewSubmit.setOnClickListener(listener);
        binding.btnTextForgotPswd.setOnClickListener(listener);
        binding.linearRemember.setOnClickListener(listener);
        binding.checkBoxPassword.setOnClickListener(listener);
        binding.btnTextGooffline.setOnClickListener(listener);
        AppSingle.getInstance().getActivity().showToolBar(false);
        if (AppPrefrence.getInstance().isToRemember()) {
            binding.checkBoxRemember.setChecked(true);
            isToRember = true;
            binding.editTextPswd.setText(AppPrefrence.getInstance().getPasword());
            binding.editTextAccessKey.setText(AppPrefrence.getInstance().getAccessToken());
            binding.editTextName.setText(AppPrefrence.getInstance().getUserNamee());
        }

        if (!checkPermission()) {
            requestPermission();
        }

        if (getArguments() != null) {
            menu = getArguments().getString("Menu");
            if (!menu.equalsIgnoreCase("")) {
                if (!menu.equalsIgnoreCase("-1")) {
                    callAsyncAutoLogin(menu);
                }
            }
        }
    }

    protected void openConfirmDialog() {

        final Dialog dialog = new Dialog(AppSingle.getInstance().getActivity());
        dialog.setCancelable(false);
        View view = getLayoutInflater()
                .inflate(R.layout.opening_stock_popup, null);
        CustomTextViewBold text = (CustomTextViewBold) view.findViewById(R.id.txtmsgdilog);
        CustomTextView btn_cancel = (CustomTextView) view.findViewById(R.id.osdilog_btncancel);
        CustomTextView btnOk = (CustomTextView) view.findViewById(R.id.osdilog_btnproceed);
        text.setText("Are you sure to work in off line mode where you have no internet connectivity.You may be less feature in offline mode");
        btn_cancel.setText("Ignore");
        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                AppConstants.isoffline = true;
                FlowOrganizer.getInstance().add(new FragmentMenu());
                dialog.dismiss();
            }
        });
        dialog.setContentView(view);
        dialog.show();
    }

    View.OnClickListener listener = new View.OnClickListener() {

        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_text_view_submit:
                    if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
                        if (AppPrefrence.getInstance().getUserNamee().equalsIgnoreCase("") && AppPrefrence.getInstance().getPasword().equalsIgnoreCase("")) {
                            showToast("There is no internet connection there is something wrong with the proxy server");
                        } else {
                            final String userName1 = binding.editTextName.getText().toString();
                            final String pwd1 = binding.editTextPswd.getText().toString();
                            final String accessToken1 = binding.editTextAccessKey.getText().toString();
                            if (AppValidate.isValidString(userName1))
                                if (AppValidate.isValidString(pwd1))
                                    if (AppValidate.isValidString(accessToken1)) {
                                        if (userName1.equalsIgnoreCase(AppPrefrence.getInstance().getUserNamee()) && pwd1.equalsIgnoreCase(AppPrefrence.getInstance().getPasword()) && accessToken1.equalsIgnoreCase(AppPrefrence.getInstance().getAccessToken())) {
                                            openConfirmDialog();
                                        } else {
                                            showToast("Entered Username or Password Incorrect");
                                        }
                                    } else
                                        binding.editTextAccessKey.setError(AppConstants.IMessages.VALID_ACCESS);
                                else
                                    binding.editTextPswd.setError(AppConstants.IMessages.VALID_PWD);
                            else
                                binding.editTextName.setError(AppConstants.IMessages.VALID_NAME);
                            return;
                        }
                    } else {
                        callAsync();
                    }

                    break;
                case R.id.btn_text_gooffline:
                    FlowOrganizer.getInstance().add(new FragmentUserManual(),true);
                    break;
                case R.id.btn_text_forgot_pswd:
                    FlowOrganizer.getInstance().add(new FragmentForgetPassword(), true);
                    break;
                case R.id.linear_remember:
                    isToRember = !isToRember;
                    binding.checkBoxRemember.setChecked(isToRember);
                    break;
                case R.id.check_box_password:
                    isToShowPwd = !isToShowPwd;
                    binding.checkBoxPassword.setChecked(isToShowPwd);
                    switchPasswordField();
                    break;
            }
        }
    };

    private void switchPasswordField() {

        if (!isToShowPwd) {
            binding.editTextPswd.setTransformationMethod(new PasswordTransformationMethod());
        } else {
            binding.editTextPswd.setTransformationMethod(null);
        }
    }

    private void callAsync() {
        final String userName = binding.editTextName.getText().toString();
        final String pwd = binding.editTextPswd.getText().toString();
        final String accessToken = binding.editTextAccessKey.getText().toString();
        if (AppValidate.isValidString(userName))
            if (AppValidate.isValidString(pwd))
                if (AppValidate.isValidString(accessToken)) {

                    ApiInterface apiService = ApiClient.getDefaultClient().create(ApiInterface.class);
                    Call<UserModel> call = apiService.login(RequestFormater.login(userName, pwd, accessToken, AppPrefrence.getInstance().getDeviceId(), AppPrefrence.getInstance().getPushToken()));
                    showProgessBar();
                    call.enqueue(new Callback<UserModel>() {
                        @Override
                        public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                            hideProgessBar();
                            if (response.code() == 200) {
                                if (response.body().getAuthKey() != null) {
                                    if (isToRember)
                                        rememberMe(userName, pwd, accessToken);
                                    AppPrefrence.getInstance().setToken(response.body().getAuthKey());
                                    AppPrefrence.getInstance().setIsActivityExecuted(true);
                                    AppPrefrence.getInstance().setStock("");
                                    AppPrefrence.getInstance().setUserid(response.body().getUserId());
                                    AppPrefrence.getInstance().setEntityId(response.body().getEntityId());
                                    AppSingle.getInstance().setLoginUser(response.body());
                                    AppPrefrence.getInstance().setRetName(response.body().getRetName());
                                    AppPrefrence.getInstance().setLastLogin(response.body().getLastLogin());
                                    AppPrefrence.getInstance().setStock(response.body().getIsOpeningStockEntered());
                                    if (response.body().getIsOpeningStockEntered().equalsIgnoreCase("1")) {
                                        FlowOrganizer.getInstance().add(new FragmentMenu());
                                    } else {
                                        FlowOrganizer.getInstance().add(new FragmentOpeningStock(), true);
                                    }
                                } else {
                                    showToast("Entered Username or Password Incorrect");
                                }
                            } else {
                                showResponseMessage(response.code());
                            }
                        }

                        @Override
                        public void onFailure(Call<UserModel> call, Throwable t) {
                            hideProgessBar();
                            showToast("Login Failed");
                        }
                    });
                } else
                    binding.editTextAccessKey.setError(AppConstants.IMessages.VALID_ACCESS);
            else
                binding.editTextPswd.setError(AppConstants.IMessages.VALID_PWD);
        else
            binding.editTextName.setError(AppConstants.IMessages.VALID_NAME);
    }
    private void callAsyncAutoLogin(final String menupass) {
        final String userName = AppPrefrence.getInstance().getUserNamee();
        final String pwd = AppPrefrence.getInstance().getPasword();
        final String accessToken =  AppPrefrence.getInstance().getAccessToken();
        final String deviceid=AppPrefrence.getInstance().getDeviceId();
        final String pushtoken=AppPrefrence.getInstance().getPushToken();
        if (AppValidate.isValidString(userName))
            if (AppValidate.isValidString(pwd))
                if (AppValidate.isValidString(accessToken)) {

                    ApiInterface apiService = ApiClient.getDefaultClient().create(ApiInterface.class);
                    Call<UserModel> call = apiService.login(RequestFormater.login(userName, pwd, accessToken, deviceid, pushtoken));
                    showProgessBar();
                    call.enqueue(new Callback<UserModel>() {
                        @Override
                        public void onResponse(Call<UserModel> call, Response<UserModel> response) {
                            hideProgessBar();
                            if (response.code() == 200) {
                                if (response.body().getAuthKey() != null) {
                                    AppPrefrence.getInstance().setToken(response.body().getAuthKey());
                                    AppPrefrence.getInstance().setIsActivityExecuted(true);
                                    AppPrefrence.getInstance().setStock("");
                                    AppPrefrence.getInstance().setUserid(response.body().getUserId());
                                    AppPrefrence.getInstance().setEntityId(response.body().getEntityId());
                                    AppSingle.getInstance().setLoginUser(response.body());
                                    AppPrefrence.getInstance().setRetName(response.body().getRetName());
                                    AppPrefrence.getInstance().setLastLogin(response.body().getLastLogin());
                                    AppPrefrence.getInstance().setStock(response.body().getIsOpeningStockEntered());
                                    if (response.body().getIsOpeningStockEntered().equalsIgnoreCase("1")) {
                                        Bundle bundle = new Bundle();
                                        bundle.putString("Menu", menupass);
                                        FlowOrganizer.getInstance().add(new FragmentMenu(), false, bundle);
                                        //FlowOrganizer.getInstance().add(new FragmentMenu());
                                    }
                                } else {
                                    showToast("Entered Username or Password Incorrect");
                                }
                            } else {
                                showResponseMessage(response.code());
                            }
                        }

                        @Override
                        public void onFailure(Call<UserModel> call, Throwable t) {
                            hideProgessBar();
                            showToast("Login Failed");
                        }
                    });
                } else
                    binding.editTextAccessKey.setError(AppConstants.IMessages.VALID_ACCESS);
            else
                binding.editTextPswd.setError(AppConstants.IMessages.VALID_PWD);
        else
            binding.editTextName.setError(AppConstants.IMessages.VALID_NAME);
    }
    protected void openConfirmDialog(String str) {
        final Dialog dialog = new Dialog(AppSingle.getInstance().getActivity());
        dialog.setCancelable(false);
        View view = getLayoutInflater()
                .inflate(R.layout.order_success_popup, null);
        CustomTextViewBold text = (CustomTextViewBold) view.findViewById(R.id.txtmsg);
        text.setText(str);
        CustomTextView btnOk = (CustomTextView) view.findViewById(R.id.btn_order_close);
        btnOk.setText("Update");

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("market://details?id=" + "dC1MDwAAQBAJ")));
                } catch (android.content.ActivityNotFoundException anfe) {
                    startActivity(new Intent(Intent.ACTION_VIEW, Uri.parse("https://play.google.com/store/apps/details?id=" + "dC1MDwAAQBAJ")));
                }
                dialog.dismiss();
            }
        });
        dialog.setContentView(view);
        dialog.show();
    }


    private void callAsyncVersionCheck() {
        ApiInterface apiService = ApiClient.getDefaultClient().create(ApiInterface.class);
        Call<String> call = apiService.checkVersion(RequestFormater.versionCheck(AppPrefrence.getInstance().getUserNamee(), AppPrefrence.getInstance().getAccessToken(), AppPrefrence.getInstance().getPushToken()));
        call.enqueue(new Callback<String>() {
            @Override
            public void onResponse(Call<String> call, Response<String> response) {
                if (response.code() == 200) {
                    if (response.body().toString().contains("LatestAPKVersion")) {
                        try {
                            JSONObject reader = new JSONObject(response.body().toString());
                            int ver = Integer.parseInt(reader.getString("LatestAPKVersion"));
                            if (AppSingle.getInstance().getAppVersion() != ver) {
                                openConfirmDialog("This version of app is out of date. Please update the app.");
                            }
                        } catch (Exception e) {
                        }
                    }
                }
            }

            @Override
            public void onFailure(Call<String> call, Throwable t) {

            }
        });
    }

    private boolean isAvlId(int id, List<MenuListData> menuList) {
        boolean result = false;
        for (int i = 0; i < menuList.size(); i++) {
            if (menuList.get(i).getMenuId() == id) {
                result = true;
                break;
            }
        }
        return result;
    }

    private List<MenuList> isAvl(List<MenuListData> menuList) {
        List<MenuList> result = new ArrayList<>();
        List<MenuList> menus = AppSingle.getInstance().getActivity().getMenuList();
        for (int i = 0; i < menus.size(); i++) {
            if (isAvlId(menus.get(i).getMenuid(), menuList)) {
                result.add(menus.get(i));
            }
        }
        return result;
    }

    private void rememberMe(String userName, String pwd, String accessToken) {
        AppPrefrence.getInstance().setIsToRemember(true);
        AppPrefrence.getInstance().setUserName(userName);
        AppPrefrence.getInstance().setPassword(pwd);
        AppPrefrence.getInstance().setAccessToken(accessToken);
    }


    private boolean checkPermission() {
        int result = ContextCompat.checkSelfPermission(AppSingle.getInstance().getActivity(), CALL_PHONE);
        int result1 = ContextCompat.checkSelfPermission(AppSingle.getInstance().getActivity(), CAMERA);

        return result == PackageManager.PERMISSION_GRANTED && result1 == PackageManager.PERMISSION_GRANTED;
    }

    private void requestPermission() {

        ActivityCompat.requestPermissions(AppSingle.getInstance().getActivity(), new String[]{CALL_PHONE, CAMERA}, PERMISSION_REQUEST_CODE);

    }

    @Override
    public void onRequestPermissionsResult(int requestCode, String permissions[], int[] grantResults) {
        switch (requestCode) {
            case PERMISSION_REQUEST_CODE:
                if (grantResults.length > 0) {

                    boolean cal_phoneAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    boolean cameraAccepted = grantResults[1] == PackageManager.PERMISSION_GRANTED;

                    if (cal_phoneAccepted && cameraAccepted)
                        Toast.makeText(AppSingle.getInstance().getActivity(), "Permission Granted, Now you can access location data and camera.", Toast.LENGTH_SHORT).show();
                    else {
                        Toast.makeText(AppSingle.getInstance().getActivity(), "Permission Denied, You cannot access location data and camera.", Toast.LENGTH_SHORT).show();

                        if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                            if (shouldShowRequestPermissionRationale(CALL_PHONE)) {
                                showMessageOKCancel("You need to allow access to both the permissions",
                                        new DialogInterface.OnClickListener() {
                                            @Override
                                            public void onClick(DialogInterface dialog, int which) {
                                                if (Build.VERSION.SDK_INT >= Build.VERSION_CODES.M) {
                                                    requestPermissions(new String[]{CALL_PHONE, CAMERA},
                                                            PERMISSION_REQUEST_CODE);
                                                }
                                            }
                                        });
                                return;
                            }
                        }

                    }
                }


                break;
        }
    }


    private void showMessageOKCancel(String message, DialogInterface.OnClickListener okListener) {
        new AlertDialog.Builder(AppSingle.getInstance().getActivity())
                .setMessage(message)
                .setPositiveButton("OK", okListener)
                .setNegativeButton("Cancel", null)
                .create()
                .show();
    }

}

