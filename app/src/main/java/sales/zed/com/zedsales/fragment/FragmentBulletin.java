package sales.zed.com.zedsales.fragment;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.LinearLayout;
import android.widget.ScrollView;
import android.widget.TextView;

import com.google.gson.Gson;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.adapter.AdapterBulletin;
import sales.zed.com.zedsales.adapter.CurrentStockAdapter;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.custom.CustomTextView;
import sales.zed.com.zedsales.custom.CustomTextViewBold;
import sales.zed.com.zedsales.databinding.FragmentBulletinBinding;
import sales.zed.com.zedsales.models.BulletinSubCategory;
import sales.zed.com.zedsales.models.ListBulletin;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by root on 09/12/17.
 */
public class FragmentBulletin extends BaseFragment {

    FragmentBulletinBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_bulletin, container, false);
        AppSingle.getInstance().getActivity().setTitle("Bulletin", true);
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("Bulletin"));
        callAsync();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_text_view_submit:
                    callAsync();
                    break;
            }
        }
    };

    private void callAsync() {
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<ListBulletin> call = apiService.GetBulletinList(AppSingle.getInstance().getLoginUser().getUserId());
        showProgessBar();
        call.enqueue(new Callback<ListBulletin>() {
            @Override
            public void onResponse(Call<ListBulletin> call, Response<ListBulletin> response) {
                hideProgessBar();
                if (response.code() == 200)
                    if (response.body().getGetBulletinCategoryList() != null && response.body().getGetBulletinCategoryList().size() > 0)
                        for (int i = 0; i < response.body().getGetBulletinCategoryList().size(); i++)
                            addlayout(binding.linerBulletins, response.body().getGetBulletinCategoryList().get(i).getCategoryName(), response.body().getGetBulletinCategoryList().get(i).getBulletinSubCategory());
                    else showToast("List is empty");
                else {
                    showResponseMessage(response.code());
                    if (response.code() == 401) callAsync();
                }
            }

            @Override
            public void onFailure(Call<ListBulletin> call, Throwable t) {
                hideProgessBar();
                showToast("Fail");
            }
        });
    }

    public void addlayout(LinearLayout base, String cat_name, List<BulletinSubCategory> bulletinSubCategorie) {
        LayoutInflater layoutInflater = (LayoutInflater) AppSingle.getInstance().getActivity().getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(R.layout.layout_bulletin, null);
        CustomTextViewBold tvSub = (CustomTextViewBold) convertView.findViewById(R.id.txt_bulletin_cat);
        ExpandableListView expandableListView = (ExpandableListView) convertView.findViewById(R.id.elv_bulletin_subcat_list);
        tvSub.setText(cat_name);
        AdapterBulletin adapterBulletin = new AdapterBulletin(AppSingle.getInstance().getActivity(), bulletinSubCategorie);
        expandableListView.setAdapter(adapterBulletin);
        expandableListView.setOnGroupClickListener(new ExpandableListView.OnGroupClickListener() {
            @Override
            public boolean onGroupClick(ExpandableListView parent, View v, int groupPosition, long id) {
                setListViewHeight(parent, groupPosition);
                return false;
            }
        });
        base.addView(convertView);
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }

    private void setListViewHeight(ExpandableListView listView, int group) {
        ExpandableListAdapter listAdapter = (ExpandableListAdapter) listView.getExpandableListAdapter();
        int totalHeight = 0;
        int desiredWidth = View.MeasureSpec.makeMeasureSpec(listView.getWidth(), View.MeasureSpec.EXACTLY);
        for (int i = 0; i < listAdapter.getGroupCount(); i++) {
            View groupItem = listAdapter.getGroupView(i, false, null, listView);
            groupItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
            totalHeight += groupItem.getMeasuredHeight();
            if (((listView.isGroupExpanded(i)) && (i != group)) || ((!listView.isGroupExpanded(i)) && (i == group)))
                for (int j = 0; j < listAdapter.getChildrenCount(i); j++) {
                    View listItem = listAdapter.getChildView(i, j, false, null, listView);
                    listItem.measure(desiredWidth, View.MeasureSpec.UNSPECIFIED);
                    totalHeight += listItem.getMeasuredHeight();
                }
            binding.bulletinscroll.post(new Runnable() {
                public void run() {
                    binding.bulletinscroll.fullScroll(View.FOCUS_DOWN);
                }
            });
        }
        ViewGroup.LayoutParams params = listView.getLayoutParams();
        int height = totalHeight + (listView.getDividerHeight() * (listAdapter.getGroupCount() - 1));
        if (height < 10) height = 200;
        params.height = height;
        listView.setLayoutParams(params);
        listView.requestLayout();
    }
}
