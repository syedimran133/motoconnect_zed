package sales.zed.com.zedsales.support;


import android.app.Application;
import android.arch.persistence.room.Room;
import android.text.InputFilter;
import android.text.Spanned;
import android.text.format.DateFormat;
import android.widget.EditText;


import java.util.Calendar;
import java.util.Date;

import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.fragment.BaseFragment;
import sales.zed.com.zedsales.models.SerialMaster;
import sales.zed.com.zedsales.models.UserModel;
import sales.zed.com.zedsales.room.MyDatabase;


/**
 * Created by Mukesh on 12-12-2016.
 */

public class AppSingle extends Application {

    private static AppSingle _app;
    private MainActivity mainActivity;
    private boolean isAllowBack = true;
    private UserModel loginUser;
    private FeatchMaster featchMaster;
    private int appVersion;

    public AppSingle() {
    }

    public void getfeatchobj(BaseFragment baseFragment) {
        featchMaster = new FeatchMaster(baseFragment);
        featchMaster.callAsyncGRNStatus();
    }
    public static void EdFilterNumber(EditText et, int maxchar) {
        InputFilter filter = new InputFilter() {
            public CharSequence filter(CharSequence source, int start, int end,
                                       Spanned dest, int dstart, int dend) {
                for (int i = start; i < end; i++) {
                    if (!Character.isDigit(source.charAt(i))) {
                        return "";
                    }
                }
                return null;
            }
        };

        et.setFilters(new InputFilter[]{filter,
                new InputFilter.LengthFilter(maxchar)});
    }

    @Override
    public void onCreate() {
        super.onCreate();
        ReportHandler.install(this, "syedimran133@gmail.com");
    }

    /**
     * @return
     */
    public static synchronized AppSingle getInstance() {
        if (_app == null)
            synchronized (AppSingle.class) {
                if (_app == null)
                    _app = new AppSingle();

            }
        return _app;
    }


    public MainActivity getActivity() {
        return mainActivity;
    }

    public void initActivity(MainActivity mainActivity) {
        this.mainActivity = mainActivity;
    }

    public boolean isAllowBack() {
        return isAllowBack;
    }

    public void setAllowBack(boolean allowBack) {
        isAllowBack = allowBack;
    }

    public UserModel getLoginUser() {
        return loginUser;
    }

    public void setLoginUser(UserModel loginUser) {
        this.loginUser = loginUser;
    }

    public static String getCurrentDate() {
        Date d = new Date();
        CharSequence s = DateFormat.format("dd MMM yyyy ", d.getTime());
        return s.toString();
    }

    public static String getfirstDate() {
        Calendar c = Calendar.getInstance();   // this takes current date
        c.set(Calendar.DAY_OF_MONTH, 1);
        CharSequence s = DateFormat.format("dd MMM yyyy ", c.getTime());
        return s.toString();
    }

    public int getAppVersion() {
        return appVersion;
    }

    public void setAppVersion(int appVersion) {
        this.appVersion = appVersion;
    }
}
