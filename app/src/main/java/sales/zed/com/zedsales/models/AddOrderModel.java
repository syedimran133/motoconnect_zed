package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 2/2/18.
 */

public class AddOrderModel {

    @SerializedName("SkuId")
    @Expose
    private String orderskuId;

    @SerializedName("SkuName")
    @Expose
    private String orderskuName;

    @SerializedName("OrderQuantity")
    @Expose
    private String orderQuantity;

    @SerializedName("TotalPrice")
    @Expose
    private String ordertotalPrice;

    @SerializedName("SerialisedMode")
    @Expose
    private int serialisedMode;

    public String getOrderskuId() {
        return orderskuId;
    }

    public void setOrderskuId(String orderskuId) {
        this.orderskuId = orderskuId;
    }

    public String getOrderskuName() {
        return orderskuName;
    }

    public void setOrderskuName(String orderskuName) {
        this.orderskuName = orderskuName;
    }

    public String getOrderQuantity() {
        return orderQuantity;
    }

    public void setOrderQuantity(String orderQuantity) {
        this.orderQuantity = orderQuantity;
    }

    public String getOrdertotalPrice() {
        return ordertotalPrice;
    }

    public void setOrdertotalPrice(String ordertotalPrice) {
        this.ordertotalPrice = ordertotalPrice;
    }

    public int getSerialisedMode() {
        return serialisedMode;
    }

    public void setSerialisedMode(int serialisedMode) {
        this.serialisedMode = serialisedMode;
    }
}
