package sales.zed.com.zedsales.fragment;

import android.graphics.Color;
import android.os.Bundle;
import android.databinding.DataBindingUtil;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.LinearLayout;
import android.widget.TextView;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.constants.AppConstants;
import sales.zed.com.zedsales.databinding.FragmentShowProfilebankBinding;
import sales.zed.com.zedsales.models.ProfileData;
import sales.zed.com.zedsales.models.ProfileItems;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FlowOrganizer;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by root on 1/24/18.
 */

public class FragmentShowProfileBank extends BaseFragment {

    private FragmentShowProfilebankBinding binding;
    private ProfileData profileData;
    private TextView tv_enter_ac_name;
    private TextView tv_enter_ac_no;
    private TextView tv_enter_bankname;
    private TextView tv_enter_branchname;
    private TextView tv_enter_branchloc;
    private TextView tv_enter_ifsc_code;
    private TextView tv_enter_aadhar_no;
    private TextView tv_enter_pan_no;
    private TextView tv_enter_gstinno;
    private LinearLayout edit;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_show_profilebank, container, false);
        AppSingle.getInstance().getActivity().setTitle("Profile", true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("My Profile"));
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            set_ui(getprofile());
        } else {
            callAsyncProfile();
        }
        binding.lineredit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtil.isConnectivityAvailable(getActivity())) {
                    FlowOrganizer.getInstance().add(new FragmentMyProfileBankDtl(), true);
                } else {
                    showToast("You are in offline mode.");
                }
            }
        });
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            binding.textviewShowbankBankDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    binding.textviewShowbankBankDetail.setBackgroundColor(Color.parseColor("#00bfff"));
                    binding.textviewShowbankPrsnlDetail.setBackgroundColor(Color.parseColor("#A39F9E"));
                    FlowOrganizer.getInstance().add(new FragmentShowProfileBank(), true);
                }
            });

            binding.textviewShowbankPrsnlDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    binding.textviewShowbankPrsnlDetail.setBackgroundColor(Color.parseColor("#00bfff"));
                    binding.textviewShowbankBankDetail.setBackgroundColor(Color.parseColor("#A39F9E"));
                    FlowOrganizer.getInstance().add(new FragmentShowProfileDetails(), true);
                }
            });
        } catch (Exception e) {
        }
    }

    private void callAsyncProfile() {
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<ProfileData> call = apiService.getprofiledata(AppSingle.getInstance().getLoginUser().getUserId());
        showProgessBar();
        call.enqueue(new Callback<ProfileData>() {
            @Override
            public void onResponse(Call<ProfileData> call, Response<ProfileData> response) {
                hideProgessBar();
                profileData = response.body();
                try {
                    if (response.code() == 200) {
                        set_ui(profileData.getRetailerProfile().get(0));
                    } else {
                        showResponseMessage(response.code());
                        if (response.code() == 401)
                            callAsyncProfile();
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<ProfileData> call, Throwable t) {
                hideProgessBar();
                showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }

    public void set_ui(ProfileItems profileData) {
        binding.etEnterAcName.setText(profileData.getAccountHolderName());
        binding.etEnterAcNo.setText(profileData.getAccountNumber());
        binding.etEnterBankname.setText(profileData.getBankName());
        binding.etEnterBranchname.setText(profileData.getBranchName());
        binding.etEnterBranchloc.setText(profileData.getBranchLocation());
        binding.etEnterIfscCode.setText(profileData.getIfscCode());
        binding.etEnterAadharNo.setText(profileData.getAadharNumber());
        binding.etEnterPanNo.setText(profileData.getPanNumber());
        binding.etEnterGstinno.setText(profileData.getGstinNo());
    }
}
