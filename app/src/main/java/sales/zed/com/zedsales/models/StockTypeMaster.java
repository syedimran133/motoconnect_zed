package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 2/8/18.
 */

public class StockTypeMaster {

    @SerializedName("StockTypeList")
    @Expose
    private List<StockTypeItem> stockTypeList;

    public List<StockTypeItem> getStockTypeList() {
        return stockTypeList;
    }

    public void setStockTypeList(List<StockTypeItem> stockTypeList) {
        this.stockTypeList = stockTypeList;
    }

    @Override
    public String toString() {
        return
                "StockTypeList{" +
                        "stockTypeList = '" + stockTypeList + '\'' +
                        "}";
    }

}
