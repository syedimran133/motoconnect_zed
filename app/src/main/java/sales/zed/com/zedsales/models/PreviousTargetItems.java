package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 2/20/18.
 */

public class PreviousTargetItems {

    @SerializedName("Target")
    @Expose
    private String target;

    @SerializedName("TargetType")
    @Expose
    private String targetType;

    @SerializedName("TargetSubType")
    @Expose
    private String targetSubType;

    @SerializedName("Period")
    @Expose
    private String period;

    @SerializedName("TargetBasedOn")
    @Expose
    private String targetBasedOn;

    @SerializedName("SkuName")
    @Expose
    private String skuName;

    @SerializedName("ModelName")
    @Expose
    private String modelName;

    @SerializedName("TotalTarget")
    @Expose
    private String totalTarget;

    @SerializedName("TargetAchieved")
    @Expose
    private String targetAchieved;

    @SerializedName("TargetAchievedPercent")
    @Expose
    private String targetAchievedPercent;

    public String getTarget() {
        return target;
    }

    public void setTarget(String target) {
        this.target = target;
    }

    public String getTargetType() {
        return targetType;
    }

    public void setTargetType(String targetType) {
        this.targetType = targetType;
    }

    public String getTargetSubType() {
        return targetSubType;
    }

    public void setTargetSubType(String targetSubType) {
        this.targetSubType = targetSubType;
    }

    public String getPeriod() {
        return period;
    }

    public void setPeriod(String period) {
        this.period = period;
    }

    public String getTargetBasedOn() {
        return targetBasedOn;
    }

    public void setTargetBasedOn(String targetBasedOn) {
        this.targetBasedOn = targetBasedOn;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getModelName() {
        return modelName;
    }

    public void setModelName(String modelName) {
        this.modelName = modelName;
    }

    public String getTotalTarget() {
        return totalTarget;
    }

    public void setTotalTarget(String totalTarget) {
        this.totalTarget = totalTarget;
    }

    public String getTargetAchieved() {
        return targetAchieved;
    }

    public void setTargetAchieved(String targetAchieved) {
        this.targetAchieved = targetAchieved;
    }

    public String getTargetAchievedPercent() {
        return targetAchievedPercent;
    }

    public void setTargetAchievedPercent(String targetAchievedPercent) {
        this.targetAchievedPercent = targetAchievedPercent;
    }
}
