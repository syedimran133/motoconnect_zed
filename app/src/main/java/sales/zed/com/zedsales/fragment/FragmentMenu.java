package sales.zed.com.zedsales.fragment;

import android.Manifest;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.AsyncTask;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.GridLayoutManager;
import android.support.v7.widget.LinearLayoutManager;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.adapter.RVAdapterMenu;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.custom.SpacesItemDecorationRight;
import sales.zed.com.zedsales.databinding.FragmentMenuBinding;
import sales.zed.com.zedsales.models.GRNStatusItem;
import sales.zed.com.zedsales.models.MenuList;
import sales.zed.com.zedsales.models.MenuListData;
import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.models.MenuModel;
import sales.zed.com.zedsales.room.DaoAccess;
import sales.zed.com.zedsales.room.MyDatabase;
import sales.zed.com.zedsales.support.AppPrefrence;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FlowOrganizer;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by root on 09/12/17.
 */

public class FragmentMenu extends BaseFragment {

    private FragmentMenuBinding binding;
    private List<MenuList> listData;
    private MenuModel menuModel, menuModel2;
    private DaoAccess doaacc = null;
    private int menu = -1;
    RVAdapterMenu adapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_menu, container, false);
        doaacc = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().enableDrawer(true);
        AppSingle.getInstance().getActivity().showToolBar(true);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        listData = AppSingle.getInstance().getActivity().getMenuList();
        int numberOfColumns = 3;
        binding.rvList.setLayoutManager(new GridLayoutManager(AppSingle.getInstance().getActivity(), numberOfColumns));
        binding.rvList.setNestedScrollingEnabled(false);
        binding.rvList.hasFixedSize();
        binding.txtRetName.setText("Name:- " + AppPrefrence.getInstance().getRetName());
        binding.txtLastLogin.setText("Last Login:- " + AppPrefrence.getInstance().getLastLogin());
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showlist();
        } else {
            callAsync();
        }
        if (getArguments() != null) {
            String imenu = getArguments().getString("Menu");
            if (!imenu.equalsIgnoreCase("")) {
                if (!imenu.equalsIgnoreCase("-1")) {
                    menu = Integer.parseInt(imenu);
                }
            }
        }
        if (menu == -1) {
            AppSingle.getInstance().getfeatchobj(this);
        }


    }

    private boolean isAvlId(int id, List<MenuListData> menuList) {
        boolean result = false;
        for (int i = 0; i < menuList.size(); i++) {
            if (menuList.get(i).getMenuId() == id) {
                result = true;
                break;
            }
        }
        return result;
    }

    private List<MenuList> isAvl(List<MenuListData> menuList) {
        List<MenuList> result = new ArrayList<>();
        try {
            List<MenuList> menus = AppSingle.getInstance().getActivity().getMenuList();

            for (int i = 0; i < menus.size(); i++) {
                if (isAvlId(menus.get(i).getMenuid(), menuList)) {
                    result.add(menus.get(i));
                }
            }
            //Collections.reverse(result);
        } catch (Exception e) {
            //result = AppSingle.getInstance().getActivity().getMenuList();
        }
        return result;
    }

    private void callAsync() {

        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<MenuModel> call = apiService.menu(AppPrefrence.getInstance().getUserid());
        showProgessBar();
        call.enqueue(new Callback<MenuModel>() {
            @Override
            public void onResponse(Call<MenuModel> call, Response<MenuModel> response) {
                hideProgessBar();
                //List<MenuList> data=AppSingle.getInstance().getActivity().getMenuList();
                if (response.code() == 200) {
                    if (response.body() != null) {
                        try {

                            doaacc.deleteMenulistItem();
                            for (MenuListData menudatalist : response.body().getMenuList()) {
                                doaacc.insertMenu(menudatalist);
                            }
                            List<MenuList> data = isAvl(response.body().getMenuList());
                            binding.rvList.addItemDecoration(new SpacesItemDecorationRight(AppSingle.getInstance().getActivity().getResources().getDimensionPixelOffset(R.dimen._2dp)));
                            adapter = new RVAdapterMenu(AppSingle.getInstance().getActivity(), data,menu);
                            adapter.registerItemClickLisener(new RVAdapterMenu.IonItemClickListener() {
                                @Override
                                public void onItemClick(int position) {
                                    openPage(position);
                                }
                            });
                            binding.rvList.setAdapter(adapter);
                            if (MainActivity.drawerListAdapter != null)
                                MainActivity.drawerListAdapter.updatelist(data);
                            AppSingle.getInstance().getActivity().papulatelistmenu(data);

                        } catch (Exception e) {
                        }
                    }
                } else {
                    showResponseMessage(response.code());
                    if (response.code() == 401)
                        callAsync();
                    //showToast("Menu not getting loaded beacuse of slow network issue");
                }
            }

            @Override
            public void onFailure(Call<MenuModel> call, Throwable t) {
                hideProgessBar();
                //showToast("Menu not getting loaded beacuse of slow network issue");
                showlist();
            }
        });
    }

    public static void openPage(int position) {

        //AppPrefrence.getInstance().getDeviceId()
        switch (position) {
            case 0:
                FlowOrganizer.getInstance().add(new FragmentGRNSearch(), true);
                break;
            case 1:
                FlowOrganizer.getInstance().add(new FragmentMyOrderNewOrder(), true);
                break;
            case 2:
                try {
                    if (ContextCompat.checkSelfPermission(AppSingle.getInstance().getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                        FlowOrganizer.getInstance().add(new FragmentMysaleCurrent(), true);
                    } else {
                        //showToast("Permission not found.");
                    }
                } catch (Exception e) {
                    //showToast(e.toString());
                }
                break;
            case 3:
                FlowOrganizer.getInstance().add(new FragmentMyReturnsInner(), true);
                break;
            case 4:
                FlowOrganizer.getInstance().add(new FragmentSchemeMain(), true);
                //showToast("Coming Soon");
                break;
            case 5:
                FlowOrganizer.getInstance().add(new FragmentTargetList(), true);
                break;
            case 6:
                FlowOrganizer.getInstance().add(new FragmentHierarchy(), true);
                break;
            case 7:
                FlowOrganizer.getInstance().add(new FragmentQueryManagement(), true);
                //showToast("Coming Soon");
                break;
            case 8:
                FlowOrganizer.getInstance().add(new FragmentClaimPriceDrop(), true);
                //showToast("Coming Soon");
                break;
            case 9:
                FlowOrganizer.getInstance().add(new FragmentNotification(), true);
                break;
            case 10:
                FlowOrganizer.getInstance().add(new FragmentShowProfileDetails(), true);
                break;
            case 11:
                FlowOrganizer.getInstance().add(new FragmentBulletin(), true);
                break;
            case 12:
                //FlowOrganizer.getInstance().add(new FragmentQueryManagement(), true);
                //showToast("Coming Soon");
                break;
            case 13:
                FlowOrganizer.getInstance().add(new FragmentReport(), true);
                break;
            default:
               // showToast("Menu not in the list");
                break;
        }
    }

    private void showlist() {
        List<MenuList> data = isAvl(doaacc.fetchMenu());
        if (data != null) {
            if (data.size() != 0) {
                binding.rvList.addItemDecoration(new SpacesItemDecorationRight(AppSingle.getInstance().getActivity().getResources().getDimensionPixelOffset(R.dimen._2dp)));
                adapter = new RVAdapterMenu(AppSingle.getInstance().getActivity(), data,menu);
                adapter.registerItemClickLisener(new RVAdapterMenu.IonItemClickListener() {
                    @Override
                    public void onItemClick(int position) {
                        openPage(position);
                    }
                });
                binding.rvList.setAdapter(adapter);
                //binding.rvList.isShown()
                if (MainActivity.drawerListAdapter != null)
                    MainActivity.drawerListAdapter.updatelist(data);
                AppSingle.getInstance().getActivity().papulatelistmenu(data);
            }
        }
    }
}
