package sales.zed.com.zedsales.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.databinding.FragmentClaimpricedropBinding;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FlowOrganizer;

/**
 * Created by Mukesh on 09/12/17.
 */

public class FragmentClaimPriceDrop extends BaseFragment {

    private FragmentClaimpricedropBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_claimpricedrop, container, false);
        AppSingle.getInstance().getActivity().setTitle("Claim Price Drop", true);
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("Claim Price Drop"));
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.btnTxtAdd.setOnClickListener(listener);
        binding.textviewBtnsearch.setOnClickListener(listener);
        binding.txtClaimpricedrop.setOnClickListener(listener);
        binding.txtViewPreviousPricedrop.setOnClickListener(listener);
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_txt_add:
                    callAsync();
                    break;
                case R.id.txt_claimpricedrop:
                    binding.txtViewPreviousPricedrop.setBackgroundColor(getResources().getColor(R.color.bg_grey, null));
                    binding.txtClaimpricedrop.setBackgroundColor(getResources().getColor(R.color.bg_blue, null));
                    FlowOrganizer.getInstance().add(new FragmentClaimPriceDrop(), true);
                    break;
                case R.id.txt_view_previous_pricedrop:
                    binding.txtViewPreviousPricedrop.setBackgroundColor(getResources().getColor(R.color.bg_blue, null));
                    binding.txtClaimpricedrop.setBackgroundColor(getResources().getColor(R.color.bg_grey, null));
                    //FlowOrganizer.getInstance().add(new FragmentReportMySaleViewpr(), true);
                    break;
                case R.id.textview_btnsearch:

                    break;
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }

    private void callAsync() {

    }
}
