package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 1/30/18.
 */

public class SaleList {

    @SerializedName("InvoiceNumber")
    @Expose
    private String InvoiceNumber;

    @SerializedName("CustomerName")
    @Expose
    private String CustomerName;

    @SerializedName("MobileNumber")
    @Expose
    private String MobileNumber;

    @SerializedName("SkuList")
    @Expose
    private List<SaleSkuListItem> SkuList;


    public String getInvoiceNumber() {
        return InvoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        InvoiceNumber = invoiceNumber;
    }

    public String getCustomerName() {
        return CustomerName;
    }

    public void setCustomerName(String customerName) {
        CustomerName = customerName;
    }

    public String getMobileNumber() {
        return MobileNumber;
    }

    public void setMobileNumber(String mobileNumber) {
        MobileNumber = mobileNumber;
    }

    public List<SaleSkuListItem> getSkuList() {
        return SkuList;
    }

    public void setSkuList(List<SaleSkuListItem> skuList) {
        SkuList = skuList;
    }

}
