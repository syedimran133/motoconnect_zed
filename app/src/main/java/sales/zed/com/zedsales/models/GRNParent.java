package sales.zed.com.zedsales.models;

import java.util.ArrayList;
import java.util.List;

/**
 * Created by root on 1/29/18.
 */

public class GRNParent {

    public List<GRNSkuList> parents;
    /* Create an instance variable for your list of children */
    private List<String> mChildrenList;
    private String name, price, qty,serialmode,edtable;
    private boolean isselected;

    public void setName(String name) {
        this.name = name;
    }

    public void setPrice(String price) {
        this.price = price;
    }

    public void setQty(String qty) {
        this.qty = qty;
    }

    public GRNParent(List<GRNSkuList> parents,String edtable) {
        this.parents = parents;
        this.edtable=edtable;
    }

    /**
     * Your constructor and any other accessor
     * methods should go here.
     */

    public List<String> getChildObjectList() {
        return mChildrenList;
    }

    public void setChildObjectList(List<String> list) {
        mChildrenList = list;
    }


    public String getName() {
        return name;
    }

    public String getPrice() {
        return price;
    }

    public String getQty() {
        return qty;
    }

    public List<GRNParent> getlist() {
        List<GRNParent> result = new ArrayList<>();
        for (int i = 0; i < parents.size(); i++) {
            GRNParent parentinner = new GRNParent(parents,edtable);
            parentinner.setName(parents.get(i).getSkuName());
            parentinner.setPrice(parents.get(i).getSkuPrice());
            parentinner.setQty(parents.get(i).getSkuQuantity());
            parentinner.setSerialmode(parents.get(i).getSerialisedMode());
            parentinner.setEdtable(edtable);
            List<String> objectList = new ArrayList<>();
            if (parents.get(i).getSerialList().size() != 0) {
                for (int j = 0; j < parents.get(i).getSerialList().size(); j++) {
                    if (parents.get(i).getSerialList().get(j).getSerial1() != null) {
                        objectList.add(parents.get(i).getSerialList().get(j).getSerial1());
                    } else {
                       // objectList.add("");
                    }
                }
            } else {
                //objectList.add("");
            }
            parentinner.setChildObjectList(objectList);
            result.add(parentinner);
        }
        return result;
    }

    public boolean isIsselected() {
        return isselected;
    }

    public void setIsselected(boolean isselected) {
        this.isselected = isselected;
    }

    public String getSerialmode() {
        return serialmode;
    }

    public void setSerialmode(String serialmode) {
        this.serialmode = serialmode;
    }

    public String getEdtable() {
        return edtable;
    }

    public void setEdtable(String edtable) {
        this.edtable = edtable;
    }
}
