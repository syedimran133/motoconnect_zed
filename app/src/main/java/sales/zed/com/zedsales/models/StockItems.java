package sales.zed.com.zedsales.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import android.support.annotation.NonNull;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 2/7/18.
 */
@Entity
public class StockItems {

    @PrimaryKey
    @SerializedName("RetailerStockId")
    @Expose
    private int RetailerStockId;

    @SerializedName("RetailerId")
    @Expose
    private String RetailerId;

    @SerializedName("SkuId")
    @Expose
    private String SkuId;

    @SerializedName("Quantity")
    @Expose
    private String Quantity;

    @SerializedName("StockBinTypeMasterID")
    @Expose
    private String StockBinTypeMasterID;

    @SerializedName("RecordCreationDate")
    @Expose
    private String RecordCreationDate;

    @SerializedName("ModifiedOn")
    @Expose
    private String ModifiedOn;

    public int getRetailerStockId() {
        return RetailerStockId;
    }

    public void setRetailerStockId(int retailerStockId) {
        RetailerStockId = retailerStockId;
    }

    public String getRetailerId() {
        return RetailerId;
    }

    public void setRetailerId(String retailerId) {
        RetailerId = retailerId;
    }

    public String getSkuId() {
        return SkuId;
    }

    public void setSkuId(String skuId) {
        SkuId = skuId;
    }

    public String getQuantity() {
        return Quantity;
    }

    public void setQuantity(String quantity) {
        Quantity = quantity;
    }

    public String getStockBinTypeMasterID() {
        return StockBinTypeMasterID;
    }

    public void setStockBinTypeMasterID(String stockBinTypeMasterID) {
        StockBinTypeMasterID = stockBinTypeMasterID;
    }

    public String getRecordCreationDate() {
        return RecordCreationDate;
    }

    public void setRecordCreationDate(String recordCreationDate) {
        RecordCreationDate = recordCreationDate;
    }

    public String getModifiedOn() {
        return ModifiedOn;
    }

    public void setModifiedOn(String modifiedOn) {
        ModifiedOn = modifiedOn;
    }

    @Override
    public String toString() {
        return
                "RetailerStockList{" +
                        "RetailerStockId = '" + RetailerStockId + '\'' +
                        ",RetailerId = '" + RetailerId + '\'' +
                        ",SkuId = '" + SkuId + '\'' +
                        ",Quantity = '" + Quantity + '\'' +
                        ",StockBinTypeMasterID = '" + StockBinTypeMasterID + '\'' +
                        ",RecordCreationDate = '" + RecordCreationDate + '\'' +
                        ",ModifiedOn = '" + ModifiedOn + '\'' +
                        "}";
    }
}
