package sales.zed.com.zedsales.fragment;

import android.databinding.DataBindingUtil;
import android.graphics.Canvas;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.support.v7.widget.helper.ItemTouchHelper;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.adapter.RNNotificatiopn;
import sales.zed.com.zedsales.adapter.RecyclerItemTouchHelper;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.databinding.FragmentNotificationBinding;
import sales.zed.com.zedsales.models.NotificationList;
import sales.zed.com.zedsales.models.NotificationData;
import sales.zed.com.zedsales.models.SerialMaster;
import sales.zed.com.zedsales.room.MyDatabase;
import sales.zed.com.zedsales.support.AppPrefrence;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FireBaseMessagingService;
import sales.zed.com.zedsales.support.NetworkUtil;

public class FragmentNotification extends BaseFragment implements RecyclerItemTouchHelper.RecyclerItemTouchHelperListener {

    FragmentNotificationBinding binding;
    private RNNotificatiopn adapter = null;
    private List<NotificationData> data;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_notification, container, false);
        AppSingle.getInstance().getActivity().setTitle("Notification", true);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        // data = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchOffLineData();

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            data = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchNotificationData();
            adapter = new RNNotificatiopn(AppSingle.getInstance().getActivity(), data);
            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
            binding.rvListNotification.setLayoutManager(mLayoutManager);
            binding.rvListNotification.setItemAnimator(new DefaultItemAnimator());
            binding.rvListNotification.setAdapter(adapter);

        } else {
            callAsync();
        }
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback = new RecyclerItemTouchHelper(0, ItemTouchHelper.LEFT, this);
        new ItemTouchHelper(itemTouchHelperCallback).attachToRecyclerView(binding.rvListNotification);
        ItemTouchHelper.SimpleCallback itemTouchHelperCallback1 = new ItemTouchHelper.SimpleCallback(0, ItemTouchHelper.LEFT) {

            @Override
            public boolean onMove(RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, RecyclerView.ViewHolder target) {
                return false;
            }

            @Override
            public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction) {
                // Row is swiped from recycler view
                // remove it from adapter
                if (direction == ItemTouchHelper.RIGHT) {
                    Toast.makeText(getContext(), "Swiped right", Toast.LENGTH_SHORT).show();
                } else {
                    Toast.makeText(getContext(), "Swiped left", Toast.LENGTH_SHORT).show();
                }
            }

            @Override
            public void onChildDraw(Canvas c, RecyclerView recyclerView, RecyclerView.ViewHolder viewHolder, float dX, float dY, int actionState, boolean isCurrentlyActive) {
                super.onChildDraw(c, recyclerView, viewHolder, dX, dY, actionState, isCurrentlyActive);
            }
        };

        // attaching the touch helper to recycler view
        new ItemTouchHelper(itemTouchHelperCallback1).attachToRecyclerView(binding.rvListNotification);
        int count = FireBaseMessagingService.count;
        if (count > 0) {
            binding.tvunread.setText(count + " UnRead Notification");
        }

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }

    private void callAsync() {


        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<NotificationList> call = apiService.GetNotification(AppPrefrence.getInstance().getUserid());
        showProgessBar();
        call.enqueue(new Callback<NotificationList>() {
            @Override
            public void onResponse(Call<NotificationList> call, Response<NotificationList> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    if (response.body().getNotificationData() != null && response.body().getNotificationData().size() > 0) {

                        data = response.body().getNotificationData();
                        adapter = new RNNotificatiopn(AppSingle.getInstance().getActivity(), response.body().getNotificationData());
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
                        binding.rvListNotification.setLayoutManager(mLayoutManager);
                        binding.rvListNotification.setItemAnimator(new DefaultItemAnimator());
                        binding.rvListNotification.setAdapter(adapter);

                        for (NotificationData data : response.body().getNotificationData()) {
                            MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertNotificationData(data);
                        }
                    } else {
                        showToast("List is empty");
                    }
                } else {
                    showResponseMessage(response.code());
                    if (response.code() == 401)
                        callAsync();
                }
            }

            @Override
            public void onFailure(Call<NotificationList> call, Throwable t) {
                hideProgessBar();
                showToast("Fail");
            }
        });
    }

    @Override
    public void onSwiped(RecyclerView.ViewHolder viewHolder, int direction, int position) {
        if (viewHolder instanceof RNNotificatiopn.ViewHolder) {
            // get the removed item name to display it in snack bar
            String name = data.get(viewHolder.getAdapterPosition()).getTitle();

            // backup of removed item for undo purpose
            final NotificationData deletedItem = data.get(viewHolder.getAdapterPosition());
            final int deletedIndex = viewHolder.getAdapterPosition();

            // remove the item from recycler view
            adapter.removeItem(viewHolder.getAdapterPosition());

          /*  // showing snack bar with Undo option
            Snackbar snackbar = Snackbar
                    .make(coordinatorLayout, name + " removed from cart!", Snackbar.LENGTH_LONG);
            snackbar.setAction("UNDO", new View.OnClickListener() {
                @Override
                public void onClick(View view) {*/

            // undo is selected, restore the deleted item
            //adapter.restoreItem(deletedItem, deletedIndex);
                /*}
            });
            snackbar.setActionTextColor(Color.YELLOW);
            snackbar.show();*/
        }
    }
}
