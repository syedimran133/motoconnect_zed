package sales.zed.com.zedsales.fragment;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import org.json.JSONObject;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.constants.AppConstants;
import sales.zed.com.zedsales.custom.CustomEditText;
import sales.zed.com.zedsales.custom.CustomTextView;
import sales.zed.com.zedsales.databinding.FragmentShowProfiledataBinding;
import sales.zed.com.zedsales.models.ProfileData;
import sales.zed.com.zedsales.models.ProfileItems;
import sales.zed.com.zedsales.support.AppPrefrence;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FeatchMaster;
import sales.zed.com.zedsales.support.FlowOrganizer;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by root on 1/24/18.
 */

public class FragmentShowProfileDetails extends BaseFragment {

    private FragmentShowProfiledataBinding binding;
    private ProfileData profileData;
    private CustomTextView tv_enter_companyname;
    private CustomTextView tv_enter_retailercode;
    private CustomTextView tv_enter_retailername;
    private CustomTextView tv_enter_regmobno;
    private CustomTextView tv_enter_altmobno;
    private CustomTextView tv_enter_email;
    private CustomTextView tv_enter_dob;
    private CustomTextView tv_enter_doa;
    private CustomTextView tv_enter_address;
    private CustomTextView tv_married;
    private CustomTextView tv_enter_city;
    private CustomTextView tv_enter_state;
    private CustomTextView tv_enter_pincode;
    private CustomTextView tv_enter_cp_value;
    private CustomTextView tv_enter_cp_volume;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_show_profiledata, container, false);
        AppSingle.getInstance().getActivity().setTitle("Profile", true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("My Profile"));
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        if (NetworkUtil.isConnectivityAvailable(getActivity())) {
            callAsyncProfile();
        } else {
            setdata(getprofile());
        }

        binding.pedit.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (NetworkUtil.isConnectivityAvailable(getActivity())) {
                    FlowOrganizer.getInstance().add(new FragmentMyProfilePrsnlDtl(), true);
                } else {
                    showToast("You are in offline mode.");
                }
            }
        });
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            binding.textviewShowprsnlBankDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    binding.textviewShowprsnlBankDetail.setBackgroundColor(Color.parseColor("#00bfff"));
                    binding.textviewShowprsnlPrsnlDetail.setBackgroundColor(Color.parseColor("#A39F9E"));
                    FlowOrganizer.getInstance().add(new FragmentShowProfileBank(), true);
                    //FlowOrganizer.getInstance().addToInnerFrame(new FragmentShowProfileBank());
                }
            });

            binding.textviewShowprsnlPrsnlDetail.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    binding.textviewShowprsnlPrsnlDetail.setBackgroundColor(Color.parseColor("#00bfff"));
                    binding.textviewShowprsnlBankDetail.setBackgroundColor(Color.parseColor("#A39F9E"));
                    FlowOrganizer.getInstance().add(new FragmentShowProfileDetails(), true);
                    //FlowOrganizer.getInstance().addToInnerFrame(new FragmentShowProfileDetails());
                }
            });
        } catch (Exception e) {
        }
    }

    private void callAsyncProfile() {
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<ProfileData> call = apiService.getprofiledata(AppSingle.getInstance().getLoginUser().getUserId());
        showProgessBar();
        call.enqueue(new Callback<ProfileData>() {
            @Override
            public void onResponse(Call<ProfileData> call, Response<ProfileData> response) {
                hideProgessBar();
                profileData = response.body();
                AppPrefrence.getInstance().setProfile(getString(profileData.getRetailerProfile().get(0)));
                try {
                    if (response.code() == 200) {
                        setdata(profileData.getRetailerProfile().get(0));
                    } else {
                        showResponseMessage(response.code());
                        if (response.code() == 401)
                            callAsyncProfile();
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<ProfileData> call, Throwable t) {
                hideProgessBar();
                showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }

    public void setdata(ProfileItems profileData) {
        try {
            binding.tvCompanyName.setText(profileData.getCompanyName().trim());
            binding.tvRetailerCode.setText(profileData.getUserCode().trim());
            binding.tvRetailerName.setText(profileData.getUserName().trim());
            binding.tvRegMobno.setText(profileData.getRegisteredMobileNo().trim());
            binding.tvAltrntNo.setText(profileData.getAlternateMobileNo().trim());
            binding.tvEnterEmail.setText(profileData.getEmailId().trim());
            binding.tvEnterDob.setText(profileData.getDateOfBirth().trim());
            binding.tvEnterDoa.setText(profileData.getDateOfAnniversary().trim());
            binding.tvEnterAddress.setText(profileData.getAddress().trim());
            binding.tvEnterCity.setText(profileData.getCity().trim());
            binding.tvEnterState.setText(profileData.getState().trim());
            binding.tvEnterPincode.setText(profileData.getPinCode().trim());
            binding.tvEnterCpValue.setText(profileData.getCounterPotentialValue().trim());
            binding.tvEnterCpVolume.setText(profileData.getCounterPotentialVolume().trim());
            binding.tvMarried.setText(profileData.getMarried().trim());
            if (profileData.getMarried().trim().equalsIgnoreCase("No")) {
                binding.llShowDoa.setVisibility(View.GONE);
            } else {
                binding.llShowDoa.setVisibility(View.VISIBLE);
            }
        } catch (Exception e) {
        }
    }
}
