package sales.zed.com.zedsales.fragment;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.databinding.FragmentGrnSearchBinding;
import sales.zed.com.zedsales.databinding.FragmentMyreturnsViewprBinding;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FlowOrganizer;

/**
 * Created by Mukesh on 09/12/17.
 */

public class FragmentMyReturnsViewPr extends BaseFragment {

    private FragmentMyreturnsViewprBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_myreturns_viewpr, container, false);
        AppSingle.getInstance().getActivity().setTitle("My Returns", true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("My Returns"));
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.txtViewMyReturns.setOnClickListener(listener);
        binding.txtViewPreviousReturns.setOnClickListener(listener);
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            switch (view.getId()) {
                case R.id.btn_text_view_submit:
                    callAsync();
                    break;
                case R.id.txt_view_my_returns:
                    binding.txtViewPreviousReturns.setBackgroundColor(Color.parseColor("#A39F9E"));
                    binding.txtViewMyReturns.setBackgroundColor(Color.parseColor("#00bfff"));
                    FlowOrganizer.getInstance().add(new FragmentMyReturnsInner(), true);
                    break;
                case R.id.txt_view_previous_returns:
                    binding.txtViewPreviousReturns.setBackgroundColor(Color.parseColor("#00bfff"));
                    binding.txtViewMyReturns.setBackgroundColor(Color.parseColor("#A39F9E"));
                    FlowOrganizer.getInstance().add(new FragmentMyReturnsViewPr(), true);
                    break;
            }
        }
    };

    private void callAsync() {

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }
}
