package sales.zed.com.zedsales.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 2/8/18.
 */
@Entity
public class StockTypeItem {

    @SerializedName("StockBinTypeMasterId")
    @PrimaryKey
    @Expose
    private int StockBinTypeMasterId;

    @SerializedName("StockBinTypeDesc")
    @Expose
    private String StockBinTypeDesc;

    @SerializedName("StockStatusId")
    @Expose
    private String StockStatusId;

    @SerializedName("StockBinTypeCode")
    @Expose
    private String StockBinTypeCode;

    @SerializedName("IsAdjustmentAllowed")
    @Expose
    private String IsAdjustmentAllowed;

    @SerializedName("Active")
    @Expose
    private boolean Active;

    @SerializedName("StockOwner")
    @Expose
    private String StockOwner;

    @SerializedName("RecordCreationDate")
    @Expose
    private String RecordCreationDate;

    public int getStockBinTypeMasterId() {
        return StockBinTypeMasterId;
    }

    public void setStockBinTypeMasterId(int stockBinTypeMasterId) {
        StockBinTypeMasterId = stockBinTypeMasterId;
    }

    public String getStockBinTypeDesc() {
        return StockBinTypeDesc;
    }

    public void setStockBinTypeDesc(String stockBinTypeDesc) {
        StockBinTypeDesc = stockBinTypeDesc;
    }

    public String getStockStatusId() {
        return StockStatusId;
    }

    public void setStockStatusId(String stockStatusId) {
        StockStatusId = stockStatusId;
    }

    public String getStockBinTypeCode() {
        return StockBinTypeCode;
    }

    public void setStockBinTypeCode(String stockBinTypeCode) {
        StockBinTypeCode = stockBinTypeCode;
    }

    public String getIsAdjustmentAllowed() {
        return IsAdjustmentAllowed;
    }

    public void setIsAdjustmentAllowed(String isAdjustmentAllowed) {
        IsAdjustmentAllowed = isAdjustmentAllowed;
    }

    public boolean getActive() {
        return Active;
    }

    public void setActive(boolean active) {
        Active = active;
    }

    public String getStockOwner() {
        return StockOwner;
    }

    public void setStockOwner(String stockOwner) {
        StockOwner = stockOwner;
    }

    public String getRecordCreationDate() {
        return RecordCreationDate;
    }

    public void setRecordCreationDate(String recordCreationDate) {
        RecordCreationDate = recordCreationDate;
    }
    @Override
    public String toString(){
        return
                "StockTypeList{" +
                        "StockBinTypeMasterId = '" + StockBinTypeMasterId + '\'' +
                        ",StockBinTypeDesc = '" + StockBinTypeDesc + '\'' +
                        ",StockStatusId = '" + StockStatusId + '\'' +
                        ",StockBinTypeCode = '" + StockBinTypeCode + '\'' +
                        ",IsAdjustmentAllowed = '" + IsAdjustmentAllowed + '\'' +
                        ",Active = '" + Active + '\'' +
                        ",StockOwner = '" + StockOwner + '\'' +
                        ",RecordCreationDate = '" + RecordCreationDate + '\'' +
                        "}";
    }
}
