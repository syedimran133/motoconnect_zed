package sales.zed.com.zedsales.fragment;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.CompoundButton;
import android.widget.Toast;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.ArrayList;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.adapter.AdapterDialogListItem;
import sales.zed.com.zedsales.adapter.AddOrderAdapter;
import sales.zed.com.zedsales.adapter.RVAdapterGRNList;
import sales.zed.com.zedsales.adapter.SaleAdapter;
import sales.zed.com.zedsales.adapter.SuggestedAdapter;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.apiz.RequestFormater;
import sales.zed.com.zedsales.constants.AppConstants;
import sales.zed.com.zedsales.custom.CustomTextView;
import sales.zed.com.zedsales.custom.CustomTextViewBold;
import sales.zed.com.zedsales.databinding.DialogListBinding;
import sales.zed.com.zedsales.databinding.DilogOpeningStockBinding;
import sales.zed.com.zedsales.databinding.FragmentMyorderNeworderBinding;
import sales.zed.com.zedsales.interfaces.IDialogListClickListener;
import sales.zed.com.zedsales.interfaces.MyAdapterListener;
import sales.zed.com.zedsales.models.AddOrderModel;
import sales.zed.com.zedsales.models.AddOrderResponse;
import sales.zed.com.zedsales.models.BrandListsItem;
import sales.zed.com.zedsales.models.MenuModel;
import sales.zed.com.zedsales.models.ModelListsItem;
import sales.zed.com.zedsales.models.OffLineData;
import sales.zed.com.zedsales.models.ProductCategoryListsItem;
import sales.zed.com.zedsales.models.SaleSkuListItem;
import sales.zed.com.zedsales.models.SerialMaster;
import sales.zed.com.zedsales.models.SkuListsItem;
import sales.zed.com.zedsales.models.SuggestedItems;
import sales.zed.com.zedsales.models.SuggestedModel;
import sales.zed.com.zedsales.room.MyDatabase;
import sales.zed.com.zedsales.support.AppPrefrence;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FlowOrganizer;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by root on 09/12/17.
 */

public class FragmentMyOrderNewOrder extends BaseFragment {

    private List<AddOrderModel> items = new ArrayList<>();
    private FragmentMyorderNeworderBinding binding;
    private SuggestedAdapter adapter = null;
    private AddOrderAdapter add_adapter = null;
    private DilogOpeningStockBinding dilogbinding = null;
    final ArrayList<String> listproductcatid = new ArrayList<>();
    final ArrayList<String> listbrandid = new ArrayList<>();
    final ArrayList<String> listskuid = new ArrayList<>();
    final ArrayList<String> listmodelid = new ArrayList<>();
    private String brandid = "", productcatid = "", modelid = "", skuid = "";
    private int selectedBrandPos = -1, selectedProductCategoryPos = -1, selectedModelPos = -1, selectedSkuPos = -1, selectedSerialPos = -1;
    private List<String> listBrand, listPorductCategory, listModel, listSku;
    private List<BrandListsItem> fetchBrandList;
    private List<ProductCategoryListsItem> fetchProductCategoryList;
    private List<ModelListsItem> fetchMasterList;
    private List<SkuListsItem> fetchSkuMasterList;
    private List<SuggestedItems> suggestedLists;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_myorder_neworder, container, false);
        AppSingle.getInstance().getActivity().setTitle("My Order", true);
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("My Order"));

        callAsyncSuggestedOrder();
        binding.btnSuggestAdd.setOnClickListener(listener);
        binding.btnPlaceorder.setOnClickListener(listener);
        binding.textNewNeworder.setOnClickListener(listener);
        binding.textNewPreviousorder.setOnClickListener(listener);
        binding.btnUpperadd.setOnClickListener(listener);
        return binding.getRoot();
    }

    protected void openConfirmDialog(String str) {
        final Dialog dialog = new Dialog(AppSingle.getInstance().getActivity());
        dialog.setCancelable(false);
        View view = getLayoutInflater()
                .inflate(R.layout.order_success_popup, null);
        CustomTextViewBold text = (CustomTextViewBold) view.findViewById(R.id.txtmsg);
        text.setText(str);
        CustomTextView btnOk = (CustomTextView) view.findViewById(R.id.btn_order_close);

        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                FlowOrganizer.getInstance().add(new FragmentMenu());
            }
        });
        dialog.setContentView(view);
        dialog.show();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.suggestAllcheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                try {
                    adapter.selectall(isChecked, true);
                } catch (Exception e) {
                }
                //Toast.makeText(AppSingle.getInstance().getActivity(), "" + isChecked, Toast.LENGTH_LONG).show();
            }
        });
    }

    public boolean checksku(String sku, int qty) {
        boolean result = false;
        for (int i = 0; i < items.size(); i++) {
            if (items.get(i).getOrderskuName().equalsIgnoreCase(sku)) {
                int r = Integer.parseInt(items.get(i).getOrderQuantity()) + qty;
                items.get(i).setOrderQuantity("" + r);
                result = true;
                break;
            }
        }
        return result;
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_suggest_add:
                    try {
                        List<AddOrderModel> itemslist = adapter.getlist();
                        adapter.selectall(false, true);
                        binding.suggestAllcheck.setChecked(false);
                        for (int i = 0; i < itemslist.size(); i++) {
                            if (items != null) {
                                if (!checksku(itemslist.get(i).getOrderskuName(), Integer.parseInt(itemslist.get(i).getOrderQuantity())))
                                    items.add(itemslist.get(i));
                            }
                        }
                        if (items.size() != 0) {

                            add_adapter = new AddOrderAdapter(AppSingle.getInstance().getActivity(), items, binding.txtTotelAmount, binding.txtTotelQty, binding.totallayout);
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
                            binding.rvAddsuggestedlist.setLayoutManager(mLayoutManager);
                            binding.rvAddsuggestedlist.setItemAnimator(new DefaultItemAnimator());
                            binding.rvAddsuggestedlist.setAdapter(add_adapter);
                            Toast.makeText(AppSingle.getInstance().getActivity(), "Added Successfully.", Toast.LENGTH_SHORT).show();

                        } else {
                            showToast("Please select values");
                        }
                    } catch (Exception e) {
                    }
                    break;
                case R.id.btn_placeorder:
                    if (add_adapter != null) {
                        if (add_adapter.getorderlist().size() != 0) {
                            callAsyncAddOrder();
                        } else {
                            showToast("Please add your order before placing your order.");
                        }
                    } else {
                        showToast("Please add your order before placing your order.");
                    }
                    break;
                case R.id.btn_upperadd:
                    openAddDialog();
                    break;
                case R.id.text_new_neworder:
                    try {
                        binding.textNewPreviousorder.setBackgroundColor(Color.parseColor("#A39F9E"));
                        binding.textNewNeworder.setBackgroundColor(Color.parseColor("#00bfff"));
                        FlowOrganizer.getInstance().add(new FragmentMyOrderNewOrder(), true);
                        //FlowOrganizer.getInstance().addToInnerFrame(binding.frameMyorder, new FragmentMyOrderNewOrder());
                    } catch (Exception e) {
                    }
                    break;
                case R.id.text_new_previousorder:
                    try {
                        binding.textNewPreviousorder.setBackgroundColor(Color.parseColor("#00bfff"));
                        binding.textNewNeworder.setBackgroundColor(Color.parseColor("#A39F9E"));
                        FlowOrganizer.getInstance().add(new FragmentMyOrderViewpr(), true);
                        //FlowOrganizer.getInstance().addToInnerFrame(binding.frameMyorder, new FragmentMyOrderViewpr());
                    } catch (Exception e) {
                    }
                    break;
            }
        }
    };


    private String getPrice(String skuname, String qty) {
        String price = "0";
        for (int i = 0; i < fetchSkuMasterList.size(); i++) {
            if (("" + fetchSkuMasterList.get(i).getSkuName()).equalsIgnoreCase(skuname)) {
                price = "" + fetchSkuMasterList.get(i).getPrice();
            }
        }
        try {
            price = "" + Integer.parseInt(price) * Integer.parseInt(qty);
        } catch (Exception e) {
            price = "0";
        }
        return price;
    }

    private String getSkuId(String skuname) {
        String price = "";
        for (int i = 0; i < fetchSkuMasterList.size(); i++) {
            if (("" + fetchSkuMasterList.get(i).getSkuName()).equalsIgnoreCase(skuname)) {
                price = "" + fetchSkuMasterList.get(i).getSkuId();
            }
        }
        return price;
    }


    protected void openAddDialog() {
        dilogbinding = DataBindingUtil.inflate(LayoutInflater.from(AppSingle.getInstance().getActivity().getApplicationContext()), R.layout.dilog_opening_stock, null, false);
        final Dialog dialog = new Dialog(AppSingle.getInstance().getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(dilogbinding.getRoot());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        //dilogbinding.tvDilogAddstockQty.clearFocus();
        window.setAttributes(lp);
        featchDB();
        dilogbinding.dilogRelativeBrand.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setBrandList();
            }
        });

        dilogbinding.dilogRelativeProduct.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                setProductList();
            }
        });

        dilogbinding.dilogRelativeModel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!dilogbinding.txtDilogBrand.getText().toString().equalsIgnoreCase("") && !dilogbinding.txtDilogProduct.getText().toString().equalsIgnoreCase("")) {
                    setModelList();
                } else {
                    showToast("Please select Brand and Product frist.");
                }
            }
        });

        dilogbinding.dilogRelativeSku.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                if (!dilogbinding.txtDilogModel.getText().toString().equalsIgnoreCase("")) {
                    setSkuList();
                } else {
                    showToast("Please select model frist.");
                }
            }
        });
        dilogbinding.btnClose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
            }
        });
        dilogbinding.upperAdd.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                dialog.dismiss();
                if (!dilogbinding.txtDilogBrand.getText().toString().equalsIgnoreCase("") && !dilogbinding.txtDilogProduct.getText().toString().equalsIgnoreCase("") && !dilogbinding.txtDilogModel.getText().toString().equalsIgnoreCase("") && !dilogbinding.txtDilogSku.getText().toString().equalsIgnoreCase("") && !dilogbinding.tvDilogAddstockQty.getText().toString().equalsIgnoreCase("")) {
                    try {
                        if (Integer.parseInt(dilogbinding.tvDilogAddstockQty.getText().toString()) > 0) {
                            AddOrderModel addOrderModel = new AddOrderModel();
                            addOrderModel.setOrderskuId(getSkuId(dilogbinding.txtDilogSku.getText().toString()));
                            addOrderModel.setOrderskuName(dilogbinding.txtDilogSku.getText().toString());
                            addOrderModel.setOrderQuantity(dilogbinding.tvDilogAddstockQty.getText().toString());
                            addOrderModel.setOrdertotalPrice(getPrice(dilogbinding.txtDilogSku.getText().toString(), dilogbinding.tvDilogAddstockQty.getText().toString()));
                            addOrderModel.setSerialisedMode(getserialisedMode(dilogbinding.txtDilogSku.getText().toString()));
                            if (items != null) {
                                if (!checksku(addOrderModel.getOrderskuName(), Integer.parseInt(addOrderModel.getOrderQuantity())))
                                    items.add(addOrderModel);
                            }

                        } else {
                            showToast("Qty must be greater than zero");
                        }
                    } catch (Exception e) {
                        showToast("Qty must be greater than zero");
                    }
                    if (items.size() != 0) {
                        add_adapter = new AddOrderAdapter(AppSingle.getInstance().getActivity(), items, binding.txtTotelAmount, binding.txtTotelQty, binding.totallayout);
                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
                        binding.rvAddsuggestedlist.setLayoutManager(mLayoutManager);
                        binding.rvAddsuggestedlist.setItemAnimator(new DefaultItemAnimator());
                        binding.rvAddsuggestedlist.setAdapter(add_adapter);
                        Toast.makeText(AppSingle.getInstance().getActivity(), "Added Successfully.", Toast.LENGTH_SHORT).show();
                    } else {
                        showToast("Please select values");
                    }
                } else {
                    showToast("Please fill all detail.");
                }
            }
        });
        //dialog.setCancelable(true);
        //dialog.setCanceledOnTouchOutside(false);
        dialog.show();
    }

    private void setBrandList() {

        setdata(1);
        openListDialog("Select Brand", selectedBrandPos, listBrand, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedBrandPos = selectedPos;
                    dilogbinding.txtDilogBrand.setText(listBrand.get(selectedPos));
                    brandid = listbrandid.get(selectedPos);
                } catch (Exception e) {
                }
            }
        });

    }

    private void setProductList() {
        setdata(2);
        openListDialog("Select Product Category", selectedProductCategoryPos, listPorductCategory, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedProductCategoryPos = selectedPos;
                    dilogbinding.txtDilogProduct.setText(listPorductCategory.get(selectedPos));
                    productcatid = listproductcatid.get(selectedPos);

                } catch (Exception e) {
                }
            }
        });

    }

    private void setModelList() {
        setdata(3);
        openListDialog("Select Model", selectedModelPos, listModel, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedModelPos = selectedPos;
                    dilogbinding.txtDilogModel.setText(listModel.get(selectedPos));
                    modelid = listmodelid.get(selectedPos);

                } catch (Exception e) {
                }
            }
        });

    }

    private void setSkuList() {

        setdata(4);
        openListDialog("Select Product Category", selectedSkuPos, listSku, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedSkuPos = selectedPos;
                    dilogbinding.txtDilogSku.setText(listSku.get(selectedPos));
                    skuid = listskuid.get(selectedPos);

                } catch (Exception e) {
                }
            }
        });

    }

    protected void openListDialog(String title, int selectedPos, final List<String> listData, final IDialogListClickListener listener) {
        DialogListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(AppSingle.getInstance().getActivity().getApplicationContext()), R.layout.dialog_list, null, false);
        final Dialog dialog = new Dialog(AppSingle.getInstance().getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(binding.getRoot());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        binding.txtViewTitle.setText(title);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        LinearLayoutManager lLayout = new LinearLayoutManager(AppSingle.getInstance().getActivity());
        RecyclerView rView = binding.rvList;
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);
        AdapterDialogListItem adapter = new AdapterDialogListItem(listData, selectedPos);
        rView.setAdapter(adapter);
        adapter.registerOnItemClickListener(new AdapterDialogListItem.IonItemSelect() {
            @Override
            public void onItemSelect(int position) {
                if (listener != null)
                    listener.onClick(listData.get(position), position);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    public void featchDB() {
        fetchBrandList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchBrandList();
        fetchProductCategoryList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchProductCategoryList();
        fetchMasterList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchMasterList();
        fetchSkuMasterList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchSkuMasterList();
    }


    public void setdata(int code) {
        switch (code) {
            case 1:
                if (listBrand == null) {
                    listBrand = new ArrayList<>();
                    for (BrandListsItem data : fetchBrandList) {
                        if (data.isStatus()) {
                            listBrand.add(data.getBrandName());
                            listbrandid.add("" + data.getBrandId());
                        }

                    }
                }
                try {
                    listPorductCategory = null;
                    listModel = null;
                    listSku = null;
                    productcatid = "";
                    modelid = "";
                    skuid = "";
                    listproductcatid.clear();
                    listmodelid.clear();
                    listSku.clear();
                } catch (Exception e) {
                }
                dilogbinding.txtDilogProduct.setText("");
                dilogbinding.txtDilogModel.setText("");
                dilogbinding.txtDilogSku.setText("");
                break;
            case 2:
                if (listPorductCategory == null) {
                    listPorductCategory = new ArrayList<>();
                    for (ProductCategoryListsItem data : fetchProductCategoryList) {
                        if (data.isStatus()) {
                            listPorductCategory.add(data.getProductCategoryName());
                            listproductcatid.add("" + data.getProductCategoryId());
                        }
                    }
                }
                try {
                    listModel = null;
                    listSku = null;
                    modelid = "";
                    skuid = "";
                    listmodelid.clear();
                    listSku.clear();
                } catch (Exception e) {
                }
                dilogbinding.txtDilogModel.setText("");
                dilogbinding.txtDilogSku.setText("");
                break;
            case 3:
                if (listModel == null) {
                    listModel = new ArrayList<>();
                    for (ModelListsItem data : fetchMasterList) {
                        if (data.isStatus()) {
                            if ((data.getBrandId() + "").equalsIgnoreCase(brandid) && ("" + data.getProductCategoryId()).equalsIgnoreCase(productcatid)) {
                                listModel.add(data.getModelName());
                                listmodelid.add("" + data.getModelId());
                            }
                        }
                    }
                }
                try {
                    listSku = null;
                    //listSerial = null;
                    skuid = "";
                    listSku.clear();
                } catch (Exception e) {
                }
                dilogbinding.txtDilogSku.setText("");
                // binding.txtViewSerialNo.setText("");
                break;
            case 4:
                if (listSku == null) {
                    listSku = new ArrayList<>();
                    for (SkuListsItem data : fetchSkuMasterList) {
                        if (data.isStatus() && modelid.equalsIgnoreCase("" + data.getModelId())) {
                            listSku.add(data.getSkuName());
                            listskuid.add("" + data.getSkuId());
                        }
                    }
                }
                break;
        }
    }

    private void callAsyncSuggestedOrder() {
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            //showToast(NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<SuggestedModel> call = apiService.getsuggestedorder(AppPrefrence.getInstance().getUserid());
        showProgessBar();
        call.enqueue(new Callback<SuggestedModel>() {
            @Override
            public void onResponse(Call<SuggestedModel> call, Response<SuggestedModel> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    try {
                        if (response.body().getItemLists() != null && response.body().getItemLists().size() > 0) {
                            suggestedLists = response.body().getItemLists();
                            adapter = new SuggestedAdapter(AppSingle.getInstance().getActivity(), response.body().getItemLists());
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
                            binding.rvListsuggested.setLayoutManager(mLayoutManager);
                            binding.rvListsuggested.setItemAnimator(new DefaultItemAnimator());
                            binding.rvListsuggested.setAdapter(adapter);
                        } else {
                            showToast("Suggested Order List is empty.");
                        }
                    } catch (Exception e) {
                    }
                } else {
                    showResponseMessage(response.code());
                    if (response.code() == 401)
                        callAsyncSuggestedOrder();
                }
            }

            @Override
            public void onFailure(Call<SuggestedModel> call, Throwable t) {
                hideProgessBar();
                showToast("Fail");
            }
        });
    }

    private void callAsyncAddOrder() {
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast("Unable to process your request you request save offline when ever get connectivity we send it to server");
            OffLineData data = new OffLineData();
            data.setName("AddOrder");
            data.setTime(MainActivity.getCurrentTime());
            data.setType("2");
            data.setRequestData(RequestFormater.addorder(add_adapter.getorderlist()));
            data.setStatus("1");
            data.setRemark("Save in local database");
            MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertOffLineData(data);
            MainActivity.setnotification();
            List<AddOrderModel> models = new ArrayList<>();
            add_adapter.updateData(models);
            return;
        } else {
            if (!isdublicate(RequestFormater.addorder(add_adapter.getorderlist()))) {
                ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
                Call<AddOrderResponse> call = apiService.addorder(AppSingle.getInstance().getLoginUser().getUserId(), RequestFormater.addorder(add_adapter.getorderlist()));
                showProgessBar();
                call.enqueue(new Callback<AddOrderResponse>() {
                    @Override
                    public void onResponse(Call<AddOrderResponse> call, Response<AddOrderResponse> response) {
                        hideProgessBar();
                        //menuModel = response.body();
                        //showResponseMessage(response.code());
                        if (response.code() == 200 && response.body() != null) {
                            //showToast(response.body().getMessage() + ". Your Order No. " + response.body().getOrderNumber());
                            openConfirmDialog(response.body().getMessage() + ". Your Order No. " + response.body().getOrderNumber());
                            items.clear();
                            binding.rvAddsuggestedlist.invalidate();
                        } else {
                            showResponseMessage(response.code());
                            if (response.code() == 401)
                                callAsyncAddOrder();
                        }
                    }

                    @Override
                    public void onFailure(Call<AddOrderResponse> call, Throwable t) {
                        hideProgessBar();
                        showToast(AppConstants.IMessages.FAIL);
                    }
                });
            } else {
                showToast("This request already in the offline data plz accept their only");
            }
        }
    }

    public int getserialisedMode(String skuname) {
        int result = 0;
        for (int i = 0; i < fetchSkuMasterList.size(); i++) {
            if (fetchSkuMasterList.get(i).getSkuName().equalsIgnoreCase(skuname)) {
                result = fetchSkuMasterList.get(i).getSerialisedMode();
            }
        }
        return result;
    }

    private boolean isdublicate(String str) {

        boolean dublicate = false;
        try {
            List<OffLineData> data = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchOffLineData();
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).getRequestData().equalsIgnoreCase(str))
                    dublicate = true;
            }
        } catch (Exception e) {
        }
        return dublicate;
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }
}
