package sales.zed.com.zedsales.adapter;

import android.view.View;

import com.bignerdranch.expandablerecyclerview.ViewHolder.ParentViewHolder;

import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.custom.CustomTextView;

/**
 * Created by root on 1/29/18.
 */

public class GRNParentViewHolder extends ParentViewHolder {

    public CustomTextView mSkuName;
    public CustomTextView mPrice;
    public CustomTextView mQty;

    public GRNParentViewHolder(View itemView) {
        super(itemView);
        mSkuName = (CustomTextView) itemView.findViewById(R.id.tv_sku_name);
        mPrice = (CustomTextView) itemView.findViewById(R.id.tv_price);
        mQty = (CustomTextView) itemView.findViewById(R.id.tv_qty);
    }
}
