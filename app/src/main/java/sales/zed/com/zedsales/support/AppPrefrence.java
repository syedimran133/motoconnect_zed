package sales.zed.com.zedsales.support;

import android.content.Context;
import android.content.SharedPreferences;
import android.preference.PreferenceManager;

import sales.zed.com.zedsales.constants.AppConstants;


/**
 * Created by root on 23-12-2016.
 */

public class AppPrefrence implements AppConstants.IPrefConstant {

    private static AppPrefrence _appPref;
    private static Object obj = new Object();
    private SharedPreferences _prefrence;

    public static AppPrefrence getInstance() {
        if (_appPref == null) {
            synchronized (obj) {
                _appPref = new AppPrefrence();
            }
        }
        return _appPref;
    }

    public void initPrefrence(Context context) {
        if (context != null)
            _prefrence = PreferenceManager.getDefaultSharedPreferences(context);
    }

    public void clearPrefrence() {
        if (_prefrence != null)
            _prefrence.edit().clear().commit();
    }


    public String getToken() {
        if (_prefrence != null)
            return _prefrence.getString(PREF_TOKEN, "");
        return "";
    }

    public void setToken(String token) {
        if (_prefrence != null)
            _prefrence.edit().putString(PREF_TOKEN, token).commit();
    }


    public String getPushToken() {
        if (_prefrence != null)
            return _prefrence.getString(PREF_PUSH_TOKEN, "");
        return "";
    }

    public void setPushToken(String token) {
        if (_prefrence != null)
            _prefrence.edit().putString(PREF_PUSH_TOKEN, token).commit();
    }

    public String getPushStr() {
        if (_prefrence != null)
            return _prefrence.getString(PREF_PUSH_STR, "");
        return "";
    }

    public void setPushStr(String str) {
        if (_prefrence != null)
            _prefrence.edit().putString(PREF_PUSH_STR, str).commit();
    }

    public String getRetName() {
        if (_prefrence != null)
            return _prefrence.getString(PREF_RET_NAME, "");
        return "";
    }

    public void setRetName(String name) {
        if (_prefrence != null)
            _prefrence.edit().putString(PREF_RET_NAME, name).commit();
    }

    public String getLastLogin() {
        if (_prefrence != null)
            return _prefrence.getString(PREF_LAST_LOGIN, "");
        return "";
    }

    public void setLastLogin(String lastLogin) {
        if (_prefrence != null)
            _prefrence.edit().putString(PREF_LAST_LOGIN, lastLogin).commit();
    }

    public int getUserid() {
        if (_prefrence != null)
            return _prefrence.getInt(USER_ID, 0);
        return 0;
    }

    public void setUserid(int userid) {
        if (_prefrence != null)
            _prefrence.edit().putInt(USER_ID, userid).commit();
    }

    public String getEntityId() {
        if (_prefrence != null)
            return _prefrence.getString(ENTITY_ID, "");
        return "";
    }

    public void setEntityId(String entityId) {
        if (_prefrence != null)
            _prefrence.edit().putString(ENTITY_ID, entityId).commit();
    }

    public String getHierarchy() {
        if (_prefrence != null)
            return _prefrence.getString(HIERARCHY, "");
        return "";
    }

    public void setHierarchy(String hierarchy) {
        if (_prefrence != null)
            _prefrence.edit().putString(HIERARCHY, hierarchy).commit();
    }

    public String getProfile() {
        if (_prefrence != null)
            return _prefrence.getString(PROFILE, "");
        return "";
    }

    public void setProfile(String profile) {
        if (_prefrence != null)
            _prefrence.edit().putString(PROFILE, profile).commit();
    }

    public String getStock() {
        if (_prefrence != null)
            return _prefrence.getString(STOCK_FLAG, "");
        return "";
    }

    public void setStock(String stock) {
        if (_prefrence != null)
            _prefrence.edit().putString(STOCK_FLAG, stock).commit();
    }

    public String getDeviceId() {
        if (_prefrence != null)
            return _prefrence.getString(PREF_DEVICE_ID, "");
        return "";
    }

    public void setDeviceId(String deviceId) {
        if (_prefrence != null)
            _prefrence.edit().putString(PREF_DEVICE_ID, deviceId).commit();
    }


    public String getUserNamee() {
        if (_prefrence != null)
            return _prefrence.getString(PREF_INITIATOR, "");
        return "";
    }

    public void setUserName(String userName) {
        if (_prefrence != null)
            _prefrence.edit().putString(PREF_INITIATOR, userName).commit();
    }


    public String getPasword() {
        if (_prefrence != null)
            return _prefrence.getString(PREF_PIN, "");
        return "";
    }

    public void setPassword(String password) {
        if (_prefrence != null)
            _prefrence.edit().putString(PREF_PIN, password).commit();
    }


    public String getAccessToken() {
        if (_prefrence != null)
            return _prefrence.getString(PREF_ACCESS_TOKEN, "");
        return "";
    }

    public void setAccessToken(String accessToken) {
        if (_prefrence != null)
            _prefrence.edit().putString(PREF_ACCESS_TOKEN, accessToken).commit();
    }


    public boolean isToRemember() {
        if (_prefrence != null)
            return _prefrence.getBoolean(PREF_IS_TO_REMEMBER, false);
        return false;
    }

    public void setDataSync(boolean isdatasync) {
        if (_prefrence != null)
            _prefrence.edit().putBoolean(PREF_IS_DATA_SYNC, isdatasync).commit();
    }

    public boolean isDataSync() {
        if (_prefrence != null)
            return _prefrence.getBoolean(PREF_IS_DATA_SYNC, false);
        return false;
    }

    public void setIsToRemember(boolean isToRemember) {
        if (_prefrence != null)
            _prefrence.edit().putBoolean(PREF_IS_TO_REMEMBER, isToRemember).commit();
    }

    public boolean isActivityExecuted() {
        if (_prefrence != null)
            return _prefrence.getBoolean(PREF_ACTIVITY_EXECUTED, false);
        return false;
    }

    public void setIsActivityExecuted(boolean isExecuted) {
        if (_prefrence != null)
            _prefrence.edit().putBoolean(PREF_ACTIVITY_EXECUTED, isExecuted).commit();
    }

}
