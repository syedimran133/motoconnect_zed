package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 2/8/18.
 */

public class OpeningStockMain {

    @SerializedName("OpeningStockDate")
    @Expose
    private String OpeningStockDate;

    @SerializedName("SkuOpeningStockList")
    @Expose
    private List<SkuOpeningStockList> SkuOpeningStockList;

    public String getOpeningStockDate() {
        return OpeningStockDate;
    }

    public void setOpeningStockDate(String openingStockDate) {
        OpeningStockDate = openingStockDate;
    }

    public List<sales.zed.com.zedsales.models.SkuOpeningStockList> getSkuOpeningStockList() {
        return SkuOpeningStockList;
    }

    public void setSkuOpeningStockList(List<sales.zed.com.zedsales.models.SkuOpeningStockList> skuOpeningStockList) {
        SkuOpeningStockList = skuOpeningStockList;
    }

}
