package sales.zed.com.zedsales.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.apiz.RequestFormater;
import sales.zed.com.zedsales.databinding.FragmentChangePasswordBinding;
import sales.zed.com.zedsales.models.ForgetModel;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by rot on 09/12/17.
 */

public class FragmentChangePassword extends BaseFragment {

    private FragmentChangePasswordBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_change_password, container, false);
        AppSingle.getInstance().getActivity().setTitle("Change Password", true);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTitle("", true);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.btnTextViewSubmit.setOnClickListener(listener);
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_text_view_submit:
                    callAsync();
                    break;
            }
        }
    };

    private void callAsync() {

        String password = binding.editTextOld.getText().toString();
        String newpassword = binding.editTextNew.getText().toString();
        String confirmpassword = binding.editTextConfirm.getText().toString();

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<ForgetModel> call = apiService.changepass(RequestFormater.changepin("" + AppSingle.getInstance().getLoginUser().getUserName(), password, confirmpassword));
        showProgessBar();
        call.enqueue(new Callback<ForgetModel>() {
            @Override
            public void onResponse(Call<ForgetModel> call, Response<ForgetModel> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    Toast.makeText(getContext(), response.body().getMessage(), Toast.LENGTH_LONG).show();
                    AppSingle.getInstance().getActivity().onBackPressed();
                } else {
                    showResponseMessage(response.code());
                    if (response.code() == 401)
                        callAsync();
                }
            }

            @Override
            public void onFailure(Call<ForgetModel> call, Throwable t) {
                showToast("Fail");
                hideProgessBar();
            }
        });

    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }
}

