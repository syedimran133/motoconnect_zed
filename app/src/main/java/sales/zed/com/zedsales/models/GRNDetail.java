package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 1/29/18.
 */

public class GRNDetail {

    @SerializedName("SkuList")
    @Expose
    private List<GRNSkuList> grnSkuLists;
    //when it is 1 all filed edtable when 0 not edtable
    @SerializedName("ShortReceiveAllow")
    @Expose
    private String shortReceiveAllow;
    //when its 0 hide reject button when 1 show reject button
    @SerializedName("RejectAllowed")
    @Expose
    private String rejectAllowed;

    @SerializedName("InvoiceNumber")
    @Expose
    private String invoiceNumber;

    @SerializedName("InvoiceDate")
    @Expose
    private String invoiceDate;

    @SerializedName("TotalInvoiceAmount")
    @Expose
    private String totalInvoiceAmount;

    @SerializedName("SerialisedMode")
    @Expose
    private String serialisedMode;

    public List<GRNSkuList> getGrnSkuLists() {
        return grnSkuLists;
    }

    public void setGrnSkuLists(List<GRNSkuList> grnSkuLists) {
        this.grnSkuLists = grnSkuLists;
    }

    public String getShortReceiveAllow() {
        return shortReceiveAllow;
    }

    public void setShortReceiveAllow(String shortReceiveAllow) {
        this.shortReceiveAllow = shortReceiveAllow;
    }

    public String getRejectAllowed() {
        return rejectAllowed;
    }

    public void setRejectAllowed(String rejectAllowed) {
        this.rejectAllowed = rejectAllowed;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getTotalInvoiceAmount() {
        return totalInvoiceAmount;
    }

    public void setTotalInvoiceAmount(String totalInvoiceAmount) {
        this.totalInvoiceAmount = totalInvoiceAmount;
    }

    public String getSerialisedMode() {
        return serialisedMode;
    }

    public void setSerialisedMode(String serialisedMode) {
        this.serialisedMode = serialisedMode;
    }
}
