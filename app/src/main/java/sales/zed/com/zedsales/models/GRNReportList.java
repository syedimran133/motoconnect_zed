package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 2/14/18.
 */

public class GRNReportList {

    @SerializedName("PreviousGrnList")
    @Expose
    private List<SaleReportModel> previousGrnList;

    public List<SaleReportModel> getPreviousGrnList() {
        return previousGrnList;
    }

    public void setPreviousGrnList(List<SaleReportModel> previousGrnList) {
        this.previousGrnList = previousGrnList;
    }
}
