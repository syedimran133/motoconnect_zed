package sales.zed.com.zedsales.models;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BrandListModel {

    @SerializedName("BrandLists")
    @Expose

    private List<BrandListsItem> brandLists;

    public void setBrandLists(List<BrandListsItem> brandLists) {
        this.brandLists = brandLists;
    }

    public List<BrandListsItem> getBrandLists() {
        return brandLists;
    }

    @Override
    public String toString() {
        return
                "BrandListModel{" +
                        "brandLists = '" + brandLists + '\'' +
                        "}";
    }
}