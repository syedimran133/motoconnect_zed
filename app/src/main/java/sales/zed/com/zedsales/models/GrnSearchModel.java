package sales.zed.com.zedsales.models;

/**
 * Created by Mukesh on 13/12/17.
 */

        import java.util.List;
        import com.google.gson.annotations.Expose;
        import com.google.gson.annotations.SerializedName;

public class GrnSearchModel {

    @SerializedName("InvoiceId")
    @Expose
    private String invoiceId;
    @SerializedName("SaleFrom")
    @Expose
    private String saleFrom;
    @SerializedName("InvoiceNumber")
    @Expose
    private String invoiceNumber;
    @SerializedName("InvoiceDate")
    @Expose
    private String invoiceDate;
    @SerializedName("TotalQuantity")
    @Expose
    private String totalQuantity;
    @SerializedName("ShortAllowed")
    @Expose
    private String shortAllowed;
    @SerializedName("itemList")
    @Expose
    private List<ItemList> itemList = null;

    public String getInvoiceId() {
        return invoiceId;
    }

    public void setInvoiceId(String invoiceId) {
        this.invoiceId = invoiceId;
    }

    public String getSaleFrom() {
        return saleFrom;
    }

    public void setSaleFrom(String saleFrom) {
        this.saleFrom = saleFrom;
    }

    public String getInvoiceNumber() {
        return invoiceNumber;
    }

    public void setInvoiceNumber(String invoiceNumber) {
        this.invoiceNumber = invoiceNumber;
    }

    public String getInvoiceDate() {
        return invoiceDate;
    }

    public void setInvoiceDate(String invoiceDate) {
        this.invoiceDate = invoiceDate;
    }

    public String getTotalQuantity() {
        return totalQuantity;
    }

    public void setTotalQuantity(String totalQuantity) {
        this.totalQuantity = totalQuantity;
    }

    public String getShortAllowed() {
        return shortAllowed;
    }

    public void setShortAllowed(String shortAllowed) {
        this.shortAllowed = shortAllowed;
    }

    public List<ItemList> getItemList() {
        return itemList;
    }

    public void setItemList(List<ItemList> itemList) {
        this.itemList = itemList;
    }

}