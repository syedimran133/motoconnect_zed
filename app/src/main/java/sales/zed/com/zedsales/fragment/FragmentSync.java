package sales.zed.com.zedsales.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import java.util.List;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.adapter.RVOfflineData;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.constants.AppConstants;
import sales.zed.com.zedsales.databinding.FragmentSyncBinding;
import sales.zed.com.zedsales.interfaces.MyAdapterListener;
import sales.zed.com.zedsales.models.AddOrderResponse;
import sales.zed.com.zedsales.models.ForgetModel;
import sales.zed.com.zedsales.models.OffLineData;
import sales.zed.com.zedsales.models.SerialMaster;
import sales.zed.com.zedsales.models.SerialMasterList;
import sales.zed.com.zedsales.room.MyDatabase;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.NetworkUtil;

public class FragmentSync extends BaseFragment {

    private FragmentSyncBinding binding;
    private RVOfflineData adapter = null;
    private List<OffLineData> data,data2;
    private String currentDate = "";
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_sync, container, false);
        AppSingle.getInstance().getActivity().setTitle("Sync Data", true);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        data = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchOffLineData();
        data2 = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchOffLineDatawithstatus("1");
        currentDate = "01/01/2000 09:44:00";
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        adapter = new RVOfflineData(AppSingle.getInstance().getActivity(), data2, new MyAdapterListener() {
            @Override
            public void iconButtonViewOnClick(View v, int position) {
                syncdata(data.get(position));
            }
        });
        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
        binding.rvListSyncdata.setLayoutManager(mLayoutManager);
        binding.rvListSyncdata.setItemAnimator(new DefaultItemAnimator());
        binding.rvListSyncdata.setAdapter(adapter);

        binding.btnSyncall.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                for (int i = 0; i < data2.size(); i++) {
                    if (data2.get(i).getStatus().equalsIgnoreCase("1"))
                        syncdata(data2.get(i));
                }
            }
        });
        if (getCount(data) > 1) {
            binding.btnSyncall.setVisibility(View.VISIBLE);
        } else {
            binding.btnSyncall.setVisibility(View.GONE);
        }
        AppSingle.getInstance().getActivity().setTopNavBar(false);
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }

    private void syncdata(OffLineData offLineData) {

        int type = Integer.parseInt(offLineData.getType());
        switch (type) {

            case 1:
                callAsyncGRN(offLineData);
                break;
            case 2:
                callAsyncAddOrder(offLineData);
                break;
            case 3:
                callAsyncSale(offLineData);
                break;
        }

    }

    private void callAsyncGRN(final OffLineData requestdata) {

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<ForgetModel> call = apiService.savependinggrn(AppSingle.getInstance().getLoginUser().getUserId(), requestdata.getRequestData());
        showProgessBar();
        call.enqueue(new Callback<ForgetModel>() {
            @Override
            public void onResponse(Call<ForgetModel> call, Response<ForgetModel> response) {
                hideProgessBar();
                try {
                    if (response.code() == 200) {
                        if (response.body() != null) {
                            if (response.body().getStatusCode() == 0) {
                                showToast(response.body().getMessage());
                                requestdata.setRemark(response.body().getMessage());
                                requestdata.setStatus("0");
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().updateOffLineData(requestdata);
                            } else {
                                showToast(response.body().getMessage());
                                requestdata.setRemark(response.body().getMessage());
                                requestdata.setStatus("1");
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().updateOffLineData(requestdata);
                            }
                        } else {
                            showToast("Response is null");
                        }

                    } else {
                        showResponseMessage(response.code());
                        requestdata.setRemark(responseMessage(response.code()));
                        requestdata.setStatus("1");
                        MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().updateOffLineData(requestdata);

                    }
                    adapter.updateData(MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchOffLineDatawithstatus("1"));
                    MainActivity.setnotification();
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<ForgetModel> call, Throwable t) {
                hideProgessBar();
                showToast("Fail");
            }
        });
    }

    private void callAsyncAddOrder(final OffLineData requestdata) {

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<AddOrderResponse> call = apiService.addorder(AppSingle.getInstance().getLoginUser().getUserId(), requestdata.getRequestData());
        showProgessBar();
        call.enqueue(new Callback<AddOrderResponse>() {
            @Override
            public void onResponse(Call<AddOrderResponse> call, Response<AddOrderResponse> response) {
                hideProgessBar();
                if (response.code() == 200 && response.body() != null) {
                    //showToast(response.body().getMessage() + ". Your Order No. " + response.body().getOrderNumber());
                    if (response.body().getStatusCode().equalsIgnoreCase("0")) {
                        requestdata.setRemark(response.body().getMessage() + "Your Order No. " + response.body().getOrderNumber());
                        requestdata.setStatus("0");
                        MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().updateOffLineData(requestdata);
                    } else {
                        requestdata.setRemark(response.body().getMessage());
                        requestdata.setStatus("1");
                        MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().updateOffLineData(requestdata);

                    }
                } else {
                    showResponseMessage(response.code());
                    requestdata.setRemark(responseMessage(response.code()));
                    requestdata.setStatus("1");
                    MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().updateOffLineData(requestdata);

                }
                adapter.updateData(MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchOffLineDatawithstatus("1"));
                MainActivity.setnotification();
            }

            @Override
            public void onFailure(Call<AddOrderResponse> call, Throwable t) {
                hideProgessBar();
                showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    private void callAsyncSale(final OffLineData requestdata) {

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast("NO_INTERNET");
            return;
        }
        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<ForgetModel> call = apiService.saveSale(AppSingle.getInstance().getLoginUser().getUserId(), requestdata.getRequestData());
        showProgessBar();
        call.enqueue(new Callback<ForgetModel>() {
            @Override
            public void onResponse(Call<ForgetModel> call, Response<ForgetModel> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().getStatusCode() == 0) {
                            if (response.body().getMessage() != null) {
                                showToast(response.body().getMessage());
                                requestdata.setRemark(response.body().getMessage());
                                requestdata.setStatus("0");
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().updateOffLineData(requestdata);

                            } else {
                                showToast("Message not received from server.");
                                requestdata.setRemark("Message not received from server.");
                                requestdata.setStatus("0");
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().updateOffLineData(requestdata);

                            }
                        } else {
                            showToast(response.body().getMessage());
                            requestdata.setRemark(response.body().getMessage());
                            requestdata.setStatus("1");
                            MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().updateOffLineData(requestdata);

                        }
                    } else {
                        showToast("response not received from server.");
                    }
                } else {
                    showResponseMessage(response.code());
                    requestdata.setRemark(responseMessage(response.code()));
                    requestdata.setStatus("1");
                    MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().updateOffLineData(requestdata);

                }
                adapter.updateData(MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchOffLineDatawithstatus("1"));
                MainActivity.setnotification();
            }

            @Override
            public void onFailure(Call<ForgetModel> call, Throwable t) {
                hideProgessBar();
                showToast(AppConstants.IMessages.FAIL);
            }
        });
    }
    private void callAsyncSerialMaster() {
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast("NO_INTERNET");
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<SerialMasterList> call = apiService.getSerialMaster(AppSingle.getInstance().getLoginUser().getUserId(), currentDate);
        showProgessBar();
        call.enqueue(new Callback<SerialMasterList>() {
            @Override
            public void onResponse(Call<SerialMasterList> call, Response<SerialMasterList> response) {
                hideProgessBar();
                //showResponseMessage(response.code());
                if (response.code() == 200) {
                    if (response.body() != null)
                        if (response.body().getSerialLists() != null && response.body().getSerialLists().size() > 0)
                            for (SerialMaster data : response.body().getSerialLists()) {
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertSerialMaster(data);
                            }
                } else {
                    showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<SerialMasterList> call, Throwable t) {
                hideProgessBar();
                showToast(AppConstants.IMessages.FAIL);
            }
        });
    }
    private int getCount(List<OffLineData> lineData) {
        int count = 0;
        for (int i = 0; i < lineData.size(); i++) {
            if (lineData.get(i).getStatus().equalsIgnoreCase("1"))
                count++;
        }
        return count;
    }
}
