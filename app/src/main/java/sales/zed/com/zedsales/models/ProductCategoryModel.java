package sales.zed.com.zedsales.models;

import java.util.List;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ProductCategoryModel {

    @SerializedName("ProductCategoryLists")
    @Expose
    private List<ProductCategoryListsItem> productCategoryLists;

    public void setProductCategoryLists(List<ProductCategoryListsItem> productCategoryLists) {
        this.productCategoryLists = productCategoryLists;
    }

    public List<ProductCategoryListsItem> getProductCategoryLists() {
        return productCategoryLists;
    }

    @Override
    public String toString() {
        return
                "ProductCategoryModel{" +
                        "productCategoryLists = '" + productCategoryLists + '\'' +
                        "}";
    }
}