package sales.zed.com.zedsales.fragment;

import android.app.DatePickerDialog;
import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.Toast;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.adapter.AdapterDialogListItem;
import sales.zed.com.zedsales.adapter.SaleReportAdapter;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.apiz.RequestFormater;
import sales.zed.com.zedsales.constants.AppConstants;
import sales.zed.com.zedsales.databinding.DialogListBinding;
import sales.zed.com.zedsales.databinding.FragmentGrnSearchBinding;
import sales.zed.com.zedsales.databinding.FragmentMyorderViewprBinding;
import sales.zed.com.zedsales.interfaces.IDialogListClickListener;
import sales.zed.com.zedsales.models.BrandListsItem;
import sales.zed.com.zedsales.models.ForgetModel;
import sales.zed.com.zedsales.models.ModelListsItem;
import sales.zed.com.zedsales.models.OrderReportList;
import sales.zed.com.zedsales.models.ReportTypeItem;
import sales.zed.com.zedsales.models.SaleReportList;
import sales.zed.com.zedsales.room.MyDatabase;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FlowOrganizer;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by Mukesh on 09/12/17.
 */

public class FragmentMyOrderViewpr extends BaseFragment {

    private FragmentMyorderViewprBinding binding;
    private final int INT_FROM = 0, INT_TO = 1;
    private int Date_day, Date_month, Date_year, selectedDateOption;
    private List<BrandListsItem> fetchBrandList;
    private List<ModelListsItem> fetchMasterList;
    private List<ReportTypeItem> fetchReportTypeList;
    private List<String> listBrand, listBrandid, listModel, listModelid, listReportType = null;
    private int selectedBrandPos = -1, selectedModelPos = -1, selectedReportPos = -1;
    private String brandid;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_myorder_viewpr, container, false);
        AppSingle.getInstance().getActivity().setTitle("My Order", true);
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("My Order"));
        featchDB();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.llOrderUpper.setVisibility(View.VISIBLE);
        binding.btnReportOrder.setOnClickListener(listener);
        binding.ivOrderreportFrom.setOnClickListener(listener);
        binding.ivOrderreportTo.setOnClickListener(listener);
        binding.relativeOrderreportBrand.setOnClickListener(listener);
        binding.relativeOrderreportModel.setOnClickListener(listener);
        binding.relativeOrderreportData.setOnClickListener(listener);
        binding.btnReportReset.setOnClickListener(listener);
        binding.textPreviewNeworder.setOnClickListener(listener);
        binding.textPreviewPreviousorder.setOnClickListener(listener);
        binding.txtOrderreportFromdate.setText(AppSingle.getfirstDate());
        binding.txtOrderreportTodate.setText(AppSingle.getCurrentDate());
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_report_order:
                    //if(!binding.txtOrderreportData.getText().toString().equalsIgnoreCase(""))
                    sendToServer(getReportTypeIndex(binding.txtOrderreportData.getText().toString()));

                    break;
                case R.id.iv_orderreport_from:
                    showDatePicker(INT_FROM);
                    break;
                case R.id.iv_orderreport_to:
                    showDatePicker(INT_TO);
                    break;
                case R.id.btn_report_reset:
                    binding.llOrderUpper.setVisibility(View.VISIBLE);
                    binding.llRv.setVisibility(View.GONE);
                    binding.txtOrderreportFromdate.setText(AppSingle.getfirstDate());
                    binding.txtOrderreportTodate.setText(AppSingle.getCurrentDate());
                    binding.txtOrderreportBrand.setText("");
                    binding.txtOrderreportModel.setText("");
                    binding.txtOrderreportData.setText("Data");
                    break;
                case R.id.relative_orderreport_brand:
                    listModel = null;
                    binding.txtOrderreportBrand.setText("");
                    binding.txtOrderreportBrand.setHint("Brand");
                    binding.txtOrderreportBrand.setHintTextColor(Color.parseColor("#C9C9C9"));
                    binding.txtOrderreportModel.setText("");
                    binding.txtOrderreportModel.setHint("Model");
                    binding.txtOrderreportModel.setHintTextColor(Color.parseColor("#C9C9C9"));
                    setBrandList();
                    break;
                case R.id.relative_orderreport_model:
                    binding.txtOrderreportModel.setText("");
                    binding.txtOrderreportModel.setHint("Model");
                    binding.txtOrderreportModel.setHintTextColor(Color.parseColor("#C9C9C9"));
                    setModelList();
                    break;
                case R.id.relative_orderreport_data:
                    setReportTypeList();
                    break;
                case R.id.text_preview_neworder:
                    try {
                        binding.textPreviewPreviousorder.setBackgroundColor(Color.parseColor("#A39F9E"));
                        binding.textPreviewNeworder.setBackgroundColor(Color.parseColor("#00bfff"));
                        FlowOrganizer.getInstance().add(new FragmentMyOrderNewOrder(), true);
                        //FlowOrganizer.getInstance().addToInnerFrame(binding.frameMyorder, new FragmentMyOrderNewOrder());
                    } catch (Exception e) {
                    }
                    break;
                case R.id.text_preview_previousorder:
                    try {
                        binding.textPreviewPreviousorder.setBackgroundColor(Color.parseColor("#00bfff"));
                        binding.textPreviewNeworder.setBackgroundColor(Color.parseColor("#A39F9E"));
                        FlowOrganizer.getInstance().add(new FragmentMyOrderViewpr(), true);
                        //FlowOrganizer.getInstance().addToInnerFrame(binding.frameMyorder, new FragmentMyOrderViewpr());
                    } catch (Exception e) {
                    }
                    break;
            }
        }
    };

    private void setBrandList() {

        if (listBrand == null) {
            listBrand = new ArrayList<>();
            listBrandid = new ArrayList<>();
            for (BrandListsItem data : fetchBrandList) {
                if (data.isStatus()) {
                    listBrand.add(data.getBrandName());
                    listBrandid.add("" + data.getBrandId());
                }
            }
        }
        openListDialog("Select Brand", selectedBrandPos, listBrand, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedBrandPos = selectedPos;
                    binding.txtOrderreportBrand.setText(listBrand.get(selectedPos));
                    binding.txtOrderreportBrand.setHint(listBrandid.get(selectedPos));
                    binding.txtOrderreportBrand.setHintTextColor(Color.parseColor("#FFFFFF"));
                    brandid = "" + listBrandid.get(selectedPos);
                } catch (Exception e) {
                }
            }
        });

    }

    private void setReportTypeList() {

        if (listReportType == null) {
            listReportType = new ArrayList<>();
            for (ReportTypeItem data : fetchReportTypeList) {
                if (data.getStatus().equalsIgnoreCase("1")) {
                    listReportType.add(data.getReportName());
                }
            }
        }
        openListDialog("Select Data", selectedReportPos, listReportType, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedReportPos = selectedPos;
                    binding.txtOrderreportData.setText(listReportType.get(selectedPos));
                } catch (Exception e) {
                }
            }
        });

    }

    private void setModelList() {
        if (listModel == null) {
            listModel = new ArrayList<>();
            listModelid = new ArrayList<>();
            for (ModelListsItem data : fetchMasterList) {
                if (data.isStatus()) {
                    if ((""+data.getBrandId()).equalsIgnoreCase(brandid)) {
                        listModel.add(data.getModelName());
                        listModelid.add("" + data.getModelId());
                    }
                }
            }
        }
        openListDialog("Select Model", selectedModelPos, listModel, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedModelPos = selectedPos;
                    binding.txtOrderreportModel.setText(listModel.get(selectedPos));
                    binding.txtOrderreportModel.setHint(listModelid.get(selectedPos));
                    binding.txtOrderreportModel.setHintTextColor(Color.parseColor("#FFFFFF"));
                } catch (Exception e) {
                }
            }
        });

    }

    protected void openListDialog(String title, int selectedPos, final List<String> listData, final IDialogListClickListener listener) {
        DialogListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(AppSingle.getInstance().getActivity().getApplicationContext()), R.layout.dialog_list, null, false);
        final Dialog dialog = new Dialog(AppSingle.getInstance().getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(binding.getRoot());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        binding.txtViewTitle.setText(title);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        LinearLayoutManager lLayout = new LinearLayoutManager(AppSingle.getInstance().getActivity());
        RecyclerView rView = binding.rvList;
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);
        AdapterDialogListItem adapter = new AdapterDialogListItem(listData, selectedPos);
        rView.setAdapter(adapter);
        adapter.registerOnItemClickListener(new AdapterDialogListItem.IonItemSelect() {
            @Override
            public void onItemSelect(int position) {
                if (listener != null)
                    listener.onClick(listData.get(position), position);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void callAsync() {
        String brand = "";
        String model = "";
        String fromdate = binding.txtOrderreportFromdate.getText().toString();
        String todate = binding.txtOrderreportTodate.getText().toString();
        if (!binding.txtOrderreportBrand.getHint().toString().equalsIgnoreCase("Brand")) {
            brand = binding.txtOrderreportBrand.getHint().toString();
        }
        if (!binding.txtOrderreportModel.getHint().toString().equalsIgnoreCase("Model")) {
            model = binding.txtOrderreportModel.getHint().toString();
        }
        String data = binding.txtOrderreportData.getText().toString();
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<OrderReportList> call = apiService.getPreviousOrder(AppSingle.getInstance().getLoginUser().getUserId(), RequestFormater.getsalereport(brand, model, fromdate, todate, data));
        showProgessBar();
        call.enqueue(new Callback<OrderReportList>() {
            @Override
            public void onResponse(Call<OrderReportList> call, Response<OrderReportList> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null)
                        if (response.body().getPreviousOrderList() != null && response.body().getPreviousOrderList().size() > 0) {
                            binding.llOrderUpper.setVisibility(View.GONE);
                            binding.llRv.setVisibility(View.VISIBLE);
                            SaleReportAdapter adapter = new SaleReportAdapter(AppSingle.getInstance().getActivity(), response.body().getPreviousOrderList());
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
                            binding.rvListReportOrder.setLayoutManager(mLayoutManager);
                            binding.rvListReportOrder.setItemAnimator(new DefaultItemAnimator());
                            binding.rvListReportOrder.setAdapter(adapter);
                            //Toast.makeText(AppSingle.getInstance().getActivity(), response.body().getPreviousOrderList().get(0).getModel(), Toast.LENGTH_SHORT).show();
                        } else {
                            showToast("List is empty.");
                        }
                } else {
                    showResponseMessage(response.code());
                    if (response.code() == 401)
                        callAsync();
                }
            }

            @Override
            public void onFailure(Call<OrderReportList> call, Throwable t) {
                hideProgessBar();
                showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    public void sendToServer(int type) {

        switch (type) {
            case 0:
                showToast("Coming soon");
                break;
            case 1:
                callAsync();
                break;
            case 2:
                callAsyncMailTo();
                break;
        }
    }

    public int getReportTypeIndex(String name) {
        int index = 0;
        for (int i = 0; i < AppConstants.IPrefConstant.reporttype.length; i++) {
            if (AppConstants.IPrefConstant.reporttype[i].equalsIgnoreCase(name.trim())) {
                index = i;
            }
        }
        return index;
    }

    private void callAsyncMailTo() {
        String brand = "";
        String model = "";
        String fromdate = binding.txtOrderreportFromdate.getText().toString();
        String todate = binding.txtOrderreportTodate.getText().toString();
        if (!binding.txtOrderreportBrand.getHint().toString().equalsIgnoreCase("Brand")) {
            brand = binding.txtOrderreportBrand.getHint().toString();
        }
        if (!binding.txtOrderreportModel.getHint().toString().equalsIgnoreCase("Model")) {
            model = binding.txtOrderreportModel.getHint().toString();
        }
        String data = binding.txtOrderreportData.getText().toString();
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<ForgetModel> call = apiService.getPreviousOrderMailTo(AppSingle.getInstance().getLoginUser().getUserId(), RequestFormater.getsalereport(brand, model, fromdate, todate, data));
        showProgessBar();
        call.enqueue(new Callback<ForgetModel>() {
            @Override
            public void onResponse(Call<ForgetModel> call, Response<ForgetModel> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().getStatusCode() == 0) {
                            Toast.makeText(AppSingle.getInstance().getActivity(), response.body().getMessage(), Toast.LENGTH_SHORT).show();
                        } else {
                            showToast(response.body().getMessage());
                        }
                    } else {
                        showToast("Message body is null");
                    }
                } else {
                    showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<ForgetModel> call, Throwable t) {
                hideProgessBar();
                showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    private void showDatePicker(int type) {
        try {
            selectedDateOption = type;
            resetDateToToday();
            getDateDialog(type).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetDateToToday() {
        final Calendar c1 = Calendar.getInstance();
        Date_year = c1.get(Calendar.YEAR);
        Date_month = c1.get(Calendar.MONTH);
        Date_day = c1.get(Calendar.DAY_OF_MONTH);
    }

    private DatePickerDialog getDateDialog(int type) {
        switch (type) {
            case INT_FROM:
                DatePickerDialog dialog = new DatePickerDialog(AppSingle.getInstance().getActivity(),
                        pickerListener, Date_year, Date_month, Date_day);
                try {
                    dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return dialog;
            case INT_TO:
                DatePickerDialog dialoga = new DatePickerDialog(AppSingle.getInstance().getActivity(),
                        pickerListener, Date_year, Date_month, Date_day);
                try {
                    dialoga.getDatePicker().setMaxDate(System.currentTimeMillis());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return dialoga;
            default:
                return null;
        }
    }


    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            Date_year = selectedYear;
            Date_month = selectedMonth;
            Date_day = selectedDay;
            Calendar calendar = Calendar.getInstance();
            calendar.set(selectedYear, selectedMonth, selectedDay);
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
            String strDate = format.format(calendar.getTime());
            try {
                switch (selectedDateOption) {
                    case INT_FROM:
                        binding.txtOrderreportFromdate.setText(strDate.trim());
                        break;
                    case INT_TO:
                        binding.txtOrderreportTodate.setText(strDate.trim());
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    public void featchDB() {
        fetchBrandList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchBrandList();
        fetchMasterList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchMasterList();
        fetchReportTypeList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchReportTypeList();
    }

}
