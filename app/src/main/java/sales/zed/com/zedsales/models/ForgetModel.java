package sales.zed.com.zedsales.models;

/**
 * Created by apple on 09/12/17.
 */

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ForgetModel {

    @SerializedName("status")
    @Expose
    private int StatusCode;

    @SerializedName("message")
    @Expose
    private String Message;

    public int getStatusCode() {
        return StatusCode;
    }
    public String getMessage() {
        return Message;
    }
}
