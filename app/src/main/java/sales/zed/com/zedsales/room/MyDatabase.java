package sales.zed.com.zedsales.room;

import android.arch.persistence.room.Database;
import android.arch.persistence.room.Room;
import android.arch.persistence.room.RoomDatabase;
import android.content.Context;
import sales.zed.com.zedsales.models.BrandListsItem;
import sales.zed.com.zedsales.models.GRNStatusItem;
import sales.zed.com.zedsales.models.MenuListData;
import sales.zed.com.zedsales.models.ModelListsItem;
import sales.zed.com.zedsales.models.NotificationData;
import sales.zed.com.zedsales.models.OffLineData;
import sales.zed.com.zedsales.models.ProductCategoryListsItem;
import sales.zed.com.zedsales.models.ProductListsItem;
import sales.zed.com.zedsales.models.ReportTypeItem;
import sales.zed.com.zedsales.models.SerialMaster;
import sales.zed.com.zedsales.models.SkuListsItem;
import sales.zed.com.zedsales.models.StockItems;
import sales.zed.com.zedsales.models.StockTypeItem;
import sales.zed.com.zedsales.models.UserModel;

/**
 * Created by root on 17/12/17.
 */


@Database(entities = {UserModel.class, BrandListsItem.class, ProductCategoryListsItem.class, ProductListsItem.class, ModelListsItem.class,
        SkuListsItem.class, SerialMaster.class, StockItems.class, StockTypeItem.class, ReportTypeItem.class, GRNStatusItem.class,OffLineData.class,MenuListData.class,NotificationData.class}, version = 17)
public abstract class MyDatabase extends RoomDatabase {
    public abstract DaoAccess daoAccess();

    private static MyDatabase currentDatabase;

    public static MyDatabase getCurretDatabase(Context context) {
        if (currentDatabase == null)
            currentDatabase = Room.databaseBuilder(context,
                    MyDatabase.class, "zed-axis-db")
                    .allowMainThreadQueries().fallbackToDestructiveMigration().build();
        return currentDatabase;
    }
}
