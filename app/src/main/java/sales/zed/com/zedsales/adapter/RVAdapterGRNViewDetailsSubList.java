package sales.zed.com.zedsales.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.databinding.AdapterGrnDetailListCBinding;
import sales.zed.com.zedsales.databinding.AdapterGrnDetailListPBinding;
import sales.zed.com.zedsales.interfaces.MyAdapterListener;
import sales.zed.com.zedsales.models.GRNParent;

/**
 * Created by root on 09/12/17.
 */

public class RVAdapterGRNViewDetailsSubList extends RecyclerView.Adapter<RVAdapterGRNViewDetailsSubList.ViewHolder> {
    private List<String> listData;
    private LayoutInflater mInflater;

    public RVAdapterGRNViewDetailsSubList(Context context, List<String> listData) {
        this.listData = listData;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.adapter_grn_detail_list_c, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public void onBindViewHolder(final ViewHolder holder, int position) {
        final String data = listData.get(position);
        holder.binding.txtViewData.setText(data);
        //holder.binding.grnCheck.setVisibility(View.VISIBLE);
    }

    @Override
    public int getItemCount() {
        if (listData == null)
            return 0;
        return listData.size();
    }


    public class ViewHolder extends RecyclerView.ViewHolder {
        AdapterGrnDetailListCBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);

        }
    }
}