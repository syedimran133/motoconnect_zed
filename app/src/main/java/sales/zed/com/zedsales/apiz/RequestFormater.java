package sales.zed.com.zedsales.apiz;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.List;

import retrofit2.http.Field;
import sales.zed.com.zedsales.models.AddOpeningStock;
import sales.zed.com.zedsales.models.AddOrderModel;
import sales.zed.com.zedsales.models.GRNSkuList;
import sales.zed.com.zedsales.models.SaleSkuListItem;
import sales.zed.com.zedsales.models.SkuOpeningStockList;

/**
 * Created by root on 09/12/17.
 */

public class RequestFormater {


    public static String versionCheck(@Field("UserName") String userName, @Field("ClientKey") String clientkey,
                                      @Field("DeviceToken") String deviceid) {
        try {
            JSONObject jsonSend = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            JSONObject paramObject = new JSONObject();
            paramObject.put("UserName", userName);
            paramObject.put("ClientKey", clientkey);
            paramObject.put("DeviceToken", deviceid);
            jsonArray.put(paramObject);
            jsonSend.put("LoginDetail", jsonArray);
            String send = jsonSend.toString().replaceAll("\\\\", "");
            return send;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String login(@Field("UserName") String userName, @Field("Password") String password,
                               @Field("AccessKey") String accessKey, @Field("DeviceId") String deviceId, @Field("DeviceToken") String deviceToken) {
        try {
            JSONObject jsonSend = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            JSONObject paramObject = new JSONObject();
            paramObject.put("UserName", userName);
            paramObject.put("Password", password);
            paramObject.put("ClientKey", accessKey);
            paramObject.put("DeviceId", deviceId);
            paramObject.put("DeviceToken", deviceToken);
            jsonArray.put(paramObject);
            jsonSend.put("LoginDetail", jsonArray);
            String send = jsonSend.toString().replaceAll("\\\\", "");
            return send;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String addorder(@Field("OrderItemLists") List<AddOrderModel> orderlist) {
        try {
            JSONObject jsonSend = new JSONObject();
            JSONArray jsonArray = new JSONArray();
            for (int i = 0; i < orderlist.size(); i++) {
                JSONObject paramObject = new JSONObject();
                paramObject.put("SkuId", orderlist.get(i).getOrderskuId());
                paramObject.put("SkuName", orderlist.get(i).getOrderskuName());
                paramObject.put("OrderQuantity", orderlist.get(i).getOrderQuantity());
                jsonArray.put(paramObject);
            }
            jsonSend.put("OrderItemLists", jsonArray);
            String send = jsonSend.toString().replaceAll("\\\\", "");
            return send;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String openingstock(@Field("OpeningStockDate") String date, @Field("SkuOpeningStockList") List<SkuOpeningStockList> stocklist) {
        List<SkuOpeningStockList> innerStockList = stocklist;
        int size = stocklist.size();
        JSONArray jsonArray = new JSONArray();
        for (int k = 0; k < size; k++) {
            try {
                JSONObject paramObject = new JSONObject();
                paramObject.put("SkuCode", innerStockList.get(k).getSkuCode());
                paramObject.put("SkuName", innerStockList.get(k).getSkuName());
                paramObject.put("SkuQuantity", innerStockList.get(k).getSkuQuantity());
                paramObject.put("StockBinType", innerStockList.get(k).getStockBinType());
                JSONArray jsonArrayserial = new JSONArray();
                try {
                    for (int l = 0; l < innerStockList.get(k).getSerialOpeningStockList().size(); l++) {
                        JSONObject paramObjectinner = new JSONObject();
                        paramObjectinner.put("Serial1", innerStockList.get(k).getSerialOpeningStockList().get(l).getSerial1());
                        paramObjectinner.put("Serial2", innerStockList.get(k).getSerialOpeningStockList().get(l).getSerial2());
                        paramObjectinner.put("Serial3", innerStockList.get(k).getSerialOpeningStockList().get(l).getSerial3());
                        paramObjectinner.put("Serial4", innerStockList.get(k).getSerialOpeningStockList().get(l).getSerial4());
                        jsonArrayserial.put(paramObjectinner);
                    }
                } catch (Exception e) {

                }
                paramObject.put("SerialOpeningStockList", jsonArrayserial);
                jsonArray.put(paramObject);
            } catch (Exception e) {
            }
        }
        try {
            JSONObject jsonSend = new JSONObject();
            jsonSend.put("OpeningStockDate", date);
            jsonSend.put("SkuOpeningStockList", jsonArray);
            String send = jsonSend.toString().replaceAll("\\\\", "");
            return send;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String savesale(@Field("InvoiceNumber") String invoiceNumber,
                                  @Field("CustomerName") String customerName, @Field("MobileNumber") String mobileNumber, @Field("SkuList") List<SaleSkuListItem> skuList) {
        List<SaleSkuListItem> innerskuList = skuList;
        int size = innerskuList.size();
        JSONArray jsonArray = new JSONArray();
        for (int k = 0; k < size; k++) {
            try {
                JSONObject paramObject = new JSONObject();
                paramObject.put("SkuId", innerskuList.get(k).getSkuId());
                paramObject.put("SkuCode", innerskuList.get(k).getSkuCode());
                paramObject.put("SkuName", innerskuList.get(k).getSkuName());
                paramObject.put("SkuQuantity", innerskuList.get(k).getSkuQuantity());
                JSONArray jsonArrayserial = new JSONArray();
                for (int l = 0; l < innerskuList.get(k).getSerialList().size(); l++) {
                    JSONObject paramObjectinner = new JSONObject();
                    paramObjectinner.put("Serial1", innerskuList.get(k).getSerialList().get(l).getSerial1());
                    paramObjectinner.put("Serial2", innerskuList.get(k).getSerialList().get(l).getSerial2());
                    paramObjectinner.put("Serial3", innerskuList.get(k).getSerialList().get(l).getSerial3());
                    paramObjectinner.put("Serial4", innerskuList.get(k).getSerialList().get(l).getSerial4());
                    jsonArrayserial.put(paramObjectinner);
                }
                paramObject.put("SerialList", jsonArrayserial);
                jsonArray.put(paramObject);
            } catch (Exception e) {
            }
        }
        try {
            JSONObject jsonSend = new JSONObject();
            jsonSend.put("InvoiceNumber", invoiceNumber);
            jsonSend.put("CustomerName", customerName);
            jsonSend.put("MobileNumber", mobileNumber);

            jsonSend.put("SkuList", jsonArray);
            String send = jsonSend.toString().replaceAll("\\\\", "");
            return send;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String saveGRN(@Field("GrnAction") String grnAction,
                                 @Field("InvoiceId") String invoiceId, @Field("InvoiceNumber") String invoiceNumber, @Field("InvoiceDate") String invoiceDate, @Field("TotalInvoiceAmount") String totalInvoiceAmount, @Field("SkuList") List<GRNSkuList> skuList) {
        List<GRNSkuList> innerskuList = skuList;
        int size = innerskuList.size();
        JSONArray jsonArray = new JSONArray();
        for (int k = 0; k < size; k++) {
            try {
                JSONObject paramObject = new JSONObject();
                JSONArray jsonArrayserial = new JSONArray();
                for (int l = 0; l < innerskuList.get(k).getSerialList().size(); l++) {
                    JSONObject paramObjectinner = new JSONObject();
                    paramObjectinner.put("Serial1", innerskuList.get(k).getSerialList().get(l).getSerial1());
                    paramObjectinner.put("Serial2", innerskuList.get(k).getSerialList().get(l).getSerial2());
                    paramObjectinner.put("Serial3", innerskuList.get(k).getSerialList().get(l).getSerial3());
                    paramObjectinner.put("Serial4", innerskuList.get(k).getSerialList().get(l).getSerial4());
                    jsonArrayserial.put(paramObjectinner);
                }
                paramObject.put("SerialList", jsonArrayserial);
                paramObject.put("SkuId", innerskuList.get(k).getSkuId());
                paramObject.put("SkuCode", innerskuList.get(k).getSkuCode());
                paramObject.put("SkuName", innerskuList.get(k).getSkuName());
                paramObject.put("SkuQuantity", innerskuList.get(k).getSkuQuantity());
                paramObject.put("SkuPrice", innerskuList.get(k).getSkuPrice());
                paramObject.put("StockBinTypeCode", innerskuList.get(k).getStockBinTypeCode());
                jsonArray.put(paramObject);
            } catch (Exception e) {
            }
        }
        try {
            JSONObject jsonSend = new JSONObject();
            jsonSend.put("GrnAction", grnAction);
            jsonSend.put("InvoiceId", invoiceId);
            jsonSend.put("InvoiceNumber", invoiceNumber);
            jsonSend.put("InvoiceDate", invoiceDate);
            jsonSend.put("TotalInvoiceAmount", totalInvoiceAmount);

            jsonSend.put("SkuList", jsonArray);
            String send = jsonSend.toString().replaceAll("\\\\", "");
            return send;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String forget(@Field("UserName") String userName, @Field("AccessKey") String accessKey) {
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("UserName", userName);
            paramObject.put("ClientKey", accessKey);
            String send = paramObject.toString().replaceAll("\\\\", "");
            return send;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String changepin(@Field("UserName") String userName,
                                   @Field("OldPassword") String oldpassword, @Field("NewPassword") String newpassword) {
        try {
            JSONObject paramObject = new JSONObject();
            paramObject.put("UserName", userName);
            paramObject.put("OldPassword", oldpassword);
            paramObject.put("NewPassword", newpassword);
            String send = paramObject.toString().replaceAll("\\\\", "");
            return send;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getpendinggrn(@Field("InvoiceNo") String invoiceNo,
                                       @Field("RequestFromDate") String requestFromDate, @Field("RequestToDate") String requestToDate) {
        try {
            JSONObject jsonSend = new JSONObject();
            jsonSend.put("InvoiceNo", invoiceNo);
            jsonSend.put("RequestFromDate", requestFromDate);
            jsonSend.put("RequestToDate", requestToDate);
            return jsonSend.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String gettargetjson(@Field("FromDate") String fromDate, @Field("ToDate") String toDate) {
        try {
            JSONObject jsonSend = new JSONObject();
            jsonSend.put("FromDate", fromDate);
            jsonSend.put("ToDate", toDate);
            return jsonSend.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getsalereport(@Field("Brand") String brand, @Field("Model") String model, @Field("FromDate") String fromDate, @Field("ToDate") String toDate, @Field("PreviewType") String previewType) {
        try {
            JSONObject jsonSend = new JSONObject();
            jsonSend.put("Brand", brand);
            jsonSend.put("Model", model);
            jsonSend.put("FromDate", fromDate);
            jsonSend.put("ToDate", toDate);
            jsonSend.put("PreviewType", previewType);
            return jsonSend.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getcurrentstockreport(@Field("BrandId") String brand, @Field("ModelId") String model, @Field("SkuId") String skuId, @Field("StockTypeId") String stockTypeId, @Field("PreviewType") String previewType) {
        try {
            JSONObject jsonSend = new JSONObject();
            jsonSend.put("BrandId", brand);
            jsonSend.put("ModelId", model);
            jsonSend.put("SkuId", skuId);
            jsonSend.put("StockTypeId", stockTypeId);
            jsonSend.put("PreviewType", previewType);
            return jsonSend.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getgrnreport(@Field("BrandId") String brand, @Field("ModelId") String model, @Field("FromDate") String fromDate, @Field("ToDate") String toDate, @Field("Status") String status, @Field("PreviewType") String previewType) {
        try {
            JSONObject jsonSend = new JSONObject();
            jsonSend.put("BrandId", brand);
            jsonSend.put("ModelId", model);
            jsonSend.put("FromDate", fromDate);
            jsonSend.put("ToDate", toDate);
            jsonSend.put("Status", status);
            jsonSend.put("PreviewType", previewType);
            return jsonSend.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String logout(@Field("DeviceId") String deviceId) {
        try {
            JSONObject jsonSend = new JSONObject();
            jsonSend.put("DeviceId", deviceId);
            String send = jsonSend.toString().replaceAll("\\\\", "");
            return send;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String pendingGRNlist(@Field("InvoiceId") String invoiceId) {
        try {
            JSONObject jsonSend = new JSONObject();
            jsonSend.put("InvoiceId", invoiceId);
            String send = jsonSend.toString().replaceAll("\\\\", "");
            return send;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public static String ProceedWithZeroStock(@Field("OpeningStockDate") String openingstockdate) {
        try {
            JSONObject jsonSend = new JSONObject();
            jsonSend.put("OpeningStockDate", openingstockdate);
            String send = jsonSend.toString().replaceAll("\\\\", "");
            return send;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
    public static String pendingGRNDetails(@Field("InvoiceId") String invoiceId) {
        try {
            JSONObject jsonSend = new JSONObject();
            jsonSend.put("InvoiceId", invoiceId);
            String send = jsonSend.toString().replaceAll("\\\\", "");
            return send;
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }

    public static String getupdateprofile(@Field("CompanyName") String companyName, @Field("UserCode") String userCode, @Field("UserName") String userName, @Field("RegisteredMobileNo") String registeredMobileNo, @Field("AlternateMobileNo") String alternateMobileNo, @Field("EmailId") String emailId, @Field("DateOfBirth") String dateOfBirth, @Field("Married") String married, @Field("DateOfAnniversary") String dateOfAnniversary, @Field("Address") String address, @Field("City") String city, @Field("State") String state, @Field("PinCode") String pinCode, @Field("CounterPotentialValue") String counterPotentialValue, @Field("AccountHolderName") String accountHolderName, @Field("AccountNumber") String accountNumber, @Field("BankName") String bankName, @Field("BranchName") String branchName, @Field("BranchLocation") String branchLocation, @Field("IfscCode") String ifscCode, @Field("AadharNumber") String aadharNumber, @Field("PanNumber") String panNumber, @Field("GstinNo") String gstinNo) {
        try {
            JSONObject jsonSend = new JSONObject();
            jsonSend.put("CompanyName", companyName);
            jsonSend.put("UserCode", userCode);
            jsonSend.put("UserName", userName);
            jsonSend.put("RegisteredMobileNo", registeredMobileNo);
            jsonSend.put("AlternateMobileNo", alternateMobileNo);
            jsonSend.put("EmailId", emailId);
            jsonSend.put("DateOfBirth", dateOfBirth);
            jsonSend.put("Married", married);
            jsonSend.put("DateOfAnniversary", dateOfAnniversary);
            jsonSend.put("Address", address);
            jsonSend.put("City", city);
            jsonSend.put("State", state);
            jsonSend.put("PinCode", pinCode);
            jsonSend.put("CounterPotentialValue", counterPotentialValue);
            jsonSend.put("AccountHolderName", accountHolderName);
            jsonSend.put("AccountNumber", accountNumber);
            jsonSend.put("BankName", bankName);
            jsonSend.put("BranchName", branchName);
            jsonSend.put("BranchLocation", branchLocation);
            jsonSend.put("IfscCode", ifscCode);
            jsonSend.put("AadharNumber", aadharNumber);
            jsonSend.put("PanNumber", panNumber);
            jsonSend.put("GstinNo", gstinNo);

            return jsonSend.toString();
        } catch (Exception e) {
            e.printStackTrace();
        }
        return null;
    }
}
