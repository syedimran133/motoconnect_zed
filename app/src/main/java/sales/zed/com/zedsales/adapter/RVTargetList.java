package sales.zed.com.zedsales.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.databinding.FragmentTargetInnerViewdetailBinding;
import sales.zed.com.zedsales.databinding.OpeningstockListBinding;
import sales.zed.com.zedsales.interfaces.MyAdapterListener;
import sales.zed.com.zedsales.models.SkuOpeningStockList;
import sales.zed.com.zedsales.models.TargetModel;

/**
 * Created by root on 2/14/18.
 */

public class RVTargetList extends RecyclerView.Adapter<RVTargetList.ViewHolder> {

    private List<TargetModel> listData;
    private LayoutInflater mInflater;
    private MyAdapterListener click_listener;

    public RVTargetList(Context context, List<TargetModel> listData, MyAdapterListener listener) {
        this.listData = listData;
        click_listener = listener;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, int position) {
        TargetModel targetitem = listData.get(position);
        holder.binding.txtTargetlistTargetname.setText(targetitem.getTargetName());
        holder.binding.txtTargetlistTarget.setText(targetitem.getTarget());
        holder.binding.txtTargetlistDaysleft.setText(targetitem.getDaysLeft()+" Days");
        holder.binding.txtTargetlistAchievement.setText(targetitem.getTargetAchieved());
        holder.binding.txtAchpercnt.setText(targetitem.getAchievementPercent());
        try {
            holder.binding.pd.setProgress((int) Double.parseDouble(targetitem.getAchievementPercent().replace("%", "").trim()));
        } catch (Exception e) {
        }

    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.fragment_target_inner_viewdetail, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    @Override
    public int getItemCount() {
        if (listData == null)
            return 0;
        return listData.size();
    }

    public class ViewHolder extends RecyclerView.ViewHolder {

        FragmentTargetInnerViewdetailBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);

            itemView.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    if (click_listener != null)
                        click_listener.iconButtonViewOnClick(v, getAdapterPosition());
                }
            });
        }
    }
}
