package sales.zed.com.zedsales.custom;

/**
 * Created by Mukesh on 03-01-2017.
 */

import android.content.Context;
import android.graphics.Canvas;
import android.graphics.Typeface;
import android.util.AttributeSet;

import sales.zed.com.zedsales.support.AppValidate;


public class CustomTextViewBold extends android.support.v7.widget.AppCompatTextView {


    public CustomTextViewBold(Context context) {
        super(context);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/GOTHICB.TTF");
        this.setTypeface(face);
    }

    public CustomTextViewBold(Context context, AttributeSet attrs) {
        super(context, attrs);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/GOTHICB.TTF");
        this.setTypeface(face);
    }

    public CustomTextViewBold(Context context, AttributeSet attrs, int defStyle) {
        super(context, attrs, defStyle);
        Typeface face = Typeface.createFromAsset(context.getAssets(), "fonts/GOTHICB.TTF");
        this.setTypeface(face);
    }

    @Override
    public void setError(CharSequence error) {
        super.setError(error);
        try {
            if (AppValidate.isValidString(error.toString()))
                requestFocus();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    protected void onDraw(Canvas canvas) {
        super.onDraw(canvas);
    }

}