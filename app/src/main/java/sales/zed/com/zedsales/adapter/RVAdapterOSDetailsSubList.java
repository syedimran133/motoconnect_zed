package sales.zed.com.zedsales.adapter;

import android.content.Context;
import android.databinding.DataBindingUtil;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.Toast;

import java.util.List;

import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.databinding.OpeningstockInnerlistBinding;
import sales.zed.com.zedsales.models.SaleSerialListItems;
import sales.zed.com.zedsales.support.AppSingle;

/**
 * Created by root on 2/9/18.
 */

public class RVAdapterOSDetailsSubList extends RecyclerView.Adapter<RVAdapterOSDetailsSubList.ViewHolder> {

    private List<SaleSerialListItems> listData;
    private LayoutInflater mInflater;
    RVAdapterOSDetails rvAdapterOSDetails;
    int parentindex;
    public RVAdapterOSDetailsSubList(Context context, List<SaleSerialListItems> listData,RVAdapterOSDetails rvAdapterOSDetails,int parentindex) {
        this.listData = listData;
        this.rvAdapterOSDetails=rvAdapterOSDetails;
        this.parentindex=parentindex;
        mInflater = LayoutInflater.from(context);
    }

    @Override
    public void onBindViewHolder(ViewHolder holder, final int position) {
        final SaleSerialListItems data = listData.get(position);
        holder.binding.txtOsinnerSerial.setText(data.getSerial1());
        holder.binding.ivOdDelete.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                removeAt(position);
            }
        });
    }

    @Override
    public ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mInflater.inflate(R.layout.openingstock_innerlist, parent, false);
        final ViewHolder holder = new ViewHolder(view);
        return holder;
    }

    public void removeAt(int position) {
        if (listData.size() > 0) {
            listData.remove(position);
            notifyItemRemoved(position);
            notifyItemRangeChanged(position, listData.size());
        } else {
            Toast.makeText(AppSingle.getInstance().getActivity(), "No element in the list.", Toast.LENGTH_LONG).show();
        }
    }

    @Override
    public int getItemCount() {
        if (listData == null)
            return 0;
        return listData.size();
    }

    public List<SaleSerialListItems> getListData() {
        return listData;
    }

    public void setListData(List<SaleSerialListItems> listData) {
        this.listData = listData;
    }

    public class ViewHolder extends RecyclerView.ViewHolder {
        OpeningstockInnerlistBinding binding;

        public ViewHolder(View itemView) {
            super(itemView);
            binding = DataBindingUtil.bind(itemView);

        }
    }
}
