package sales.zed.com.zedsales.adapter;

import android.content.Context;
import android.text.Html;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.BaseExpandableListAdapter;
import android.widget.LinearLayout;
import android.widget.TextView;

import java.util.List;

import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.custom.CustomTextView;
import sales.zed.com.zedsales.models.Bulletin;
import sales.zed.com.zedsales.models.BulletinSubCategory;

/**
 * Created by root on 2/20/18.
 */

public class AdapterBulletin extends BaseExpandableListAdapter {

    private Context context;
    private List<BulletinSubCategory> bulletinSubCategorie;

    public AdapterBulletin(Context context, List<BulletinSubCategory> bulletinSubCategorie) {
        this.context = context;
        this.bulletinSubCategorie = bulletinSubCategorie;
    }

    @Override
    public Bulletin getChild(int groupPosition, int childPosition) {
        return this.bulletinSubCategorie.get(groupPosition).getBulletin().get(childPosition);
    }

    @Override
    public long getChildId(int groupPosition, int childPosition) {
        return childPosition;
    }

    @Override
    public View getChildView(int groupPosition, int childPosition, boolean isLastChild, View convertView, ViewGroup parent) {

        Bulletin bulletin = bulletinSubCategorie.get(groupPosition).getBulletin().get(childPosition);

        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context
                    .getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.bulletin_news_layout, null);
        }

        TextView tvSub = (TextView) convertView
                .findViewById(R.id.txt_bulletin_sub);
        TextView tvPDate = (TextView) convertView
                .findViewById(R.id.txt_publish_date);
        TextView tvEDate = (TextView) convertView
                .findViewById(R.id.txt_expiry_date);
        TextView tvDescription = (TextView) convertView
                .findViewById(R.id.txt_description);
        TextView tvLint1 = (TextView) convertView
                .findViewById(R.id.txt_link1);
        TextView tvLink2 = (TextView) convertView
                .findViewById(R.id.txt_link2);
        LinearLayout llLint1 = (LinearLayout) convertView
                .findViewById(R.id.ll_link1);
        LinearLayout llLink2 = (LinearLayout) convertView
                .findViewById(R.id.ll_link2);

        tvSub.setText(bulletin.getSubject());
        tvPDate.setText(bulletin.getPublishDate());
        tvEDate.setText(bulletin.getExpireDate());
        tvDescription.setText(Html.fromHtml(bulletin.getDescription()));
        if (bulletin.getLink1().equalsIgnoreCase("")) {
            llLint1.setVisibility(View.GONE);
        } else {
            llLint1.setVisibility(View.VISIBLE);
            tvLint1.setText(bulletin.getLink1());
        }
        if (bulletin.getLink2().equalsIgnoreCase("")) {
            llLink2.setVisibility(View.GONE);
        } else {
            llLink2.setVisibility(View.VISIBLE);
            tvLink2.setText(bulletin.getLink2());
        }

        return convertView;
    }

    @Override
    public int getChildrenCount(int groupPosition) {
        return this.bulletinSubCategorie.get(groupPosition).getBulletin().size();
    }

    @Override
    public BulletinSubCategory getGroup(int groupPosition) {
        return this.bulletinSubCategorie.get(groupPosition);
    }

    @Override
    public long getGroupId(int groupPosition) {
        return groupPosition;
    }

    @Override
    public int getGroupCount() {
        return this.bulletinSubCategorie.size();
    }

    @Override
    public View getGroupView(int groupPosition, boolean isExpanded, View convertView, ViewGroup parent) {
        BulletinSubCategory bulletinSubCat = bulletinSubCategorie.get(groupPosition);
        if (convertView == null) {
            LayoutInflater layoutInflater = (LayoutInflater) this.context.
                    getSystemService(Context.LAYOUT_INFLATER_SERVICE);
            convertView = layoutInflater.inflate(R.layout.bulletin_listlayout_item, null);
        }
        CustomTextView listmodelTextView = (CustomTextView) convertView
                .findViewById(R.id.txt_subcat_name);
        listmodelTextView.setText(bulletinSubCat.getSubCategoryName());

        return convertView;
    }

    @Override
    public boolean hasStableIds() {
        return false;
    }

    @Override
    public boolean isChildSelectable(int groupPosition, int childPosition) {
        return true;
    }
}
