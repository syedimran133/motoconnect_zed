package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 2/16/18.
 */

public class CurrentStockSkuItem {

    @SerializedName("CurrentStockSerialList")
    @Expose
    private List<CurrentStockSerial> currentStockSerialList;

    @SerializedName("Model")
    @Expose
    private String model;

    @SerializedName("SkuName")
    @Expose
    private String skuName;

    @SerializedName("Quantity")
    @Expose
    private String quantity;

    @SerializedName("StockBinTypeDesc")
    @Expose
    private String stockBinTypeDesc;

    public List<CurrentStockSerial> getCurrentStockSerialList() {
        return currentStockSerialList;
    }

    public void setCurrentStockSerialList(List<CurrentStockSerial> currentStockSerialList) {
        this.currentStockSerialList = currentStockSerialList;
    }

    public String getModel() {
        return model;
    }

    public void setModel(String model) {
        this.model = model;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getQuantity() {
        return quantity;
    }

    public void setQuantity(String quantity) {
        this.quantity = quantity;
    }

    public String getStockBinTypeDesc() {
        return stockBinTypeDesc;
    }

    public void setStockBinTypeDesc(String stockBinTypeDesc) {
        this.stockBinTypeDesc = stockBinTypeDesc;
    }
}
