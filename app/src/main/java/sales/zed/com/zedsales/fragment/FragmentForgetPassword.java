package sales.zed.com.zedsales.fragment;

import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.apiz.RequestFormater;
import sales.zed.com.zedsales.constants.AppConstants;
import sales.zed.com.zedsales.databinding.FragmentForgetPasswordBinding;
import sales.zed.com.zedsales.models.ForgetModel;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.AppValidate;
import sales.zed.com.zedsales.support.FlowOrganizer;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by root on 09/12/17.
 */

public class FragmentForgetPassword extends BaseFragment {

    private FragmentForgetPasswordBinding binding;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_forget_password, container, false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);

        binding.btnTextViewSubmit.setOnClickListener(listener);
        binding.btnTxtBack.setOnClickListener(listener);
        binding.btnTextChangePswd.setOnClickListener(listener);
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_text_view_submit:
                    callAsync();
                    break;
                case R.id.btn_txt_back:
                    AppSingle.getInstance().getActivity().onBackPressed();
                    break;
                case R.id.btn_text_change_pswd:
                    FlowOrganizer.getInstance().add(new FragmentChangePassword(),true);
                    break;
            }
        }
    };

    private void callAsync() {
        String userName = binding.editTextName.getText().toString();
        String accesskey = binding.editTextAccessKey.getText().toString();
        if (AppValidate.isValidString(userName)) {
            if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
                showToast(NO_INTERNET);
                return;
            }
            ApiInterface apiService = ApiClient.getDefaultClient().create(ApiInterface.class);
            Call<ForgetModel> call = apiService.forget(RequestFormater.forget(userName, accesskey));
            showProgessBar();
            call.enqueue(new Callback<ForgetModel>() {
                @Override
                public void onResponse(Call<ForgetModel> call, Response<ForgetModel> response) {
                    hideProgessBar();
                    if (response.code() == 200) {
                        showToast(response.body().getMessage());
                        if (response.body().getStatusCode() == 0) {
                            //AppSingle.getInstance().getActivity().onBackPressed();
                            binding.txtViewMessage.setText(response.body().getMessage());
                        }

                    } else {
                        showResponseMessage(response.code());
                    }
                }

                @Override
                public void onFailure(Call<ForgetModel> call, Throwable t) {
                    hideProgessBar();
                    showToast("Fail");
                }
            });
        } else
            binding.editTextName.setError(AppConstants.IMessages.VALID_NAME);
    }
}
