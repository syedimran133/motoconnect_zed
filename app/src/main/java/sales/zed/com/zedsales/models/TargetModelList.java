package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 2/14/18.
 */

public class TargetModelList {

    @SerializedName("TargetDetailsList")
    @Expose
    private List<TargetModel> targetList;

    @SerializedName("PageCount")
    @Expose
    private String PageCount;

    public List<TargetModel> getTargetList() {
        return targetList;
    }

    public void setTargetList(List<TargetModel> targetList) {
        this.targetList = targetList;
    }

    public String getPageCount() {
        return PageCount;
    }

    public void setPageCount(String pageCount) {
        PageCount = pageCount;
    }
}
