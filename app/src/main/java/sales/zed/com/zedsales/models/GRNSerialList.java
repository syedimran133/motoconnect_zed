package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 1/29/18.
 */

public class GRNSerialList {

    @SerializedName("Serial1")
    @Expose
    private String Serial1;

    @SerializedName("Serial2")
    @Expose
    private String Serial2;

    @SerializedName("Serial3")
    @Expose
    private String Serial3;

    @SerializedName("Serial4")
    @Expose
    private String Serial4;

    public String getSerial1() {
        return Serial1;
    }

    public String getSerial2() {
        return Serial2;
    }

    public String getSerial3() {
        return Serial3;
    }

    public String getSerial4() {
        return Serial4;
    }

}
