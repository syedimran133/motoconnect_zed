package sales.zed.com.zedsales.fragment;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.adapter.GRNEepdlistview;
import sales.zed.com.zedsales.adapter.RVAdapterGRNViewDetails;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.apiz.RequestFormater;
import sales.zed.com.zedsales.databinding.FragmentGrnDetailsBinding;
import sales.zed.com.zedsales.models.ForgetModel;
import sales.zed.com.zedsales.models.GRNDetail;
import sales.zed.com.zedsales.models.GRNParent;
import sales.zed.com.zedsales.models.OffLineData;
import sales.zed.com.zedsales.room.MyDatabase;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FlowOrganizer;
import sales.zed.com.zedsales.support.NetworkUtil;
import sales.zed.com.zedsales.R;

/**
 * Created by root on 1/25/18.
 */

public class FragmentGRNDetails extends BaseFragment {

    private FragmentGrnDetailsBinding binding;
    private String InvNo = "";
    private GRNDetail grnDetail;
    private RVAdapterGRNViewDetails mAdapter;
    private List<GRNParent> listData;
    private BaseFragment mConBase;
    private GRNEepdlistview expandableListAdapter;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {

        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_grn_details, container, false);
        mConBase = this;
        AppSingle.getInstance().getActivity().setTitle("GRN", true);
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("Manage GRN"));
        if (getArguments() != null) {
            InvNo = getArguments().getString("InvoiceID");
            //Toast.makeText(AppSingle.getInstance().getActivity(), InvNo, Toast.LENGTH_LONG).show();
            callAsync();
        }
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.btnReject.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    callAsync("Reject");
                    //AppSingle.getInstance().getActivity().onBackPressed();
                } catch (Exception e) {
                }
            }
        });
        binding.btnAccept.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    callAsync("Accept");
                    //AppSingle.getInstance().getActivity().onBackPressed();
                } catch (Exception e) {
                }
            }
        });
    }

    private void callAsync() {

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<GRNDetail> call = apiService.pendingGNRDetails(AppSingle.getInstance().getLoginUser().getUserId(), RequestFormater.pendingGRNlist(InvNo));
        showProgessBar();
        call.enqueue(new Callback<GRNDetail>() {
            @Override
            public void onResponse(Call<GRNDetail> call, Response<GRNDetail> response) {
                hideProgessBar();
                try {
                    if (response.code() == 200) {
                        if (response.body() != null)
                            if (response.body().getGrnSkuLists() != null && response.body().getGrnSkuLists().size() > 0) {
                                grnDetail = response.body();
                                GRNParent resultList = new GRNParent(response.body().getGrnSkuLists(), grnDetail.getShortReceiveAllow());
                                binding.tvInvno.setText(grnDetail.getInvoiceNumber());
                                binding.tvInvdate.setText(grnDetail.getInvoiceDate());
                                binding.tvInvamount.setText(grnDetail.getTotalInvoiceAmount());
                                if (grnDetail.getRejectAllowed().equalsIgnoreCase("0")) {
                                    binding.btnReject.setEnabled(false);
                                    binding.btnReject.setBackgroundColor(Color.parseColor("#B2B2B2"));
                                    binding.btnReject.setVisibility(View.GONE);
                                }
                                listData = resultList.getlist();
                                expandableListAdapter = new GRNEepdlistview(AppSingle.getInstance().getActivity(), listData);
                                binding.rvListgrndetails.setAdapter(expandableListAdapter);
                            }
                    } else {
                        showResponseMessage(response.code());
                        if (response.code() == 401)
                            callAsync();
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<GRNDetail> call, Throwable t) {
                hideProgessBar();
                showToast("Fail");
            }
        });
    }

    private void callAsync(final String flag) {

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            OffLineData data = new OffLineData();
            data.setName("SaveGrn");
            data.setTime(MainActivity.getCurrentTime());
            data.setType("1");
            data.setRequestData(RequestFormater.saveGRN(flag, InvNo, binding.tvInvno.getText().toString(), binding.tvInvdate.getText().toString(), binding.tvInvamount.getText().toString(), grnDetail.getGrnSkuLists()));
            data.setStatus("1");
            data.setRemark("Save in local database");
            if (!isdublicate(data.getRequestData())) {
                showToast("Unable to process your request you request save offline when ever get connectivity we send it to server");
                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertOffLineData(data);
                //MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchOffLineData();
                MainActivity.setnotification();
            } else {
                showToast("Duplicate entry");
            }
            binding.btnAccept.setEnabled(false);
            binding.btnAccept.setBackgroundColor(Color.parseColor("#D3D3D3"));
            return;
        } else {
            if (!isdublicate(RequestFormater.saveGRN(flag, InvNo, binding.tvInvno.getText().toString(), binding.tvInvdate.getText().toString(), binding.tvInvamount.getText().toString(), grnDetail.getGrnSkuLists()))) {
                ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
                Call<ForgetModel> call = apiService.savependinggrn(AppSingle.getInstance().getLoginUser().getUserId(), RequestFormater.saveGRN(flag, InvNo, binding.tvInvno.getText().toString(), binding.tvInvdate.getText().toString(), binding.tvInvamount.getText().toString(), grnDetail.getGrnSkuLists()));
                showProgessBar();
                call.enqueue(new Callback<ForgetModel>() {
                    @Override
                    public void onResponse(Call<ForgetModel> call, Response<ForgetModel> response) {
                        hideProgessBar();
                        try {
                            if (response.code() == 200) {
                                if (response.body() != null) {
                                    showToast(response.body().getMessage());
                                    AppSingle.getInstance().getfeatchobj(mConBase);
                                    FlowOrganizer.getInstance().add(new FragmentMenu(), false);
                                } else {
                                    showToast("Response is null");
                                }

                            } else {
                                showResponseMessage(response.code());
                                if (response.code() == 401)
                                    callAsync(flag);
                            }
                        } catch (Exception e) {
                        }
                    }

                    @Override
                    public void onFailure(Call<ForgetModel> call, Throwable t) {
                        hideProgessBar();
                        showToast("Fail");
                    }
                });
            } else {
                showToast("This request already in the offline data plz accept their only");
            }
        }
    }

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }

    private boolean isdublicate(String str) {

        boolean dublicate = false;
        try {
            List<OffLineData> data = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchOffLineData();
            for (int i = 0; i < data.size(); i++) {
                if (data.get(i).getRequestData().equalsIgnoreCase(str))
                    dublicate = true;
            }
        } catch (Exception e) {
        }
        return dublicate;
    }
}
