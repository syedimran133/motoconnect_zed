package sales.zed.com.zedsales.fragment;

import android.app.DatePickerDialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.format.DateFormat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.DatePicker;

import java.text.SimpleDateFormat;
import java.util.Calendar;
import java.util.Date;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.adapter.RVTargetPrvList;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.apiz.RequestFormater;
import sales.zed.com.zedsales.constants.AppConstants;
import sales.zed.com.zedsales.databinding.FragmentPrvTargetBinding;
import sales.zed.com.zedsales.models.PreviousTargetList;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FlowOrganizer;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by root on 2/14/18.
 */

public class FragmentTargetDetail extends BaseFragment {

    private FragmentPrvTargetBinding binding;
    private final int INT_FROM = 0, INT_TO = 1;
    private int Date_day, Date_month, Date_year, selectedDateOption;
    private String type = "2";

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_prv_target, container, false);
        AppSingle.getInstance().getActivity().setTitle("Target", true);
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("Target"));
        try {
            binding.btnDetailTargetcurrent.setOnClickListener(listener);
            binding.btnDetailTargetpreviousprv.setOnClickListener(listener);
        } catch (Exception e) {
        }
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.ivTargetFrom.setOnClickListener(listener);
        binding.ivTargetTo.setOnClickListener(listener);
        binding.btnTargetProcess.setOnClickListener(listener);

        if (getArguments() != null) {
            type = getArguments().getString("TYPE");
            if (type.equalsIgnoreCase("1")) {
                Date d = new Date();
                CharSequence s = DateFormat.format("dd MMM yyyy ", d.getTime());
                binding.txtTargetFromdate.setText(s);
                binding.txtTargetTodate.setText(s);
                callAsync(0);
            } else {
                binding.txtTargetFromdate.setText(AppSingle.getfirstDate());
                binding.txtTargetTodate.setText(AppSingle.getCurrentDate());
            }
        }else{
            binding.txtTargetFromdate.setText(AppSingle.getfirstDate());
            binding.txtTargetTodate.setText(AppSingle.getCurrentDate());
        }
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.iv_target_from:
                    showDatePicker(INT_FROM);
                    break;
                case R.id.iv_target_to:
                    showDatePicker(INT_TO);
                    break;
                case R.id.btn_target_process:
                    callAsync(0);
                    break;
                case R.id.btn_detail_targetcurrent:
                    try {
                        binding.btnDetailTargetpreviousprv.setBackgroundColor(Color.parseColor("#A39F9E"));
                        binding.btnDetailTargetcurrent.setBackgroundColor(Color.parseColor("#00bfff"));
                        //FlowOrganizer.getInstance().addToInnerFrame(binding.frameInnerTarget, new FragmentTargetList());
                        FlowOrganizer.getInstance().add(new FragmentTargetList(), true);
                    } catch (Exception e) {
                    }
                    break;
                case R.id.btn_detail_targetpreviousprv:
                    try {
                        binding.btnDetailTargetpreviousprv.setBackgroundColor(Color.parseColor("#00bfff"));
                        binding.btnDetailTargetcurrent.setBackgroundColor(Color.parseColor("#A39F9E"));
                        Bundle bundle = new Bundle();
                        bundle.putString("TYPE", "2");
                        //FlowOrganizer.getInstance().addToInnerFrame(binding.frameInnerTarget, new FragmentTargetDetail(), true, bundle);
                        FlowOrganizer.getInstance().add(new FragmentTargetDetail(), true);
                    } catch (Exception e) {
                    }
                    break;
            }
        }
    };

    @Override
    public void onDestroy() {
        super.onDestroy();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }

    private void callAsync(int index) {
        String fromdate = binding.txtTargetFromdate.getText().toString();
        String todate = binding.txtTargetTodate.getText().toString();
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<PreviousTargetList> call = apiService.getTargetPrvList(AppSingle.getInstance().getLoginUser().getUserId(), index, RequestFormater.gettargetjson(fromdate, todate));
        showProgessBar();
        call.enqueue(new Callback<PreviousTargetList>() {
            @Override
            public void onResponse(Call<PreviousTargetList> call, Response<PreviousTargetList> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null)
                        if (response.body().getTargetList() != null && response.body().getTargetList().size() > 0) {
                            binding.llPrvTargetUpper.setVisibility(View.GONE);
                            RVTargetPrvList adapter = new RVTargetPrvList(AppSingle.getInstance().getActivity(), response.body().getTargetList());
                            RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
                            binding.rvListTargetReport.setLayoutManager(mLayoutManager);
                            binding.rvListTargetReport.setItemAnimator(new DefaultItemAnimator());
                            binding.rvListTargetReport.setAdapter(adapter);
                        } else {
                            showToast("Target List is empty.");
                        }
                } else {
                    showResponseMessage(response.code());
                    if (response.code() == 401)
                        callAsync(0);
                }
            }

            @Override
            public void onFailure(Call<PreviousTargetList> call, Throwable t) {
                hideProgessBar();
                showToast(AppConstants.IMessages.FAIL);
            }
        });
    }


    private void showDatePicker(int type) {
        try {
            selectedDateOption = type;
            resetDateToToday();
            getDateDialog(type).show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetDateToToday() {
        final Calendar c1 = Calendar.getInstance();
        Date_year = c1.get(Calendar.YEAR);
        Date_month = c1.get(Calendar.MONTH);
        Date_day = c1.get(Calendar.DAY_OF_MONTH);
    }

    private DatePickerDialog getDateDialog(int type) {
        switch (type) {
            case INT_FROM:
                DatePickerDialog dialog = new DatePickerDialog(AppSingle.getInstance().getActivity(),
                        pickerListener, Date_year, Date_month, Date_day);
                try {
                    dialog.getDatePicker().setMaxDate(System.currentTimeMillis());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return dialog;
            case INT_TO:
                DatePickerDialog dialoga = new DatePickerDialog(AppSingle.getInstance().getActivity(),
                        pickerListener, Date_year, Date_month, Date_day);
                try {
                    dialoga.getDatePicker().setMaxDate(System.currentTimeMillis());
                } catch (Exception e) {
                    e.printStackTrace();
                }
                return dialoga;
            default:
                return null;
        }
    }

    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {

            Date_year = selectedYear;
            Date_month = selectedMonth;
            Date_day = selectedDay;
            Calendar calendar = Calendar.getInstance();
            calendar.set(selectedYear, selectedMonth, selectedDay);
            SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
            String strDate = format.format(calendar.getTime());
            try {
                switch (selectedDateOption) {
                    case INT_FROM:
                        binding.txtTargetFromdate.setText(strDate.trim());
                        break;
                    case INT_TO:
                        binding.txtTargetTodate.setText(strDate.trim());
                        break;
                }
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };
}
