package sales.zed.com.zedsales.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.CheckBox;
import android.widget.ImageView;
import android.widget.LinearLayout;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.custom.CustomEditText;
import sales.zed.com.zedsales.custom.CustomTextView;
import sales.zed.com.zedsales.custom.CustomTextViewBold;

/**
 * Created by root on 2/2/18.
 */

public class SuggestedViewHolder extends RecyclerView.ViewHolder {

    public CustomTextViewBold mSuggestedSkuName;
    public CustomTextViewBold mSuggestedCurrentStock;
    public CustomTextView mSuggestedQuantity;
    public CheckBox mSuggestedCheck;
    public ImageView sminus,splus;
    public LinearLayout llSplusminus;

    public SuggestedViewHolder(View itemView) {
        super(itemView);

        mSuggestedSkuName=(CustomTextViewBold)itemView.findViewById(R.id.tv_skuname_suggested);
        mSuggestedCurrentStock=(CustomTextViewBold)itemView.findViewById(R.id.tv_corrent_stock);
        mSuggestedQuantity=(CustomTextView) itemView.findViewById(R.id.tv_suggestedquantity);
        mSuggestedCheck=(CheckBox)itemView.findViewById(R.id.suggest_check);
        sminus=(ImageView)itemView.findViewById(R.id.s_minus);
        splus=(ImageView)itemView.findViewById(R.id.s_plus);
        llSplusminus=(LinearLayout)itemView.findViewById(R.id.ll_incremental);
    }
}
