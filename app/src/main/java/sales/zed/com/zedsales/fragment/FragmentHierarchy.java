package sales.zed.com.zedsales.fragment;

import android.Manifest;
import android.content.Context;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.net.Uri;
import android.os.Bundle;
import android.support.annotation.NonNull;
import android.support.annotation.Nullable;
import android.support.v4.content.ContextCompat;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import com.google.gson.internal.LinkedTreeMap;

import org.json.JSONArray;
import org.json.JSONObject;

import java.util.EnumSet;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.databinding.FragmentHierarchyBinding;
import sales.zed.com.zedsales.databinding.LayouthercyBinding;
import sales.zed.com.zedsales.models.CurrentStockSkuItem;
import sales.zed.com.zedsales.models.HierarchyModel;
import sales.zed.com.zedsales.models.MenuModel;
import sales.zed.com.zedsales.support.AppPrefrence;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.Log;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by root on 09/12/17.
 */

public class FragmentHierarchy extends BaseFragment {

    private FragmentHierarchyBinding binding;
    private String[] perms = {"android.permission.CALL_PHONE"};
    private int permsRequestCode = 200;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_hierarchy, container, false);
        AppSingle.getInstance().getActivity().setTitle("Hierarchy", true);
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("Hierarchy"));
        return binding.getRoot();
    }

    public void addlayout(LinearLayout base, String name, String email, String phone) {

        LayoutInflater layoutInflater = (LayoutInflater) AppSingle.getInstance().getActivity().
                getSystemService(Context.LAYOUT_INFLATER_SERVICE);
        View convertView = layoutInflater.inflate(R.layout.layouthercy, null);

        TextView tvName = (TextView) convertView.findViewById(R.id.txt_hercy_name);
        final TextView tvEmail = (TextView) convertView.findViewById(R.id.txt_hercy_email);
        ImageView ivEmail = (ImageView) convertView.findViewById(R.id.ivEmail);
        final TextView tvPhone = (TextView) convertView.findViewById(R.id.txt_hercy_phone);
        LinearLayout ivPhone = (LinearLayout) convertView.findViewById(R.id.ivCall);

        ivEmail.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    Intent emailintent = new Intent(android.content.Intent.ACTION_SEND);
                    emailintent.setType("plain/text");
                    emailintent.putExtra(android.content.Intent.EXTRA_EMAIL, new String[]{tvEmail.getText().toString()});
                    emailintent.putExtra(android.content.Intent.EXTRA_SUBJECT, "");
                    emailintent.putExtra(android.content.Intent.EXTRA_TEXT, "");
                    startActivity(Intent.createChooser(emailintent, "Send mail..."));
                } catch (Exception e) {
                }
            }
        });

        ivPhone.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    requestPermissions(perms, permsRequestCode);
                    if (ContextCompat.checkSelfPermission(AppSingle.getInstance().getActivity(), Manifest.permission.CALL_PHONE) == PackageManager.PERMISSION_GRANTED) {
                        String number = "tel:" + tvPhone.getText().toString().trim();
                        Intent callIntent = new Intent(Intent.ACTION_CALL);
                        callIntent.setData(Uri.parse(number));
                        startActivity(callIntent);
                    }
                } catch (Exception e) {
                }
            }
        });

        tvName.setText(name);
        tvEmail.setText(email);
        tvPhone.setText(phone);
        base.addView(convertView);
    }

    @Override
    public void onRequestPermissionsResult(int requestCode, @NonNull String[] permissions, @NonNull int[] grantResults) {
        super.onRequestPermissionsResult(requestCode, permissions, grantResults);
        try {
            switch (permsRequestCode) {
                case 200:
                    boolean cameraAccepted = grantResults[0] == PackageManager.PERMISSION_GRANTED;
                    break;
            }
        } catch (Exception e) {
        }
    }

    public void addlayout2(LinearLayout base, String name, String email, String phone) {
        try {
            LayouthercyBinding binding = DataBindingUtil.inflate(LayoutInflater.from(AppSingle.getInstance().getActivity().getApplicationContext()), R.layout.layouthercy, null, false);
            binding.txtHercyName.setText(name);
            binding.txtHercyEmail.setText(email);
            binding.txtHercyPhone.setText(phone);
            base.addView(binding.getRoot());
        } catch (Exception e) {
        }
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        try {
            if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
                JSONObject jsnobject = new JSONObject(AppPrefrence.getInstance().getHierarchy());
                populate_ui(jsnobject);
            } else {
                callAsync();
            }
        } catch (Exception e) {
            Log.e("ERROR",e.toString());
        }
    }


    private void callAsync() {
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<Object> call = apiService.GetHierarchy(AppSingle.getInstance().getLoginUser().getUserId());
        showProgessBar();
        call.enqueue(new Callback<Object>() {
            @Override
            public void onResponse(Call<Object> call, Response<Object> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    JSONObject jsnobject = new JSONObject(((LinkedTreeMap) response.body()));
                    AppPrefrence.getInstance().setHierarchy(jsnobject.toString());
                    populate_ui(jsnobject);
                } else {
                    showResponseMessage(response.code());
                    if (response.code() == 401)
                        callAsync();
                }
            }

            @Override
            public void onFailure(Call<Object> call, Throwable t) {
                hideProgessBar();
                showToast("Fail");
            }
        });
    }

    @Override
    public void onDestroyView() {
        super.onDestroyView();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }

    private void populate_ui(JSONObject jsnobj) {
        try {
            JSONObject jsnobject = jsnobj;
            JSONArray dse = jsnobject.getJSONArray("DSE");
            for (int i = 0; i < dse.length(); i++) {
                try {
                    JSONObject explrObject = dse.getJSONObject(i);
                    String name = explrObject.getString("Name");
                    String email = explrObject.getString("Email");
                    String mobile = explrObject.getString("MobileNo");
                    addlayout(binding.llhercyDse, name, email, mobile);
                } catch (Exception e) {
                }
            }

            JSONArray distributor = jsnobject.getJSONArray("Distributor");
            for (int i = 0; i < distributor.length(); i++) {
                try {
                    JSONObject explrObject = distributor.getJSONObject(i);
                    String name = explrObject.getString("Name");
                    String email = explrObject.getString("Email");
                    String mobile = explrObject.getString("MobileNo");
                    addlayout(binding.llhercyDistributor, name, email, mobile);
                } catch (Exception e) {
                }
            }
            JSONArray areaSalesManager = jsnobject.getJSONArray("AreaSalesManager");
            for (int i = 0; i < areaSalesManager.length(); i++) {
                try {
                    JSONObject explrObject = areaSalesManager.getJSONObject(i);
                    String name = explrObject.getString("Name");
                    String email = explrObject.getString("Email");
                    String mobile = explrObject.getString("MobileNo");
                    addlayout(binding.llhercyAreasalesmanager, name, email, mobile);
                } catch (Exception e) {
                }
            }
            JSONArray regionalSalesManager = jsnobject.getJSONArray("RegionalSalesManager");
            for (int i = 0; i < regionalSalesManager.length(); i++) {
                try {
                    JSONObject explrObject = regionalSalesManager.getJSONObject(i);
                    String name = explrObject.getString("Name");
                    String email = explrObject.getString("Email");
                    String mobile = explrObject.getString("MobileNo");
                    addlayout(binding.llhercyRegionalsalesmanager, name, email, mobile);
                } catch (Exception e) {
                }
            }
            JSONArray nationalSaleManager = jsnobject.getJSONArray("NationalSalesManager");
            for (int i = 0; i < regionalSalesManager.length(); i++) {
                try {
                    JSONObject explrObject = nationalSaleManager.getJSONObject(i);
                    String name = explrObject.getString("Name");
                    String email = explrObject.getString("Email");
                    String mobile = explrObject.getString("MobileNo");
                    addlayout(binding.llhercyNationalSaleManager, name, email, mobile);
                } catch (Exception e) {
                }
            }
        } catch (Exception e) {
        }
    }
}
