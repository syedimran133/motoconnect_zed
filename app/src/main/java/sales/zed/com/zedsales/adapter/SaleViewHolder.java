package sales.zed.com.zedsales.adapter;

import android.support.v7.widget.RecyclerView;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.TextView;

import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.custom.CustomTextView;

/**
 * Created by root on 1/30/18.
 */

public class SaleViewHolder extends RecyclerView.ViewHolder {

    public TextView mSaleSkuName;
    public TextView mSaleSerial;
    public CustomTextView mSaleQty;
    public ImageView mSaleDelete;
    public Button minus, plus;
    public LinearLayout llPlusminus, serlayout;

    public SaleViewHolder(View itemView) {
        super(itemView);
        mSaleSkuName = (TextView) itemView.findViewById(R.id.tv_sale_sku_name);
        mSaleSerial = (TextView) itemView.findViewById(R.id.tv_sale_serial);
        mSaleQty = (CustomTextView) itemView.findViewById(R.id.tv_sale_qty);
        mSaleDelete = (ImageView) itemView.findViewById(R.id.iv_delete);
        minus = (Button) itemView.findViewById(R.id.decrease);
        llPlusminus = (LinearLayout) itemView.findViewById(R.id.ll_plusminus);
        serlayout = (LinearLayout) itemView.findViewById(R.id.ll_serlayout);
        plus = (Button) itemView.findViewById(R.id.increase);
    }
}
