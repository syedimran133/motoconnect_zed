package sales.zed.com.zedsales.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class SkuListsItem {

    @SerializedName("Status")
    @Expose
    private boolean status;

    @SerializedName("SkuDescription")
    @Expose
    private String skuDescription;

    @SerializedName("ModifiedOn")
    @Expose
    private String modifiedOn;

    @SerializedName("SkuCode")
    @Expose
    private String skuCode;

    @SerializedName("ProductCategoryId")
    @Expose
    private int productCategoryId;

    @SerializedName("RecordCreationDate")
    @Expose
    private String recordCreationDate;

    @SerializedName("SkuName")
    @Expose
    private String skuName;

    @SerializedName("ProductId")
    @Expose
    private int productId;

    @PrimaryKey
    @SerializedName("SkuId")
    @Expose
    private int skuId;

    @SerializedName("BrandId")
    @Expose
    private int brandId;

    @SerializedName("ModelId")
    @Expose
    private int modelId;

    @SerializedName("ColorId")
    @Expose
    private int colorId;

    @SerializedName("SerialisedMode")
    @Expose
    private int serialisedMode;

    @SerializedName("Price")
    @Expose
    private int price;

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }

    public void setSkuDescription(String skuDescription) {
        this.skuDescription = skuDescription;
    }

    public String getSkuDescription() {
        return skuDescription;
    }

    public void setModifiedOn(String modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String getModifiedOn() {
        return modifiedOn;
    }

    public void setSkuCode(String skuCode) {
        this.skuCode = skuCode;
    }

    public String getSkuCode() {
        return skuCode;
    }

    public void setProductCategoryId(int productCategoryId) {
        this.productCategoryId = productCategoryId;
    }

    public int getProductCategoryId() {
        return productCategoryId;
    }

    public void setRecordCreationDate(String recordCreationDate) {
        this.recordCreationDate = recordCreationDate;
    }

    public String getRecordCreationDate() {
        return recordCreationDate;
    }

    public void setSkuName(String skuName) {
        this.skuName = skuName;
    }

    public String getSkuName() {
        return skuName;
    }

    public void setProductId(int productId) {
        this.productId = productId;
    }

    public int getProductId() {
        return productId;
    }

    public void setSkuId(int skuId) {
        this.skuId = skuId;
    }

    public int getSkuId() {
        return skuId;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getBrandId() {
        return brandId;
    }

    public void setModelId(int modelId) {
        this.modelId = modelId;
    }

    public int getModelId() {
        return modelId;
    }

    public void setColorId(int colorId) {
        this.colorId = colorId;
    }

    public int getColorId() {
        return colorId;
    }

    public int getSerialisedMode() {
        return serialisedMode;
    }

    public void setSerialisedMode(int serialisedMode) {
        this.serialisedMode = serialisedMode;
    }


    public int getPrice() {
        return price;
    }

    public void setPrice(int price) {
        this.price = price;
    }

    @Override
    public String toString() {
        return
                "SkuListsItem{" +
                        "status = '" + status + '\'' +
                        ",skuDescription = '" + skuDescription + '\'' +
                        ",modifiedOn = '" + modifiedOn + '\'' +
                        ",skuCode = '" + skuCode + '\'' +
                        ",productCategoryId = '" + productCategoryId + '\'' +
                        ",recordCreationDate = '" + recordCreationDate + '\'' +
                        ",skuName = '" + skuName + '\'' +
                        ",productId = '" + productId + '\'' +
                        ",skuId = '" + skuId + '\'' +
                        ",brandId = '" + brandId + '\'' +
                        ",modelId = '" + modelId + '\'' +
                        ",colorId = '" + colorId + '\'' +
                        ",serialisedMode = '" + serialisedMode + '\'' +
                        ",price = '" + price + '\'' +
                        "}";
    }
}