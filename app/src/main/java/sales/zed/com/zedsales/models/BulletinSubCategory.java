
package sales.zed.com.zedsales.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class BulletinSubCategory {

    @SerializedName("Bulletin")
    @Expose
    private List<Bulletin> bulletin = null;

    @SerializedName("SubCategoryName")
    @Expose
    private String subCategoryName;

    @SerializedName("SubCategoryId")
    @Expose
    private String subCategoryId;

    public List<Bulletin> getBulletin() {
        return bulletin;
    }

    public void setBulletin(List<Bulletin> bulletin) {
        this.bulletin = bulletin;
    }

    public String getSubCategoryName() {
        return subCategoryName;
    }

    public void setSubCategoryName(String subCategoryName) {
        this.subCategoryName = subCategoryName;
    }

    public String getSubCategoryId() {
        return subCategoryId;
    }

    public void setSubCategoryId(String subCategoryId) {
        this.subCategoryId = subCategoryId;
    }

}
