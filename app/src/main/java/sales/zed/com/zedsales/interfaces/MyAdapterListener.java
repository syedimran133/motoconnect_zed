package sales.zed.com.zedsales.interfaces;

import android.view.View;

/**
 * Created by root on 1/29/18.
 */

public interface MyAdapterListener {
    void iconButtonViewOnClick(View v, int position);
}
