
package sales.zed.com.zedsales.models;

import java.util.List;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

public class ListBulletin {

    @SerializedName("GetBulletinCategoryList")
    @Expose
    private List<BulletinCategoryList> getBulletinCategoryList = null;

    public List<BulletinCategoryList> getGetBulletinCategoryList() {
        return getBulletinCategoryList;
    }

    public void setGetBulletinCategoryList(List<BulletinCategoryList> getBulletinCategoryList) {
        this.getBulletinCategoryList = getBulletinCategoryList;
    }

}
