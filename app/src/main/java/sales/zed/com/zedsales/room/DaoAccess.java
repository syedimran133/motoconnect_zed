package sales.zed.com.zedsales.room;

import android.arch.persistence.room.Dao;
import android.arch.persistence.room.Delete;
import android.arch.persistence.room.Insert;
import android.arch.persistence.room.OnConflictStrategy;
import android.arch.persistence.room.Query;
import android.arch.persistence.room.Update;

import java.util.List;

import sales.zed.com.zedsales.models.BrandListsItem;
import sales.zed.com.zedsales.models.GRNStatusItem;
import sales.zed.com.zedsales.models.MenuListData;
import sales.zed.com.zedsales.models.ModelListsItem;
import sales.zed.com.zedsales.models.NotificationData;
import sales.zed.com.zedsales.models.OffLineData;
import sales.zed.com.zedsales.models.ProductCategoryListsItem;
import sales.zed.com.zedsales.models.ProductListsItem;
import sales.zed.com.zedsales.models.ProfileData;
import sales.zed.com.zedsales.models.ReportTypeItem;
import sales.zed.com.zedsales.models.SerialMaster;
import sales.zed.com.zedsales.models.SkuListsItem;
import sales.zed.com.zedsales.models.StockItems;
import sales.zed.com.zedsales.models.StockTypeItem;
import sales.zed.com.zedsales.models.UserModel;

/**
 * Created by Mukesh on 17/12/17.
 */

@Dao
public interface DaoAccess {

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertUser(UserModel UserModel);

    @Query("SELECT * FROM UserModel")
    List<UserModel> fetchUserList();

//    @Query("SELECT * FROM UserModel WHERE clgid =:college_id")
//    UserModel getSingleRecord(int college_id);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateUser(UserModel data);

    @Query("DELETE FROM UserModel")
    void deleteUser();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertBrand(BrandListsItem data);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateBrand(BrandListsItem data);

    @Query("DELETE FROM BrandListsItem")
    void deleteBrand();

    @Query("SELECT * FROM BrandListsItem")
    List<BrandListsItem> fetchBrandList();


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertProduct(ProductListsItem data);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateProduct(ProductListsItem data);

    @Query("DELETE FROM ProductListsItem")
    void deleteProductList();

    @Query("SELECT * FROM ProductListsItem")
    List<ProductListsItem> fetchProductList();


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertProductCategory(ProductCategoryListsItem data);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateProductCategory(ProductCategoryListsItem data);

    @Query("DELETE FROM ProductCategoryListsItem")
    void deleteProductCategoryListsItem();

    @Query("SELECT * FROM ProductCategoryListsItem")
    List<ProductCategoryListsItem> fetchProductCategoryList();


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertMaster(ModelListsItem data);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateMaster(ModelListsItem data);

    @Query("DELETE FROM ModelListsItem")
    void deleteModelLists();

    @Query("SELECT * FROM ModelListsItem")
    List<ModelListsItem> fetchMasterList();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertSkuMaster(SkuListsItem data);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateSkuMaster(SkuListsItem data);

    @Query("DELETE FROM SkuListsItem")
    void deleteSkuMaster();

    @Query("SELECT * FROM SkuListsItem")
    List<SkuListsItem> fetchSkuMasterList();

    /*@Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertProfile(ProfileData data);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateProfile(ProfileData data);

    @Query("SELECT * FROM ProfileData")
    List<ProfileData> fetchProfiledata();*/

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertSerialMaster(SerialMaster data);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateSerialMaster(SerialMaster data);

    @Query("SELECT * FROM SerialMaster where RetailerId= :r_id")
    List<SerialMaster> fetchSerialMaster(String r_id);

    @Query("SELECT * FROM SerialMaster where RetailerId= :r_id and SkuId= :skuname")
    List<SerialMaster> fetchSerialMasterSale(String r_id,String skuname);

    @Query("SELECT * FROM SerialMaster where SerialNumber1= :s_no")
    List<SerialMaster> fetchSerialMasterSaleSerial(String s_no);

    @Query("DELETE FROM SerialMaster")
    void deleteSerialMaster();

    @Query("DELETE FROM SerialMaster where SerialNumber1= :s_no")
    void deleteSerialMasterBySr(String s_no);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertStock(StockItems data);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateStock(StockItems data);

    @Query("DELETE FROM StockItems")
    void deleteStock();

    @Query("SELECT * FROM StockItems")
    List<StockItems> fetchStockList();


    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertStockType(StockTypeItem data);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateStockType(StockTypeItem data);

    @Query("DELETE FROM StockTypeItem")
    void deleteStockTypeItem();

    @Query("SELECT * FROM StockTypeItem")
    List<StockTypeItem> fetchStockTypeList();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertReportType(ReportTypeItem data);

    @Query("DELETE FROM ReportTypeItem")
    void deleteReportType();

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateReportType(ReportTypeItem data);

    @Query("SELECT * FROM ReportTypeItem")
    List<ReportTypeItem> fetchReportTypeList();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertGRNStatus(GRNStatusItem data);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateGRNStatus(GRNStatusItem data);

    @Query("DELETE FROM GRNStatusItem")
    void deleteGRNStatusItem();

    @Query("SELECT * FROM GRNStatusItem")
    List<GRNStatusItem> fetchGRNStatus();

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertMenu(MenuListData data);

    @Query("DELETE FROM MenuListData")
    void deleteMenulistItem();

    @Query("SELECT * FROM MenuListData")
    List<MenuListData> fetchMenu();


    @Query("DELETE FROM OffLineData where id= :s_no")
    void deleteSerialOffLineData(String s_no);

    @Insert(onConflict = OnConflictStrategy.REPLACE)
    void insertOffLineData(OffLineData data);

    @Query("SELECT * FROM OffLineData")
    List<OffLineData> fetchOffLineData();

    @Query("SELECT * FROM OffLineData where status= :status")
    List<OffLineData> fetchOffLineDatawithstatus(String status);

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateOffLineData(OffLineData data);

    @Query("DELETE FROM NotificationData where id= :s_no")
    void deleteNotificationData(String s_no);

    @Insert(onConflict = OnConflictStrategy.IGNORE)
    void insertNotificationData(NotificationData data);

    @Query("SELECT * FROM NotificationData")
    List<NotificationData> fetchNotificationData();

    @Update(onConflict = OnConflictStrategy.IGNORE)
    void updateNotificationData(NotificationData data);
}
