package sales.zed.com.zedsales.models;

import android.arch.persistence.room.Entity;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by Syed Imran Iqbal on 1/20/2018.
 */

@Entity
public class ProfileData {

    @SerializedName("Editable")
    @Expose
    private List<EditableProfileData> edtabledata;

    @SerializedName("RetailerProfile")
    @Expose
    private List<ProfileItems> retailerProfile;

    public List<EditableProfileData> getEdtabledata() {
        return edtabledata;
    }

    public void setEdtabledata(List<EditableProfileData> edtabledata) {
        this.edtabledata = edtabledata;
    }

    public List<ProfileItems> getRetailerProfile() {
        return retailerProfile;
    }

    public void setRetailerProfile(List<ProfileItems> retailerProfile) {
        this.retailerProfile = retailerProfile;
    }

   /* @Override
    public String toString() {
        return
                "MenuLists{" +
                        "MenuId = '" + menuId + '\'' +
                        ",MenuName = '" + menuName + '\'' +
                        ",DisplayOrder = '" + displayOrder + '\'' +
                        ",ShowInMenuBar = '" + showInMenuBar + '\'' +
                        "}";
    }
	"RetailerProfile{" +
		"CompanyName = '"+retailerProfile.get(0)+ '\'' +
		"UserCode = '"++ '\'' +
		"UserName = '": "demo"+ '\'' +
		"RegisteredMobileNo = '": "8860414336"+ '\'' +
		"AlternateMobileNo = '": "8882858828"+ '\'' +
		"EmailId = '": "syedimran133@gmail.com"+ '\'' +
		"DateOfBirth = '": "12 Apr 1968"+ '\'' +
		"Married = '": "NO"+ '\'' +
		"DateOfAnniversary = '": "01 Jan 1900"+ '\'' +
		"Address = '": "Lajpat Nager 4  "+ '\'' +
		"City = '": "New Delhi"+ '\'' +
		"State = '": "Delhi"+ '\'' +
		"PinCode = '": "110058"+ '\'' +
		"CounterPotentialValue = '": "2"+ '\'' +
		"CounterPotentialVolume = '": ""+ '\'' +
		"AccountHolderName = '": "SYED IMRAN IQBAL"+ '\'' +
		"AccountNumber = '": "1038476535467354"+ '\'' +
		"BankName = '": "aXIS bANK",+ '\'' +
		"BranchName = '": "jAMIA"+ '\'' +
		"BranchLocation = '": "Okhla Vihar"+ '\'' +
		"IfscCode = '": "HDFC0002014"+ '\'' +
		"AadharNumber = '": ""+ '\'' +
		"PanNumber = '": ""+ '\'' +
		"GstinNo = '": ""
	}+ '\'' +
	"Editable{"
		"CompanyName = '": "0"+ '\'' +
		"UserCode = '": "0"+ '\'' +
		"UserName = '": "0"+ '\'' +
		"RegisteredMobileNo = '": "1"+ '\'' +
		"AlternateMobileNo = '": "1"+ '\'' +
		"EmailId = '": "1"+ '\'' +
		"DateOfBirth = '": "1"+ '\'' +
		"Married = '": "1"+ '\'' +
		"DateOfAnniversary = '": "1"+ '\'' +
		"Address = '": "1"+ '\'' +
		"City = '": "1"+ '\'' +
		"State = '": "1",+ '\'' +
		"PinCode = '": "1"+ '\'' +
		"CounterPotentialValue = '": "1"+ '\'' +
		"CounterPotentialVolume = '": "0"+ '\'' +
		"AccountHolderName = '": "1"+ '\'' +
		"AccountNumber = '": "1"+ '\'' +
		"BankName = '": "1"+ '\'' +
		"BranchName = '": "1"+ '\'' +
		"BranchLocation = '": "1"+ '\'' +
		"IfscCode = '": "1"+ '\'' +
		"AadharNumber = '": "0"+ '\'' +
		"PanNumber = '": "0"+ '\'' +
		"GstinNo = '": "0"
	}*/
}
