package sales.zed.com.zedsales.fragment;

import android.app.Dialog;
import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.ExpandableListAdapter;
import android.widget.ExpandableListView;
import android.widget.Toast;

import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.adapter.AdapterDialogListItem;
import sales.zed.com.zedsales.adapter.CurrentStockAdapter;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.apiz.RequestFormater;
import sales.zed.com.zedsales.constants.AppConstants;
import sales.zed.com.zedsales.databinding.DialogListBinding;
import sales.zed.com.zedsales.databinding.FragmentReportsMyCurrentStockBinding;
import sales.zed.com.zedsales.interfaces.IDialogListClickListener;
import sales.zed.com.zedsales.models.BrandListsItem;
import sales.zed.com.zedsales.models.CurrentStockSkuList;
import sales.zed.com.zedsales.models.ForgetModel;
import sales.zed.com.zedsales.models.ModelListsItem;
import sales.zed.com.zedsales.models.ReportTypeItem;
import sales.zed.com.zedsales.models.SaleSkuListItem;
import sales.zed.com.zedsales.models.SkuListsItem;
import sales.zed.com.zedsales.models.StockTypeItem;
import sales.zed.com.zedsales.room.MyDatabase;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by Mukesh on 09/12/17.
 */

public class FragmentReportMyCurrentStock extends BaseFragment {

    private ExpandableListAdapter expandableListAdapter;
    private FragmentReportsMyCurrentStockBinding binding;
    final ArrayList<String> listbrandid = new ArrayList<>();
    final ArrayList<String> listskuid = new ArrayList<>();
    final ArrayList<String> listmodelid = new ArrayList<>();
    private String brandid = "", modelid = "", skuid = "";
    private int selectedBrandPos = -1, selectedModelPos = -1, selectedSkuPos = -1, selectedStockTypePos = -1, selectedReportPos = -1;
    private List<String> listBrand, listBrandid, listModel, listModelid, listSku, listReportType, listskytype;
    private List<BrandListsItem> fetchBrandList;
    private List<ModelListsItem> fetchMasterList;
    private List<SkuListsItem> fetchSkuMasterList;
    private List<ReportTypeItem> fetchReportTypeList;
    private List<StockTypeItem> fetchStockType;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_reports_my_current_stock, container, false);
        featchDB();
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.relativeCurrentstockBrand.setOnClickListener(listener);
        binding.relativeCurrentstockModel.setOnClickListener(listener);
        binding.relativeCurrentstockSku.setOnClickListener(listener);
        binding.relativeCurrentstockDatatype.setOnClickListener(listener);
        binding.relativeCurrentstockStocktype.setOnClickListener(listener);
        binding.btnCurrentstockProcess.setOnClickListener(listener);
        //binding.explist.
    }

    public String getstockid(String stockName) {

        String stockid = "";
        for (int i = 0; i < fetchStockType.size(); i++) {
            if (fetchStockType.get(i).getStockBinTypeDesc().equalsIgnoreCase(stockName)) {
                stockid = "" + fetchStockType.get(i).getStockBinTypeMasterId();
            }
        }
        return stockid;
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.relative_currentstock_brand:
                    setBrandList();
                    break;
                case R.id.relative_currentstock_model:
                    setModelList();
                    break;
                case R.id.relative_currentstock_sku:
                    setSkuList();
                    break;
                case R.id.relative_currentstock_datatype:
                    setReportTypeList();
                    break;
                case R.id.relative_currentstock_stocktype:
                    setStockTypeList();
                    break;
                case R.id.btn_currentstock_process:
                    if (!binding.txtCurrentstockData.getText().toString().equalsIgnoreCase("")) {
                        sendToServer(getReportTypeIndex(binding.txtCurrentstockData.getText().toString()));
                    } else {
                        showToast("Please select preview type");
                    }
                    break;
            }
        }
    };

    private void callAsync(int pageno) {

        String brand = "";
        String model = "";
        String sku = "";

        if (!binding.txtCurrentstockBrand.getHint().toString().equalsIgnoreCase("Brand")) {
            brand = binding.txtCurrentstockBrand.getHint().toString();
        }
        if (!binding.txtCurrentstockModel.getHint().toString().equalsIgnoreCase("Model")) {
            model = binding.txtCurrentstockModel.getHint().toString();
        }
        if (!binding.txtCurrentstockSku.getHint().toString().equalsIgnoreCase("Sku")) {
            sku = binding.txtCurrentstockSku.getHint().toString();
        }

        String stocktypeid = getstockid(binding.txtCurrentstockStocktype.getText().toString());
        String previewstype = binding.txtCurrentstockData.getText().toString();

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<CurrentStockSkuList> call = apiService.getcurrentstock(AppSingle.getInstance().getLoginUser().getUserId(), pageno, RequestFormater.getcurrentstockreport(brand, model, sku, stocktypeid, previewstype));
        showProgessBar();
        call.enqueue(new Callback<CurrentStockSkuList>() {
            @Override
            public void onResponse(Call<CurrentStockSkuList> call, Response<CurrentStockSkuList> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null)
                        if (response.body().getCurrentStockSkuList() != null && response.body().getCurrentStockSkuList().size() > 0) {
                            binding.explist.setVisibility(View.VISIBLE);
                            expandableListAdapter = new CurrentStockAdapter(AppSingle.getInstance().getActivity(), response.body().getCurrentStockSkuList());
                            binding.explist.setAdapter(expandableListAdapter);

                        } else {
                            binding.explist.setVisibility(View.GONE);
                            showToast("List is empty.");
                        }
                } else {
                    showResponseMessage(response.code());
                    if (response.code() == 401)
                        callAsync(0);
                }
            }

            @Override
            public void onFailure(Call<CurrentStockSkuList> call, Throwable t) {
                hideProgessBar();
                showToast(AppConstants.IMessages.FAIL);
            }
        });

    }

    public void sendToServer(int type) {

        switch (type) {
            case 0:
                showToast("Coming soon");
                break;
            case 1:
                callAsync(0);
                break;
            case 2:
                callAsyncMailTo();
                break;
        }
    }

    public int getReportTypeIndex(String name) {
        int index = 0;
        for (int i = 0; i < AppConstants.IPrefConstant.reporttype.length; i++) {
            if (AppConstants.IPrefConstant.reporttype[i].equalsIgnoreCase(name.trim())) {
                index = i;
            }
        }
        return index;
    }

    private void callAsyncMailTo() {

        String brand = "";
        String model = "";
        String sku = "";

        if (!binding.txtCurrentstockBrand.getHint().toString().equalsIgnoreCase("Brand")) {
            brand = binding.txtCurrentstockBrand.getHint().toString();
        }
        if (!binding.txtCurrentstockModel.getHint().toString().equalsIgnoreCase("Model")) {
            model = binding.txtCurrentstockModel.getHint().toString();
        }
        if (!binding.txtCurrentstockSku.getHint().toString().equalsIgnoreCase("Sku")) {
            sku = binding.txtCurrentstockSku.getHint().toString();
        }
        String stocktypeid = getstockid(binding.txtCurrentstockStocktype.getText().toString());
        String previewstype = binding.txtCurrentstockData.getText().toString();

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            showToast(NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<ForgetModel> call = apiService.getcurrentstockmailto(AppSingle.getInstance().getLoginUser().getUserId(), 0, RequestFormater.getcurrentstockreport(brand, model, sku, stocktypeid, previewstype));
        showProgessBar();
        call.enqueue(new Callback<ForgetModel>() {
            @Override
            public void onResponse(Call<ForgetModel> call, Response<ForgetModel> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null) {
                        if (response.body().getStatusCode() == 0) {

                            showToast(response.body().getMessage());
                        } else {
                            showToast(response.body().getMessage());
                        }
                    } else {
                        showToast("Message body is null");
                    }
                } else {
                    showResponseMessage(response.code());
                    if (response.code() == 401)
                        callAsyncMailTo();
                }
            }

            @Override
            public void onFailure(Call<ForgetModel> call, Throwable t) {
                hideProgessBar();
                showToast(AppConstants.IMessages.FAIL);
            }
        });

    }

    private void setReportTypeList() {

        if (listReportType == null) {
            listReportType = new ArrayList<>();
            for (ReportTypeItem data : fetchReportTypeList) {
                if (data.getStatus().equalsIgnoreCase("1")) {
                    listReportType.add(data.getReportName());
                }

            }
        }
        openListDialog("Select Data", selectedReportPos, listReportType, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedReportPos = selectedPos;
                    binding.txtCurrentstockData.setText(listReportType.get(selectedPos));
                } catch (Exception e) {
                }
            }
        });

    }

    public void setdata(int code) {
        switch (code) {
            case 1:
                if (listBrand == null) {
                    listBrand = new ArrayList<>();
                    for (BrandListsItem data : fetchBrandList) {
                        if (data.isStatus()) {
                            listBrand.add(data.getBrandName());
                            listbrandid.add("" + data.getBrandId());
                        }

                    }
                }
                try {
                    listModel = null;
                    listSku = null;
                    modelid = "";
                    skuid = "";
                    listmodelid.clear();
                    listSku.clear();
                } catch (Exception e) {
                }
                binding.txtCurrentstockBrand.setText("");
                binding.txtCurrentstockModel.setText("");
                binding.txtCurrentstockSku.setText("");
                binding.txtCurrentstockBrand.setHint("Brand");
                binding.txtCurrentstockModel.setHint("Model");
                binding.txtCurrentstockSku.setHint("Sku");
                break;

            case 2:
                if (listModel == null) {
                    listModel = new ArrayList<>();
                    for (ModelListsItem data : fetchMasterList) {
                        if (data.isStatus()) {
                            if ((data.getBrandId() + "").equalsIgnoreCase(brandid)) {
                                listModel.add(data.getModelName());
                                listmodelid.add("" + data.getModelId());
                            }
                        }
                    }
                }
                try {
                    listSku = null;
                    skuid = "";
                    listSku.clear();
                } catch (Exception e) {
                }
                binding.txtCurrentstockModel.setText("");
                binding.txtCurrentstockSku.setText("");
                binding.txtCurrentstockModel.setHint("Model");
                binding.txtCurrentstockSku.setHint("Sku");
                break;
            case 3:
                if (listSku == null) {
                    listSku = new ArrayList<>();
                    for (SkuListsItem data : fetchSkuMasterList) {
                        if (data.isStatus() && modelid.equalsIgnoreCase("" + data.getModelId())) {
                            listSku.add(data.getSkuName());
                            listskuid.add("" + data.getSkuId());
                        }
                    }
                }

                break;
        }
    }

    private void setBrandList() {

        setdata(1);
        openListDialog("Select Brand", selectedBrandPos, listBrand, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedBrandPos = selectedPos;
                    binding.txtCurrentstockBrand.setText(listBrand.get(selectedPos));
                    binding.txtCurrentstockBrand.setHint(listbrandid.get(selectedPos));
                    //binding.txtCurrentstockBrand.setHintTextColor(Color.parseColor("#FFFFFF"));
                    brandid = listbrandid.get(selectedPos);
                } catch (Exception e) {
                }
            }
        });
    }

    private void setModelList() {
        setdata(2);
        openListDialog("Select Model", selectedModelPos, listModel, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedModelPos = selectedPos;
                    binding.txtCurrentstockModel.setText(listModel.get(selectedPos));
                    binding.txtCurrentstockModel.setHint(listmodelid.get(selectedPos));
                    //binding.txtCurrentstockModel.setHintTextColor(Color.parseColor("#FFFFFF"));
                    modelid = listmodelid.get(selectedPos);

                } catch (Exception e) {
                }
            }
        });

    }

    private void setSkuList() {

        setdata(3);
        openListDialog("Select Product Category", selectedSkuPos, listSku, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedSkuPos = selectedPos;
                    binding.txtCurrentstockSku.setText(listSku.get(selectedPos));
                    binding.txtCurrentstockSku.setHint(listskuid.get(selectedPos));
                    //binding.txtCurrentstockSku.setHintTextColor(Color.parseColor("#FFFFFF"));
                    skuid = listskuid.get(selectedPos);

                } catch (Exception e) {
                }
            }
        });

    }

    protected void openListDialog(String title, int selectedPos, final List<String> listData, final IDialogListClickListener listener) {
        DialogListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(AppSingle.getInstance().getActivity().getApplicationContext()), R.layout.dialog_list, null, false);
        final Dialog dialog = new Dialog(AppSingle.getInstance().getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(binding.getRoot());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        binding.txtViewTitle.setText(title);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        LinearLayoutManager lLayout = new LinearLayoutManager(AppSingle.getInstance().getActivity());
        RecyclerView rView = binding.rvList;
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);
        AdapterDialogListItem adapter = new AdapterDialogListItem(listData, selectedPos);
        rView.setAdapter(adapter);
        adapter.registerOnItemClickListener(new AdapterDialogListItem.IonItemSelect() {
            @Override
            public void onItemSelect(int position) {
                if (listener != null)
                    listener.onClick(listData.get(position), position);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    private void setStockTypeList() {

        if (listskytype == null) {
            listskytype = new ArrayList<>();
            for (StockTypeItem data : fetchStockType) {
                if (data.getActive()) {
                    listskytype.add(data.getStockBinTypeDesc());
                }
            }
        }
        openListDialog("Select Stock Type", selectedStockTypePos, listskytype, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedStockTypePos = selectedPos;
                    binding.txtCurrentstockStocktype.setText(listskytype.get(selectedPos));
                } catch (Exception e) {
                }
            }
        });

    }

    public void featchDB() {
        fetchBrandList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchBrandList();
        fetchReportTypeList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchReportTypeList();
        fetchStockType = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchStockTypeList();
        fetchMasterList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchMasterList();
        fetchSkuMasterList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchSkuMasterList();
    }

}
