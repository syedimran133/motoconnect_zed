package sales.zed.com.zedsales.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;
import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class BrandListsItem {

    @SerializedName("BrandName")
    @Expose
    private String brandName;

    @SerializedName("Status")
    @Expose
    private boolean status;

    @SerializedName("ModifiedOn")
    @Expose
    private String modifiedOn;

    @SerializedName("RecordCreationDate")
    @Expose
    private String recordCreationDate;

    @SerializedName("BrandCode")
    @Expose
    private String brandCode;

    @PrimaryKey
    @SerializedName("BrandId")
    @Expose
    private int brandId;

    public void setBrandName(String brandName) {
        this.brandName = brandName;
    }

    public String getBrandName() {
        return brandName;
    }

    public void setStatus(boolean status) {
        this.status = status;
    }

    public boolean isStatus() {
        return status;
    }

    public void setModifiedOn(String modifiedOn) {
        this.modifiedOn = modifiedOn;
    }

    public String getModifiedOn() {
        return modifiedOn;
    }

    public void setRecordCreationDate(String recordCreationDate) {
        this.recordCreationDate = recordCreationDate;
    }

    public String getRecordCreationDate() {
        return recordCreationDate;
    }

    public void setBrandCode(String brandCode) {
        this.brandCode = brandCode;
    }

    public String getBrandCode() {
        return brandCode;
    }

    public void setBrandId(int brandId) {
        this.brandId = brandId;
    }

    public int getBrandId() {
        return brandId;
    }

    @Override
    public String toString() {
        return
                "BrandListsItem{" +
                        "brandName = '" + brandName + '\'' +
                        ",status = '" + status + '\'' +
                        ",modifiedOn = '" + modifiedOn + '\'' +
                        ",recordCreationDate = '" + recordCreationDate + '\'' +
                        ",brandCode = '" + brandCode + '\'' +
                        ",brandId = '" + brandId + '\'' +
                        "}";
    }
}