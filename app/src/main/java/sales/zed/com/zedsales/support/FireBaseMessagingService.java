package sales.zed.com.zedsales.support;

import android.app.NotificationManager;
import android.app.PendingIntent;
import android.content.Intent;
import android.media.RingtoneManager;
import android.net.Uri;
import android.support.v4.app.NotificationCompat;
import com.google.firebase.messaging.FirebaseMessagingService;
import com.google.firebase.messaging.RemoteMessage;
import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;

public class FireBaseMessagingService extends FirebaseMessagingService {

    private static final String TAG = "MyFirebaseMsgService";
    public static int count = 0;

    @Override
    public void onMessageReceived(RemoteMessage remoteMessage) {

        //Calling method to generate notification

        try {
            //remoteMessage.getNotification().setClick
            sendNotification("" + remoteMessage.getNotification().getTitle(),
                    "" + remoteMessage.getNotification().getBody(), remoteMessage.getData().get("menu"));
            //Toast.makeText(this, remoteMessage.getNotification().getClickAction(), Toast.LENGTH_SHORT).show();
        } catch (Exception e) {
        }
    }

    private void sendNotification(String messageTitle, String messageBody, String action) {
        try {
            Intent notifyIntent = new Intent(this, MainActivity.class);
            // Set the Activity to start in a new, empty task
            notifyIntent.setFlags(Intent.FLAG_ACTIVITY_NEW_TASK
                    | Intent.FLAG_ACTIVITY_CLEAR_TASK);
            notifyIntent.putExtra("menu", action);
            // Create the PendingIntent
            PendingIntent contentIntent = PendingIntent.getActivity(
                    this, count, notifyIntent, PendingIntent.FLAG_UPDATE_CURRENT
            );
            NotificationManager nMgr = (NotificationManager) getSystemService(NOTIFICATION_SERVICE);
            Uri defaultSoundUri = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
            NotificationCompat.Builder nBuilder =
                    new NotificationCompat.Builder(this)
                            .setSmallIcon(R.mipmap.ic_launcher)
                            .setContentTitle("" + messageTitle)
                            .setContentText("" + messageBody)
                            .setSound(defaultSoundUri)
                            .setAutoCancel(true)
                            .setContentIntent(contentIntent);
            nMgr.notify(count, nBuilder.build());
            count++;
        } catch (Exception e) {
        }
    }
/*
    public class NotificationReceiver extends WakefulBroadcastReceive {
        @Override
        public void onReceive(Context context, Intent intent) {
            playNotificationSound(context);
        }

        public void playNotificationSound(Context context) {
            try {
                Uri notification = RingtoneManager.getDefaultUri(RingtoneManager.TYPE_NOTIFICATION);
                Ringtone r = RingtoneManager.getRingtone(context, notification);
                r.play();
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    }*/
}