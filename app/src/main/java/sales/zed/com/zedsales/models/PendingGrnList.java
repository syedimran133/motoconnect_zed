package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 1/23/18.
 */

public class PendingGrnList {

    @SerializedName("PendingGrnList")
    @Expose
    private List<PendingGRN> pendingGrnList;

    @SerializedName("PageCount")
    @Expose
    private String PageCount;

    public List<PendingGRN> getPendingGrnList() {
        return pendingGrnList;
    }

    public void setPendingGrnList(List<PendingGRN> pendingGrnList) {
        this.pendingGrnList = pendingGrnList;
    }

    @Override
    public String toString() {
        return
                "PendingGrnList{" +
                        "pendingGrnList = '" + pendingGrnList + '\'' +
                        "PageCount = '" + PageCount + '\'' +
                        "}";
    }

    public String getPageCount() {
        return PageCount;
    }

    public void setPageCount(String pageCount) {
        PageCount = pageCount;
    }
}
