package sales.zed.com.zedsales.fragment;

import android.Manifest;
import android.app.Activity;
import android.app.DatePickerDialog;
import android.app.Dialog;
import android.content.Intent;
import android.content.pm.PackageManager;
import android.databinding.DataBindingUtil;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v4.content.ContextCompat;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.text.InputType;
import android.text.method.DigitsKeyListener;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.view.Window;
import android.view.WindowManager;
import android.widget.DatePicker;
import android.widget.EditText;
import android.widget.ImageView;
import android.widget.LinearLayout;
import android.widget.Toast;

import com.google.zxing.integration.android.IntentIntegrator;
import com.google.zxing.integration.android.IntentResult;

import java.text.SimpleDateFormat;
import java.util.ArrayList;
import java.util.Calendar;
import java.util.Date;
import java.util.HashSet;
import java.util.List;
import java.util.Set;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.adapter.AdapterDialogListItem;
import sales.zed.com.zedsales.adapter.RVAdapterOSDetails;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.apiz.RequestFormater;
import sales.zed.com.zedsales.constants.AppConstants;
import sales.zed.com.zedsales.custom.CustomTextView;
import sales.zed.com.zedsales.custom.CustomTextViewBold;
import sales.zed.com.zedsales.databinding.DialogListBinding;
import sales.zed.com.zedsales.databinding.FragmentOpeningstockBinding;
import sales.zed.com.zedsales.interfaces.AsyncResponse;
import sales.zed.com.zedsales.interfaces.IDialogListClickListener;
import sales.zed.com.zedsales.models.BrandListModel;
import sales.zed.com.zedsales.models.BrandListsItem;
import sales.zed.com.zedsales.models.ForgetModel;
import sales.zed.com.zedsales.models.MasterModel;
import sales.zed.com.zedsales.models.ModelListsItem;
import sales.zed.com.zedsales.models.ProductCategoryListsItem;
import sales.zed.com.zedsales.models.ProductCategoryModel;
import sales.zed.com.zedsales.models.ProductListsItem;
import sales.zed.com.zedsales.models.ProductModel;
import sales.zed.com.zedsales.models.SaleSerialListItems;
import sales.zed.com.zedsales.models.SerialMaster;
import sales.zed.com.zedsales.models.SerialMasterList;
import sales.zed.com.zedsales.models.SkuListsItem;
import sales.zed.com.zedsales.models.SkuMasterModel;
import sales.zed.com.zedsales.models.SkuOpeningStockList;
import sales.zed.com.zedsales.models.StockItems;
import sales.zed.com.zedsales.models.StockItemsList;
import sales.zed.com.zedsales.models.StockTypeItem;
import sales.zed.com.zedsales.models.StockTypeMaster;
import sales.zed.com.zedsales.room.MyDatabase;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FlowOrganizer;
import sales.zed.com.zedsales.support.Log;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by root on 09/12/17.
 */

public class FragmentOpeningStock extends Fragment {

    private RVAdapterOSDetails mAdapter;
    private FragmentOpeningstockBinding binding;
    private List<SaleSerialListItems> serialList;
    private String currentDate = "";
    private SkuOpeningStockList skuOpeningStockList = null;
    private List<SkuOpeningStockList> lists = new ArrayList<>();
    final ArrayList<String> listproductcatid = new ArrayList<>();
    final ArrayList<String> listbrandid = new ArrayList<>();
    final ArrayList<String> listskuid = new ArrayList<>();
    final ArrayList<String> listmodelid = new ArrayList<>();
    private String brandid = "", productcatid = "", modelid = "", skuid = "";
    private int selectedStockTypePos = -1, selectedBrandPos = -1, selectedProductCategoryPos = -1, selectedModelPos = -1, selectedSkuPos = -1;
    private List<String> listskytype, listBrand, listPorductCategory, listModel, listSku;
    private List<BrandListsItem> fetchBrandList;
    private List<ProductCategoryListsItem> fetchProductCategoryList;
    private List<ModelListsItem> fetchMasterList;
    private List<SkuListsItem> fetchSkuMasterList;
    private List<StockTypeItem> fetchStockType;
    private List<SerialMaster> fetchSerialMasterList;
    private int Date_day, Date_month, Date_year, selectedDateOption;
    private Fragment fragment;
    private BaseFragment mBaseFragment;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fragment_openingstock, container, false);
        mBaseFragment = new BaseFragment();
        fragment = this;
        AppSingle.getInstance().getActivity().setTitle("Stock", false);
        currentDate = "01/01/2000 09:44:00";
        callAsyncSkockType();

        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        binding.btnOsSubmit.setOnClickListener(listener);
        binding.relativeOpeningStocktype.setOnClickListener(listener);
        binding.relativeOpeningBrand.setOnClickListener(listener);
        binding.relativeOpeningProductCat.setOnClickListener(listener);
        binding.relativeOpeningModel.setOnClickListener(listener);
        binding.relativeOpeningSku.setOnClickListener(listener);
        binding.btnAddserial.setOnClickListener(listener);
        binding.btnAddtolist.setOnClickListener(listener);
        binding.ivClaOpening.setOnClickListener(listener);
        binding.ivOpenbarcode.setOnClickListener(listener);
        binding.btnBarcodeAdd.setOnClickListener(listener);
        binding.btnOsZeroQty.setOnClickListener(listener);
    }

    private boolean isSerial(String m_name) {
        boolean result = false;
        for (int i = 0; i < fetchMasterList.size(); i++) {
            if (fetchMasterList.get(i).getModelName().equalsIgnoreCase(m_name)) {
                if (("" + fetchMasterList.get(i).getSerialisedMode()).equalsIgnoreCase("3")) {
                    binding.btnAddserial.setVisibility(View.VISIBLE);
                    result = true;
                } else {
                    binding.btnAddserial.setVisibility(View.GONE);
                    result = false;
                }
            }
        }
        return result;
    }

    private boolean isSerialIMEI(String m_name) {
        boolean result = false;
        try {
            for (int i = 0; i < fetchMasterList.size(); i++) {
                if (fetchMasterList.get(i).getModelName().equalsIgnoreCase(m_name)) {
                    if (("" + fetchMasterList.get(i).getSerialisedMode()).equalsIgnoreCase("3")) {
                        result = true;
                        break;
                    } else {
                        result = false;
                        break;
                    }
                }
            }
        } catch (Exception e) {
        }
        return result;
    }

    @Override
    public void onActivityResult(int requestCode, int resultCode, Intent data) {
        super.onActivityResult(requestCode, resultCode, data);
        switch (requestCode) {
            case IntentIntegrator.REQUEST_CODE:
                if (resultCode == Activity.RESULT_OK) {
                    // Parsing bar code reader result
                    IntentResult result = IntentIntegrator.parseActivityResult(requestCode, resultCode, data);
                    binding.txtOpenBarcode.setText(result.getContents());
                    //Toast.makeText(AppSingle.getInstance().getActivity(), result.getContents(), Toast.LENGTH_LONG).show();
                }
                break;
        }
    }

    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {

            switch (view.getId()) {
                case R.id.btn_os_submit:
                    if (mAdapter != null) {
                        if (mAdapter.getListData().size() != 0) {
                            if (!binding.txtOpeningDate.getText().toString().equalsIgnoreCase("")) {
                                openConfirmDialog();
                            } else {
                                mBaseFragment.showToast("Please select date before submit");
                            }
                        }
                    } else {
                        mBaseFragment.showToast("Please fill details");
                    }
                    break;
                case R.id.relative_opening_stocktype:
                    if (fetchStockType != null) {
                        if (fetchStockType.size() != 0)
                            setStockTypeList();
                        else
                            callAsyncSkockType();
                    } else {
                        callAsyncSkockType();
                    }
                    break;
                case R.id.relative_opening_brand:
                    if (fetchBrandList != null) {
                        if (fetchBrandList.size() != 0)
                            setBrandList();
                        else
                            callAsyncBrand();
                    } else {
                        callAsyncBrand();
                    }
                    break;
                case R.id.relative_opening_product_cat:
                    if (fetchProductCategoryList != null) {
                        if (fetchProductCategoryList.size() != 0)
                            setProductList();
                        else
                            callAsyncProduct();
                    } else {
                        callAsyncProduct();
                    }
                    break;
                case R.id.relative_opening_model:
                    if (!binding.txtOpeningBrand.getText().toString().equalsIgnoreCase("") && !binding.txtOpeningProcat.getText().toString().equalsIgnoreCase("")) {
                        if (fetchMasterList != null) {
                            if (fetchMasterList.size() != 0)
                                setModelList();
                            else
                                callAsyncMaster();
                        } else {
                            callAsyncMaster();
                        }
                    } else {
                        mBaseFragment.showToast("Please select Brand and Product frist.");
                    }
                    break;
                case R.id.relative_opening_sku:
                    if (!binding.txtOpeningModel.getText().toString().equalsIgnoreCase("")) {
                        if (fetchSkuMasterList != null) {
                            if (fetchSkuMasterList.size() != 0)
                                setSkuList();
                            else
                                callAsyncSku();
                        } else {
                            callAsyncSku();
                        }
                    } else {
                        mBaseFragment.showToast("Please select model frist.");
                    }
                    break;
                case R.id.btn_addserial:
                    try {
                        int qty = Integer.parseInt(binding.txtOpeningQty.getText().toString());
                        if (qty > 0)
                            openSerialDialog(qty);
                        else
                            mBaseFragment.showToast("Please enter valid serial no first");
                    } catch (Exception e) {
                        mBaseFragment.showToast("Please enter serial no first");
                    }
                    break;
                case R.id.btn_addtolist:
                    SkuOpeningStockList stockList = getopeningstock();
                    if (binding.btnAddserial.isShown() && serialList != null) {
                        if (serialList.size() != 0) {
                            if (stockList != null) {
                                if (!isDublicateMultipul(stockList.getSerialOpeningStockList())) {
                                    if (checksku(stockList.getSkuName(), Integer.parseInt(stockList.getSkuQuantity()), stockList.getSerialOpeningStockList()))
                                        lists.add(stockList);
                                    RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
                                    binding.rvListopeningstock.setLayoutManager(mLayoutManager);
                                    mAdapter = new RVAdapterOSDetails(getActivity(), lists);
                                    binding.rvListopeningstock.setAdapter(mAdapter);
                                    setempty();
                                } else {
                                    Toast.makeText(AppSingle.getInstance().getActivity(), "Dublicate element found in the list", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(AppSingle.getInstance().getActivity(), "Please fill detail", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(AppSingle.getInstance().getActivity(), "Please fill detail", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        if (stockList != null && !stockList.isSerail()) {
                            if (Integer.parseInt(stockList.getSkuQuantity()) > 0) {
                                if (checksku(stockList.getSkuName(), Integer.parseInt(stockList.getSkuQuantity()), stockList.getSerialOpeningStockList()))
                                    lists.add(stockList);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
                                binding.rvListopeningstock.setLayoutManager(mLayoutManager);
                                mAdapter = new RVAdapterOSDetails(getActivity(), lists);
                                binding.rvListopeningstock.setAdapter(mAdapter);
                                setempty();
                            } else {
                                Toast.makeText(AppSingle.getInstance().getActivity(), "Zero qty not accepted", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(AppSingle.getInstance().getActivity(), "Please fill serial no", Toast.LENGTH_LONG).show();
                        }
                    }

                    break;
                case R.id.iv_cla_opening:
                    showDatePicker();
                    break;
                case R.id.btn_os_zero_qty:
                    if (!binding.txtOpeningDate.getText().toString().equalsIgnoreCase("")) {
                        callAsynczero();
                    } else {
                        mBaseFragment.showToast("Please select date before submit");
                    }
                    break;
                case R.id.iv_openbarcode:
                    try {
                        if (ContextCompat.checkSelfPermission(AppSingle.getInstance().getActivity(), Manifest.permission.CAMERA) == PackageManager.PERMISSION_GRANTED) {
                            IntentIntegrator.forSupportFragment(fragment).initiateScan();
                        } else {
                            mBaseFragment.showToast("Permission not found.");
                        }
                    } catch (Exception e) {
                        mBaseFragment.showToast(e.toString());
                    }
                    break;
                case R.id.btn_barcode_add:
                    try {
                        scanimei();
                    } catch (Exception e) {
                    }
                    break;
            }
        }
    };

    private SerialMaster getserial(String serial) {
        SerialMaster serialMaster = null;
        for (int i = 0; i < fetchSerialMasterList.size(); i++) {
            if (fetchSerialMasterList.get(i).getSerialNumber1().equalsIgnoreCase(serial)) {
                serialMaster = fetchSerialMasterList.get(i);
                break;
            }
        }
        return serialMaster;
    }

    private SkuListsItem getsku(String skuid) {
        SkuListsItem id = null;
        for (SkuListsItem data : fetchSkuMasterList) {
            if (data.isStatus() && ("" + data.getSkuId()).equalsIgnoreCase(skuid)) {
                id = data;
            }
        }
        return id;
    }

    private void setStockTypeList() {

        if (listskytype == null) {
            listskytype = new ArrayList<>();
            for (StockTypeItem data : fetchStockType) {
                if (data.getActive()) {
                    listskytype.add(data.getStockBinTypeDesc());
                }
            }
        }
        openListDialog("Select Stock Type", selectedStockTypePos, listskytype, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedStockTypePos = selectedPos;
                    binding.txtOpeningStocktype.setText(listskytype.get(selectedPos));
                } catch (Exception e) {
                }
            }
        });

    }

    private void setBrandList() {

        setdata(1);
        openListDialog("Select Brand", selectedBrandPos, listBrand, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedBrandPos = selectedPos;
                    binding.txtOpeningBrand.setText(listBrand.get(selectedPos));
                    brandid = listbrandid.get(selectedPos);
                } catch (Exception e) {
                }
            }
        });

    }

    private void setProductList() {
        setdata(2);
        openListDialog("Select Product Category", selectedProductCategoryPos, listPorductCategory, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedProductCategoryPos = selectedPos;
                    binding.txtOpeningProcat.setText(listPorductCategory.get(selectedPos));
                    productcatid = listproductcatid.get(selectedPos);

                } catch (Exception e) {
                }
            }
        });

    }

    private void setModelList() {
        setdata(3);
        openListDialog("Select Model", selectedModelPos, listModel, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedModelPos = selectedPos;
                    binding.txtOpeningModel.setText(listModel.get(selectedPos));
                    isSerial(listModel.get(selectedPos));
                    modelid = listmodelid.get(selectedPos);

                } catch (Exception e) {
                }
            }
        });

    }

    private void setSkuList() {

        setdata(4);
        openListDialog("Select Product Category", selectedSkuPos, listSku, new IDialogListClickListener() {
            @Override
            public void onClick(String data, int selectedPos) {
                try {
                    selectedSkuPos = selectedPos;
                    binding.txtOpeningSku.setText(listSku.get(selectedPos));
                    skuid = listskuid.get(selectedPos);

                } catch (Exception e) {
                }
            }
        });

    }

    protected void openListDialog(String title, int selectedPos, final List<String> listData, final IDialogListClickListener listener) {
        DialogListBinding binding = DataBindingUtil.inflate(LayoutInflater.from(AppSingle.getInstance().getActivity().getApplicationContext()), R.layout.dialog_list, null, false);
        final Dialog dialog = new Dialog(AppSingle.getInstance().getActivity());
        dialog.requestWindowFeature(Window.FEATURE_NO_TITLE);
        dialog.setContentView(binding.getRoot());
        WindowManager.LayoutParams lp = new WindowManager.LayoutParams();
        Window window = dialog.getWindow();
        lp.copyFrom(window.getAttributes());
        lp.width = WindowManager.LayoutParams.MATCH_PARENT;
        lp.height = WindowManager.LayoutParams.WRAP_CONTENT;
        window.setAttributes(lp);
        binding.txtViewTitle.setText(title);
        dialog.setCancelable(true);
        dialog.setCanceledOnTouchOutside(false);
        LinearLayoutManager lLayout = new LinearLayoutManager(AppSingle.getInstance().getActivity());
        RecyclerView rView = binding.rvList;
        rView.setHasFixedSize(true);
        rView.setLayoutManager(lLayout);
        AdapterDialogListItem adapter = new AdapterDialogListItem(listData, selectedPos);
        rView.setAdapter(adapter);
        adapter.registerOnItemClickListener(new AdapterDialogListItem.IonItemSelect() {
            @Override
            public void onItemSelect(int position) {
                if (listener != null)
                    listener.onClick(listData.get(position), position);
                dialog.dismiss();
            }
        });
        dialog.show();
    }

    protected void openSerialDialog(final int no) {
        final Dialog dialog = new Dialog(AppSingle.getInstance().getActivity());
        dialog.setCancelable(false);
        final ArrayList<EditText> myEditTextList = new ArrayList<EditText>();
        EditText et;
        View view = getLayoutInflater()
                .inflate(R.layout.dilog_serial_popup, null);
        LinearLayout lv = (LinearLayout) view.findViewById(R.id.liner_serials);
        ImageView btnclose = (ImageView) view.findViewById(R.id.btn_serial_close);
        CustomTextView addtolist = (CustomTextView) view.findViewById(R.id.serial_add);
        for (int i = 0; i < no; i++) {
            et = new EditText(AppSingle.getInstance().getActivity());
            AppSingle.EdFilterNumber(et, 15);
            et.setKeyListener(DigitsKeyListener.getInstance("0123456789"));
            LinearLayout.LayoutParams p = new LinearLayout.LayoutParams(ViewGroup.LayoutParams.MATCH_PARENT, ViewGroup.LayoutParams.WRAP_CONTENT);
            p.setMargins(8, 1, 8, 1);
            et.setLayoutParams(p);
            et.setInputType(InputType.TYPE_CLASS_NUMBER);
            et.setHint("Serail No." + (i + 1));
            et.setId(i);
            lv.addView(et);
            myEditTextList.add(et);
        }
        btnclose.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        addtolist.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    List<SaleSerialListItems> serial = new ArrayList<>();
                    boolean empty = false;
                    SaleSerialListItems result = null;
                    for (int j = 0; j < myEditTextList.size(); j++) {
                        if (!myEditTextList.get(j).getText().toString().equalsIgnoreCase("")) {
                            result = new SaleSerialListItems();
                            result.setSerial1(myEditTextList.get(j).getText().toString());
                            serial.add(result);
                        } else {
                            empty = true;
                            break;
                        }
                    }

                    if (!empty) {
                        if (!findDuplicates(serial)) {
                            serialList = serial;
                            dialog.dismiss();
                        } else {
                            mBaseFragment.showToast("List are the duplicate");
                        }
                    } else {
                        mBaseFragment.showToast("Value should not be empty");
                    }
                } catch (Exception e) {
                }
            }
        });
        dialog.setContentView(view);
        dialog.show();
    }

    public boolean serach(List<SaleSerialListItems> listContainingDuplicates, SaleSerialListItems value) {
        boolean result = false;
        try {
            int count = 0;
            for (int i = 0; i < listContainingDuplicates.size(); i++) {
                if (listContainingDuplicates.get(i).getSerial1().equalsIgnoreCase(value.getSerial1())) {
                    count++;
                }
            }
            if (count > 1) {
                result = true;
            }
        } catch (Exception e) {
        }
        return result;
    }

    public boolean findDuplicates(List<SaleSerialListItems> listContainingDuplicates) {

        boolean result = false;
        try {
            for (int o = 0; o < listContainingDuplicates.size(); o++) {
                if (serach(listContainingDuplicates, listContainingDuplicates.get(o))) {
                    result = true;
                }
            }
        } catch (Exception e) {
            Toast.makeText(AppSingle.getInstance().getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
        }
        return result;
    }

    protected void openConfirmDialog() {
        final Dialog dialog = new Dialog(AppSingle.getInstance().getActivity());
        dialog.setCancelable(false);
        View view = getLayoutInflater()
                .inflate(R.layout.opening_stock_popup, null);
        CustomTextView btn_cancel = (CustomTextView) view.findViewById(R.id.osdilog_btncancel);
        CustomTextView btnOk = (CustomTextView) view.findViewById(R.id.osdilog_btnproceed);

        btn_cancel.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {

                dialog.dismiss();
            }
        });
        btnOk.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                callAsync();
                dialog.dismiss();
            }
        });
        dialog.setContentView(view);
        dialog.show();
    }

    protected void openConfirmDialog2() {
        try {
            final Dialog dialog = new Dialog(AppSingle.getInstance().getActivity());
            dialog.setCancelable(false);
            View view = getLayoutInflater()
                    .inflate(R.layout.opening_stock_popup, null);
            CustomTextView btn_cancel = (CustomTextView) view.findViewById(R.id.osdilog_btncancel);
            CustomTextView btnOk = (CustomTextView) view.findViewById(R.id.osdilog_btnproceed);
            CustomTextViewBold displayMsg = (CustomTextViewBold) view.findViewById(R.id.txtmsgdilog);
            displayMsg.setText("We are not able to fetch IMEI information from server please process with manual input the data");
            btn_cancel.setVisibility(View.GONE);
            btnOk.setOnClickListener(new View.OnClickListener() {
                @Override
                public void onClick(View v) {
                    binding.btnBarcodeAdd.setText("+ Add Manual");
                    binding.llAddToList.setVisibility(View.GONE);
                    dialog.dismiss();
                }
            });
            dialog.setContentView(view);
            dialog.show();
        } catch (Exception e) {
        }
    }

    public void featchDB() {
        fetchStockType = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchStockTypeList();
        fetchBrandList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchBrandList();
        fetchProductCategoryList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchProductCategoryList();
        fetchMasterList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchMasterList();
        fetchSkuMasterList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchSkuMasterList();
        fetchSerialMasterList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchSerialMaster(AppSingle.getInstance().getLoginUser().getEntityId());
    }

    public void setdata(int code) {
        switch (code) {
            case 1:
                if (listBrand == null && fetchBrandList != null) {
                    listBrand = new ArrayList<>();
                    for (BrandListsItem data : fetchBrandList) {
                        if (data.isStatus()) {
                            listBrand.add(data.getBrandName());
                            listbrandid.add("" + data.getBrandId());
                        }

                    }
                }
                try {
                    listPorductCategory = null;
                    listModel = null;
                    listSku = null;
                    productcatid = "";
                    modelid = "";
                    skuid = "";
                    listproductcatid.clear();
                    listmodelid.clear();
                    listSku.clear();
                } catch (Exception e) {
                }
                binding.txtOpeningProcat.setText("");
                binding.txtOpeningModel.setText("");
                binding.txtOpeningSku.setText("");
                break;
            case 2:
                if (listPorductCategory == null && fetchProductCategoryList != null) {
                    listPorductCategory = new ArrayList<>();
                    for (ProductCategoryListsItem data : fetchProductCategoryList) {
                        if (data.isStatus()) {
                            listPorductCategory.add(data.getProductCategoryName());
                            listproductcatid.add("" + data.getProductCategoryId());
                        }
                    }
                }
                try {
                    listModel = null;
                    listSku = null;
                    modelid = "";
                    skuid = "";
                    listmodelid.clear();
                    listSku.clear();
                } catch (Exception e) {
                }
                binding.txtOpeningModel.setText("");
                binding.txtOpeningSku.setText("");
                break;
            case 3:
                if (listModel == null && fetchMasterList != null) {
                    listModel = new ArrayList<>();
                    for (ModelListsItem data : fetchMasterList) {
                        if (data.isStatus()) {
                            if ((data.getBrandId() + "").equalsIgnoreCase(brandid) && ("" + data.getProductCategoryId()).equalsIgnoreCase(productcatid)) {
                                listModel.add(data.getModelName());
                                listmodelid.add("" + data.getModelId());
                            }
                        }
                    }
                }
                try {
                    listSku = null;
                    //listSerial = null;
                    skuid = "";
                    listSku.clear();
                } catch (Exception e) {
                }
                binding.txtOpeningSku.setText("");
                // binding.txtViewSerialNo.setText("");
                break;
            case 4:
                if (listSku == null && fetchSkuMasterList != null) {
                    listSku = new ArrayList<>();
                    for (SkuListsItem data : fetchSkuMasterList) {
                        if (data.isStatus() && modelid.equalsIgnoreCase("" + data.getModelId())) {
                            listSku.add(data.getSkuName());
                            listskuid.add("" + data.getSkuId());
                        }
                    }
                }
                break;
        }
    }

    public SkuOpeningStockList getopeningstock() {
        SkuOpeningStockList openingStockItem = null;
        try {

            String SkuCode = getSkuCode();
            String SkuName = binding.txtOpeningSku.getText().toString();
            String SkuQuantity = "1";
            if (!binding.btnBarcodeAdd.getText().toString().equalsIgnoreCase("+ Add Manual")) {
                SkuQuantity = binding.txtOpeningQty.getText().toString();
            }
            String StockBinType = getStockBinType();
            if (!SkuCode.equalsIgnoreCase("") && !SkuName.equalsIgnoreCase("") && !SkuQuantity.equalsIgnoreCase("") && !StockBinType.equalsIgnoreCase("")) {
                openingStockItem = new SkuOpeningStockList();
                openingStockItem.setSkuCode(SkuCode);
                openingStockItem.setSkuName(SkuName);
                openingStockItem.setSkuQuantity(SkuQuantity);
                openingStockItem.setStockBinType(StockBinType);
                boolean isSerial = isSerial(binding.txtOpeningModel.getText().toString());
                openingStockItem.setSerail(isSerial);
                if (isSerial) {
                    openingStockItem.setSerialOpeningStockList(serialList);
                }
            } else {
                Toast.makeText(AppSingle.getInstance().getActivity(), "Please fill all details.", Toast.LENGTH_LONG).show();
            }
        } catch (Exception e) {
            Toast.makeText(AppSingle.getInstance().getActivity(), "Please fill all details.", Toast.LENGTH_LONG).show();
        }
        return openingStockItem;
    }

    public String getStockBinType() {
        String openingStockItem = "";
        try {
            for (int i = 0; i < fetchStockType.size(); i++) {
                if (binding.txtOpeningStocktype.getText().toString().equalsIgnoreCase(fetchStockType.get(i).getStockBinTypeDesc())) {
                    openingStockItem = fetchStockType.get(i).getStockBinTypeCode();
                }
            }
        } catch (Exception e) {
        }
        return openingStockItem;
    }

    public String getSkuCode() {
        String openingSkuCode = "";
        try {
            for (int i = 0; i < fetchSkuMasterList.size(); i++) {
                if (binding.txtOpeningSku.getText().toString().equalsIgnoreCase(fetchSkuMasterList.get(i).getSkuName())) {
                    openingSkuCode = fetchSkuMasterList.get(i).getSkuCode();
                }
            }
        } catch (Exception e) {
        }
        return openingSkuCode;
    }

    private void showDatePicker() {
        try {
            resetDateToToday();
            getDateDialog().show();
        } catch (Exception e) {
            e.printStackTrace();
        }
    }

    private void resetDateToToday() {
        final Calendar c1 = Calendar.getInstance();
        Date_year = c1.get(Calendar.YEAR);
        Date_month = c1.get(Calendar.MONTH);
        Date_day = c1.get(Calendar.DAY_OF_MONTH);
    }

    private DatePickerDialog getDateDialog() {

        DatePickerDialog dialoga = new DatePickerDialog(AppSingle.getInstance().getActivity(),
                pickerListener, Date_year, Date_month, Date_day);
        try {
            dialoga.getDatePicker().setMaxDate(System.currentTimeMillis());
            Calendar cal = Calendar.getInstance();
            cal.add(Calendar.DAY_OF_MONTH, -5);
            Date result = cal.getTime();
            dialoga.getDatePicker().setMinDate(result.getTime());
        } catch (Exception e) {
            e.printStackTrace();
        }
        return dialoga;
    }


    private DatePickerDialog.OnDateSetListener pickerListener = new DatePickerDialog.OnDateSetListener() {

        @Override
        public void onDateSet(DatePicker view, int selectedYear, int selectedMonth, int selectedDay) {
            try {
                Date_year = selectedYear;
                Date_month = selectedMonth;
                Date_day = selectedDay;
                Calendar calendar = Calendar.getInstance();
                calendar.set(selectedYear, selectedMonth, selectedDay);
                SimpleDateFormat format = new SimpleDateFormat("dd MMM yyyy");
                String strDate = format.format(calendar.getTime());

                binding.txtOpeningDate.setText(strDate.trim());
            } catch (Exception e) {
                e.printStackTrace();
            }
        }
    };

    private void callAsync() {

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            mBaseFragment.showToast("NO_INTERNET");
            return;
        }
        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<ForgetModel> call = apiService.addOpeningStock(AppSingle.getInstance().getLoginUser().getUserId(), RequestFormater.openingstock(binding.txtOpeningDate.getText().toString(), mAdapter.getListData()));
        mBaseFragment.showProgessBar();
        call.enqueue(new Callback<ForgetModel>() {
            @Override
            public void onResponse(Call<ForgetModel> call, Response<ForgetModel> response) {
                mBaseFragment.hideProgessBar();
                try {
                    if (response.code() == 200) {
                        if (response.body().getStatusCode() == 0) {
                            Log.e("Response", response.body().toString());
                            setemptyall();
                            mBaseFragment.showToast(response.body().getMessage());
                            mAdapter.updateData(lists);
                            FlowOrganizer.getInstance().add(new FragmentMenu());
                        } else {
                            mBaseFragment.showToast(response.body().getMessage());
                        }
                    } else {
                        mBaseFragment.showResponseMessage(response.code());
                        if (response.code() == 401)
                            callAsync();
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<ForgetModel> call, Throwable t) {
                mBaseFragment.hideProgessBar();
                mBaseFragment.showToast("Fail");
            }
        });
    }

    private void callAsynczero() {

        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            mBaseFragment.showToast("NO_INTERNET");
            return;
        }

        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<ForgetModel> call = apiService.OpeningStockWithZeroQuantity(AppSingle.getInstance().getLoginUser().getUserId(), RequestFormater.ProceedWithZeroStock(binding.txtOpeningDate.getText().toString()));
        mBaseFragment.showProgessBar();
        call.enqueue(new Callback<ForgetModel>() {
            @Override
            public void onResponse(Call<ForgetModel> call, Response<ForgetModel> response) {
                mBaseFragment.hideProgessBar();
                try {
                    if (response.code() == 200) {
                        if (response.body().getStatusCode() == 0) {
                            mBaseFragment.showToast(response.body().getMessage());
                            FlowOrganizer.getInstance().add(new FragmentMenu());
                        } else {
                            mBaseFragment.showToast(response.body().getMessage());
                        }
                    } else {
                        mBaseFragment.showResponseMessage(response.code());
                        if (response.code() == 401)
                            callAsynczero();
                    }
                } catch (Exception e) {
                }
            }

            @Override
            public void onFailure(Call<ForgetModel> call, Throwable t) {
                mBaseFragment.hideProgessBar();
                mBaseFragment.showToast("Fail");
            }
        });
    }

    public void setemptyall() {
        binding.txtOpeningBrand.setText("");
        binding.txtOpeningProcat.setText("");
        binding.txtOpeningModel.setText("");
        binding.txtOpeningSku.setText("");
        binding.txtOpeningStocktype.setText("");
        binding.txtOpeningQty.setText("");
        binding.txtOpeningDate.setText("");
        lists.clear();
        listPorductCategory = null;
        listModel = null;
        listSku = null;
        productcatid = "";
        modelid = "";
        skuid = "";
        try {
            listproductcatid.clear();
            listmodelid.clear();
            listSku.clear();
        } catch (Exception e) {
        }
    }

    public boolean checksku(String sku, int qty, List<SaleSerialListItems> SerialOpeningStockList) {

        boolean result = true;
        try {
            if (lists != null) {
                if (lists.size() != 0) {
                    for (int i = 0; i < lists.size(); i++) {
                        if (lists.get(i).getSkuName().equalsIgnoreCase(sku)) {

                            boolean wecando = false;
                            try {
                                for (int j = 0; j < SerialOpeningStockList.size(); j++) {
                                    if (isDublicate(SerialOpeningStockList.get(j).getSerial1())) {
                                        wecando = true;
                                    }
                                }
                                if (!wecando) {
                                    lists.get(i).getSerialOpeningStockList().addAll(SerialOpeningStockList);
                                    lists.get(i).setSerialOpeningStockList(lists.get(i).getSerialOpeningStockList());
                                }
                            } catch (Exception e) {
                                Log.d("TRY", e.toString());
                            }
                            if (!wecando) {
                                int r = Integer.parseInt(lists.get(i).getSkuQuantity()) + qty;
                                lists.get(i).setSkuQuantity("" + r);
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
                                binding.rvListopeningstock.setLayoutManager(mLayoutManager);
                                mAdapter = new RVAdapterOSDetails(getActivity(), lists);
                                binding.rvListopeningstock.setAdapter(mAdapter);
                            } else {
                                Toast.makeText(AppSingle.getInstance().getActivity(), "Dublicate element found in the list.", Toast.LENGTH_SHORT).show();
                            }
                            result = false;
                            break;
                        }
                    }
                }
            }
        } catch (Exception e) {
        }
        return result;
    }

    public void setempty() {
        binding.txtOpeningBrand.setText("");
        binding.txtOpeningProcat.setText("");
        binding.txtOpeningModel.setText("");
        binding.txtOpeningSku.setText("");
        binding.txtOpeningStocktype.setText("");
        binding.txtOpeningQty.setText("");
        binding.txtOpenBarcode.setText("");
        binding.btnAddserial.setVisibility(View.VISIBLE);
        if (!binding.btnBarcodeAdd.getText().toString().equalsIgnoreCase("+ Add Manual"))
            serialList = null;
    }

    private void callAsyncSkockType() {
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            mBaseFragment.showToast("NO_INTERNET");
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<StockTypeMaster> call = apiService.getStockType(currentDate);
        mBaseFragment.showProgessBar();
        call.enqueue(new Callback<StockTypeMaster>() {
            @Override
            public void onResponse(Call<StockTypeMaster> call, Response<StockTypeMaster> response) {
                mBaseFragment.hideProgessBar();
                //showResponseMessage(response.code());
                if (response.code() == 200) {
                    if (response.body() != null)
                        if (response.body().getStockTypeList() != null && response.body().getStockTypeList().size() > 0)
                            for (StockTypeItem data : response.body().getStockTypeList()) {
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertStockType(data);
                            }
                    callAsyncSkockMaster();
                } else {
                    mBaseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<StockTypeMaster> call, Throwable t) {
                mBaseFragment.hideProgessBar();
                mBaseFragment.showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    private void callAsyncSkockMaster() {
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            mBaseFragment.showToast("NO_INTERNET");
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<StockItemsList> call = apiService.getStockList(AppSingle.getInstance().getLoginUser().getUserId(), currentDate);
        mBaseFragment.showProgessBar();
        call.enqueue(new Callback<StockItemsList>() {
            @Override
            public void onResponse(Call<StockItemsList> call, Response<StockItemsList> response) {
                mBaseFragment.hideProgessBar();
                //showResponseMessage(response.code());
                if (response.code() == 200) {
                    if (response.body() != null)
                        if (response.body().getRetailerStockList() != null && response.body().getRetailerStockList().size() > 0)
                            for (StockItems data : response.body().getRetailerStockList()) {
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertStock(data);
                            }
                    callAsyncSerialMaster();
                } else {
                    mBaseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<StockItemsList> call, Throwable t) {
                mBaseFragment.hideProgessBar();
                mBaseFragment.showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    private void callAsyncSerialMaster() {
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            mBaseFragment.showToast("NO_INTERNET");
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<SerialMasterList> call = apiService.getSerialMaster(AppSingle.getInstance().getLoginUser().getUserId(), currentDate);
        mBaseFragment.showProgessBar();
        call.enqueue(new Callback<SerialMasterList>() {
            @Override
            public void onResponse(Call<SerialMasterList> call, Response<SerialMasterList> response) {
                mBaseFragment.hideProgessBar();
                //showResponseMessage(response.code());
                if (response.code() == 200) {
                    if (response.body() != null)
                        if (response.body().getSerialLists() != null && response.body().getSerialLists().size() > 0)
                            for (SerialMaster data : response.body().getSerialLists()) {
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertSerialMaster(data);
                            }
                    callAsyncBrand();
                } else {
                    mBaseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<SerialMasterList> call, Throwable t) {
                mBaseFragment.hideProgessBar();
                mBaseFragment.showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    private void callAsyncBrand() {
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            mBaseFragment.showToast("NO_INTERNET");
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<BrandListModel> call = apiService.getBrand(currentDate);
        mBaseFragment.showProgessBar();
        call.enqueue(new Callback<BrandListModel>() {
            @Override
            public void onResponse(Call<BrandListModel> call, Response<BrandListModel> response) {
                mBaseFragment.hideProgessBar();
                // showResponseMessage(response.code());
                if (response.code() == 200) {
                    if (response.body() != null)
                        if (response.body().getBrandLists() != null && response.body().getBrandLists().size() > 0)
                            for (BrandListsItem data : response.body().getBrandLists())
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertBrand(data);
                    callAsyncCategory();
                } else {
                    mBaseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<BrandListModel> call, Throwable t) {
                mBaseFragment.hideProgessBar();
                mBaseFragment.showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    private void callAsyncCategory() {
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            mBaseFragment.showToast("NO_INTERNET");
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<ProductCategoryModel> call = apiService.getCategory(currentDate);
        mBaseFragment.showProgessBar();
        call.enqueue(new Callback<ProductCategoryModel>() {
            @Override
            public void onResponse(Call<ProductCategoryModel> call, Response<ProductCategoryModel> response) {
                mBaseFragment.hideProgessBar();
//                showResponseMessage(response.code());
                if (response.code() == 200) {
                    if (response.body() != null)
                        if (response.body().getProductCategoryLists() != null && response.body().getProductCategoryLists().size() > 0)
                            for (ProductCategoryListsItem data : response.body().getProductCategoryLists())
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertProductCategory(data);
                    callAsyncProduct();
                } else {
                    mBaseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<ProductCategoryModel> call, Throwable t) {
                mBaseFragment.hideProgessBar();
                mBaseFragment.showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    private void callAsyncProduct() {
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            mBaseFragment.showToast("NO_INTERNET");
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<ProductModel> call = apiService.getProduct(currentDate);
        mBaseFragment.showProgessBar();
        call.enqueue(new Callback<ProductModel>() {
            @Override
            public void onResponse(Call<ProductModel> call, Response<ProductModel> response) {
                mBaseFragment.hideProgessBar();
//                showResponseMessage(response.code());
                if (response.code() == 200) {
                    if (response.body() != null)
                        if (response.body().getProductLists() != null && response.body().getProductLists().size() > 0)
                            for (ProductListsItem data : response.body().getProductLists())
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertProduct(data);
                    callAsyncMaster();
                } else {
                    mBaseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<ProductModel> call, Throwable t) {
                mBaseFragment.hideProgessBar();
                mBaseFragment.showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    private void callAsyncMaster() {
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            mBaseFragment.showToast("NO_INTERNET");
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<MasterModel> call = apiService.getMaster(currentDate);
        mBaseFragment.showProgessBar();
        call.enqueue(new Callback<MasterModel>() {
            @Override
            public void onResponse(Call<MasterModel> call, Response<MasterModel> response) {
                mBaseFragment.hideProgessBar();
                //showResponseMessage(response.code());
                if (response.code() == 200) {
                    if (response.body() != null)
                        if (response.body().getModelLists() != null && response.body().getModelLists().size() > 0)
                            for (ModelListsItem data : response.body().getModelLists())
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertMaster(data);
                    callAsyncSku();
                } else {
                    mBaseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<MasterModel> call, Throwable t) {
                mBaseFragment.hideProgessBar();
                mBaseFragment.showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    private void callAsyncSku() {
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            mBaseFragment.showToast("NO_INTERNET");
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<SkuMasterModel> call = apiService.getSkuMaster(currentDate);
        mBaseFragment.showProgessBar();
        call.enqueue(new Callback<SkuMasterModel>() {
            @Override
            public void onResponse(Call<SkuMasterModel> call, Response<SkuMasterModel> response) {
                mBaseFragment.hideProgessBar();
//                showResponseMessage(response.code());
                if (response.code() == 200) {
                    if (response.body() != null)
                        if (response.body().getSkuLists() != null && response.body().getSkuLists().size() > 0)
                            for (SkuListsItem data : response.body().getSkuLists())
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertSkuMaster(data);
                    featchDB();
                } else {
                    mBaseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<SkuMasterModel> call, Throwable t) {
                mBaseFragment.hideProgessBar();
                mBaseFragment.showToast(AppConstants.IMessages.FAIL);
            }
        });
    }

    public SkuOpeningStockList getopeningstockserial(SerialMaster s, SkuOpeningStockList sku) {
        try {
            sku.setSerail(true);
            sku.setSerialOpeningStockList(serialList);
        } catch (Exception e) {
        }
        return sku;
    }

    private void callAsyncIMEI(String imei, final AsyncResponse callback) {
        skuOpeningStockList = null;
        if (!NetworkUtil.isConnectivityAvailable(getActivity())) {
            mBaseFragment.showToast("NO_INTERNET");
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<SkuOpeningStockList> call = apiService.getIMEIDetails(imei);
        mBaseFragment.showProgessBar();
        call.enqueue(new Callback<SkuOpeningStockList>() {
            @Override
            public void onResponse(Call<SkuOpeningStockList> call, Response<SkuOpeningStockList> response) {
                mBaseFragment.hideProgessBar();
                if (response.code() == 200) {
                    callback.processFinish(response.body(), response.code());
                } else {
                    mBaseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<SkuOpeningStockList> call, Throwable t) {
                mBaseFragment.hideProgessBar();
            }
        });
    }

    //354741089854880
    public void scanimei() {
        /*binding.txtOpenBarcode.setText("354741089854880");*/
        try {
            if (!binding.txtOpenBarcode.getText().toString().equalsIgnoreCase("")) {
                if (binding.btnBarcodeAdd.getText().toString().equalsIgnoreCase("+ Add Manual")) {
                    SkuOpeningStockList mStockList = getopeningstock();
                    if (isSerialIMEI(binding.txtOpeningModel.getText().toString())) {
                        if (serialList != null) {
                            if (serialList.size() != 0) {
                                if (mStockList != null) {
                                    if (!isDublicate(serialList.get(0).getSerial1())) {
                                        mStockList.setSerail(true);
                                        lists.add(mStockList);
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
                                        binding.rvListopeningstock.setLayoutManager(mLayoutManager);
                                        mAdapter = new RVAdapterOSDetails(getActivity(), lists);
                                        binding.rvListopeningstock.setAdapter(mAdapter);
                                        setempty();
                                        binding.txtOpenBarcode.setText("");
                                        binding.llAddToList.setVisibility(View.VISIBLE);
                                        binding.btnBarcodeAdd.setText("+ Add");
                                    } else {
                                        binding.txtOpenBarcode.setText("");
                                        binding.llAddToList.setVisibility(View.VISIBLE);
                                        binding.btnBarcodeAdd.setText("+ Add");
                                        Toast.makeText(AppSingle.getInstance().getActivity(), "This serial already in the list", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    Toast.makeText(AppSingle.getInstance().getActivity(), "Please fill detail", Toast.LENGTH_LONG).show();
                                }
                            } else {
                                Toast.makeText(AppSingle.getInstance().getActivity(), "Serial number not find", Toast.LENGTH_LONG).show();
                            }
                        } else {
                            Toast.makeText(AppSingle.getInstance().getActivity(), "Serial number not find", Toast.LENGTH_LONG).show();
                        }
                    } else {
                        setempty();
                        Toast.makeText(AppSingle.getInstance().getActivity(), "please fill valid input", Toast.LENGTH_LONG).show();
                    }
                } else {
                    callAsyncIMEI(binding.txtOpenBarcode.getText().toString(), new AsyncResponse() {
                        @Override
                        public void processFinish(SkuOpeningStockList output, int resultcode) {
                            try {
                                if (resultcode == 200 && output.getSkuName() != null) {
                                    if (!isDublicate(output.getSerialOpeningStockList().get(0).getSerial1())) {
                                        output.setSerail(true);
                                        lists.add(output);
                                        RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
                                        binding.rvListopeningstock.setLayoutManager(mLayoutManager);
                                        mAdapter = new RVAdapterOSDetails(getActivity(), lists);
                                        binding.rvListopeningstock.setAdapter(mAdapter);
                                        skuOpeningStockList = null;
                                        binding.txtOpenBarcode.setText("");
                                    } else {
                                        binding.txtOpenBarcode.setText("");
                                        binding.llAddToList.setVisibility(View.VISIBLE);
                                        binding.btnBarcodeAdd.setText("+ Add");
                                        Toast.makeText(AppSingle.getInstance().getActivity(), "This serial already in the list", Toast.LENGTH_SHORT).show();
                                    }
                                } else {
                                    serialList = new ArrayList<>();
                                    SaleSerialListItems serial = new SaleSerialListItems();
                                    serial.setSerial1(binding.txtOpenBarcode.getText().toString());
                                    serial.setSerial2("");
                                    serial.setSerial3("");
                                    serial.setSerial4("");
                                    serialList.add(serial);
                                    openConfirmDialog2();
                                }
                            } catch (Exception e) {
                                Toast.makeText(AppSingle.getInstance().getActivity(), e.toString(), Toast.LENGTH_SHORT).show();
                            }
                        }
                    });
                }
            } else {
                Toast.makeText(AppSingle.getInstance().getActivity(), "Please scan IMEI before adding", Toast.LENGTH_SHORT).show();
            }
        } catch (Exception e) {
        }
    }

    public boolean isDublicate(String sarial) {
        boolean result = false;
        try {
            for (int i = 0; i < lists.size(); i++) {
                for (int j = 0; j < lists.get(i).getSerialOpeningStockList().size(); j++) {
                    if (lists.get(i).getSerialOpeningStockList().get(j).getSerial1().equalsIgnoreCase(sarial))
                        result = true;
                    break;
                }
            }
        } catch (Exception e) {
        }
        return result;
    }

    public boolean isDublicateMultipul(List<SaleSerialListItems> sarial) {
        boolean result = false;
        try {
            for (int k = 0; k < sarial.size(); k++) {
                for (int i = 0; i < lists.size(); i++) {
                    for (int j = 0; j < lists.get(i).getSerialOpeningStockList().size(); j++) {
                        if (lists.get(i).getSerialOpeningStockList().get(j).getSerial1().equalsIgnoreCase(sarial.get(k).getSerial1()))
                            result = true;
                        break;
                    }
                }
            }
        } catch (Exception e) {
        }
        return result;
    }
}
