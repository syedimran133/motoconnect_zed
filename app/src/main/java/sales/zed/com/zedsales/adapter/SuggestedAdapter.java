package sales.zed.com.zedsales.adapter;

import android.content.Context;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.CompoundButton;

import java.util.ArrayList;
import java.util.List;

import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.interfaces.MyAdapterListener;
import sales.zed.com.zedsales.models.AddOrderModel;
import sales.zed.com.zedsales.models.SaleSkuListItem;
import sales.zed.com.zedsales.models.SkuListsItem;
import sales.zed.com.zedsales.models.SuggestedItems;
import sales.zed.com.zedsales.room.MyDatabase;
import sales.zed.com.zedsales.support.AppSingle;

/**
 * Created by root on 2/2/18.
 */

public class SuggestedAdapter extends RecyclerView.Adapter<SuggestedViewHolder> {

    private List<SuggestedItems> listSuggestedData;
    private Context context;
    private LayoutInflater mSuggestedInflater;
    private MyAdapterListener click_listener_suggested;
    //private int count = 0;
    private boolean all = false, selectall = false;
    private List<SkuListsItem> fetchSkuMasterList;

    public SuggestedAdapter(Context context, List<SuggestedItems> listSuggestedData) {
        this.listSuggestedData = listSuggestedData;
        this.context = context;
        fetchSkuMasterList = MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchSkuMasterList();
        mSuggestedInflater = LayoutInflater.from(context);
    }

    @Override
    public void onBindViewHolder(final SuggestedViewHolder holder, final int position) {

        final SuggestedItems suggestedItems = listSuggestedData.get(position);

        holder.mSuggestedSkuName.setText(suggestedItems.getSkuName());
        holder.mSuggestedCurrentStock.setText(suggestedItems.getStock());
        holder.mSuggestedQuantity.setText(suggestedItems.getSuggestedQuantity());
        holder.mSuggestedCheck.setChecked(suggestedItems.isSelected());

        holder.mSuggestedCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                listSuggestedData.get(position).setSelected(isChecked);
            }
        });
        if (selectall) {
            if (all) {
                holder.mSuggestedCheck.setChecked(true);
                listSuggestedData.get(position).setSelected(true);
            } else {
                holder.mSuggestedCheck.setChecked(false);
                listSuggestedData.get(position).setSelected(false);
            }
        }
        holder.sminus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int count = Integer.parseInt(listSuggestedData.get(position).getSuggestedQuantity());
                    if (count > 1) {
                        count--;
                        selectall=false;
                        holder.mSuggestedQuantity.setText(String.valueOf(count));
                        listSuggestedData.get(position).setSuggestedQuantity("" + count);
                        notifyDataSetChanged();
                    }
                } catch (Exception e) {

                }
            }
        });

        holder.splus.setOnClickListener(new View.OnClickListener() {
            @Override
            public void onClick(View v) {
                try {
                    int count = Integer.parseInt(listSuggestedData.get(position).getSuggestedQuantity());
                    count++;
                    selectall=false;
                    holder.mSuggestedQuantity.setText(String.valueOf(count));
                    listSuggestedData.get(position).setSuggestedQuantity("" + count);
                    notifyDataSetChanged();
                } catch (Exception e) {

                }
            }
        });

        holder.mSuggestedCheck.setOnCheckedChangeListener(new CompoundButton.OnCheckedChangeListener() {
            @Override
            public void onCheckedChanged(CompoundButton buttonView, boolean isChecked) {
                if (buttonView.isChecked()) {
                    listSuggestedData.get(position).setSelected(true);
                } else {
                    listSuggestedData.get(position).setSelected(false);
                }
            }
        });
    }

    @Override
    public SuggestedViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View view = mSuggestedInflater.inflate(R.layout.myorder_neworder_innerlist, parent, false);
        final SuggestedViewHolder holder = new SuggestedViewHolder(view);
        return holder;
    }

    public int getserialisedMode(String skuname) {
        int result = 0;
        for (int i = 0; i < fetchSkuMasterList.size(); i++) {
            if (fetchSkuMasterList.get(i).getSkuName().equalsIgnoreCase(skuname)) {
                result = fetchSkuMasterList.get(i).getSerialisedMode();
            }
        }
        return result;
    }

    @Override
    public int getItemCount() {
        return listSuggestedData.size();
    }

    public List<AddOrderModel> getlist() {
        List<AddOrderModel> items = new ArrayList<>();
        for (int i = 0; i < listSuggestedData.size(); i++) {
            if (listSuggestedData.get(i).isSelected()) {
                AddOrderModel addOrderAdapter = new AddOrderModel();
                addOrderAdapter.setOrderskuId(listSuggestedData.get(i).getSkuId());
                addOrderAdapter.setOrderskuName(listSuggestedData.get(i).getSkuName());
                addOrderAdapter.setSerialisedMode(getserialisedMode(listSuggestedData.get(i).getSkuName()));
                addOrderAdapter.setOrderQuantity(listSuggestedData.get(i).getSuggestedQuantity());
                float price = 0;
                try {
                    price = Float.parseFloat(listSuggestedData.get(i).getSuggestedQuantity()) * Float.parseFloat(listSuggestedData.get(i).getPrice());
                } catch (Exception e) {

                }
                addOrderAdapter.setOrdertotalPrice("" + price);
                items.add(addOrderAdapter);
            }
        }
        return items;
    }

    public float getprice(String skuid) {
        float result = 0;
        for (int i = 0; i < fetchSkuMasterList.size(); i++) {
            if (("" + fetchSkuMasterList.get(i).getSkuId()).equalsIgnoreCase("skuid"))
                result =Float.parseFloat(""+fetchSkuMasterList.get(i).getPrice());
        }
        return result;
    }

    public void selectall(boolean check, boolean selectall) {
        all = check;
        this.selectall = selectall;
        notifyDataSetChanged();
    }
}
