package sales.zed.com.zedsales.models;

import android.arch.persistence.room.Entity;
import android.arch.persistence.room.PrimaryKey;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

@Entity
public class ProductCategoryListsItem{

	@SerializedName("Status")
	@Expose
	private boolean status;

	@SerializedName("ProductCategoryCode")
	@Expose
	private String productCategoryCode;

	@SerializedName("ModifiedOn")
	@Expose
	private String modifiedOn;

	@PrimaryKey
	@SerializedName("ProductCategoryId")
	@Expose
	private int productCategoryId;

	@SerializedName("RecordCreationDate")
	@Expose
	private String recordCreationDate;

	@SerializedName("ProductCategoryName")
	@Expose
	private String productCategoryName;

	public void setStatus(boolean status){
		this.status = status;
	}

	public boolean isStatus(){
		return status;
	}

	public void setProductCategoryCode(String productCategoryCode){
		this.productCategoryCode = productCategoryCode;
	}

	public String getProductCategoryCode(){
		return productCategoryCode;
	}

	public void setModifiedOn(String modifiedOn){
		this.modifiedOn = modifiedOn;
	}

	public String getModifiedOn(){
		return modifiedOn;
	}

	public void setProductCategoryId(int productCategoryId){
		this.productCategoryId = productCategoryId;
	}

	public int getProductCategoryId(){
		return productCategoryId;
	}

	public void setRecordCreationDate(String recordCreationDate){
		this.recordCreationDate = recordCreationDate;
	}

	public String getRecordCreationDate(){
		return recordCreationDate;
	}

	public void setProductCategoryName(String productCategoryName){
		this.productCategoryName = productCategoryName;
	}

	public String getProductCategoryName(){
		return productCategoryName;
	}

	@Override
 	public String toString(){
		return 
			"ProductCategoryListsItem{" + 
			"status = '" + status + '\'' + 
			",productCategoryCode = '" + productCategoryCode + '\'' + 
			",modifiedOn = '" + modifiedOn + '\'' + 
			",productCategoryId = '" + productCategoryId + '\'' + 
			",recordCreationDate = '" + recordCreationDate + '\'' + 
			",productCategoryName = '" + productCategoryName + '\'' + 
			"}";
		}
}