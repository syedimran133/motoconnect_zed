package sales.zed.com.zedsales.support;

import android.os.AsyncTask;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.fragment.BaseFragment;
import sales.zed.com.zedsales.models.BrandListModel;
import sales.zed.com.zedsales.models.BrandListsItem;
import sales.zed.com.zedsales.models.GRNStatusItem;
import sales.zed.com.zedsales.models.GRNStatusList;
import sales.zed.com.zedsales.models.MasterModel;
import sales.zed.com.zedsales.models.ModelListsItem;
import sales.zed.com.zedsales.models.ProductCategoryListsItem;
import sales.zed.com.zedsales.models.ProductCategoryModel;
import sales.zed.com.zedsales.models.ProductListsItem;
import sales.zed.com.zedsales.models.ProductModel;
import sales.zed.com.zedsales.models.ReportTypeItem;
import sales.zed.com.zedsales.models.ReportTypeList;
import sales.zed.com.zedsales.models.SerialMaster;
import sales.zed.com.zedsales.models.SerialMasterList;
import sales.zed.com.zedsales.models.SkuListsItem;
import sales.zed.com.zedsales.models.SkuMasterModel;
import sales.zed.com.zedsales.models.StockItems;
import sales.zed.com.zedsales.models.StockItemsList;
import sales.zed.com.zedsales.models.StockTypeItem;
import sales.zed.com.zedsales.models.StockTypeMaster;
import sales.zed.com.zedsales.room.MyDatabase;

/**
 * Created by root on 2/21/18.
 */

public class FeatchMaster {

    BaseFragment baseFragment;
    public static String currentDate = "01/01/2000 09:44:00";

    public FeatchMaster(BaseFragment baseFragment) {
        this.baseFragment = baseFragment;
        /*FetchAsyncTask asyncTask=new FetchAsyncTask();
        asyncTask.execute();*/
    }


    public class FetchAsyncTask extends AsyncTask<String,Void,String> {
        @Override
        protected void onPreExecute() {
            super.onPreExecute();
        }

        @Override
        protected String doInBackground(String... strings) {
            callAsyncGRNStatus();
            return null;
        }

        @Override
        protected void onPostExecute(String s) {
            super.onPostExecute(s);
        }

        @Override
        protected void onProgressUpdate(Void... values) {
            super.onProgressUpdate(values);
        }
    }


    public void callAsyncGRNStatus() {
        //MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().fetchSerialMaster("");
        if (!NetworkUtil.isConnectivityAvailable(baseFragment.getActivity())) {
            //baseFragment.showToast(baseFragment.NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<GRNStatusList> call = apiService.getGRNStatusMaster(AppPrefrence.getInstance().getUserid(), currentDate);
        baseFragment.showProgessBar();
        try {
            MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().deleteSerialMaster();
            MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().deleteStock();
            Log.d("DELETE :: ", "Deleted");
        } catch (Exception e) {
            Log.d("DELETE :: ", e.toString());
        }
        call.enqueue(new Callback<GRNStatusList>() {
            @Override
            public void onResponse(Call<GRNStatusList> call, Response<GRNStatusList> response) {
                baseFragment.hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null)
                        MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().deleteGRNStatusItem();
                        if (response.body().getGRNStatusList() != null && response.body().getGRNStatusList().size() > 0)
                            for (GRNStatusItem data : response.body().getGRNStatusList()) {
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertGRNStatus(data);
                            }
                    callAsyncReportType();
                } else {
                    //baseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<GRNStatusList> call, Throwable t) {
                baseFragment.hideProgessBar();
            }
        });
    }

    private void callAsyncReportType() {
        if (!NetworkUtil.isConnectivityAvailable(baseFragment.getActivity())) {
            //baseFragment.showToast(baseFragment.NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<ReportTypeList> call = apiService.getReportType(AppPrefrence.getInstance().getUserid(), currentDate);
        baseFragment.showProgessBar();
        call.enqueue(new Callback<ReportTypeList>() {
            @Override
            public void onResponse(Call<ReportTypeList> call, Response<ReportTypeList> response) {
                baseFragment.hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null)
                        MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().deleteReportType();
                        if (response.body().getReportTypeMasterList() != null && response.body().getReportTypeMasterList().size() > 0)
                            for (ReportTypeItem data : response.body().getReportTypeMasterList()) {
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertReportType(data);
                            }
                    callAsyncSkockType();
                } else {
                    //baseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<ReportTypeList> call, Throwable t) {
               baseFragment.hideProgessBar();
            }
        });
    }

    private void callAsyncSkockType() {
        if (!NetworkUtil.isConnectivityAvailable(baseFragment.getActivity())) {
            //baseFragment.showToast(baseFragment.NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<StockTypeMaster> call = apiService.getStockType(currentDate);
        baseFragment.showProgessBar();
        call.enqueue(new Callback<StockTypeMaster>() {
            @Override
            public void onResponse(Call<StockTypeMaster> call, Response<StockTypeMaster> response) {
                baseFragment.hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null)
                        MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().deleteStockTypeItem();
                        if (response.body().getStockTypeList() != null && response.body().getStockTypeList().size() > 0)
                            for (StockTypeItem data : response.body().getStockTypeList()) {
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertStockType(data);
                            }
                    callAsyncStockMaster();
                } else {
                    //baseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<StockTypeMaster> call, Throwable t) {
                baseFragment.hideProgessBar();
            }
        });
    }

    private void callAsyncStockMaster() {
        if (!NetworkUtil.isConnectivityAvailable(baseFragment.getActivity())) {
            //baseFragment.showToast(baseFragment.NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<StockItemsList> call = apiService.getStockList(AppPrefrence.getInstance().getUserid(), currentDate);
        baseFragment.showProgessBar();
        call.enqueue(new Callback<StockItemsList>() {
            @Override
            public void onResponse(Call<StockItemsList> call, Response<StockItemsList> response) {
               baseFragment.hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null)
                        MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().deleteStock();
                        if (response.body().getRetailerStockList() != null && response.body().getRetailerStockList().size() > 0)
                            for (StockItems data : response.body().getRetailerStockList()) {
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertStock(data);
                            }
                    callAsyncSerialMaster();
                } else {
                    //baseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<StockItemsList> call, Throwable t) {
                baseFragment.hideProgessBar();
            }
        });
    }

    private void callAsyncSerialMaster() {
        if (!NetworkUtil.isConnectivityAvailable(baseFragment.getActivity())) {
            //baseFragment.showToast(baseFragment.NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<SerialMasterList> call = apiService.getSerialMaster(AppPrefrence.getInstance().getUserid(), currentDate);
        baseFragment.showProgessBar();
        call.enqueue(new Callback<SerialMasterList>() {
            @Override
            public void onResponse(Call<SerialMasterList> call, Response<SerialMasterList> response) {
               baseFragment.hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null)
                        MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().deleteSerialMaster();
                        if (response.body().getSerialLists() != null && response.body().getSerialLists().size() > 0)
                            for (SerialMaster data : response.body().getSerialLists()) {
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertSerialMaster(data);
                            }
                    callAsyncBrand();
                } else {
                    //baseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<SerialMasterList> call, Throwable t) {
                baseFragment.hideProgessBar();
            }
        });
    }

    private void callAsyncBrand() {
        if (!NetworkUtil.isConnectivityAvailable(baseFragment.getActivity())) {
           // baseFragment.showToast(baseFragment.NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<BrandListModel> call = apiService.getBrand(currentDate);
        baseFragment.showProgessBar();
        call.enqueue(new Callback<BrandListModel>() {
            @Override
            public void onResponse(Call<BrandListModel> call, Response<BrandListModel> response) {
                baseFragment.hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null)
                        MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().deleteBrand();
                        if (response.body().getBrandLists() != null && response.body().getBrandLists().size() > 0)
                            for (BrandListsItem data : response.body().getBrandLists())
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertBrand(data);
                    callAsyncCategory();
                } else {
                    //baseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<BrandListModel> call, Throwable t) {
                baseFragment.hideProgessBar();
            }
        });
    }

    private void callAsyncCategory() {
        if (!NetworkUtil.isConnectivityAvailable(baseFragment.getActivity())) {
            //baseFragment.showToast(baseFragment.NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<ProductCategoryModel> call = apiService.getCategory(currentDate);
        baseFragment.showProgessBar();
        call.enqueue(new Callback<ProductCategoryModel>() {
            @Override
            public void onResponse(Call<ProductCategoryModel> call, Response<ProductCategoryModel> response) {
                baseFragment.hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null)
                        MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().deleteProductCategoryListsItem();
                        if (response.body().getProductCategoryLists() != null && response.body().getProductCategoryLists().size() > 0)
                            for (ProductCategoryListsItem data : response.body().getProductCategoryLists())
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertProductCategory(data);
                    callAsyncProduct();
                } else {
                    //baseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<ProductCategoryModel> call, Throwable t) {
                baseFragment.hideProgessBar();
            }
        });
    }

    private void callAsyncProduct() {
        if (!NetworkUtil.isConnectivityAvailable(baseFragment.getActivity())) {
            //baseFragment.showToast(baseFragment.NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<ProductModel> call = apiService.getProduct(currentDate);
        baseFragment.showProgessBar();
        call.enqueue(new Callback<ProductModel>() {
            @Override
            public void onResponse(Call<ProductModel> call, Response<ProductModel> response) {
                baseFragment.hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null)
                        MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().deleteProductList();
                        if (response.body().getProductLists() != null && response.body().getProductLists().size() > 0)
                            for (ProductListsItem data : response.body().getProductLists())
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertProduct(data);
                    callAsyncMaster();
                } else {
                    //baseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<ProductModel> call, Throwable t) {
                baseFragment.hideProgessBar();
            }
        });
    }

    private void callAsyncMaster() {
        if (!NetworkUtil.isConnectivityAvailable(baseFragment.getActivity())) {
            //baseFragment.showToast(baseFragment.NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<MasterModel> call = apiService.getMaster(currentDate);
        baseFragment.showProgessBar();
        call.enqueue(new Callback<MasterModel>() {
            @Override
            public void onResponse(Call<MasterModel> call, Response<MasterModel> response) {
                baseFragment.hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null)
                        MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().deleteModelLists();
                        if (response.body().getModelLists() != null && response.body().getModelLists().size() > 0)
                            for (ModelListsItem data : response.body().getModelLists())
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertMaster(data);
                    callAsyncSku();
                } else {
                    //baseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<MasterModel> call, Throwable t) {
                baseFragment.hideProgessBar();
            }
        });
    }

    private void callAsyncSku() {
        if (!NetworkUtil.isConnectivityAvailable(baseFragment.getActivity())) {
            //baseFragment.showToast(baseFragment.NO_INTERNET);
            return;
        }
        ApiInterface apiService = ApiClient.getOtherClient().create(ApiInterface.class);
        Call<SkuMasterModel> call = apiService.getSkuMaster(currentDate);
        baseFragment.showProgessBar();
        call.enqueue(new Callback<SkuMasterModel>() {
            @Override
            public void onResponse(Call<SkuMasterModel> call, Response<SkuMasterModel> response) {
                baseFragment.hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null)
                        MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().deleteSkuMaster();
                        if (response.body().getSkuLists() != null && response.body().getSkuLists().size() > 0)
                            for (SkuListsItem data : response.body().getSkuLists())
                                MyDatabase.getCurretDatabase(AppSingle.getInstance().getActivity()).daoAccess().insertSkuMaster(data);
                AppPrefrence.getInstance().setDataSync(true);
                } else {
                    //baseFragment.showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<SkuMasterModel> call, Throwable t) {
                baseFragment.hideProgessBar();
            }
        });
    }
}
