package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

/**
 * Created by root on 2/22/18.
 */

public class ProfileItems {

    @SerializedName("CompanyName")
    @Expose
    private String CompanyName;

    @SerializedName("UserCode")
    @Expose
    private String UserCode;

    @SerializedName("UserName")
    @Expose
    private String UserName;

    @SerializedName("RegisteredMobileNo")
    @Expose
    private String RegisteredMobileNo;

    @SerializedName("AlternateMobileNo")
    @Expose
    private String AlternateMobileNo;

    @SerializedName("EmailId")
    @Expose
    private String EmailId;

    @SerializedName("DateOfBirth")
    @Expose
    private String DateOfBirth;

    @SerializedName("Married")
    @Expose
    private String Married;

    @SerializedName("DateOfAnniversary")
    @Expose
    private String DateOfAnniversary;

    @SerializedName("Address")
    @Expose
    private String Address;

    @SerializedName("City")
    @Expose
    private String City;

    @SerializedName("State")
    @Expose
    private String State;

    @SerializedName("PinCode")
    @Expose
    private String PinCode;

    @SerializedName("CounterPotentialValue")
    @Expose
    private String CounterPotentialValue;

    @SerializedName("CounterPotentialVolume")
    @Expose
    private String CounterPotentialVolume;

    @SerializedName("AccountHolderName")
    @Expose
    private String AccountHolderName;

    @SerializedName("AccountNumber")
    @Expose
    private String AccountNumber;

    @SerializedName("BankName")
    @Expose
    private String BankName;

    @SerializedName("BranchName")
    @Expose
    private String BranchName;

    @SerializedName("BranchLocation")
    @Expose
    private String BranchLocation;

    @SerializedName("IfscCode")
    @Expose
    private String IfscCode;

    @SerializedName("AadharNumber")
    @Expose
    private String AadharNumber;

    @SerializedName("PanNumber")
    @Expose
    private String PanNumber;

    @SerializedName("GstinNo")
    @Expose
    private String GstinNo;

    public String getCompanyName() {
        if (CompanyName != null)
            return CompanyName;
        else
            return "";
    }

    public void setCompanyName(String companyName) {
        CompanyName = companyName;
    }

    public String getUserCode() {

        if (UserCode != null)
            return UserCode;
        else
            return "";
    }

    public void setUserCode(String userCode) {
        UserCode = userCode;
    }

    public String getUserName() {

        if (UserName != null)
            return UserName;
        else
            return "";
    }

    public void setUserName(String userName) {
        UserName = userName;
    }

    public String getRegisteredMobileNo() {

        if (RegisteredMobileNo != null)
            return RegisteredMobileNo;
        else
            return "";
    }

    public void setRegisteredMobileNo(String registeredMobileNo) {
        RegisteredMobileNo = registeredMobileNo;
    }

    public String getAlternateMobileNo() {

        if (AlternateMobileNo != null)
            return AlternateMobileNo;
        else
            return "";
    }

    public void setAlternateMobileNo(String alternateMobileNo) {
        AlternateMobileNo = alternateMobileNo;
    }

    public String getEmailId() {

        if (EmailId != null)
            return EmailId;
        else
            return "";
    }

    public void setEmailId(String emailId) {
        EmailId = emailId;
    }

    public String getDateOfBirth() {

        if (DateOfBirth != null)
            return DateOfBirth;
        else
            return "";
    }

    public void setDateOfBirth(String dateOfBirth) {
        DateOfBirth = dateOfBirth;
    }

    public String getMarried() {

        if (Married != null)
            return Married;
        else
            return "";
    }

    public void setMarried(String married) {
        Married = married;
    }

    public String getDateOfAnniversary() {

        if (DateOfAnniversary != null)
            return DateOfAnniversary;
        else
            return "";
    }

    public void setDateOfAnniversary(String dateOfAnniversary) {
        DateOfAnniversary = dateOfAnniversary;
    }

    public String getAddress() {

        if (Address != null)
            return Address;
        else
            return "";
    }

    public void setAddress(String address) {
        Address = address;
    }

    public String getCity() {

        if (City != null)
            return City;
        else
            return "";
    }

    public void setCity(String city) {
        City = city;
    }

    public String getState() {

        if (State != null)
            return State;
        else
            return "";
    }

    public void setState(String state) {
        State = state;
    }

    public String getPinCode() {

        if (PinCode != null)
            return PinCode;
        else
            return "";
    }

    public void setPinCode(String pinCode) {
        PinCode = pinCode;
    }

    public String getCounterPotentialValue() {

        if (CounterPotentialValue != null)
            return CounterPotentialValue;
        else
            return "";
    }

    public void setCounterPotentialValue(String counterPotentialValue) {
        CounterPotentialValue = counterPotentialValue;
    }

    public String getCounterPotentialVolume() {

        if (CounterPotentialVolume != null)
            return CounterPotentialVolume;
        else
            return "";
    }

    public void setCounterPotentialVolume(String counterPotentialVolume) {
        CounterPotentialVolume = counterPotentialVolume;
    }

    public String getAccountHolderName() {

        if (AccountHolderName != null)
            return AccountHolderName;
        else
            return "";
    }

    public void setAccountHolderName(String accountHolderName) {
        AccountHolderName = accountHolderName;
    }

    public String getAccountNumber() {

        if (AccountNumber != null)
            return AccountNumber;
        else
            return "";
    }

    public void setAccountNumber(String accountNumber) {
        AccountNumber = accountNumber;
    }

    public String getBankName() {

        if (BankName != null)
            return BankName;
        else
            return "";
    }

    public void setBankName(String bankName) {
        BankName = bankName;
    }

    public String getBranchName() {

        if (BranchName != null)
            return BranchName;
        else
            return "";
    }

    public void setBranchName(String branchName) {
        BranchName = branchName;
    }

    public String getBranchLocation() {
        if (BranchLocation != null)
            return BranchLocation;
        else
            return "";
    }

    public void setBranchLocation(String branchLocation) {
        BranchLocation = branchLocation;
    }

    public String getIfscCode() {

        if (IfscCode != null)
            return IfscCode;
        else
            return "";
    }

    public void setIfscCode(String ifscCode) {
        IfscCode = ifscCode;
    }

    public String getAadharNumber() {
        if (AadharNumber != null)
            return AadharNumber;
        else
            return "";
    }

    public void setAadharNumber(String aadharNumber) {
        AadharNumber = aadharNumber;
    }

    public String getPanNumber() {

        if (PanNumber != null)
            return PanNumber;
        else
            return "";
    }

    public void setPanNumber(String panNumber) {
        PanNumber = panNumber;
    }

    public String getGstinNo() {
        if (GstinNo != null)
            return GstinNo;
        else
            return "";
    }

    public void setGstinNo(String gstinNo) {
        GstinNo = gstinNo;
    }


    @Override
    public String toString() {
        return
                "RetailerProfile{" +
                        "CompanyName = '" + CompanyName + '\'' +
                        "UserCode = '" + UserCode + '\'' +
                        "UserName = '" + UserName + '\'' +
                        "RegisteredMobileNo = '" + RegisteredMobileNo + '\'' +
                        "AlternateMobileNo = '" + AlternateMobileNo + '\'' +
                        "EmailId = '" + EmailId + '\'' +
                        "DateOfBirth = '" + DateOfBirth + '\'' +
                        "Married = '" + Married + '\'' +
                        "DateOfAnniversary = '" + DateOfAnniversary + '\'' +
                        "Address = '" + Address + '\'' +
                        "City = '" + City + '\'' +
                        "State = '" + State + '\'' +
                        "PinCode = '" + PinCode + '\'' +
                        "CounterPotentialValue = '" + CounterPotentialValue + '\'' +
                        "CounterPotentialVolume = '" + CounterPotentialVolume + '\'' +
                        "AccountHolderName = '" + AccountHolderName + '\'' +
                        "AccountNumber = '" + AccountNumber + '\'' +
                        "BankName = '" + BankName + '\'' +
                        "BranchName = '" + BranchName + '\'' +
                        "BranchLocation = '" + BranchLocation + '\'' +
                        "IfscCode = '" + IfscCode + '\'' +
                        "AadharNumber = '" + AadharNumber + '\'' +
                        "PanNumber = '" + PanNumber + '\'' +
                        "GstinNo = '" + GstinNo + '\'' +
                        "}";
    }

}
