package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 2/13/18.
 */

public class AddOpeningStock {

    @SerializedName("OpeningStockDate")
    @Expose
    private String OpeningStockDate;

    @SerializedName("SkuOpeningStockList")
    @Expose
    private List<SkuOpeningStockList> stockList;

    public String getOpeningStockDate() {
        return OpeningStockDate;
    }

    public void setOpeningStockDate(String openingStockDate) {
        OpeningStockDate = openingStockDate;
    }

    public List<SkuOpeningStockList> getStockList() {
        return stockList;
    }

    public void setStockList(List<SkuOpeningStockList> stockList) {
        this.stockList = stockList;
    }

}
