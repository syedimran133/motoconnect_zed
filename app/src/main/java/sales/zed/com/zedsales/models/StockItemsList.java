package sales.zed.com.zedsales.models;

import com.google.gson.annotations.Expose;
import com.google.gson.annotations.SerializedName;

import java.util.List;

/**
 * Created by root on 2/8/18.
 */

public class StockItemsList {


    @SerializedName("RetailerStockList")
    @Expose
    private List<StockItems> retailerStockList;


    public List<StockItems> getRetailerStockList() {
        return retailerStockList;
    }

    public void setRetailerStockList(List<StockItems> retailerStockList) {
        this.retailerStockList = retailerStockList;
    }


    @Override
    public String toString() {
        return
                "RetailerStockList{" +
                        "retailerStockList = '" + retailerStockList + '\'' +
                        "}";
    }
}
