package sales.zed.com.zedsales.fragment;

import android.databinding.DataBindingUtil;
import android.graphics.Color;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v7.widget.DefaultItemAnimator;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import retrofit2.Call;
import retrofit2.Callback;
import retrofit2.Response;
import sales.zed.com.zedsales.MainActivity;
import sales.zed.com.zedsales.R;
import sales.zed.com.zedsales.apiz.ApiClient;
import sales.zed.com.zedsales.apiz.ApiInterface;
import sales.zed.com.zedsales.constants.AppConstants;
import sales.zed.com.zedsales.databinding.FargmentTargetListBinding;
import sales.zed.com.zedsales.interfaces.MyAdapterListener;
import sales.zed.com.zedsales.adapter.RVTargetList;
import sales.zed.com.zedsales.models.TargetModelList;
import sales.zed.com.zedsales.support.AppSingle;
import sales.zed.com.zedsales.support.FlowOrganizer;
import sales.zed.com.zedsales.support.NetworkUtil;

/**
 * Created by root on 2/14/18.
 */

public class FragmentTargetList extends BaseFragment {

    private FargmentTargetListBinding binding;
    private LinearLayoutManager mLayoutManager;
    private boolean loading = true;
    private int pastVisiblesItems, visibleItemCount, totalItemCount;

    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        binding = DataBindingUtil.inflate(inflater, R.layout.fargment_target_list, container, false);
        AppSingle.getInstance().getActivity().setTitle("Target", true);
        AppSingle.getInstance().getActivity().setTopNavBar(true);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(MainActivity.adapter.getcurrent("Target"));
        try {
            binding.btnTargetlistCurrent.setOnClickListener(listener);
            binding.btnTargetlistPreviousPrv.setOnClickListener(listener);
        } catch (Exception e) {
        }
        return binding.getRoot();
    }

    @Override
    public void onViewCreated(View view, @Nullable Bundle savedInstanceState) {
        super.onViewCreated(view, savedInstanceState);
        if (NetworkUtil.isConnectivityAvailable(getActivity())) {
            callAsync(0);
        }

    }
    View.OnClickListener listener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            switch (view.getId()) {
                case R.id.btn_targetlist_current:
                    try {
                        binding.btnTargetlistPreviousPrv.setBackgroundColor(Color.parseColor("#A39F9E"));
                        binding.btnTargetlistCurrent.setBackgroundColor(Color.parseColor("#00bfff"));
                        //FlowOrganizer.getInstance().addToInnerFrame(binding.frameInnerTarget, new FragmentTargetList());
                        FlowOrganizer.getInstance().add(new FragmentTargetList(), true);
                    } catch (Exception e) {
                    }
                    break;
                case R.id.btn_targetlist_previous_prv:
                    try {
                        binding.btnTargetlistPreviousPrv.setBackgroundColor(Color.parseColor("#00bfff"));
                        binding.btnTargetlistCurrent.setBackgroundColor(Color.parseColor("#A39F9E"));
                        Bundle bundle = new Bundle();
                        bundle.putString("TYPE", "2");
                        //FlowOrganizer.getInstance().addToInnerFrame(binding.frameInnerTarget, new FragmentTargetDetail(), true, bundle);
                        FlowOrganizer.getInstance().add(new FragmentTargetDetail(), true);
                    } catch (Exception e) {
                    }
                    break;
            }
        }
    };
    @Override
    public void onDestroy() {
        super.onDestroy();
        AppSingle.getInstance().getActivity().setTitle("Menu", false);
        AppSingle.getInstance().getActivity().setTopNavBar(false);
        AppSingle.getInstance().getActivity().setTopNavSelectedPos(-1);
    }

    private void callAsync(int index) {


        ApiInterface apiService = ApiClient.getDefaultClient2().create(ApiInterface.class);
        Call<TargetModelList> call = apiService.getTargetList(AppSingle.getInstance().getLoginUser().getUserId(), index/*, RequestFormater.gettargetjson(fromdate, todate)*/);
        showProgessBar();
        call.enqueue(new Callback<TargetModelList>() {
            @Override
            public void onResponse(Call<TargetModelList> call, Response<TargetModelList> response) {
                hideProgessBar();
                if (response.code() == 200) {
                    if (response.body() != null)
                        try {
                            if (response.body().getTargetList() != null && response.body().getTargetList().size() > 0) {
                                RVTargetList adapter = new RVTargetList(AppSingle.getInstance().getActivity(), response.body().getTargetList(), new MyAdapterListener() {
                                    @Override
                                    public void iconButtonViewOnClick(View v, int position) {
                                        Bundle bundle = new Bundle();
                                        bundle.putString("TYPE", "1");
                                        FlowOrganizer.getInstance().add(new FragmentTargetDetail(), true, bundle);
                                    }
                                });
                                RecyclerView.LayoutManager mLayoutManager = new LinearLayoutManager(AppSingle.getInstance().getActivity());
                                binding.rvListTraget.setLayoutManager(mLayoutManager);
                                binding.rvListTraget.setItemAnimator(new DefaultItemAnimator());
                                binding.rvListTraget.setAdapter(adapter);
                            } else {
                                showToast("Target List is empty.");
                            }
                        } catch (Exception e) {
                            showToast(e.toString());
                        }
                } else {
                    showResponseMessage(response.code());
                }
            }

            @Override
            public void onFailure(Call<TargetModelList> call, Throwable t) {
                hideProgessBar();
                showToast(AppConstants.IMessages.FAIL);
            }
        });
    }
}
